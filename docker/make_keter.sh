#! /usr/bin/env bash

# this script builds a keter file for deploying the server

set -o errexit
set -o nounset
set -o pipefail

# build a docker image for building the server
docker build -t devserver docker/devserver
# configure to use postgresql in production
cp stack.yaml stack.yaml_backup
sed -i 's/sqlite: true/sqlite: false/' stack.yaml
sed -i 's/dev: true/dev: false/' stack.yaml
docker run -ti -v `pwd`:/odfserver --rm devserver stack exec -- yesod keter --build-args odftestserver
mv stack.yaml_backup stack.yaml
rm -f odftestserver.keter
tar czfv odftestserver.keter dist/bin/odftestserver config static templates
