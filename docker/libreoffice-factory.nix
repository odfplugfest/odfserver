# nix-build docker/libreoffice-factory.nix
# docker load < result
# docker tag libreoffice-factory odfplugftest/libreoffice_5_2 
$ docker push odfplugftest/libreoffice_5_2

with import <nixpkgs> {};

let
  factory = callPackage ./factory.nix {};

in
pkgs.dockerTools.buildImage {
    name = "libreoffice-factory";
    contents = [ factory pkgs.libreoffice ];
    config = {
        Cmd = [ (factory.outPath + "/run.sh") (libreoffice.outPath + "/bin") ];
    };
}
