#! /usr/bin/env bash
set -o errexit
set -o nounset
set -o pipefail

# This script builds a docker image with the odf test server.
# First it creates an image with the dependencies:
#  - gmp
#  - postgresql
#  - poppler utils

# Then it uses the nix functionality of stack to add the compiled odftestserver.
# It proceeds to write the docker image to a tar ball and also starts the
# server locally on port 3000. The ip address is written to stdout when the
# server has started.

# The created image can be published on the docker website with
# docker login --username=odfplugftest
# docker tag odftestserver-odftestserver odfplugfest/odftestserver:latest
# docker push odfplugfest/odftestserver:latest

basedir=$(dirname "$0")

# remove the previous containers
for f in $(docker ps -a |grep odfte | cut -f 1 -d ' '); do
    docker rm -f $f
done

# remove the previous images
for f in $(docker images |grep odftestserver|cut -f 1 -d ' '); do
    docker rmi $f
done

# create deps image
nix-build "$basedir/odftestserver-deps.nix"
docker load < result

# create the image
stack --nix --allow-different-user image container
docker save odftestserver-odftestserver > odftestserver.tar

# create the container
container=$(docker create odftestserver-odftestserver)
docker start $container
docker logs $container
docker inspect $container | grep '"IPAddress"'
