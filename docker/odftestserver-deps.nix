with import <nixpkgs> {};
pkgs.dockerTools.buildImage {
    name = "odftestserver-deps";
    contents = [ pkgs.postgresql pkgs.gmp pkgs.poppler_utils ];
    config = {
        Env = [ ("PATH=$PATH:" + pkgs.poppler_utils.outPath + "/bin") ];
    };
}
