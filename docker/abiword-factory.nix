with import <nixpkgs> {};

let
  factory = callPackage ./factory.nix {};

in
pkgs.dockerTools.buildImage {
    name = "abiword-factory";
    contents = [ factory pkgs.abiword ];
    config = {
        Cmd = [ (factory.outPath + "/run.sh") (abiword.outPath + "/bin") ];
    };
}
