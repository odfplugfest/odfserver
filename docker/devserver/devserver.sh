#!/usr/bin/env bash

# This scripts starts a development version of the ODF Test Server

if [ ! -e static/bootstrap-3.3.6-dist ]; then
    wget https://github.com/twbs/bootstrap/releases/download/v3.3.6/bootstrap-3.3.6-dist.zip
    unzip -d static bootstrap-3.3.6-dist.zip
fi
docker build -t devserver docker/devserver
docker run -ti -p 3000:3000  -v `pwd`:/odfserver --rm devserver
