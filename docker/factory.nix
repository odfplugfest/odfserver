{ stdenv, fetchgit, callPackage, ant, git, coreutils, openjdk, gnused, gnugrep }:

let
  saxon = callPackage ./saxon.nix {};
in
stdenv.mkDerivation rec {
  name = "odftestfactory";
  src = fetchgit {
    url = "https://vandenoever@gitlab.com/odfplugfest/odfserver.git";
    sha256 = "0a5nal1xdylm1057mnrlpm15ka0clv8v0v3kfqkrdbyrysmq49nc";
  };

  buildInputs = [ ant saxon git ];
  propagatedBuildInputs = [ coreutils openjdk gnused gnugrep ];

  buildPhase = ''
    mkdir -p $out
    cd tools/factories
    cp ${saxon}/share/java/saxon9he.jar lib
    ant
  '';

  installPhase = ''
    mkdir -p "$out/share/java"
    cp odffactories.jar "$out/share/java"
    echo "#!/usr/bin/env bash" > $out/run.sh
    echo "if [ -z \"\$ODF_TEST_SERVER\" ]; then" >> $out/run.sh
    echo "    echo 'The environment variable ODF_TEST_SERVER has not been set.'" >> $out/run.sh
    echo "    echo 'example: docker create --env ODF_TEST_SERVER=http://172.17.0.2:3000 abiword-factory'" >> $out/run.sh
    echo "    exit 1" >> $out/run.sh
    echo "fi" >> $out/run.sh
    echo "export PATH=\$PATH:\$1:${openjdk.outPath}/bin:${coreutils.outPath}/bin:${gnused.outPath}/bin:${gnugrep.outPath}/bin" >> $out/run.sh
    echo "if [ ! -d /tmp ]; then mkdir /tmp; fi" >> $out/run.sh
    echo "java -cp $out/share/java/odffactories.jar org.odfserver.commands.ConfigureAll -h \$ODF_TEST_SERVER" >> $out/run.sh
    echo "java -cp $out/share/java/odffactories.jar org.odfserver.commands.RunAll" >> $out/run.sh
    chmod a+x $out/run.sh
  '';
}
