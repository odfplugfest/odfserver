{ stdenv, fetchurl, unzip }:

stdenv.mkDerivation {
  name = "saxon-9.7.18";

  src = fetchurl {
    url = mirror://sourceforge/saxon/SaxonHE9-7-0-18J.zip;
    sha256 = "6990c378f011582df5289691fb93e95e86c845b14f5484a9df458b4ba692a6c6";
  };

  builder = ./unzip-builder.sh;

  nativeBuildInputs = [ unzip ];

  meta = {
    platforms = stdenv.lib.platforms.unix;
  };
}
