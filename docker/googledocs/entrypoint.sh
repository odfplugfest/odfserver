#!/usr/bin/env bash

if [ -z "$GDRIVE_CREDENTIALS" ]; then
    echo 'Provide the contents of ~/.gdrive/token_v2.json in GDRIVE_CREDENTIALS.'
    echo Example:
    echo 'docker create --env GDRIVE_CREDENTIALS="$(cat ~/.gdrive/token_v2.json)"'
    exit 1
else
    mkdir ~/.gdrive
    chmod o-rwx  ~/.gdrive
    echo "$GDRIVE_CREDENTIALS" > ~/.gdrive/token_v2.json
fi

gdrive about
./run.sh Docs
