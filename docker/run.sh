#!/usr/bin/env bash
env

if [ -z "$ODF_TEST_SERVER" ]; then
    echo 'The environment variable ODF_TEST_SERVER has not been set.'
    echo 'example: docker create --env ODF_TEST_SERVER=http://172.17.0.2:3000 abiword-factory'
    exit 1
fi

for f in "$@"; do
    if [ ! -e $f.properties ]; then
        java -cp odffactories.jar org.odfserver.commands.Configure -h $ODF_TEST_SERVER -c $f.properties -s $f
    fi
done

while true; do
    java -cp odffactories.jar org.odfserver.commands.RunAll
    sleep 60
done
