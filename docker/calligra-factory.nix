with import <nixpkgs> {};

let
  factory = callPackage ./factory.nix {};

in
pkgs.dockerTools.buildImage {
    name = "calligra-factory";
    contents = [ factory pkgs.kde4.calligra ];
    config = {
        Cmd = [ (factory.outPath + "/run.sh") (pkgs.kde4.calligra.outPath + "/bin") ];
    };
}
