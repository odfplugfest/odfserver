#!/usr/bin/env bash

Xvfb :1 &
# make sure no dbus services are found
export XDG_DATA_DIRS=/tmp
export $(dbus-launch)
export XDG_DATA_DIRS=
sleep 1

export DISPLAY=:1

. run.sh "$@"
