{ stdenv, fetchgit, xorg, callPackage, ant, git, coreutils, openjdk, gnused, gnugrep, xkbcomp, xvfb_run, bash, findutils, strace}:

let
  factory = callPackage ./factory.nix {};
in
stdenv.mkDerivation rec {
  name = "xodftestfactory";
  src = fetchgit {
    url = "https://vandenoever@gitlab.com/odfplugfest/odfserver.git";
    sha256 = "0a5nal1xdylm1057mnrlpm15ka0clv8v0v3kfqkrdbyrysmq49nc";
  };
  installPhase = ''
    mkdir -p $out
    echo "#!/usr/bin/env bash" > $out/run.sh
    echo "export PATH=\$PATH:\$1:${openjdk.outPath}/bin:${coreutils.outPath}/bin:${gnused.outPath}/bin:${gnugrep.outPath}/bin:${xorg.xorgserver.outPath}/bin:${xkbcomp.outPath}/bin:${xorg.setxkbmap}.outPath}/bin:${xvfb_run.outPath}/bin:${findutils.outPath}/bin:${strace.outPath}/bin" >> $out/run.sh
    echo "echo \$PATH" >> $out/run.sh
    echo "echo ${bash.outPath}/bin/sh" >> $out/run.sh
    echo "if [ ! -d /tmp ]; then mkdir /tmp; fi" >> $out/run.sh
    echo "if [ ! -h /bin/sh ]; then ln -s ${bash.outPath}/bin/sh /bin/sh; fi" >> $out/run.sh
#    echo "Xvfb :1 -nocursor -ac -screen 0 1024x768x16 &" >> $out/run.sh
    #echo "Xephyr :1 -screen 1024x768 &" >> $out/run.sh
#    echo "export DISPLAY=:1" >> $out/run.sh
#    echo "xvfb-run ${factory.outPath}/run.sh" >> $out/run.sh
    chmod a+x $out/run.sh
  '';
}
