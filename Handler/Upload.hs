module Handler.Upload (
    blobWidget,
    checkUploadViewRights,
    renderIsBusy,
    uploadWidget,
    RenderStatus(..),
    renderStatusString,
    secsElapsed,
    getMeta,
    getRenderStatus,
    getUploadGetR,
    getUploadR,
    getUploadsR, postUploadsR,
    postUploadPublicR,
    odfValidationWidget,
    uploadForm,
    File(..)
) where

import           Codec.Archive.Zip
import           Control.Applicative   as A
import           Data.Conduit.Binary   (sinkLbs)
import           Data.List             (nub)
import           Data.Maybe            (fromJust)
import           Data.Text             (splitOn)
import           Data.Time             (addUTCTime, diffUTCTime)
import           Database.Esqueleto    hiding (update, (=.))
import           Import                hiding (Value, count, formatTime,
                                        groupBy, isNothing, on, (==.), (>.),
                                        (||.))

import           CommentWidgets        (discussionWidget)
import           CommonTypes           (epochTime)
import qualified Database              as D
import           FileType
import           Handler.Wiki          (wikiWidget)
import           Meta
import           PdfRenderer
import           TestResultTableWidget (blobDownloadLink, getValidation)
import           TimeUtil              (formatRelativeTime, formatTime, recent)

data File = File {
    file :: FileInfo
}

uploadForm :: Form File
uploadForm = renderDivs $ File
    A.<$> fileAFormReq ""

getUploadsR :: Handler Html
getUploadsR = showUploads Nothing

checkUploadViewRights :: Upload -> Handler ()
checkUploadViewRights upload = do
    maybeUserId <- maybeAuthId
    unless (Just (uploadUploader upload) == maybeUserId || uploadPublic upload) $ permissionDenied ""

showUploads :: Maybe UploadId -> Handler Html
showUploads newUpload = do
    maybeUserId <- maybeAuthId
    (widget, enctype) <- generateFormPost uploadForm
    uploads <- runDB $ select $ from $
        \(upload `InnerJoin` blob `InnerJoin` user) -> do
            on $ upload ^. UploadUploader ==. user ^. UserId
            on $ upload ^. UploadContent  ==. blob ^. BlobId
            where_ $ upload ^. UploadPublic ==. val True
                ||. just (upload ^. UploadUploader) ==. val maybeUserId
            orderBy [desc (upload ^. UploadCtime)]
            offset 0
            limit 1000
            return
                ( upload
                , blob ^. BlobSize
                , blob ^. BlobFileType
                , user ^. UserName
                )

    let types = nub $ map (\(_,_,Value t,_) -> t) uploads
    software <- runDB $ mapM getLiveSoftware types
    let softwareMap = filter (\(_,l) -> not $ null l) $ zip types software
    lang <- getLanguage
    defaultLayout $ do
        setTitleI MsgUploadedFiles
        $(widgetFile "uploads")

sw :: FileType -> [(FileType,[SoftwareOSId])] -> Maybe (SoftwareOSId,SoftwareOSId)
sw fileType softwareMap = case lookup fileType softwareMap of
                  Just [a]     -> Just (a, a)
                  Just (a:b:_) -> Just (a, b)
                  _            -> Nothing

data RenderStatus = RenderStatus {
    rsSoftware      :: SoftwareOSId,
    rsName          :: Text,
    rsLastSeen      :: UTCTime,
    rsHasResult     :: Bool,
    rsJob           :: Maybe JobId,
    rsPdf           :: Maybe BlobId,
    rsCreated       :: Maybe UTCTime,
    rsStarted       :: Maybe UTCTime,
    rsJobsScheduled :: Int,
    rsJobsFailed    :: Int
}

renderIsBusy :: RenderStatus -> Bool
renderIsBusy rs = not (rsHasResult rs) && rsJobsFailed rs < rsJobsScheduled rs

renderStatusString :: RenderStatus -> Handler Text
renderStatusString rs
    | isJust $ rsPdf rs = return "" -- there is a pdf, render finished ok
    | rsHasResult rs = return "no render output"
    | rsJobsScheduled rs > 0
        && rsJobsScheduled rs == rsJobsFailed rs = return "render failed"
    | isJust $ rsStarted rs = do
        time <- formatRelativeTime $ fromJust $ rsStarted rs
        return $ "render started " ++ time
    | isJust $ rsCreated rs = do
        time <- formatRelativeTime $ fromJust $ rsCreated rs
        return $ "render scheduled " ++ time
    | otherwise = return "rendering"

getRenderStatus :: MonadIO m => BlobId -> ReaderT SqlBackend m [RenderStatus]
getRenderStatus blobid = do
    rows <- select $ from $ \(serviceinstance `InnerJoin` service
            `InnerJoin` sos `InnerJoin` blob `LeftOuterJoin` job
            `LeftOuterJoin` jobresult `LeftOuterJoin` failed) -> do
        on $ job ?. JobId ==. failed ?. FailedJobJob
        on $ job ?. JobId ==. jobresult ?. JobResultJob
        on $ just (sos ^. SoftwareOSId) ==. job ?. JobSoftware
            &&. just (blob ^. BlobId) ==. job ?. JobInput
        on $ service ^. ServiceInputType ==. blob ^. BlobFileType
        on $ serviceinstance ^. ServiceInstanceSoftware ==. sos ^. SoftwareOSId
        on $ serviceinstance ^. ServiceInstanceService ==. service ^. ServiceId
        where_ $ isNothing (job ?. JobId)
            ||. (job ?. JobInput ==. just (val blobid))
        where_ $ service ^. ServiceOutputType ==. val FileType.PDF
        where_ $ blob ^. BlobId ==. val blobid
        groupBy (sos ^. SoftwareOSId)
        return (sos ^. SoftwareOSId,
                max_ $ serviceinstance ^. ServiceInstanceLastSeen,
                not_ $ isNothing $ min_ $ jobresult ?. JobResultId,
                max_ $ job ?. JobId,
                max_ $ jobresult ?. JobResultResult,
                max_ $ job ?. JobCreated,
                max_ $ joinV $ job ?. JobStarted,
                count $ job ?. JobId,
                count $ failed ?. FailedJobId)
    return $ map r2r rows
  where
    r2r (Value sos, Value time, Value hasResult, Value mjobid, Value mpdf, Value mcreated, Value mstarted, Value nscheduled, Value nfailed)
        = RenderStatus {
            rsSoftware = sos,
            rsName = "",
            rsLastSeen = t time,
            rsJob = m mjobid,
            rsHasResult = hasResult,
            rsPdf = m2 mpdf,
            rsCreated = m mcreated,
            rsStarted = m mstarted,
            rsJobsScheduled = nscheduled,
            rsJobsFailed = nfailed
          }
    m (Just a) = a
    m _        = Nothing
    m2 (Just (Just a)) = a
    m2 _               = Nothing
    t Nothing     = epochTime
    t (Just time) = time

secsElapsed :: UTCTime -> UTCTime -> Int
secsElapsed a b = floor $ diffUTCTime a b

-- return at most two live software instances
getLiveSoftware :: MonadIO m => FileType -> ReaderT SqlBackend m [SoftwareOSId]
getLiveSoftware filetype = do
    now <- liftIO getCurrentTime
    let deadline = addUTCTime (-120) now
    software <- select $ from $ \(serviceinstance `InnerJoin` service
            `InnerJoin` sos) -> do
        on $ serviceinstance ^. ServiceInstanceSoftware ==. sos ^. SoftwareOSId
        on $ serviceinstance ^. ServiceInstanceService ==. service ^. ServiceId
        where_ $ service ^. ServiceOutputType ==. val FileType.PDF
        where_ $ service ^. ServiceInputType ==. val filetype
        where_ $ serviceinstance ^. ServiceInstanceLastSeen >. val deadline
        groupBy $ serviceinstance ^. ServiceInstanceSoftware
        offset 0
        limit 2
        return (serviceinstance ^. ServiceInstanceSoftware)
    return $ map (\(Value a) -> a) software

postUploadsR :: Handler Html
postUploadsR = do
    userid <- requireAuthId
    ((result, _), _) <- runFormPost uploadForm
    case result of
        FormSuccess File{file} -> do
            let name = fileName file
            fileBytes <- runResourceT $ fileSource file $$ sinkLbs
            when (null fileBytes) $ do
                setMessage "The file is empty"
                redirect UploadsR
            ctime <- lift getCurrentTime
            uploadid <- runDB $ D.addUpload fileBytes userid name ctime
            showUploads $ Just uploadid
        _ -> do
            setMessageI MsgSomethingWentWrong
            redirect UploadsR

getPotentialJobs :: MonadIO m => Lang -> BlobId -> ReaderT SqlBackend m [(Route App,Text,Maybe UTCTime,UTCTime)]
getPotentialJobs lang blobid = do
    rows <- select $ from $ \(blob `InnerJoin` service
            `InnerJoin` instance' `InnerJoin` softwareos `InnerJoin`
            version `InnerJoin` software `InnerJoin` osv `InnerJoin` os
            `LeftOuterJoin` job `LeftOuterJoin` jobresult) -> do
        on (job ?. JobId ==. jobresult ?. JobResultJob)
        on (job ?. JobInput ==. just (blob ^. BlobId)
            &&. job ?. JobService ==. just (service ^. ServiceId)
            &&. job ?. JobSoftware ==. just (instance' ^. ServiceInstanceSoftware))
        on $ osv ^. OSVersionOs ==. os ^. OperatingSystemId
        on $ softwareos ^. SoftwareOSOs ==. osv ^. OSVersionId
        on $ version ^. SoftwareVersionSoftware ==. software ^. SoftwareId
        on $ softwareos ^. SoftwareOSSoftware ==. version ^. SoftwareVersionId
        on $ instance' ^. ServiceInstanceSoftware ==. softwareos ^. SoftwareOSId
        on $ service ^. ServiceId ==. instance' ^. ServiceInstanceService
        on $ blob ^. BlobFileType ==. service ^. ServiceInputType
        where_ $ blob ^. BlobId ==. val blobid
        groupBy (service ^. ServiceId, softwareos ^. SoftwareOSId,
                 version ^. SoftwareVersionId, software ^. SoftwareId,
                 os ^. OperatingSystemId, osv ^. OSVersionId,
                 job ?. JobId, jobresult ?. JobResultId)
        orderBy [asc (service ^. ServiceOutputType),
                 asc (service ^. ServiceName),
                 asc (software ^. SoftwareName),
                 asc (version ^. SoftwareVersionName),
                 asc (os ^. OperatingSystemName),
                 asc (osv ^. OSVersionName)]
        return (service,
                softwareos ^. SoftwareOSId,
                version ^. SoftwareVersionName,
                software ^. SoftwareName,
                os ^. OperatingSystemName,
                osv ^. OSVersionName, job,
                joinV $ jobresult ?. JobResultResult,
                max_ $ instance' ^. ServiceInstanceLastSeen)
    return $ map c rows
  where
    c (Entity serviceid service, Value softwareosid, Value version, Value software, Value os, Value osv, mjob, Value mresid, Value lastSeen)
        = (l mjob mresid
          , unwords [ serviceName service, "to"
                    , showNice $ serviceOutputType service, "with", software
                    , version, o os osv]
          , Nothing
          , fromJust lastSeen)
      where
        l _ (Just resid)            = BlobR resid
        l (Just (Entity jobid _)) _ = JobR lang jobid
        l _ _                       = NewJobR blobid softwareosid serviceid
    o os osv | all (not . null) [os, osv] = "on " ++ os ++ " " ++ osv
    o _ _    = ""

findThumbnail :: MonadIO m => BlobId -> FileType -> ReaderT SqlBackend m (Maybe (Route App))
findThumbnail blobid fileType | fileType == FileType.PDF = do
    (_, mrender) <- getPdfPage blobid 1
    case mrender of
        Nothing -> return Nothing
        Just r  -> return $ Just $ blobDownloadLink PNG $ renderThumbnail r
findThumbnail _ _ = return Nothing

icon :: Text -> Maybe (Route App)
icon "application/vnd.oasis.opendocument.text" = Just $ StaticR img_icons_ODF_textdocument_48x48_png
icon "application/vnd.oasis.opendocument.presentation" = Just $ StaticR img_icons_ODF_presentation_48x48_png
icon "application/vnd.oasis.opendocument.spreadsheet" = Just $ StaticR img_icons_ODF_spreadsheet_48x48_png
icon _ = Nothing

listConversions :: [(Route App, Text, Maybe UTCTime, UTCTime)] -> Handler Widget
listConversions conversions = do
    -- find all possible conversions that can be run on this upload
    now <- lift getCurrentTime
    return $(widgetFile "file_conversions")

odfValidationWidget :: Lang -> BlobId -> Handler Widget
odfValidationWidget lang blobid = do
    merrors <- runDB $ getValidation blobid
    return $(widgetFile "validationwidget")

blobJobWidget :: [(Route App, Text, Maybe UTCTime, UTCTime)] -> Handler Widget
blobJobWidget conversions = return $(widgetFile "blobjobwidget")

hasImageSuffix :: String -> Bool
hasImageSuffix path = isSuffixOf ".png" path
                      || isSuffixOf ".gif" path
                      || isSuffixOf ".jpeg" path
                      || isSuffixOf ".jpg" path
                      || isSuffixOf ".svg" path
                      || isSuffixOf ".svgz" path

getMeta :: Blob -> [(Text,Text)]
getMeta blob | isZip $ blobFileType blob
    = case toArchiveOrFail $ fromStrict $ blobContent blob of
          Left _ -> []
          Right archive -> case findEntryByPath "meta.xml" archive of
                               Nothing    -> []
                               Just entry -> metaFromXml $ fromEntry entry
getMeta blob | isXml $ blobFileType blob = metaFromXml $ fromStrict $ blobContent blob
getMeta _ = []

zipWidget :: BlobId -> Blob -> Text -> (Widget, Maybe (Route App))
zipWidget blobid blob filename = do
    let bytes = fromStrict $ blobContent blob
        (list, mthumbLink :: Maybe (Route App)) = case toArchiveOrFail bytes of
            Right archive -> (list', findThumb list')
               where list' = filter (not . isSuffixOf "/" . fst) sorted
                     sorted = sortOn fst $ map pathAndSize $ zEntries archive
            Left _ -> ([], Nothing)
    ($(widgetFile "zip"), mthumbLink)
  where
    pathAndSize e = (eRelativePath e, eUncompressedSize e)
    entryLink path = UnzipR blobid $ splitOn "/" $ pack path
    findThumb l = case find (("Thumbnails/thumbnail.png" ==) . fst) l of
        Nothing    -> Nothing
        Just (_,0) -> Nothing -- thumbnail of size 0 is invalid
        Just (r,_) -> Just $ UnzipR blobid $ splitOn "/" $ pack r

blobWidget :: BlobId -> Blob -> Text -> Route App -> Maybe User -> Maybe (UploadId, Upload) -> Widget
blobWidget blobid blob filename downloadLink muser mupload = do
    let Blob{blobSize,blobFileType} = blob
        mmimetype = map (uploadMimetype . snd) mupload
        muploadid = map fst mupload
        mimetype = fromMaybe (filetypeToMimetype blobFileType) mmimetype
        odf = isOdf blobFileType
        types = [blobFileType]
    lang <- getLanguage
    (posjobs, mthumbLink1, softwareMap) <- handlerToWidget $ runDB $ do
        posjobs <- getPotentialJobs lang blobid
        mthumbLink1 <- if "image/" `isPrefixOf` mimetype
                       then return $ Just downloadLink
                       else findThumbnail blobid blobFileType
        software <- mapM getLiveSoftware types
        let softwareMap = filter (\(_,l) -> not $ null l) $ zip types software
        return (posjobs, mthumbLink1, softwareMap)
    let (todo,done) = partition p posjobs
    conversionWidget <- handlerToWidget $ listConversions todo
    validation <- handlerToWidget $ odfValidationWidget lang blobid
    blobJobs <- handlerToWidget $ blobJobWidget done
    let (zipwidget, mthumbLink2) = zipWidget blobid blob filename
        mthumbLink = case mthumbLink2 of
                         Nothing -> mthumbLink1
                         _       -> mthumbLink2
    let meta = getMeta blob
    $(widgetFile "blob")
  where
    p (NewJobR{},_,_,_) = True
    p _                 = False

-- | Widget that shows an upload without discussion thread.
uploadWidget :: UploadId -> Widget
uploadWidget uploadid = do
    (upload, blob, user) <- handlerToWidget $ runDB $ do
        upload <- get404 uploadid
        blob <- get404 $ uploadContent upload
        user <-get404 $ uploadUploader upload
        return (upload, blob, user)
    let filename = uploadFilename upload
        blobid = uploadContent upload
        downloadLink = UploadGetR uploadid filename
    blobWidget blobid blob filename downloadLink (Just user)
            (Just (uploadid, upload))

getUploadR :: Lang -> UploadId -> Handler Html
getUploadR lang uploadid = do
    upload <- runDB $ get404 uploadid
    checkUploadViewRights upload
    defaultLayout $ do
        setTitle $ toHtml $ uploadFilename upload
        uploadWidget uploadid
        wikiWidget (`UploadR` uploadid) lang
        discussionWidget (uploadDescription upload) Nothing

getUploadGetR :: UploadId -> Text -> Handler TypedContent
getUploadGetR uploadid filename = do
    upload <- runDB $ get404 uploadid
    let (Upload f blobid _ _ mimetype _ _) = upload
    checkUploadViewRights upload
    (Blob _ content _ _) <- runDB $ get404 blobid
    if f == filename
        then do
            neverExpires
            return $ TypedContent (encodeUtf8 mimetype) $ toContent content
        else notFound

postUploadPublicR :: UploadId -> Bool -> Handler Html
postUploadPublicR uploadid public = do
    userid <- requireAuthId
    upload <- runDB $ get404 uploadid
    unless (uploadUploader upload == userid) $ permissionDenied ""
    runDB $ update uploadid [UploadPublic =. public]
    redirect UploadsR
