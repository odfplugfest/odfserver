module Handler.Software (
    getFactoriesR,
    getFactoriesViewR,
    getFactoryR,
    getFamiliesR,
    getFamilyR,
    getProducerR,
    getProducersR,
    getSoftwareOSR,
    getSoftwareR,
    getSoftwareVersionR
) where

import           Database.Esqueleto
import           Import             hiding (formatTime, groupBy, on, (==.))
import qualified Import             as I

import           CommonTypes        (epochTime)
import           FactoriesWidget
import           FileType
import           Handler.Wiki       (wikiWidget)
import           JobsWidget         (getJobsWidget)
import           TableColumns
import           TimeUtil           (formatDate, formatTime)

odfTypeList :: Esqueleto query expr backend => expr (ValueList FileType)
odfTypeList = valList $ concat allOdfTypes
odfAndPdfTypeList :: Esqueleto query expr backend => expr (ValueList FileType)
odfAndPdfTypeList = valList $ concat allOdfTypes ++ [FileType.PDF]

getFamilies :: MonadIO m => SqlPersistT m [Entity SoftwareFamily]
getFamilies =
    select $ from $ \(family `InnerJoin` software
        `InnerJoin` swversion `InnerJoin` sos `InnerJoin` sinstance
        `InnerJoin` service) -> do
        on $ sinstance ^. ServiceInstanceService ==. service ^. ServiceId
        on $ sos ^. SoftwareOSId ==. sinstance ^. ServiceInstanceSoftware
        on $ swversion ^. SoftwareVersionId ==. sos ^. SoftwareOSSoftware
        on $ software ^. SoftwareId ==. swversion ^. SoftwareVersionSoftware
        on $ family ^. SoftwareFamilyId ==. software ^. SoftwareSoftwareFamily
        where_ $ service ^. ServiceInputType `in_` odfTypeList
        where_ $ service ^. ServiceOutputType `in_` odfAndPdfTypeList
        orderBy [asc (family ^. SoftwareFamilyName)]
        groupBy (family ^. SoftwareFamilyId)
        return family

-- list software families that have services that take some odf as input
-- and give some odf or pdf as output
getFamiliesR :: Lang -> Handler Html
getFamiliesR lang = do
    families  <- runDB getFamilies
    let condensed = False
    defaultLayout $ do
        setTitle "ODF Software Families"
        $(widgetFile "families")
        wikiWidget FamiliesR lang

getFamilyR :: Lang -> SoftwareFamilyId -> Handler Html
getFamilyR lang familyid = do
    (softwares, family) <- runDB $ do
        family <- get404 familyid
        softwares <- selectList [SoftwareSoftwareFamily I.==. familyid]
            [Asc SoftwareName]
        return (softwares, family)
    let condensed = False
    defaultLayout $ do
        setTitle $ toHtml $ softwareFamilyName family
        $(widgetFile "family")
        wikiWidget (`FamilyR` familyid) lang

getSoftwareR :: Lang -> SoftwareId -> Handler Html
getSoftwareR lang softwareid = do
    (versions, software, familyid, family) <- runDB $ do
        software <- get404 softwareid
        let familyid = softwareSoftwareFamily software
        family <- get404 familyid
        versions <- select $ from $ \(swv `LeftOuterJoin` producer) -> do
            on $ swv ^. SoftwareVersionProducer ==. producer ?. ProducerId
            where_ $ swv ^. SoftwareVersionSoftware ==. val softwareid
            return (swv, producer)
        return (versions, software, familyid, family)
    let condensed = False
    defaultLayout $ do
        setTitle $ toHtml $ softwareName software
        $(widgetFile "software")
        wikiWidget (`SoftwareR` softwareid) lang

getOsAndPlatforms :: MonadIO m => Key SoftwareVersion -> SqlPersistT m [(Entity SoftwareOS, Entity Platform, Entity OSVersion, Entity OperatingSystem)]
getOsAndPlatforms swvid =
    select $ from $ \(sos `InnerJoin` platform `InnerJoin` osv `InnerJoin` os) -> do
        on $ os ^. OperatingSystemId ==. osv ^. OSVersionOs
        on $ osv ^. OSVersionId ==. sos ^. SoftwareOSOs
        on $ platform ^. PlatformId ==. sos ^. SoftwareOSPlatform
        where_ $ sos ^. SoftwareOSSoftware ==. val swvid
        return (sos, platform, osv, os)

getSoftwareVersionR :: Lang -> SoftwareVersionId -> Handler Html
getSoftwareVersionR lang versionid = do
    (oss, version, softwareid, software, familyid, family) <- runDB $ do
        version <- get404 versionid
        let softwareid = softwareVersionSoftware version
        software <- get404 softwareid
        let familyid = softwareSoftwareFamily software
        family <- get404 familyid
        oss <- getOsAndPlatforms versionid
        return (oss, version, softwareid, software, familyid, family)
    let condensed = False
    defaultLayout $ do
        setTitle $ toHtml $ softwareName software
        $(widgetFile "version")
        wikiWidget (`SoftwareVersionR` versionid) lang

getSoftwareOSR :: Lang -> SoftwareOSId -> Handler Html
getSoftwareOSR _ _ = defaultLayout $ setTitle "TODO"

getProducerR :: Lang -> ProducerId -> Handler Html
getProducerR _ _ = defaultLayout $ setTitle "TODO"

getProducersR :: Lang -> Handler Html
getProducersR _ = defaultLayout $ setTitle "TODO"

getFactoriesR :: Lang -> Handler Html
getFactoriesR lang = getFactoriesViewR lang $ FactoriesView 0 100 [] [FactoriesWidgetSort FactoriesWidgetLastSeen SortDesc] []

getFactoriesViewR :: Lang -> FactoriesView -> Handler Html
getFactoriesViewR lang factoriesview = do
    factorieswidget <- getFactoriesWidget lang True factoriesview
    defaultLayout $ do
        setTitle "Factories"
        wikiWidget (\_ -> FactoriesViewR lang factoriesview) lang
        $(widgetFile "factories")

getFactoryR :: Lang -> FactoryId -> Handler Html
getFactoryR lang factoryid = do
    factory <- runDB $ get404 factoryid
    let jobsview = JobsView 0 20 [] [] [JobsWidgetFilterFactory $ Just factoryid]
    jobswidget <- getJobsWidget lang True jobsview
    defaultLayout $ do
        setTitle "Factory"
        $(widgetFile "factory")
