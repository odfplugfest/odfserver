module Handler.Home (getHomeR) where

import           Import

getHomeR :: Handler Html
getHomeR = do
    currentLang <- getLanguage
    redirect $ WikiR currentLang []
