module Handler.Blob (
    getBlobR,
    getBlobDownloadR
) where

import           Import

import           FileType
import           Handler.Upload        (blobWidget)
import           TestResultTableWidget (makeName)

getBlobR :: BlobId -> Handler Html
getBlobR blobid = do
    blob <- runDB $ get404 blobid
    let filename = pack $ makeName blobid (blobFileType blob)
        downloadLink = BlobDownloadR blobid filename
    neverExpires
    defaultLayout $ do
        setTitle $ toHtml filename
        blobWidget blobid blob filename downloadLink Nothing Nothing

getBlobDownloadR :: BlobId -> Text -> Handler TypedContent
getBlobDownloadR blobid filename = do
    Blob{blobFileType,blobContent} <- runDB $ get404 blobid
    let mimetype = filetypeToMimetype blobFileType
    if makeName blobid blobFileType == unpack filename
        then do
            neverExpires
            return $ TypedContent (encodeUtf8 mimetype) $ toContent blobContent
        else notFound
