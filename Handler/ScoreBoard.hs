module Handler.ScoreBoard (
    getScoreBoardR,
    getScoreBoardSoftwareR,
    getSoftwareOSScores,
    getSoftwareVersionScores,
    getSoftwareScores
) where

import           Database.Esqueleto
import           Database.Esqueleto.Internal.Language (From)
import           Import                               hiding (Value, count,
                                                       groupBy, member, on,
                                                       (==.))
import qualified Import                               as I

round' :: RealFrac a => a -> Int
round' v = round (100 * v) :: Int

progressBar :: Double -> Int -> Int -> Widget
progressBar score tests testCount = do
    let value = round' $ score * fromIntegral tests / fromIntegral testCount
    $(widgetFile "progressbar")

getScoreBoardR :: TestSetId -> Handler Html
getScoreBoardR testsetid = do
    (testset, testCount, scores) <- runDB $ do
        testset <- get404 testsetid
        testCount <- countTests testsetid
        scores <- getFamilyScores testsetid
        return (testset, testCount, scores)
    -- tests that are not run are considered to have failed
    defaultLayout $ do
        setTitle "Scoreboard"
        $(widgetFile "scoreboard")

getScoreBoardSoftwareR :: TestSetId -> Handler Html
getScoreBoardSoftwareR testsetid = do
    (testset, testCount, scores) <- runDB $ do
        testset <- get404 testsetid
        testCount <- countTests testsetid
        scores <- getSoftwareScores testsetid
        return (testset, testCount, scores)
    -- tests that are not run are considered to have failed
    defaultLayout $ do
        setTitle "Scoreboard"
        $(widgetFile "scoreboard_software")

-- | count the number of tests in a test set
countTests :: MonadIO m => TestSetId -> ReaderT SqlBackend m Int
countTests testsetid = I.count [TestSetMemberSet I.==. testsetid]

getScore :: SqlExpr (Entity TestResult) -> SqlExpr (Value Double)
getScore result = count (case_ [ when_ (result ^. TestResultPass ==. val True)
                                 then_ $ val $ Just True]
                               $ else_ $ val Nothing) *. val 1.0 /. countRows

unvalue5 :: (Value a, Value b, Value c, Value d, Value e) -> (a, b, c, d, e)
unvalue5 (Value a, Value b, Value c, Value d, Value e) = (a, b, c, d, e)

type ScoreRow t = SqlQuery (SqlExpr (Value t), SqlExpr (Value Text), SqlExpr (Value Int), SqlExpr (Value Int), SqlExpr (Value Double))
type Score t = (t, Text, Int, Int, Double)

core :: TestSetId -> SqlExpr (Value t) -> SqlExpr (Value Text) -> SqlExpr (Entity TestSetMember) -> SqlExpr (Entity TestResult) -> [SqlExpr (Value Bool)] -> ScoreRow t
core testsetid key name member result ons = do
    let tests = countDistinct $ member ^. TestSetMemberTest
    on $ result ^. TestResultTest ==. member ^. TestSetMemberTest
    mapM_ on ons
    where_ $ member ^. TestSetMemberSet ==. val testsetid
    groupBy key
    return (key, name, tests, countRows, getScore result)

body :: (MonadIO m, PersistField t, Database.Esqueleto.Internal.Language.From SqlQuery SqlExpr SqlBackend a) => (a -> ScoreRow t) -> ReaderT SqlBackend m [Score t]
body f = do
    rows <- select $ from f
    return $ map unvalue5 rows

getSoftwareOSScores :: MonadIO m => TestSetId -> ReaderT SqlBackend m [Score SoftwareOSId]
getSoftwareOSScores testsetid =
    body $ \(result `InnerJoin` member) -> do
        let key = result ^. TestResultSoftware
        core testsetid key (val "") member result []

getSoftwareVersionScores :: MonadIO m => TestSetId -> ReaderT SqlBackend m [Score SoftwareVersionId]
getSoftwareVersionScores testsetid =
    body $ \(version `InnerJoin` swos `InnerJoin` result `InnerJoin` member) -> do
        let key = swos ^. SoftwareOSSoftware
            name = version ^. SoftwareVersionName
        core testsetid key name member result [
             swos ^. SoftwareOSId ==. result ^. TestResultSoftware,
             version ^. SoftwareVersionId ==. swos ^. SoftwareOSSoftware]

getSoftwareScores :: MonadIO m => TestSetId -> ReaderT SqlBackend m [Score SoftwareId]
getSoftwareScores testsetid =
    body $ \(software `InnerJoin` version `InnerJoin` swos `InnerJoin` result `InnerJoin` member) -> do
        let key = software ^. SoftwareId
            name = software ^. SoftwareName
        core testsetid key name member result [
             swos ^. SoftwareOSId ==. result ^. TestResultSoftware,
             version ^. SoftwareVersionId ==. swos ^. SoftwareOSSoftware,
             key ==. version ^. SoftwareVersionSoftware]

-- | Calculate the percentage of tests that pass for a certain family.
-- This query is simple and ignores a number of influencing factors, e.g.
--  - tests do not need to be validated by users
getFamilyScores :: MonadIO m => TestSetId -> ReaderT SqlBackend m [Score SoftwareFamilyId]
getFamilyScores testsetid =
    body $ \(family `InnerJoin` software `InnerJoin` version `InnerJoin` swos `InnerJoin` result `InnerJoin` member) -> do
        let key = family ^. SoftwareFamilyId
            name = family ^. SoftwareFamilyName
        core testsetid key name member result [
             swos ^. SoftwareOSId ==. result ^. TestResultSoftware,
             version ^. SoftwareVersionId ==. swos ^. SoftwareOSSoftware,
             software ^. SoftwareId ==. version ^. SoftwareVersionSoftware,
             key ==. software ^. SoftwareSoftwareFamily]
