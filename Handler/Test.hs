module Handler.Test (
    getTestR,
    getTestsR,
    getTestsViewR,
    postValidateTestR
) where

import           Data.Map.Lazy           (lookup)
import           Database.Esqueleto      hiding (count, delete, update, (=.))
import           Import                  hiding (Value, count, isNothing, lines,
                                          lookup, on, (==.), (||.))
import           Skylighting.Format.HTML
import           Skylighting.Syntax
import           Skylighting.Tokenizer
import           Skylighting.Types

import           FileType
import           Handler.Wiki            (wikiWidget)
import           TableColumns
import           TestResultTableWidget
import           TestsWidget

xmlToHtml :: Text -> Either String Html
xmlToHtml xml = do
    case lookup "XML" defaultSyntaxMap of
        Just xmlSyntax -> do
            let config = TokenizerConfig defaultSyntaxMap False
            lines <- tokenize config xmlSyntax xml
            return $ formatHtmlBlock defaultFormatOpts $ lines
        _ -> Left "No XML syntax available"

getAutoTest :: MonadHandler m => TestId -> ReaderT SqlBackend m (Test,Entity User, Blob, Entity Upload)
getAutoTest testid = do
    res <- select $ from $ \(test `InnerJoin` autotest `InnerJoin` user `InnerJoin` blob `InnerJoin` upload) -> do
        on (autotest ^. AutoTestInputTemplate ==. upload ^. UploadId)
        on (autotest ^. AutoTestInputFragment ==. blob ^. BlobId)
        on (test ^. TestAuthor ==. user ^. UserId)
        on (test ^. TestId ==. autotest ^. AutoTestTest)
        where_ (test ^. TestId ==. val testid)
        offset 0
        limit 1
        return (test, user, blob, upload)
    case res of
        (Entity _ test, user, Entity _ blob, upload):_ ->
            return (test, user, blob, upload)
        _ -> lift notFound

-- | Return the types of the output files for the given test
getOutputType :: MonadHandler m => TestId -> ReaderT SqlBackend m [FileType]
getOutputType testid = do
    res <- select $ from $ \(autotest `InnerJoin` output) -> do
        on (autotest ^. AutoTestId ==. output ^. AutoTestOutputTypeTest)
        where_ $ autotest ^. AutoTestTest ==. val testid
        return (output ^. AutoTestOutputTypeFileType)
    return $ map (\(Value a) -> a) res

getTestR :: Lang -> TestId -> Handler Html
getTestR lang testid = do
    (test, Entity userid user, blob, Entity uploadid upload, outputs) <- runDB $ do
        (test, user, blob, upload) <- getAutoTest testid
        outputs <- getOutputType testid
        return (test, user, blob, upload, outputs)
    let xml = decodeUtf8 $ blobContent blob
    let html' = xmlToHtml xml
    case html' of
        Left e -> error e
        Right html -> do
            resultTableWidget <- getTestResultTableWidget lang testid
            let changeLang l = TestR l testid
            defaultLayout $ do
                setTitle $ toHtml $ concat ["Test ", testName test]
                $(widgetFile "test")

getTestsR :: Lang -> Handler Html
getTestsR lang = do
    let testsview = TestsView 0 20 [TestsWidgetUser] [] []
    testswidget <- getTestsWidget lang True testsview
    defaultLayout $ do
        setTitle "Tests"
        wikiWidget TestsR lang
        $(widgetFile "tests")

getTestsViewR :: Lang -> TestsView -> Handler Html
getTestsViewR _ testsview = do
    let offset' = testsViewOffset testsview
        count = testsViewLimit testsview
    when (offset' < 0 || count < 1 || count > 100) $ invalidArgs []
    lang <- getLanguage
    testswidget <- getTestsWidget lang True testsview
    defaultLayout $ do
        setTitle "Tests"
        $(widgetFile "tests")

postValidateTestR :: Lang -> TestResultId -> Int -> Handler Html
postValidateTestR lang testresultid correct = do
    userId <- requireAuthId
    rows <- runDB $ select $ from $ \(result `LeftOuterJoin` ver) -> do
        on $ just (result ^. TestResultId) ==. ver ?. TestVerificationTestResult
        where_ $ result ^. TestResultId ==. val testresultid
        where_ $ ver ?. TestVerificationUser ==. val (Just userId)
            ||. (isNothing $ ver ?. TestVerificationId)
        limit 1
        return (
            result ^. TestResultId,
            result ^. TestResultTest,
            ver ?. TestVerificationId,
            ver ?. TestVerificationCorrect)
    case rows of
        (Value _,Value testid,Value (Just vid),Value (Just _)):_ -> do
            runDB $ case correct of
                1 -> update vid [TestVerificationCorrect =. True]
                0 -> update vid [TestVerificationCorrect =. False]
                _ -> delete vid
            redirect $ TestR lang testid
        (Value resultid,Value testid,Value Nothing,_):_ -> do
            runDB $ case correct of
                1 -> insert_ $ TestVerification resultid userId True
                0 -> insert_ $ TestVerification resultid userId False
                _ -> return ()
            redirect $ TestR lang testid
        _ -> notFound
