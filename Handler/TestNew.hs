module Handler.TestNew (
    getTestNewR, postTestNewR,
    getTestCloneR, postTestCloneR
) where

import           Codec.Archive.Zip        (Archive)
import           Control.Applicative      as A
import           Data.List                (nub)
import           Data.Maybe               (fromJust)
import           Data.Tree.NTree.TypeDefs
import           Database.Esqueleto       hiding (insertMany_)
import           Import                   hiding (check, insertMany_, on, (==.))
import           Text.XML.HXT.Core        (XNode (..), XmlTree)
import           Yesod.Form.Bootstrap3

import           Database
import           FileType
import           TestInputCreator
import           TestScheduler
import qualified XPath                    as X


-- Collection of parsed XML files
-- First is the file path, second is the parsed content
type XmlFiles = [(Text, XmlTree)]

-- State of an XPath evaluated XPath expression
data XPathState = XPathTrue            -- evaluated to true
                | XPathWrong Text Text -- expression is wrong for some reason
                | XPathInvalid Text    -- error in parsing the XPath
                | XPathIrrelevant Text -- XPath is true with and without the
                                       -- addition of the XML fragment
                deriving Eq

data XPath = XPath {
    xpathPath  :: Text,      -- file on which to apply the XPath
    xpathXPath :: Text,      -- the XPath expression
    xpathState :: XPathState -- state of the evaluated XPath
} deriving Eq

data DocumentFromTemplate = DocumentFromTemplate {
    templateText :: Text,
    templateXml  :: XmlTree
}

data FormTest = FormTest {
    formTestName        :: Text,
    formTestInput       :: DocumentFromTemplate,
    formTestOutputTypes :: [FileType],
    formXPaths          :: [XPath]
}

data RequestData = RequestData {
    reqUserId        :: UserId
  , reqOdfType       :: FileType
  , reqTemplateBlob  :: BlobId
  , reqTemplate      :: XmlFiles
  , reqArchive       :: Archive
  , reqInput         :: XmlFiles
  , reqUsesFragment  :: Bool
  , reqXmlParseError :: Maybe Text
}

parseXPath :: Maybe XmlTree -> Maybe XmlTree -> Text -> Text -> XPath
parseXPath template testfile xpath file = XPath file xpath x
  where
    x = case X.parseXPath (unpack xpath) of
            Left err -> XPathInvalid $ pack err
            Right expr ->
                case (template, testfile) of
                    (Just t, Just i) -> check (expr t) (expr i)
                    (Just t, _) -> check (expr t) (expr t)
                    _ -> XPathWrong "" (file ++ " is not in the template.")
    check (Just tr) (Just ir) | tr == ir && tr == "True" = XPathIrrelevant tr
    check (Just _) (Just "True") = XPathTrue
    check (Just _) (Just ir) = XPathWrong ir msg
    check _ _ = XPathWrong "" msg
    msg = "Result is not True"

parseXPaths :: XmlFiles -> XmlFiles -> [Text] -> [XPath]
parseXPaths template input (x:_:ys) | null x = parseXPaths template input ys
parseXPaths template input (x:y:ys) = xpath:parseXPaths template input ys
  where t = snd <$> find (\(n,_) -> n == y) template
        i = snd <$> find (\(n,_) -> n == y) input
        xpath = parseXPath t i x y
parseXPaths _ _ _ = []

xpathsField :: XmlFiles -> XmlFiles -> Bool -> Field Handler [XPath]
xpathsField template zipfiles usesFragment = Field
    { fieldParse = \rawVals _ ->
        return $ Right $ Just $ filteredParsedXPaths rawVals
    , fieldView = \idAttr nameAttr otherAttrs eResult _ -> do
        let xpaths = case eResult of
                    Left _       -> []
                    Right result -> zip result [0::Int ..]
            numPaths = length xpaths
            empty' = XPath "" "" XPathTrue
        $(widgetFile "test_form_xpath_table")
    , fieldEnctype = UrlEncoded
    }
  where
    files = map fst $ if usesFragment then zipfiles else template
    parsedXPaths rawVals = parseXPaths template zipfiles rawVals
    -- map XPathIrrelevant to XPathTrue when the fragment is not used
    filteredParsedXPaths p | usesFragment = parsedXPaths p
    filteredParsedXPaths p = map filterParsedXPath $ parsedXPaths p
    filterParsedXPath (XPath a b (XPathIrrelevant _)) = XPath a b XPathTrue
    filterParsedXPath x                               = x
    row idAttr nameAttr otherAttrs options pos xpath
        = $(widgetFile "test_form_xpath_row")
      where posid = unpack idAttr ++ show pos
            haserror = xpathState xpath /= XPathTrue

wrapInElement :: Text -> Text
wrapInElement xml = "<a " ++ ns ++ ">" ++ xml ++ "</a>"
   where
     ns = concatMap (\(a, b) -> " xmlns:" ++ b ++ "='" ++ a ++ "'") X.prefixes

parseXMLField :: [Text] -> IO (Either Text (Text, XmlTree))
parseXMLField [xml] | null xml = return $ Right (xml, NTree (XText "") [])
parseXMLField [xml] = do
    r <- parseXML $ unpack $ wrapInElement xml
    case r of
        Left e                 -> return $ Left e
        Right (NTree _ [tree]) -> return $ Right (xml, tree)
        _                      -> return $ Left "Parsing error"
parseXMLField a = return $ Left $ pack
    $ "Exactly one result expected instead of " ++ show (length a) ++ "."

xmlTemplateField :: Maybe Text -> Field Handler DocumentFromTemplate
xmlTemplateField merror = Field
    { fieldParse = \rawVals _ -> do
        r <- liftIO $ parseXMLField rawVals
        return $ case merror of
            Just err -> Left $ SomeMessage err
            Nothing -> case r of
                Right (raw, xml) -> Right $ Just $ DocumentFromTemplate raw xml
                Left err -> Left $ SomeMessage err
    , fieldView = \idAttr nameAttr otherAttrs eResult _ -> do
        let xml = case eResult of
                    Left unparsed                       -> unparsed
                    Right (DocumentFromTemplate xml' _) -> xml'
        $(widgetFile "test_form_fragment")
    , fieldEnctype= UrlEncoded
    }

getTargetTypes :: FileType -> [FileType]
getTargetTypes fileType = fromMaybe [] $ find (elem fileType) allOdfTypes

defaultInput :: FileType -> Text
defaultInput fileType | fileType `elem` odtTypes
    = "<office:text>\n  <text:p></text:p>\n</office:text>\n"
defaultInput fileType | fileType `elem` odsTypes
    = "<office:spreadsheet>\n</office:spreadsheet>\n"
defaultInput fileType | fileType `elem` odpTypes
    = "<office:presentation>\n</office:presentation>\n"
defaultInput _ = ""

defaultFragment :: FileType -> DocumentFromTemplate
defaultFragment fileType =
        DocumentFromTemplate (s ++ defaultInput fileType) (NTree (XCmt "") [])
    where s = "<office:styles>\n</office:styles>\n"

-- | A form for writing tests
testForm :: RequestData -> Maybe FormTest -> Form FormTest
testForm req test = renderDivs $ FormTest
    A.<$> areq textField (bfs ("Name" :: Text)) (formTestName <$> test)
    A.<*> areq xmlField (bfs ("Input" :: Text)) xml
    A.<*> areq versionField "Version " setTypes
    A.<*> areq xpathsField' (bfs ("XPath" :: Text)) (formXPaths <$> test)
  where
    xmlField = xmlTemplateField $ reqXmlParseError req
    versionField = checkboxesFieldList $ types odfTypes
    xpathsField' = xpathsField (reqTemplate req) (reqInput req) (reqUsesFragment req)
    odfTypes = getTargetTypes $ reqOdfType req
    types :: [FileType] -> [(Text, FileType)]
    types = map (\t -> (showOdfVersion t, t))
    setTypes :: Maybe [FileType]
    setTypes = Just $ maybe odfTypes formTestOutputTypes test
    xml = Just $ maybe (defaultFragment $ reqOdfType req) formTestInput test

-- | Wrap the FormTest in a \<form\> with css styling
-- If 'withSave' is @True@ a save button is shown.
wrapTestFormWidget :: Enctype -> Bool -> Widget -> Widget
wrapTestFormWidget enctype withSave widget = do
    setTitleI MsgWriteANewTest
    addScript $ StaticR codemirror_lib_codemirror_js
    addScript $ StaticR codemirror_addon_edit_closetag_js
    addScript $ StaticR codemirror_addon_edit_matchtags_js
    addScript $ StaticR codemirror_addon_fold_foldcode_js
    addScript $ StaticR codemirror_addon_fold_foldgutter_js
    addScript $ StaticR codemirror_addon_fold_xml_fold_js
    addScript $ StaticR codemirror_addon_hint_show_hint_js
    addScript $ StaticR codemirror_addon_hint_xml_hint_js
    addScript $ StaticR codemirror_addon_lint_lint_js
    addScript $ StaticR codemirror_addon_scroll_annotatescrollbar_js
    addScript $ StaticR codemirror_addon_search_match_highlighter_js
    addScript $ StaticR codemirror_addon_search_matchesonscrollbar_js
    addScript $ StaticR codemirror_addon_search_searchcursor_js
    addScript $ StaticR codemirror_mode_xml_xml_js
    addScript $ StaticR xmlmirror_rngLoader_js
    addScript $ StaticR xmlmirror_schemainfoCreator_js
    addScript $ StaticR xmlmirror_xmlmirror_js
    addStylesheet $ StaticR codemirror_lib_codemirror_css
    addStylesheet $ StaticR codemirror_addon_fold_foldgutter_css
    addStylesheet $ StaticR codemirror_addon_hint_show_hint_css
    addStylesheet $ StaticR codemirror_addon_lint_lint_css
    addStylesheet $ StaticR codemirror_addon_search_matchesonscrollbar_css
    $(widgetFile "test_form")

commonPost :: UploadId -> Handler RequestData
commonPost uploadid = do
    r <- runInputPostResult $ ireq (xmlTemplateField Nothing) "f2"
    common uploadid r

common :: UploadId -> FormResult DocumentFromTemplate -> Handler RequestData
common uploadid r = do
    userId <- requireAuthId
    (blobid, blob) <- runDB $ do
        upload <- get404 uploadid
        blob <- get404 $ uploadContent upload
        return (uploadContent upload, blob)
    res <- liftIO $ getXmlEntries $ blobContent blob
    case res of
        Left _ -> fail "Cannot read template file."
        Right (archive, template) -> do
            -- xml field has to be processed before the rest of the form
            -- because xpath parsing relies on the xml field
            let (input, merror) = parse template r
            liftIO $ print $ "equal ? " ++ show (template == input)
            return RequestData {
                reqUserId = userId,
                reqOdfType = blobFileType blob,
                reqTemplateBlob = blobid,
                reqTemplate = template,
                reqArchive = archive,
                reqInput = input,
                reqUsesFragment = input /= [],
                reqXmlParseError = merror
            }
  where
    parse template (FormSuccess xml)
        = case createTestInput template $ templateXml xml of
              Left err    -> (template, Just err)
              Right input -> (input, Nothing)
    parse template _ = (template, Nothing)

getTestNewR :: UploadId -> Handler Html
getTestNewR uploadId = do
    requestData <- common uploadId FormMissing
    let form = testForm requestData Nothing
    (widget, enctype) <- generateFormPost form
    defaultLayout $
        wrapTestFormWidget enctype False widget

checkFormData :: FormResult FormTest -> Either Text FormTest
checkFormData result =
    case result of
        FormSuccess form -> checkForm form
        FormFailure msgs -> Left $ concat msgs
        FormMissing      -> Left "The form was missing."
  where
    checkForm f | null $ formXPaths f = Left "At least one XPath test is needed."
    checkForm f | xpathNotAllTrue f = Left "Please fix the XPaths."
    checkForm f = Right f
    xpathNotAllTrue f = not $ all (XPathTrue ==) $ map xpathState $ formXPaths f

findTest :: MonadHandler m => Text -> BlobId -> UploadId -> BlobId -> [AutoTestXPathId] -> ReaderT SqlBackend m (Maybe TestId)
findTest testName' fragment uploadid file xpaths = do
    res <- select $ distinct $ from $ \(test `InnerJoin` autotest `InnerJoin` xpath) -> do
        on (autotest ^. AutoTestId ==. xpath ^. AutoTestXPathUseTest)
        on (test ^. TestId ==. autotest ^. AutoTestTest)
        where_ (test ^. TestName ==. val testName')
        where_ (autotest ^. AutoTestInputFragment ==. val fragment)
        where_ (autotest ^. AutoTestInputTemplate ==. val uploadid)
        where_ (autotest ^. AutoTestInputFile ==. val file)
        where_ (xpath ^. AutoTestXPathUseXpath `in_` (valList xpaths))
        orderBy [asc (xpath ^. AutoTestXPathUseXpath)]
        return (test ^. TestId, xpath ^. AutoTestXPathUseXpath)
    let xpaths' = sort $ nub $ map (\(_,Value x) -> x) res
    return $ if xpaths' == sort xpaths
        then case res of
            ((Value testid,_):_) -> Just testid
            _                    -> Nothing
        else Nothing

saveAutoTest :: RequestData -> UploadId -> FormTest -> Handler TestId
saveAutoTest requestData uploadId form = do
    let name = formTestName form
        content = encodeUtf8 $ fromStrict $ templateText $ formTestInput form
        file = createOdf (reqInput requestData) (reqArchive requestData)
    testId <- runDB $ do
        fragmentId <- addBlob content XML
        fileId <- if reqUsesFragment requestData
                  then addBlob file $ reqOdfType requestData
                  else return $ reqTemplateBlob requestData
        x <- mapM (\x -> upsert (xpathToDb x) []) $ formXPaths form
        let xids = map (\(Entity key _) -> key) x
        mid <- findTest name fragmentId uploadId fileId xids
        case mid of
            Just testId -> return testId
            Nothing -> do
                now <- liftIO getCurrentTime
                let newTest = Test Nothing now (reqUserId requestData) name
                testId <- insert newTest
                let autotest = AutoTest testId fragmentId uploadId fileId
                test <- insert autotest
                insertMany_ 2 $ map (AutoTestOutputType test) $ formTestOutputTypes form
                insertMany_ 2 $ map (\(Entity k _) -> AutoTestXPathUse test k) x
                return testId
    App{appScheduler} <- getYesod
    liftIO $ evaluateAutoTests appScheduler
    return testId
  where
    xpathToDb (XPath path xpath _) = AutoTestXPath path xpath

postTestNewR :: UploadId -> Handler Html
postTestNewR uploadId = do
    requestData <- commonPost uploadId
    let form = testForm requestData Nothing
    ((result, widget), enctype) <- runFormPost form
    isSave <- runInputPost $ iopt boolField "save"
    let result' = checkFormData result
    case result' of
        Right formData -> do
            let name = formTestName formData
            case isSave of
                Just True -> do
                    testId <- saveAutoTest requestData uploadId formData
                    App{appScheduler} <- getYesod
                    liftIO $ scheduleTestJobs appScheduler
                    setMessageI $ MsgTestSaved name
                    redirect $ TestR "en" testId
                _ -> defaultLayout $ do
                    setMessageI $ MsgTestValid name
                    wrapTestFormWidget enctype True widget
        Left errorMsg ->
            case isSave of
                Just True -> invalidArgs [errorMsg]
                _ -> do
                    setMessage $ toHtml errorMsg
                    defaultLayout $ wrapTestFormWidget enctype False widget

loadTest :: MonadIO m => Entity AutoTest -> ReaderT SqlBackend m FormTest
loadTest (Entity autotestid autotest) = do
    rows <- select $ from $ \(output `InnerJoin` use `InnerJoin` xpath) -> do
        on $ use ^. AutoTestXPathUseXpath ==. xpath ^. AutoTestXPathId
        on $ output ^. AutoTestOutputTypeTest ==. use ^. AutoTestXPathUseTest
        where_ $ output ^. AutoTestOutputTypeTest ==. val autotestid
        return (output ^. AutoTestOutputTypeFileType, xpath)
    let rows' = map (unValue *** entityVal) rows
    test <- get404 $ autoTestTest autotest
    fragment <- get404 $ autoTestInputFragment autotest
    let fragmentText = decodeUtf8 $ blobContent fragment
    Right (fragmentDoc, xml) <- liftIO $ parseXMLField [fragmentText]
    let input = DocumentFromTemplate {
            templateText = fragmentDoc,
            templateXml = xml
        }
        xpaths = nub $ map (\(_,AutoTestXPath a b) -> XPath a b XPathTrue) rows'
    return FormTest {
        formTestName        = testName test,
        formTestInput       = input,
        formTestOutputTypes = nub $ map fst rows',
        formXPaths          = xpaths
    }

getTestCloneR :: TestId -> Handler Html
getTestCloneR testid = do
    (uploadId, formData) <- runDB $ do
        mautotest <- getBy $ UniqueTest testid
        let autotest = fromJust mautotest
        let uploadId = autoTestInputTemplate $ entityVal autotest
        formData <- loadTest autotest
        return (uploadId, formData)
    requestData <- common uploadId $ FormSuccess $ formTestInput formData
    let form = testForm requestData $ Just formData
    (widget, enctype) <- generateFormPost form
    defaultLayout $
        wrapTestFormWidget enctype False widget

postTestCloneR :: TestId -> Handler Html
postTestCloneR testid = do
    mautotest <- runDB $ getBy $ UniqueTest testid
    let autotest = fromJust mautotest
    postTestNewR $ autoTestInputTemplate $ entityVal autotest
