module Handler.Service (
    getServiceR,
    getServicesViewR,
    getServicesR
) where

import           Import

import           ServicesWidget
import           TableColumns

getServiceR :: ServiceId -> Handler Html
getServiceR serviceid = do
    Service{serviceName} <- runDB $ get404 serviceid
    defaultLayout $ do
        setTitle $ toHtml serviceName
        $(widgetFile "service")

getServicesViewR :: Handler Html
getServicesViewR = getServicesR $ ServicesView 0 100 [] [] []

getServicesR :: ServicesView -> Handler Html
getServicesR servicesview = do
    serviceswidget <- getServicesWidget True servicesview
    defaultLayout $ do
        setTitle "Services"
        $(widgetFile "services")
