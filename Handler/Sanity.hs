module Handler.Sanity (
    getSanityR,
    getSanityLinksR
) where

import           Data.List                     (intersect, nub)
import qualified Data.Text                     as T
import qualified Data.Text.Lazy                as L
import           Database.Esqueleto            hiding (count)
import           Import                        hiding (Upload, Value, Wiki,
                                                groupBy, intersect, isNothing,
                                                on, (!=.), (==.))
import           Text.Blaze.Html.Renderer.Text (renderHtml)
import           Text.HTML.TagSoup             (Tag (TagOpen), parseTags)

import           FileType

getAutoTestsWithInconsistentTypes :: MonadHandler m => ReaderT SqlBackend m [(TestId, FileType, FileType)]
getAutoTestsWithInconsistentTypes = do
    rows <- select $ from $ \(test `InnerJoin` autotest `InnerJoin` upload `InnerJoin` blob1 `InnerJoin` blob2) -> do
        on $ upload ^. UploadContent ==. blob2 ^. BlobId
        on $ autotest ^. AutoTestInputFile ==. blob1 ^. BlobId
        on $ autotest ^. AutoTestInputTemplate ==. upload ^. UploadId
        on $ test ^. TestId ==. autotest ^. AutoTestTest
        where_ $ blob1 ^. BlobFileType !=. blob2 ^. BlobFileType
        return (test ^. TestId, blob1 ^. BlobFileType, blob2 ^. BlobFileType)
    return $ map (\(Value a,Value b,Value c)->(a,b,c)) rows

getResultsWithWrongJob :: MonadHandler m => ReaderT SqlBackend m [(Entity Test,BlobId,BlobId)]
getResultsWithWrongJob = do
    rows <- select $ from $ \(autoresult `InnerJoin` testresult `InnerJoin` test
                             `InnerJoin` autotest `InnerJoin` jobresult
                             `InnerJoin` job) -> do
        on $ jobresult ^. JobResultJob ==. job ^. JobId
        on $ autoresult ^. AutoTestResultJob ==. jobresult ^. JobResultId
        on $ test ^. TestId ==. autotest ^. AutoTestTest
        on $ testresult ^. TestResultTest ==. test ^. TestId
        on $ autoresult ^. AutoTestResultResult ==. testresult ^. TestResultId
        where_ $ autotest ^. AutoTestInputFile !=. job ^. JobInput
        groupBy (test ^. TestId, autotest ^. AutoTestInputFile,job ^. JobInput)
        orderBy [asc (test ^. TestId)]
        return (test,autotest ^. AutoTestInputFile,job ^. JobInput)
    return $ map (\(a,Value b,Value c) -> (a,b,c)) rows

getUnusedProse :: MonadHandler m => ReaderT SqlBackend m [(ProseId,Text)]
getUnusedProse = do
    rows <- select $ from $ \(prose `LeftOuterJoin` upload `LeftOuterJoin`
                                 comment `LeftOuterJoin` page `LeftOuterJoin`
                                 wiki) -> do
        on $ just (prose ^. ProseId) ==. wiki ?. WikiProse
        on $ just (prose ^. ProseId) ==. page ?. PageCurrent
        on $ just (prose ^. ProseId) ==. comment ?. CommentComment
        on $ just (prose ^. ProseId) ==. upload ?. UploadDescription
        where_ $ isNothing $ wiki ?. WikiId
        where_ $ isNothing $ page ?. PageId
        where_ $ isNothing $ comment ?. CommentId
        where_ $ isNothing $ upload ?. UploadId
        where_ $ not_ $ exists $ from $ \p ->
                where_ $ p ^. ProsePreviousVersion ==. just (prose ^. ProseId)
        return (prose ^. ProseId, prose ^. ProseTitle)
    return $ map (\(Value a, Value b) -> (a, b)) rows

getSanityR :: Handler Html
getSanityR = do
    lang <- getLanguage
    autoTestsWithInconsistentTypes <- runDB getAutoTestsWithInconsistentTypes
    resultsWithWrongJob <- runDB getResultsWithWrongJob
    unusedProse <- runDB getUnusedProse
    defaultLayout $ do
        setTitle "sanity checks"
        $(widgetFile "sanity")

data LocalLink = Good
               | Bad
               | Upload Lang UploadId
               | UploadGet UploadId Text
               | Wiki Lang Text

parseLocalLink :: L.Text -> Handler LocalLink
parseLocalLink link = do
    b <- catchAny (parseLocalLink' link) (\_ -> return Nothing)
    case b of
        Nothing                         -> return Bad
        Just (UploadR lang uploadid)    -> return $ Upload lang uploadid
        Just (UploadGetR uploadid name) -> return $ UploadGet uploadid name
        Just (WikiR lang path)          -> return $ Wiki lang ("wiki/" ++  intercalate "/" path)
        _                               -> return Good

parseLocalLink' :: L.Text -> Handler (Maybe (Route App))
parseLocalLink' link = return $ pr parts
  where
    parts = filter ("" /=) $ T.splitOn "/" $ toStrict link
    pr p = parseRoute (p, [])

checkLinks :: [(L.Text, LocalLink)] -> Handler [L.Text]
checkLinks ls = do
    let wikis = mapMaybe (\(l,v) -> case v of
                    Wiki lang path -> Just (l, lang, path)
                    _              -> Nothing) ls
        badothers = mapMaybe (\(l,v) -> case v of
                    Bad -> Just l
                    _   -> Nothing) ls
    badwikis <- checkWikis wikis
    return $ badwikis ++ badothers

checkWikis :: [(L.Text, Lang, Text)] -> Handler [L.Text]
checkWikis wikis = do
    let paths = map (\(_,_,p) -> p) wikis
    rows <- runDB $ select $ from (\wiki -> do
        where_ $ wiki ^. WikiPath `in_` valList paths
        return (wiki ^. WikiLanguage, wiki ^. WikiPath))
    let rows' = map (\(Value a, Value b) -> (a, b)) rows
        badwikis = mapMaybe (\(link, lang, path) -> if (lang,path) `elem` rows'
                                                        then Nothing
                                                        else Just link) wikis
    return badwikis

-- local links to images should all point to an UploadGetR route
-- this function checks in the database if they exist
-- todo: also check links to static and blob urls
checkSrcs :: [(L.Text, LocalLink)] -> Handler [L.Text]
checkSrcs ls = do
    let pidnames  = mapMaybe (\(l,v) -> case v of
                      UploadGet pid name -> Just (l, pid, name)
                      _                  -> Nothing) ls
        badothers = mapMaybe (\(l,v) -> case v of
                      UploadGet _ _ -> Nothing
                      _             -> Just l) ls
    badups <- checkUploads pidnames
    return $ badups ++ badothers

checkUploads :: [(L.Text, UploadId, Text)] -> Handler [L.Text]
checkUploads pidnames = do
    let pids = map (\(_,p,_) -> p) pidnames
    rows <- runDB $ select $ from (\upload -> do
        where_ $ upload ^. UploadId `in_` valList pids
        where_ $ upload ^. UploadPublic ==. val True
        return (upload ^. UploadId, upload ^. UploadFilename))
    let rows' = map (\(Value a, Value b) -> (a, b)) rows
        badups = mapMaybe (\(link, pid, name) -> if (pid,name) `elem` rows'
                                                    then Nothing
                                                    else Just link) pidnames
    return badups

data ProseLinks = ProseLinks {
    proseId :: ProseId,
    prose   :: Prose,
    hrefs   :: [L.Text],
    srcs    :: [L.Text]
}

-- get a/@href and img/@src that start with "/"
getLinks :: ProseId -> Prose -> ProseLinks
getLinks proseid prose = ProseLinks {
                            proseId = proseid,
                            prose = prose,
                            hrefs = mapMaybe getA tags,
                            srcs = mapMaybe getImg tags}
  where
    text = renderHtml $ proseHtmlContent prose
    tags = parseTags text
    getA (TagOpen "a" atts) = getHref atts
    getA _                  = Nothing
    getHref (("href", v):_) | "/" `L.isPrefixOf` v = Just v
    getHref (_:xs)          = getHref xs
    getHref []              = Nothing
    getImg (TagOpen "img" atts) = getSrc atts
    getImg _                    = Nothing
    getSrc (("src", v):_) | "/" `L.isPrefixOf` v = Just v
    getSrc (_:xs)         = getSrc xs
    getSrc []             = Nothing

-- return the newest version of each prose field
getCurrentProseHtml :: MonadIO m => ReaderT SqlBackend m [Entity Prose]
getCurrentProseHtml =
    select $ from $ \prose -> do
        where_ $ not_ $ exists $ from $ \p ->
                where_ $ p ^. ProsePreviousVersion ==. just (prose ^. ProseId)
        orderBy [desc (prose ^. ProseCreated)]
        return prose

getEditLinks :: [ProseLinks] -> Handler [(ProseId, Route App)]
getEditLinks links = do
    let pids = map proseId links
    rows <- runDB $ select $ from (\(prose `LeftOuterJoin` wiki) -> do
        on $ just (prose ^. ProseId) ==. wiki ?. WikiProse
        where_ $ prose ^. ProseId `in_` valList pids
        return (prose ^. ProseId, wiki))
    let m = mapMaybe (\(Value proseid, v) -> case v of
                 Nothing              -> Nothing
                 Just (Entity _ wiki) -> Just (proseid, wr wiki)) rows
    return m
  where
    wr w = WikiEditR (wikiLanguage w) (filter (not . null) $ T.splitOn "/" $ wikiPath w)

getSanityLinksR :: Handler Html
getSanityLinksR = do
    htmls <- runDB getCurrentProseHtml
    let links = map (\(Entity pid v) -> getLinks pid v) htmls
        ulinks = nub $ concatMap hrefs links
        usrcs = nub $ concatMap srcs links
    iulinks' <- mapM parseLocalLink ulinks
    iusrcs' <- mapM parseLocalLink usrcs
    iulinks <- checkLinks $ zip ulinks iulinks'
    iusrcs <- checkSrcs $ zip usrcs iusrcs'
    let badLinks = mapMaybe (filterLinks iulinks iusrcs) links
    editLinks <- getEditLinks badLinks
    defaultLayout $ do
        setTitle "sanity checks"
        $(widgetFile "sanity_links")

filterLinks :: [L.Text] -> [L.Text] -> ProseLinks -> Maybe ProseLinks
filterLinks a i p = if (null ma && null mi)
                        then Nothing
                        else Just $ p { hrefs = ma, srcs = mi }
  where
    ma = intersect a $ hrefs p
    mi = intersect i $ srcs p
