module Handler.Job (
    getJobR,
    postNewJobR,
    getJobsR,
    getJobsListR,
    postRemoveFailedJobR,
) where

import           Data.String.Conversions (cs)
import           Database.Esqueleto      hiding (count, get)
import           Import                  hiding (Value, count, formatTime, get,
                                          isNothing, on, (==.))
import qualified Import                  as I

import qualified Database                as D
import           Handler.Wiki            (wikiWidget)
import           JobsWidget
import           TableColumns
import           TestResultTableWidget   (getValidation)
import           TimeUtil

-- return instances if no job for this (blobid,serviceid,softwareid) has run
-- yet
getServiceInstances :: (YesodPersist site, YesodPersistBackend site ~ SqlBackend) => BlobId -> SoftwareOSId -> ServiceId -> HandlerT site IO [ServiceInstanceId]
getServiceInstances blobid softwareid serviceid = do
    instances <- runDB $ select $ from $ \(blob `InnerJoin`service
                                          `InnerJoin` sinstance
                                          `InnerJoin` software) -> do
        on $ sinstance ^. ServiceInstanceSoftware ==. software ^. SoftwareOSId
        on $ service ^. ServiceId ==. sinstance ^. ServiceInstanceService
        on $ blob ^. BlobFileType ==. service ^. ServiceInputType
        where_ $ blob ^. BlobId ==. val blobid
        where_ $ service ^. ServiceId ==. val serviceid
        where_ $ software ^. SoftwareOSId ==. val softwareid
        offset 0
        limit 1000
        return (sinstance ^. ServiceInstanceId)
    return $ map (\(Value si) -> si) instances

postNewJobR :: BlobId -> SoftwareOSId -> ServiceId -> Handler Html
postNewJobR blobid softwareid serviceid = do
    userid <- requireAuthId
    instances <- getServiceInstances blobid softwareid serviceid
    now <- lift getCurrentTime
    case instances of
        [] -> invalidArgs [ "No serviceinstances available." ]
        (_:_) -> do
            let job = Job userid now blobid softwareid serviceid Nothing Nothing
            App {appDatabase} <- getYesod
            jobid <- D.addJob appDatabase job
            redirect $ JobR "en" jobid

displayText:: Maybe Text -> String
displayText s = case s of
         Nothing -> ""
         Just n  -> cs n
displayInt:: Maybe Int -> String
displayInt s = case s of
         Nothing -> ""
         Just n  -> show n

getJobsListR :: Handler Html
getJobsListR = getJobsR $ JobsView 0 20 [] [] []

getJobsR :: JobsView -> Handler Html
getJobsR jobsview = do
    let offset' = jobsViewOffset jobsview
        count = jobsViewLimit jobsview
    lang <- getLanguage
    when (offset' < 0 || count < 1 || count > 100) $ invalidArgs []
    jobswidget <- getJobsWidget lang True jobsview
    defaultLayout $ do
        setTitle "Jobs"
        $(widgetFile "jobs")


getViableFactories :: MonadHandler m => JobId -> ReaderT SqlBackend m [Entity Factory]
getViableFactories jobid = do
    select $ distinct $ from $ \(job `InnerJoin` serviceinstance
            `InnerJoin` factory) -> do
        on (serviceinstance ^. ServiceInstanceFactory ==. factory ^. FactoryId)
        on (job ^. JobService ==. serviceinstance ^. ServiceInstanceService
            &&. job ^. JobSoftware ==. serviceinstance ^. ServiceInstanceSoftware)
        where_ $ isNothing (job ^. JobServiceInstance)
        where_ $ job ^. JobId ==. val jobid
        orderBy [desc (factory ^. FactoryLastSeen)]
        return factory

getFails :: MonadHandler m => JobId -> ReaderT SqlBackend m [(Entity FailedJob, Entity ServiceInstance)]
getFails jobid = do
    select $ from $ \(failedjob `InnerJoin` serviceinstance) -> do
        on (failedjob ^. FailedJobServiceInstance ==. serviceinstance ^. ServiceInstanceId)
        where_ $ failedjob ^. FailedJobJob ==. val jobid
        orderBy [desc (serviceinstance ^. ServiceInstanceLastSeen)]
        return (failedjob, serviceinstance)

showRetry :: UTCTime -> ServiceInstance -> Maybe JobResult -> FailedJob -> Bool
showRetry now serviceinstance mResult fail' =
    recent now (serviceInstanceLastSeen serviceinstance)
    && mResult == Nothing
    && failedJobRestarter fail' == Nothing

getJobR :: Lang -> JobId -> Handler Html
getJobR lang jobid = do
    muser <- maybeAuthId
    (job, user, uploads, service, software, mFactory, mResult, validationErrors, viableFactories, fails) <- runDB $ do
        job <- get404 jobid
        user <- get404 $ jobUser job
        let inputBlobId = jobInput job
        uploads <- selectList [UploadContent I.==. inputBlobId] []
        service <- get404 $ jobService job
        softwareos <- get404 $ jobSoftware job
        version <- get404 $ softwareOSSoftware softwareos
        software <- get404 $ softwareVersionSoftware version
        maybeFactory <-
            case jobServiceInstance job of
                Nothing -> return Nothing
                Just instanceId -> do
                    sinstance <- get404 instanceId
                    factory <- get404 $ serviceInstanceFactory sinstance
                    return $ Just (sinstance, factory)
        viableFactories <- getViableFactories jobid
        mResult <- case jobServiceInstance job of
            Nothing -> return Nothing
            Just _ -> do
                mr <- getBy $ UniqueJobResult jobid
                return $ unwrap mr
        validationErrors <- getValidation inputBlobId
        fails <- getFails jobid
        return (job, user, uploads, service, software, maybeFactory, mResult, validationErrors, viableFactories, fails)
    now <- lift getCurrentTime
    defaultLayout $ do
        setTitle "Job"
        $(widgetFile "job")
        wikiWidget (`JobR` jobid) lang
    where unwrap Nothing             = Nothing
          unwrap (Just (Entity _ r)) = Just r

postRemoveFailedJobR :: Lang -> FailedJobId -> Handler Html
postRemoveFailedJobR lang fjobid = do
    userid <- requireAuthId
    fjob <- runDB $ do
        fjob <- get404 fjobid
        I.update fjobid [ FailedJobRestarter I.=. Just userid ]
        return fjob
    redirect $ JobR lang $ failedJobJob fjob
