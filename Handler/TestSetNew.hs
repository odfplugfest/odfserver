module Handler.TestSetNew (
    getTestSetNewR,
    postTestSetNewR
) where

import           Control.Applicative as A
import           Data.Map            (filter, foldrWithKey, fromListWith)
import           Database.Esqueleto  hiding (groupBy, insertMany_)
import           Import              hiding (filter, insertMany_, on, (==.))

import           Database

data FormTestSet = FormTestSet {
    formTestSetName  :: Text,
    formTestSetTests :: [TestId]
}

testSetForm :: [Entity Test] -> FormTestSet -> Form FormTestSet
testSetForm tests testset = renderDivs $ FormTestSet
    A.<$> areq textField "Name" (Just $ formTestSetName testset)
    A.<*> areq testsField "Tests" setTests
  where
    entityToTuple (Entity testid Test{testName}) = (testName, testid)
    testsField = checkboxesField . optionsPairs' $ map entityToTuple tests
    setTests = Just $ formTestSetTests testset

optionsPairs' :: (ToBackendKey SqlBackend a, RenderMessage (HandlerSite m) msg, MonadHandler m)
            => [(msg, Key a)] -> m (OptionList (Key a))
optionsPairs' opts = do
  mr <- getMessageRender
  let mkOption (display, internal) =
          Option { optionDisplay       = mr display
                 , optionInternalValue = internal
                 , optionExternalValue = pack $ show $ fromSqlKey internal
                 }
  return $ mkOptionList $ map mkOption opts

common :: Handler (UserId, [Entity Test])
common = do
    userid <- requireAuthId
    tests <- runDB $ selectList [] []
    return (userid, tests)

getTestSetNewR :: Handler Html
getTestSetNewR = do
    (_, tests) <- common
    let form = testSetForm tests $ FormTestSet "" []
    (widget, enctype) <- generateFormPost form
    defaultLayout $ $(widgetFile "testset_form")

findTestSet :: MonadIO m => Text -> [TestId] -> ReaderT SqlBackend m (Maybe TestSetId)
findTestSet name tests = do
    rows <- select $ distinct $ from $ \(testset `InnerJoin` memb) -> do
        on (testset ^. TestSetId ==. memb ^. TestSetMemberSet)
        where_ (testset ^. TestSetName ==. val name)
        orderBy [asc(testset ^. TestSetId), desc (memb ^. TestSetMemberTest)]
        return (testset ^. TestSetId, memb ^. TestSetMemberTest)
    let rows' = map (unValue *** unValue) rows
        grouped = fromListWith (++) [(k, [v]) | (k, v) <- rows']
        match = filter (== tests) grouped
    return $ foldrWithKey (\a _ _ -> Just a) Nothing match

postTestSetNewR :: Handler Html
postTestSetNewR = do
    (userid, allTests) <- common
    let form = testSetForm allTests $ FormTestSet "" []
    ((result, widget), enctype) <- runFormPost form
    case result of
        FormSuccess (FormTestSet name tests) -> do
            testsetid <- runDB $ do
                now <- liftIO getCurrentTime
                mid <- findTestSet name (sort tests)
                case mid of
                    Just testsetid -> do
                        setMessage "This test set already exists."
                        return testsetid
                    Nothing -> do
                        testsetid <- insert $ TestSet Nothing now userid name
                        insertMany_ 2 $ map (TestSetMember testsetid) tests
                        setMessage "Your test set was created."
                        return testsetid
            redirect $ TestSetR "en" testsetid
        FormFailure msgs -> do
            setMessageI $ concat $ "No test set was created. " : msgs
            defaultLayout $ $(widgetFile "testset_form")
        FormMissing -> do
            setMessage "No test set was created: the form was missing."
            defaultLayout $ $(widgetFile "testset_form")
