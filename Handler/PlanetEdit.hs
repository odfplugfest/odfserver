module Handler.PlanetEdit (
    getPlanetEditR,
    postPlanetEditR
) where

import           Control.Applicative   as A
import qualified Data.Text             as T (replace)
import           Import                hiding (insertMany_)
import           Yesod.Form.Bootstrap3

import           Database

data FormPlanet = FormPlanet {
    formPlanetFeeds :: [Feed]
}

planetForm :: [Entity Feed] -> Form FormPlanet
planetForm feeds = renderDivs $ FormPlanet
    A.<$> areq feedField (bfs ("" :: Text)) (Just feeds')
  where
    feeds' = map (\(Entity _ v) -> v) feeds

-- | parse triples of text values into feed objects
parseFeedFields :: [Text] -> [Maybe Feed]
parseFeedFields (name:active:url:xs) | active == "active" = (parse >>= toFeed) : parseFeedFields xs
  where
    toFeed r = Just Feed {
                        feedName = name,
                        feedUrl = cleanURI $ pack $ show $ getUri r,
                        feedIcon = Nothing,
                        feedActive = True}
    parse = parseUrlThrow $ unpack url
parseFeedFields (name:url:xs) = (parse >>= toFeed) : parseFeedFields xs
  where
    toFeed r = Just Feed {
                        feedName = name,
                        feedUrl = cleanURI $ pack $ show $ getUri r,
                        feedIcon = Nothing,
                        feedActive = False}
    parse = parseUrlThrow $ unpack url
parseFeedFields (_:_) = []
parseFeedFields _ = []

-- 'show $ getUri' adds default port number 443 and 80 in the
-- serialized url. In this function we remove them
cleanURI :: Text -> Text
cleanURI u = if "https://" `isPrefixOf` u
                 then T.replace ":443/" "/" u
                 else T.replace ":80/" "/" u

feedField :: Field Handler [Feed]
feedField = Field
    { fieldParse = \rawVals _ ->
        return $ Right $ Just $ catMaybes $ parseFeedFields rawVals
    , fieldView = \idAttr nameAttr otherAttrs eResult _ -> do
        let feeds = case eResult of
                         Left _       -> []
                         Right result -> zip result [0::Int ..]
            numFeeds = length feeds
            empty' = Feed "" "" Nothing True
        $(widgetFile "planet_edit_feed_field")
    , fieldEnctype = UrlEncoded
    }
  where
    row idAttr nameAttr otherAttrs pos feed = do
        let checked = feedActive feed
            posid = unpack idAttr ++ show pos
        $(widgetFile "planet_edit_feed_field_row")

getFeeds :: Handler [Entity Feed]
getFeeds = do
    feeds <- runDB $ selectList [] []
    return $ sortOn sorter feeds
  where
    sorter (Entity _ Feed{feedUrl}) = feedUrl

getPlanetEditR :: Handler Html
getPlanetEditR = do
    feeds <- getFeeds
    let form = planetForm feeds
    (widget, enctype) <- generateFormPost form
    defaultLayout $ do
        setTitle "edit planet"
        $(widgetFile "planet_edit")

mergeFeedLists :: [Entity Feed] -> [Feed] -> ([Entity Feed], [Feed])
mergeFeedLists current form = (changed, new)
  where
    changed = mapMaybe check' current
    check' (Entity key feed) =
        case find (\f -> feedUrl f == feedUrl feed) form of
            Nothing -> Nothing
            Just f' ->
                if feedActive f' == feedActive feed
                    then Nothing
                    else Just $ Entity key feed {feedActive = feedActive f'}
    urls = map (\(Entity _ Feed{feedUrl}) -> feedUrl) current
    new = filter (\f -> feedUrl f `onotElem` urls) form

postPlanetEditR :: Handler Html
postPlanetEditR = do
    feeds <- getFeeds
    let form = planetForm feeds
    ((result, _), _) <- runFormPost form
    case result of
        FormSuccess FormPlanet{formPlanetFeeds} -> do
            let (changed, new) = mergeFeedLists feeds formPlanetFeeds
            _ <- runDB $ do
                mapM_ replace' changed
                insertMany_ 4 new
            setMessage "The feeds have been saved."
            redirect PlanetEditR
        FormFailure errMsgs -> do
            setMessage $ toHtml $ unlines errMsgs
            redirect PlanetEditR
        _ -> invalidArgs []
  where
    replace' (Entity key value) = replace key value
