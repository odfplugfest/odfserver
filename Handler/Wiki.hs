module Handler.Wiki (
    getWikiR,
    getWikiEditR, postWikiEditR,
    getWikiChangesR,
    postFormatMarkdownR,
    wikiWidget
) where

import qualified Data.Conduit.List  as CL
import qualified Data.Conduit.Text  as CT
import           Data.List          (nub)
import qualified Data.Text          as T
import           Database.Esqueleto hiding (isNothing, update, (=.))
import           Import             hiding (Value, count, formatTime, groupBy,
                                     on, (==.), (>.), (||.))
import qualified Import             as I
import           Network.Wai        (pathInfo)

import           Prose
import           TimeUtil

-- wiki uses
-- Any path is allowed for a wiki.
-- If the path after /wiki/$lang corresponds to an existing page in the
-- system, then a link to that page will be shown in the wiki page.


-- if the language is not in the list of the supported language,
-- redirect to the uri for the current language
checkLanguage :: (RedirectUrl (HandlerSite m) r, MonadHandler m) => Text -> t -> (Lang -> t -> r) -> m ()
checkLanguage lang page route = do
    currentLang <- getLanguage
    unless (isAvailableLanguage lang) $ redirect $ route currentLang page

-- Show a wiki page that is not associated with a system page.
getWikiR :: Lang -> [Text] -> Handler Html
getWikiR lang page = do
    checkLanguage lang page WikiR
    defaultLayout $ wikiWidget2 (`WikiR` page) lang True ("wiki":page)

wikiWidget :: (Lang -> Route App) -> Lang -> Widget
wikiWidget changeLang lang = do
    req <- handlerToWidget waiRequest
    wikiWidget2 changeLang lang False $ pathInfo req

-- return a route if it can point to a page which has wiki content
getRouteAndTitle :: Lang -> [Text] -> Handler (Route App, Maybe Text)
getRouteAndTitle lang page =
    -- parseRoute may throw an exception because of compressed urls
    -- because of laziness, the entire evaluation of the route parse result
    -- should be wrapped in a catchAny
    catchAny (getRouteAndTitle' lang page) (\_ ->
             return (WikiR lang page, Nothing))

getRouteAndTitle' :: Lang -> [Text] -> Handler (Route App, Maybe Text)
getRouteAndTitle' lang page =
    case page of
        ("wiki":rest) -> return (WikiR lang rest, Nothing)
        _ -> do
            let mroute = parseRoute (page, [])
            case mroute of
                -- list of routes that have wiki content
                Just HomeR -> return (HomeR, Nothing)
                Just (JobR l j) -> return (JobR l j, Just "")
                Just (UploadR l u) -> return (UploadR l u, Just "")
                Just (UserR l u) -> return (UserR l u, Just "")
                Just (FamiliesR l) -> return (FamiliesR l, Nothing)
                Just (FamilyR l f) -> return (FamilyR l f, Just "")
                Just (SoftwareR l s) -> return (SoftwareR l s, Just "")
                Just (SoftwareVersionR l s) -> return (SoftwareVersionR l s, Just "")
                Just (SoftwareOSR l s) -> return (SoftwareOSR l s, Just "")
                Just (ProducerR l s) -> return (ProducerR l s, Just "")
                Just (TestR l t) -> return (TestR l t, Just "")
                Just (TestsR l) -> return (TestsR l, Just "")
                Just (TestSetR l t) -> return (TestSetR l t, Just "")
                Just (TestSetsR l) -> return (TestSetsR l, Just "")
                Just (FactoriesR l) -> return (FactoriesR l, Just "")
                Just (FactoriesViewR l v) -> return (FactoriesViewR l v, Just "")
                -- otherwise it's not allowed to write to this wiki uri
                _ -> do
                    setMessageI (intercalate "/" page
                        ++ " is not an allowed wiki page.")
                    redirect HomeR

checkAllowed :: Lang -> [Text] -> Handler ()
checkAllowed lang page = do
    _ <- getRouteAndTitle lang page
    return ()

wikiWidget2 :: (Lang -> Route App) -> Lang -> Bool -> [Text] -> Widget
wikiWidget2 changeLang lang setWikiTitle page = do
    (_, mtitle) <- handlerToWidget $ getRouteAndTitle lang page
    let path = intercalate "/" page
    langs <- handlerToWidget languages
    let ulangs = nub (lang : langs)
    l <- handlerToWidget $ runDB $ selectList [WikiPath I.==. path] []
    let sl = mapMaybe (getForLang l) ulangs ++ l
    case sl of
        Entity _ wiki:_ -> do
            let currentLang = wikiLanguage wiki
            prose <- handlerToWidget $ runDB $ get404 $ wikiProse wiki
            when setWikiTitle $ setTitle $ toHtml $ proseTitle prose
            $(widgetFile "wiki")
        _ -> $(widgetFile "newwiki")
  where
    getForLang ls l = find (\(Entity _ w) -> wikiLanguage w == l) ls

findWiki :: Lang -> Text -> Handler (Maybe (WikiId, Wiki, Prose))
findWiki lang path = do
    list <- runDB $ selectList [WikiLanguage I.==. lang, WikiPath I.==. path] []
    case list of
        Entity wikiid wiki:_ -> do
            prose <- runDB $ get404 $ wikiProse wiki
            return $ Just (wikiid, wiki, prose)
        _ -> return Nothing

getWikiEditR :: Lang -> [Text] -> Handler Html
getWikiEditR lang page = do
    _ <- requireAuthId
    checkAllowed lang page
    checkLanguage lang page WikiEditR
    -- check that the page does not yet exist
    let path = intercalate "/" page
    mwiki <- findWiki lang path
    let formProse =
            case mwiki of
                Just (_, wiki, prose) ->
                     FormProse {
                         formProseTitle = proseTitle prose,
                         formProseContent = Textarea $ proseContent prose,
                         formProseParent = Nothing,
                         formProseReference = Just $ wikiProse wiki }
                _ -> FormProse {
                         formProseTitle = path,
                         formProseContent = Textarea "",
                         formProseParent = Nothing,
                         formProseReference = Nothing }
    let widget = proseFormWidget formProse
    defaultLayout $ do
        setTitleI $ case mwiki of
                    Just _ -> MsgModifyAPage
                    _      -> MsgWriteANewPage
        $(widgetFile "widget-wrap")

updateWiki :: MonadIO m => WikiId -> Prose -> Prose -> ReaderT SqlBackend m Bool
updateWiki wikiid oldProse newProse = do
    -- only save if something changed
    let updated = proseTitle oldProse /= proseTitle newProse
         || proseContent oldProse /= proseContent newProse
    when updated $ do
        newProseId <- insert newProse
        update wikiid [WikiProse =. newProseId]
    return updated

postWikiEditR :: Lang -> [Text] -> Handler Html
postWikiEditR lang page = do
    checkLanguage lang page WikiEditR
    (route, _) <- getRouteAndTitle lang page
    let path = intercalate "/" page
    (userid, _, result, widget', enctype, isSave) <- runProseForm
    case result of
        FormSuccess entry ->
            case isSave of
                Just True -> do
                    mwiki <- findWiki lang path
                    case mwiki of
                        Just (wikiid, wiki, _) -> runDB $ do
                            (prose, newProse) <- deriveProse (wikiProse wiki) userid entry
                            updated <- updateWiki wikiid prose newProse
                            when updated $ setMessageI MsgPageWasUpdated
                        _ -> do
                            newProse <- createProse userid entry
                            runDB $ do
                                proseid <- insert newProse
                                insert_ $ Wiki lang path proseid
                                return ()
                    redirect route
                _ -> defaultLayout $ do
                    let widget = wrapProseFormWidget enctype True widget'
                    $(widgetFile "widget-wrap")
        _ -> do
            setMessageI MsgSomethingWentWrong
            redirect $ WikiEditR lang page

postFormatMarkdownR :: Handler Html
postFormatMarkdownR = do
    texts <- rawRequestBody $$ CT.decode CT.utf8 =$ CL.consume
    return $ formatMarkdown $ T.concat texts

wikiRoute :: Wiki -> Route App
wikiRoute wiki = WikiR (wikiLanguage wiki) (tail' path)
  where
    tail' []     = []
    tail' (_:xs) = xs
    path = T.splitOn (T.pack "/") $ wikiPath wiki

-- Show recent changes to wiki pages
getWikiChangesR :: Handler Html
getWikiChangesR = do
    wikis <- runDB $ select $ from $
        \(wiki `InnerJoin` prose `InnerJoin` user) -> do
            on $ prose ^. ProseAuthor ==. user ^. UserId
            on $ wiki ^. WikiProse ==. prose ^. ProseId
            orderBy [desc (prose ^. ProseCreated)]
            offset 0
            limit 100
            return (wiki, prose, user)
    defaultLayout $ do
        setTitle "Wiki Changes"
        $(widgetFile "wikichanges")
