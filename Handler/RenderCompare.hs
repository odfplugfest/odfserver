module Handler.RenderCompare (
    getRenderCompareR,
    getUploadRenderCompareR,
    getRenderCompareRefreshR,
    getUploadRenderCompareRefreshR
) where

import           Data.Time             (diffUTCTime)
import           Database.Esqueleto    hiding (isNothing, update)
import           Import                hiding (Value, groupBy, on, update,
                                        (==.), (>.))
import qualified Import                as I

import qualified Database              as D (signalNewJob, waitForJobs)
import           FileType
import           Handler.Upload        (RenderStatus (..),
                                        checkUploadViewRights, getMeta,
                                        getRenderStatus, odfValidationWidget,
                                        renderIsBusy, renderStatusString,
                                        secsElapsed)
import           PdfRenderer           (getPdfPage)
import           TestResultTableWidget (makeName)

findPDF :: MonadHandler m => FileType -> BlobId -> SoftwareOSId -> [RenderStatus] -> ReaderT SqlBackend m (RenderStatus, [RenderStatus])
findPDF filetype blobid softwareid status = do
    -- find the status of the job in software1
    s <- case find (\s -> rsSoftware s == softwareid) status of
             Just s' -> return s'
             Nothing -> notFound
    -- if it's not created, create it
    newS <- case rsCreated s of
        Nothing -> do
            r <- createPDF filetype blobid softwareid
            return $ case r of
                Just (jobid, created) -> s { rsCreated = Just created,
                                             rsJob = Just jobid,
                                             rsJobsScheduled = 1 + rsJobsScheduled s}
                Nothing -> s
        _ -> return s
    -- return updated list of statuses
    return (newS, map (update newS) status)
  where
    update s1 s2 | rsSoftware s1 == rsSoftware s2 = s2
    update _ s2  = s2

createPDF :: MonadHandler m => FileType -> BlobId -> SoftwareOSId -> ReaderT SqlBackend m (Maybe (JobId, UTCTime))
createPDF filetype blobid softwareid = do
    uploads <- selectList [UploadContent I.==. blobid] []
    user <- case uploads of
                []                -> notFound
                Entity _ upload:_ -> return $ uploadUploader upload
    services <- select $ from $ \service -> do
        where_ (service ^. ServiceName ==. val "convert")
        where_ (service ^. ServiceInputType ==. val filetype)
        where_ (service ^. ServiceOutputType ==. val FileType.PDF)
        return (service ^. ServiceId)
    (now :: UTCTime) <- liftIO getCurrentTime
    case services of
        Value serviceid:_ -> do
            -- insert new job if it does not yet exist
            jobs <- select $ from $ \job -> do
                where_ (job ^. JobUser ==. val user)
                where_ (job ^. JobInput ==. val blobid)
                where_ (job ^. JobSoftware ==. val softwareid)
                where_ (job ^. JobService ==. val serviceid)
                return (job ^. JobId)
            case jobs of
                [] -> do
                    let job = Job {
                            jobUser = user,
                            jobCreated = now,
                            jobInput = blobid,
                            jobSoftware = softwareid,
                            jobService = serviceid,
                            jobStarted = Nothing,
                            jobServiceInstance = Nothing
                        }
                    jobid <- insert job
                    return $ Just (jobid, now)
                _ ->
                    return Nothing
        _ -> return Nothing

getNamedRenderStatus :: MonadIO m => BlobId -> SoftwareOSId -> SoftwareOSId -> ReaderT SqlBackend m [RenderStatus]
getNamedRenderStatus blobid sw1 sw2 = do
    status <- getRenderStatus blobid
    now <- liftIO getCurrentTime
    let recent = filter (aliveDoneOrSpecial now) status
    let ids = map rsSoftware recent
    rows <- select $ from $ \(sos `InnerJoin` version `InnerJoin` software
            `InnerJoin` family `InnerJoin` osversion `InnerJoin` os) -> do
        on $ osversion ^. OSVersionOs ==. os ^. OperatingSystemId
        on $ sos ^. SoftwareOSOs ==. osversion ^. OSVersionId
        on $ software ^. SoftwareSoftwareFamily ==. family ^. SoftwareFamilyId
        on $ version ^. SoftwareVersionSoftware ==. software ^. SoftwareId
        on $ sos ^. SoftwareOSSoftware ==. version ^. SoftwareVersionId
        where_ $ sos ^. SoftwareOSId `in_` valList ids
        return (sos ^. SoftwareOSId,
                family ^. SoftwareFamilyName,
                software ^. SoftwareName,
                version ^. SoftwareVersionName,
                os ^. OperatingSystemName,
                osversion ^. OSVersionName)
    let dict = map (\(Value a, Value b, Value c, Value d, Value e, Value f)
            -> (a, j b c ++ " " ++ d ++ " (" ++ e ++ " " ++ f ++ ")")) rows
    let unsorted = mapMaybe (newName dict) status
    return $ sortOn rsName unsorted
  where
    newName dict s = case lookup (rsSoftware s) dict of
                         Just name -> Just s{rsName = name}
                         _         -> Nothing
    j b c | b == c = b
    j b c = b ++ " " ++ c
    aliveDoneOrSpecial _ status   | rsSoftware status == sw1 = True
    aliveDoneOrSpecial _ status   | rsSoftware status == sw2 = True
    aliveDoneOrSpecial _ status   | rsHasResult status = True
    aliveDoneOrSpecial now status = secsElapsed now (rsLastSeen status) < 120

getUploadRenderCompareR :: UploadId -> SoftwareOSId -> SoftwareOSId -> Int -> Handler Html
getUploadRenderCompareR uploadid software1 software2 page = do
    (upload, blob) <- runDB $ do
        upload <- get404 uploadid
        blob <- get404 $ uploadContent upload
        return (upload, blob)
    checkUploadViewRights upload
    doRenderCompare (UploadRenderCompareR uploadid) (UploadRenderCompareRefreshR uploadid) (uploadFilename upload) (Just uploadid) (uploadContent upload) blob software1 software2 page

waitForResults :: BlobId -> Blob -> SoftwareOSId -> SoftwareOSId -> Maybe Int -> Int -> Handler ()
waitForResults blobid blob software1 software2 mnumReady waitMicrosecs = do
    (sw1, sw2, _) <- getPdfStatus blobid blob software1 software2
    let numReady = case (renderIsBusy sw1, renderIsBusy sw2) of
                       (False, False) -> 2
                       (True, True)   -> 0
                       _              -> 1
    let nr = fromMaybe numReady mnumReady
    when (nr == numReady && nr < 2) $ do
        now <- liftIO getCurrentTime
        App{appDatabase} <- getYesod
        _ <- liftIO $ D.waitForJobs appDatabase 1000000
        now2 <- liftIO getCurrentTime
        let d = 1000000 * diffUTCTime now2 now
        let waitLeft = waitMicrosecs - floor d
        when (waitLeft > 0) $
            waitForResults blobid blob software1 software2 (Just nr) waitLeft

getRenderCompareRefreshR :: BlobId -> SoftwareOSId -> SoftwareOSId -> Int -> Handler Html
getRenderCompareRefreshR blobid software1 software2 page = do
    blob <- runDB $ get404 blobid
    waitForResults blobid blob software1 software2 Nothing 50000000
    redirect $ RenderCompareR blobid software1 software2 page

getUploadRenderCompareRefreshR :: UploadId -> SoftwareOSId -> SoftwareOSId -> Int -> Handler Html
getUploadRenderCompareRefreshR uploadid software1 software2 page = do
    Upload{uploadContent} <- runDB $ get404 uploadid
    blob <- runDB $ get404 uploadContent
    waitForResults uploadContent blob software1 software2 Nothing 50000000
    redirect $ UploadRenderCompareR uploadid software1 software2 page

getRenderCompareR :: BlobId -> SoftwareOSId -> SoftwareOSId -> Int -> Handler Html
getRenderCompareR blobid software1 software2 page = do
    blob <- runDB $ get404 blobid
    let filename = pack $ makeName blobid (blobFileType blob)
    doRenderCompare (RenderCompareR blobid) (RenderCompareRefreshR blobid) filename Nothing blobid blob software1 software2 page

-- figure out if the pdf files are ready
getPdfStatus :: BlobId -> Blob -> SoftwareOSId -> SoftwareOSId -> Handler (RenderStatus, RenderStatus, [RenderStatus])
getPdfStatus blobid blob software1 software2 = runDB $ do
    let filetype = blobFileType blob
    status <- getNamedRenderStatus blobid software1 software2
    -- start jobs if needed
    (sw1, status1) <- findPDF filetype blobid software1 status
    (sw2, status2) <- findPDF filetype blobid software2 status1
    return (sw1, sw2, status2)

type CR = SoftwareOSId -> SoftwareOSId -> Int -> Route App

doRenderCompare :: CR -> CR -> Text -> Maybe UploadId -> BlobId -> Blob -> SoftwareOSId -> SoftwareOSId -> Int -> Handler Html
doRenderCompare route refresh filename muploadid blobid blob software1 software2 page = do
    (sw1, sw2, status) <- getPdfStatus blobid blob software1 software2
    -- if pdf is not ready yet, enable autoreload
    when (renderIsBusy sw1 || renderIsBusy sw2) $ do
        App{appDatabase} <- getYesod
        liftIO $ D.signalNewJob appDatabase
        let (url, _) = renderRoute $ refresh software1 software2 page
        addHeader "Refresh" ("1; url=/" ++ intercalate "/" url)
    doRenderCompareDone route filename muploadid blobid blob sw1 sw2 page status

getPage :: MonadIO m => RenderStatus -> Int -> ReaderT SqlBackend m (Maybe Int, Maybe Render)
getPage status page = case rsPdf status of
    Nothing -> return (Nothing, Nothing)
    Just pdf -> do
        (pages, mrender) <- getPdfPage pdf page
        return (Just pages, mrender)

doRenderCompareDone :: CR -> Text -> Maybe UploadId -> BlobId -> Blob -> RenderStatus -> RenderStatus -> Int -> [RenderStatus] -> Handler Html
doRenderCompareDone route filename muploadid blobid blob s1 s2 page status = do
    (mpages1, mpages2, mpng1, mpng2) <- runDB $ do
        (mpages1, mpng1) <- getPage s1 page
        (mpages2, mpng2) <- getPage s2 page
        return (mpages1, mpages2, mpng1, mpng2)
    let meta = getMeta blob
    let software1 = rsSoftware s1
    let software2 = rsSoftware s2
    let pages = case (mpages1, mpages2) of
                    (Just pages1, Just pages2) -> max pages1 pages2
                    (Just pages1, _)           -> pages1
                    (_, Just pages2)           -> pages2
                    _                          -> 0
    when (pages < page && pages > 0) $
        redirect $ RenderCompareR blobid software1 software2 pages
    let lastPage = page == pages
    let firstPage = page == 1
    statusTexts <- mapM renderStatusString status
    let statusMsgs = zip (map rsSoftware status) statusTexts
    lang <- getLanguage
    validationWidget <- odfValidationWidget lang blobid
    defaultLayout $ do
        setTitle $ toHtml filename
        $(widgetFile "rendercompare")
  where img pngid = BlobDownloadR pngid (pack $ makeName pngid FileType.PNG)
        pdf' pdfid = BlobDownloadR pdfid (pack $ makeName pdfid FileType.PDF)
        mm :: Double -> Int
        mm d = round (d * 3.175 / 9)
        smm :: Double -> Text
        smm d = pack $ show $ mm d
        cmp_mm w rw h rh = abs (mm w - rw) < 2 && abs (mm h - rh) < 2
        ratio w h rw rh = abs (w / h * rh / rw - 1) < 0.01
        size :: Double -> Double -> Text
        size w h | cmp_mm w 105 h 148 = "A6 Portrait" ++ s w h
        size w h | cmp_mm w 148 h 105 = "A6 Landscape" ++ s w h
        size w h | cmp_mm w 148 h 210 = "A5 Portrait" ++ s w h
        size w h | cmp_mm w 210 h 148 = "A5 Landscape" ++ s w h
        size w h | cmp_mm w 210 h 297 = "A4 Portrait" ++ s w h
        size w h | cmp_mm w 297 h 210 = "A4 Landscape" ++ s w h
        size w h | cmp_mm w 297 h 420 = "A3 Portrait" ++ s w h
        size w h | cmp_mm w 420 h 297 = "A3 Landscape" ++ s w h
        size w h | cmp_mm w 216 h 279 = "US Letter Portrait" ++ s w h
        size w h | cmp_mm w 279 h 216 = "US Letter Landscape" ++ s w h
        size w h | cmp_mm w 216 h 356 = "US Legal Portrait" ++ s w h
        size w h | cmp_mm w 356 h 216 = "US Legal Landscape" ++ s w h
        size w h | cmp_mm w 280 h 210 = "Screen 4:3" ++ s w h
        size w h | cmp_mm w 280 h 158 = "Screen 16:9" ++ s w h
        size w h | cmp_mm w 280 h 175 = "Screen 16:10" ++ s w h
        size w h | ratio w h 4 3 = "4 : 3" ++ s w h
        size w h | ratio w h 16 9 = "16 : 9" ++ s w h
        size w h | ratio w h 16 10 = "16 : 10" ++ s w h
        size w h = sizemm w h
        sizemm w h = smm w ++ " mm x " ++ smm h ++ " mm"
        s :: Double -> Double -> Text
        s w h = " (" ++ sizemm w h ++ ")"
