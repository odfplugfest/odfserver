module Handler.ValidationErrors (
    getValidationErrorsR
) where

import           Database.Esqueleto
import           Import             hiding (Value, groupBy, on, (==.))

getValidationErrorsR :: Handler Html
getValidationErrorsR = do
    rows <- runDB $ select $ from $ \(validation `InnerJoin` valjobresult
                                     `InnerJoin` valjob
                                     `InnerJoin` autotest) -> do
        on $ valjob ^. JobInput ==. autotest ^. AutoTestInputFile
        on $ valjobresult ^. JobResultJob ==. valjob ^. JobId
        on $ validation ^. ValidationJob ==. valjobresult ^. JobResultId
        groupBy (validation ^. ValidationError)
        let (countRows' :: SqlExpr (Value Int)) = countRows
        orderBy [desc countRows']
        return (
            countRows',
            validation ^. ValidationError,
            max_ (autotest ^. AutoTestTest))
    let errors = mapMaybe unrow rows
    defaultLayout $ do
        setTitleI ("Validation Errors" :: Text)
        $(widgetFile "validationerrors")
  where
    unrow (Value a, Value b, Value (Just c)) = Just (a, b, c)
    unrow _                                  = Nothing
