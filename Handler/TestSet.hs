module Handler.TestSet (
    getTestSetsR,
    getTestSetR
) where

import           Data.List          (nub)
import qualified Data.Map           as M
import           Database.Esqueleto hiding (count, delete, update, (=.))
import           Import             hiding (Value, count, groupBy, isNothing,
                                     on, (/=.), (==.), (||.))

import           FileType
import           Handler.Wiki       (wikiWidget)
import           TimeUtil

getTestSetRows :: MonadIO m => ReaderT SqlBackend m [(Entity TestSet, Entity User, Value Int)]
getTestSetRows = do
    rows <- select $ from $ \(testset `InnerJoin` user `InnerJoin` setmember) -> do
        on $ testset ^. TestSetId ==. setmember ^. TestSetMemberSet
        on $ testset ^. TestSetAuthor ==. user ^. UserId
        groupBy (testset ^. TestSetId, user ^. UserId)
        let (countRows' :: SqlExpr (Value Int)) = countRows
        return (testset, user, countRows')
    return rows

getTestSetsR :: Lang -> Handler Html
getTestSetsR lang = do
    testsets <- runDB $ getTestSetRows
    defaultLayout $ do
        $(widgetFile "testsets")
        wikiWidget TestSetsR lang

getTestResults :: MonadIO m => TestSetId -> ReaderT SqlBackend m (Map TestId TestRow)
getTestResults testsetid = do
    rows <- select $ from $ \(testset `InnerJoin` setmember `InnerJoin` test
            `LeftOuterJoin` result `LeftOuterJoin` aresult
            `LeftOuterJoin` jobresult `LeftOuterJoin` job
            `LeftOuterJoin` service `LeftOuterJoin` testver) -> do
        on $ result ?. TestResultId ==. testver ?. TestVerificationTestResult
        on $ job ?. JobService ==. service ?. ServiceId
        on $ jobresult ?. JobResultJob ==. job ?. JobId
        on $ aresult ?. AutoTestResultJob ==. jobresult ?. JobResultId
        on $ result ?. TestResultId ==. aresult ?. AutoTestResultResult
        on $ just (test ^. TestId) ==. result ?. TestResultTest
        on $ setmember ^. TestSetMemberTest ==. test ^. TestId
        on $ testset ^. TestSetId ==. setmember ^. TestSetMemberSet
        where_ $ testset ^. TestSetId ==. val testsetid
        let (pass :: SqlExpr (Value Int)) =
                case_ [ when_ (result ?. TestResultPass ==. (val $ Just True))
                        then_ $ val 1]
                      (else_ $ val 0)
            (tested :: SqlExpr (Value Int)) =
                case_ [ when_ (not_ $ isNothing $ result ?. TestResultPass)
                        then_ $ val 1]
                      (else_ $ val 0)
            (correct :: SqlExpr (Value Int)) =
                case_ [ when_ (testver ?. TestVerificationCorrect ==. (val $ Just True))
                        then_ $ val 1]
                      (else_ $ val 0)
            (verified :: SqlExpr (Value Int)) =
                case_ [ when_ (not_ $ isNothing $ testver ?. TestVerificationCorrect)
                        then_ $ val 1]
                      (else_ $ val 0)
        groupBy (test ^. TestId,
                 result ?. TestResultId,
                 service ?. ServiceOutputType,
                 testver ?. TestVerificationId)
        return (test,
                result,
                service ?. ServiceOutputType,
                sum_ pass,
                sum_ tested,
                sum_ correct,
                sum_ verified)
    return $ foldr groupRows M.empty rows
  where
    groupRows (Entity testid test, Just (Entity _ result), Value (Just output),
            Value mpass, Value mtested, Value mcorrect, Value mverified) =
        M.insertWith' insertRow testid $ TestRow (testName test)
            $ M.fromList [((testResultSoftware result, output),
                            col mpass mtested mcorrect mverified)]
    groupRows (Entity testid test,_,_,_,_,_,_) =
        M.insertWith' insertRow testid $ TestRow (testName test) M.empty
    col mpass mtested mcorrect mverified =
        Col  (m mpass) (m mtested) (m mcorrect) (m mverified)
    m (Just i) = i
    m Nothing  = 0
    insertRow (TestRow name c1) (TestRow _ c2) =
        TestRow name $ M.unionWith combine c1 c2
    combine (Col p1 t1 c1 v1) (Col p2 t2 c2 v2) =
        Col (p1 + p2) (t1 + t2) (c1 + c2) (v1 + v2)

data TestRow = TestRow {
    testRowName    :: Text,
    testRowColumns :: Map (SoftwareOSId, FileType) Col
} deriving (Eq)

data Col = Col {
    colGood     :: Int,
    colTested   :: Int,
    colCorrect  :: Int,
    colVerified :: Int
} deriving (Eq)

getColNames :: MonadIO m => [(SoftwareOSId, FileType)] -> ReaderT SqlBackend m (Map (SoftwareOSId, FileType) Text)
getColNames cols = do
    let sw = map fst cols
    rows <- select $ from $ \(sos `InnerJoin` version `InnerJoin` software
                     `InnerJoin` family `InnerJoin` platform
                     `InnerJoin` osversion `InnerJoin` os) -> do
        on $ osversion ^. OSVersionOs ==. os ^. OperatingSystemId
        on $ sos ^. SoftwareOSOs ==. osversion ^. OSVersionId
        on $ sos ^. SoftwareOSPlatform ==. platform ^. PlatformId
        on $ software ^. SoftwareSoftwareFamily ==. family ^. SoftwareFamilyId
        on $ version ^. SoftwareVersionSoftware ==. software ^. SoftwareId
        on $ sos ^. SoftwareOSSoftware ==. version ^. SoftwareVersionId
        where_ $ sos ^. SoftwareOSId `in_` valList sw
        groupBy (sos ^. SoftwareOSId, software ^. SoftwareId,
                 version ^. SoftwareVersionId, os ^. OperatingSystemId,
                 osversion ^. OSVersionId)
        orderBy [asc (software ^. SoftwareName),
                 asc (version ^. SoftwareVersionName),
                 asc (os ^. OperatingSystemName),
                 asc (osversion ^. OSVersionName)]
        return (sos ^. SoftwareOSId,
                software ^. SoftwareName,
                version ^. SoftwareVersionName,
                os ^. OperatingSystemName,
                osversion ^. OSVersionName)
    let rows' = M.fromList $ map (\(Value sos, Value s1, Value s2, Value s3, Value s4) ->
            (sos, unwords [s1, s2, s3, s4])) rows
    return $ foldr (\(s, t) m -> M.insert (s, t) (case M.lookup s rows' of
                                      Just v  -> v ++ " " ++ showNice t
                                      Nothing -> "") m) M.empty cols

getTotals :: Map TestId TestRow -> Map (SoftwareOSId, FileType) (Int, Int)
getTotals = M.foldr (\(TestRow _ cols) m -> M.foldrWithKey addCol m cols) M.empty
  where
    addCol k (Col pass total _ _) m = M.insert k (nv (M.lookup k m) pass total) m
    nv (Just (p1, t1)) p2 t2 = (p1 + p2, t1 + t2)
    nv _ p t                 = (p, t)

testSetTable :: Lang -> Bool -> Bool -> Map (SoftwareOSId, FileType) Text -> Map TestId TestRow -> Widget
testSetTable lang verified correct colnames rows = do
    let filteredCols = map filterCols rows
        filteredRows = M.filter (\(TestRow _ c) -> not $ null c) filteredCols
        totals = getTotals filteredRows
    let columns = nub $ concatMap (\(TestRow _ cols) -> keys cols) filteredRows
    $(widgetFile "testsettable")
  where
    filterCols (TestRow n c) = TestRow n $ M.filter filterCol c
    filterCol (Col _ _ _ v) | not verified = v == 0
    filterCol (Col _ _ c v) | verified && correct  = v > 0 && c == v
    filterCol (Col _ _ c v) = v > 0 && c /= v

getTestSetR :: Lang -> TestSetId -> Handler Html
getTestSetR lang testsetid = do
    testset <- runDB $ get404 testsetid
    rows <- runDB $ getTestResults testsetid
    let columns = nub $ concatMap (\(TestRow _ cols) -> keys cols) rows
    colnames <- runDB $ getColNames columns
    defaultLayout $ do
        setTitle $ toHtml $ "Test set " ++ testSetName testset
        $(widgetFile "testset")
        wikiWidget (`TestSetR` testsetid) lang
