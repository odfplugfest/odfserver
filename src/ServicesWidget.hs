module ServicesWidget (
    getServicesWidget
) where

import           Data.List          (nubBy)
import           Database.Esqueleto
import           Import             hiding (Asc, Desc, Value, filter,
                                     formatTime, groupBy, isNothing, on, (==.),
                                     (||.))
import qualified Import             as I (on)

import           FileType           (showNice)
import           TableColumns
import           TableWidgetUtils

-- keep only the first sort instruction per column
cleanOrder :: [ServicesWidgetSort] -> [ServicesWidgetSort]
cleanOrder = nubBy ((==) `I.on` servicesSortCol)

getServices :: MonadIO m => ServicesView -> SqlPersistT m [(Entity Service, Value Int, Value Int)]
getServices fv =
    select $ from $ \(service `InnerJoin` instance') -> do
        on (service ^. ServiceId ==. instance' ^. ServiceInstanceService)
        mapM_ (addFilter service) $ servicesViewFilter fv
        orderBy $ concatMap (toOrder service) $ servicesViewSort fv
        groupBy (service ^. ServiceId)
        offset $ fromIntegral $ servicesViewOffset fv
        limit $ fromIntegral $ servicesViewLimit fv
        return (service,countRows,countDistinct $ instance' ^. ServiceInstanceFactory)
  where
    toOrder service (ServicesWidgetSort ServicesWidgetName a) =
        [s a $ service ^. ServiceName]
    toOrder service (ServicesWidgetSort ServicesWidgetInput a) =
        [s a $ service ^. ServiceInputType]
    toOrder service (ServicesWidgetSort ServicesWidgetOutput a) =
        [s a $ service ^. ServiceOutputType]
    s a f = case a of
                SortAsc  -> asc f
                SortDesc -> desc f
    addFilter service (ServicesWidgetFilterName name) =
        where_ $ service ^. ServiceName ==. val name

getServicesWidget :: Bool -> ServicesView -> Handler Widget
getServicesWidget navLinks servicesview = getServicesWidget' navLinks servicesview'
  where
    order = cleanOrder $ servicesViewSort servicesview
                         ++ [ServicesWidgetSort ServicesWidgetName SortDesc,
                            ServicesWidgetSort ServicesWidgetInput SortDesc,
                            ServicesWidgetSort ServicesWidgetOutput SortDesc]
    servicesview' = servicesview {servicesViewSort = order}

getServicesWidget' :: Bool -> ServicesView -> Handler Widget
getServicesWidget' navLinks servicesview = do
    rows' <- runDB $ getServices servicesview
    let rows = zip [start + 1 ..] rows'
        mPrevPage = if start > 0
                    then Just $ changeOffset (max 0 $ start - size)
                    else Nothing
        mNextPage = if size == length rows
                    then Just $ changeOffset (start + size)
                    else Nothing
    return $(widgetFile "serviceswidget")
  where
    start = servicesViewOffset servicesview
    size = servicesViewLimit servicesview
    changeOffset o = ServicesR $ servicesview { servicesViewOffset = o }
    condensed = not navLinks
    order = servicesViewSort servicesview
    sortButton c = makeSortWidget (tor c SortAsc) (tor c SortDesc)
    filterService serviceid = FactoriesViewR "en" $ FactoriesView 0 100 [] [] [FactoriesWidgetFilterService serviceid]
    filterName name = ServicesR $ servicesview {
            servicesViewFilter = [ServicesWidgetFilterName name]}
    tor c d = if o == order then Nothing else Just $ ServicesR $ servicesview {servicesViewSort = o}
      where o = cleanOrder (ServicesWidgetSort c d : order)
