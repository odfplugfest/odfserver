module UploadJobResult where

import           ClassyPrelude.Yesod
import           Data.Aeson.TH        (defaultOptions, deriveJSON)
import           Data.ByteString.Lazy as L (ByteString)
import           Types.ByteString     ()

import           CommonTypes          as CT

data JobError = None | ServiceNotAvailable | SoftwareTimeout | SoftwareCrash
              | SoftwareError
      deriving (Eq)
$(deriveJSON defaultOptions ''JobError)

data UploadJobResult = UploadJobResult {
    factory     :: CT.FactoryKey,
    number      :: Int,
    started     :: UTCTime,
    finished    :: UTCTime,
    commandLine :: Text,
    outputFiles :: [L.ByteString],
    error       :: JobError,
    exitCode    :: Int,
    stdout      :: Text,
    stderr      :: Text
}
$(deriveJSON defaultOptions ''UploadJobResult)

data UploadJobResultResponse = UploadJobResultResponse {
}
$(deriveJSON defaultOptions ''UploadJobResultResponse)
