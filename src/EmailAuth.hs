{-# LANGUAGE QuasiQuotes #-}
module EmailAuth (
    sendVerifyEmail
) where

import           ClassyPrelude.Yesod
import           Network.Mail.Mime
import           Text.Blaze                    (ToMarkup)
import           Text.Blaze.Html.Renderer.Utf8 (renderHtml)
import           Text.Shakespeare.Text         (ToText, stext)

-- e.g. Thu, 18 Aug 2016 00:05:08 +0200
getMailDate :: IO Text
getMailDate = do
    now <- getCurrentTime
    let format = "%a, %e %b %Y %H:%M:%S %z"
    return $ pack $ formatTime defaultTimeLocale format now

sendVerifyEmail :: (MonadIO m, ToMarkup a, ToText a) => Text -> t -> a -> m ()
sendVerifyEmail email _ verurl =
    liftIO $ do
        date <- getMailDate
        renderSendMail (emptyMail $ Address Nothing "noreply")
            { mailTo = [Address Nothing email]
            , mailHeaders =
                [ ("From", "ODF Community Server <noreply@beta.opendocumentformat.org>")
                , ("Subject", "ODF Community Server: Verify your email address")
                , ("Date", date)
                ]
            , mailParts = [[textPart, htmlPart']]
            }
  where
    textPart = Part
        { partType = "text/plain; charset=utf-8"
        , partEncoding = None
        , partFilename = Nothing
        , partContent = encodeUtf8
            [stext|
                Thank you for registering on the ODF community website.

                Please confirm your email address by clicking on the link below.

                #{verurl}

                Cookies must be enabled!

                Thank you
            |]
        , partHeaders = []
        }
    htmlPart' = Part
        { partType = "text/html; charset=utf-8"
        , partEncoding = None
        , partFilename = Nothing
        , partContent = renderHtml
            [shamlet|
                <p>Thank you for registering on the ODF community website.

                <p>Please confirm your email address by clicking on the link below.
                <p>
                    <a href=#{verurl}>#{verurl}
                <p>Cookies must be enabled!
                <p>Thank you
            |]
        , partHeaders = []
        }

