module TimeUtil (formatRelativeTime, formatDate, formatTime, recent) where

import           Data.Time.Clock (diffUTCTime)
import           Import          hiding (formatTime)
import qualified Import          as I (formatTime)

formatDate :: UTCTime -> Text
formatDate = pack . I.formatTime defaultTimeLocale "%Y-%m-%d"

formatTime :: UTCTime -> Text
formatTime = pack . I.formatTime defaultTimeLocale "%Y-%m-%d %H:%M"

formatRelativeTime :: UTCTime -> Handler Text
formatRelativeTime time = do
    now <- liftIO getCurrentTime
    r <- getMessageRender
    let duration = diffUTCTime now time
        seconds = fromIntegral (round duration :: Int)
    return $ r $ formatDuration seconds

minute :: Num a => a
minute = 60
hour :: Num a => a
hour = 60 * minute
day :: Num a => a
day = 24 * hour

formatDuration :: Int -> AppMessage
formatDuration d | d < minute = MsgTimeSecondsAgo d
formatDuration d | d < hour = MsgTimeMinutesAgo $ d `div` minute
formatDuration d | d < day = MsgTimeHoursAgo $ d `div` hour
formatDuration d = MsgTimeDaysAgo $ d `div` day

-- check that time difference is less than 3 minutes
recent :: UTCTime -> UTCTime -> Bool
recent now lastSeen = (diffUTCTime now lastSeen) < 180

