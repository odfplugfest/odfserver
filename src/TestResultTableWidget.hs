module TestResultTableWidget (
    blobDownloadLink,
    getTestResultTableWidget,
    getValidation,
    makeName
) where

import           Data.List          (nub)
import qualified Data.Map           as M
import           Data.Maybe         (fromJust)
import           Database.Esqueleto
import           Import             hiding (Value, groupBy, isNothing, on,
                                     (==.), (||.))

import           FileType

makeName :: ToBackendKey SqlBackend record =>  Key record -> FileType -> String
makeName blobid filetype = show (fromSqlKey blobid) ++ "."
        ++ unpack (showExt filetype)

blobDownloadLink :: FileType -> BlobId -> Route App
blobDownloadLink filetype blobid =
        BlobDownloadR blobid $ pack $ makeName blobid filetype

-- If a no errors were found, the blob is associated with [].
-- If no validation was run, Nothing is returned
getValidation :: MonadIO m => BlobId -> SqlPersistT m (Maybe (JobId,Maybe [Validation]))
getValidation blob = do
    rows <- select $ from $ \(job `InnerJoin` service `LeftOuterJoin` result
                            `LeftOuterJoin` validation) -> do
        on $ result ?. JobResultId ==. validation ?. ValidationJob
        on $ just (job ^. JobId) ==. result ?. JobResultJob
        on $ job ^. JobService ==. service ^. ServiceId
        where_ $ job ^. JobInput ==. val blob
        where_ $ service ^. ServiceOutputType ==. val VALIDATIONREPORT
        groupBy (job ^. JobInput,
                 validation ?. ValidationComponent,
                 validation ?. ValidationLine,
                 validation ?. ValidationColumn,
                 validation ?. ValidationError)
        return (job ^. JobInput,
                min_ $ job ^. JobId,
                min_ $ joinV $ result ?. JobResultResult,
                joinV $ min_ $ validation ?. ValidationJob,
                joinV $ validation ?. ValidationComponent,
                joinV $ validation ?. ValidationLine,
                joinV $ validation ?. ValidationColumn,
                validation ?. ValidationError)
    return $ case rows of
        -- no validation was scheduled
        [] -> Nothing
        -- handle impossible match
        ((_,Value Nothing,_,_,_,_,_,_):_) -> Nothing
        -- no validation finished
        ((_,Value (Just job),Value Nothing,_,_,_,_,_):_) -> Just (job, Nothing)
        -- one or more validations finished
        ((_,Value (Just job),_,_,_,_,_,_):_) -> Just (job, Just $ collect rows)
  where
    collect = mapMaybe rc
    rc (_,_,_,Value (Just j),Value a, Value b, Value c, Value (Just d))
        = Just $ Validation j a b c d
    rc _ = Nothing

-- | get validation messages for each blob
getValidationResults :: MonadIO m => [BlobId] -> SqlPersistT m [(BlobId,ValidationResult)]
getValidationResults blobs = do
    -- get the list of all jobs for which a validation job ran successfully
    rows <- select $ from $ \(job `InnerJoin` service `LeftOuterJoin` result
              `LeftOuterJoin` validation) -> do
        on $ result ?. JobResultId ==. validation ?. ValidationJob
        on $ just (job ^. JobId) ==. result ?. JobResultJob
        on $ job ^. JobService ==. service ^. ServiceId
        where_ $ job ^. JobInput `in_` valList blobs
        where_ $ service ^. ServiceOutputType ==. val VALIDATIONREPORT
        groupBy (job ^. JobId, result ?. JobResultResult)
        let (err :: SqlExpr (Value Int)) =
                case_ [ when_ (isNothing $ validation ?. ValidationId)
                        then_ $ val 0]
                      (else_ $ val 1)
        return (job ^. JobInput,
                job ^. JobId,
                joinV $ result ?. JobResultResult,
                sum_ err)
    return $ map v rows
  where
    v (Value blob, _, Value (Just _), Value (Just n)) = (blob,Finished n blob)
    v (Value blob, _, Value (Just _), _)              = (blob,Finished 0 blob)
    v (Value blob, Value job, _, _)                   = (blob,NotFinished job)

getVerification :: MonadIO m => Maybe UserId -> [TestResultId] -> SqlPersistT m (Map TestResultId (Int,Int,Maybe Bool))
getVerification muid rids = do
    rows <- select $ from $ \ver -> do
        where_ $ ver ^. TestVerificationTestResult `in_` valList rids
        return ver
    return $ foldr countVers M.empty rows
  where
    countVers (Entity _ (TestVerification rid u c)) =
        M.insertWith' insertVer rid (newVer u c)
    insertVer (a, b, Just c) (d, e, _)  = (a + d, b + e, Just c)
    insertVer (a, b, Nothing) (d, e, f) = (a + d, b + e, f)
    newVer user True  | Just user == muid = (1, 0, Just True)
    newVer user False | Just user == muid = (0, 1, Just False)
    newVer _ True     = (1, 0, Nothing)
    newVer _ False    = (0, 1, Nothing)

getOutputs :: MonadIO m => Maybe UserId -> TestId -> SqlPersistT m [Output]
getOutputs muid testid = do
    rows <- select $ from $ \(autotest `InnerJoin` otype
                     `InnerJoin` service `InnerJoin` instance'
                     `LeftOuterJoin` failed `InnerJoin` job
                     `InnerJoin` sos `InnerJoin` version `InnerJoin` software
                     `InnerJoin` family `InnerJoin` platform
                     `InnerJoin` osversion `InnerJoin` os
                     `LeftOuterJoin` jobresult `LeftOuterJoin` aresult
                     `LeftOuterJoin` result
                     `LeftOuterJoin` pdfjob `LeftOuterJoin` pdf) -> do
        on $ pdfjob ?. JobId ==. pdf ?. JobResultJob
        on $ jobresult ?. JobResultResult ==. just (pdfjob ?. JobInput)
           &&. result ?. TestResultSoftware ==. pdfjob ?. JobSoftware
        on $ aresult ?. AutoTestResultResult ==. result ?. TestResultId
        on $ jobresult ?. JobResultId  ==. aresult ?. AutoTestResultJob
        on $ just (job ^. JobId) ==. jobresult ?. JobResultJob
        on $ osversion ^. OSVersionOs ==. os ^. OperatingSystemId
        on $ sos ^. SoftwareOSOs ==. osversion ^. OSVersionId
        on $ sos ^. SoftwareOSPlatform ==. platform ^. PlatformId
        on $ software ^. SoftwareSoftwareFamily ==. family ^. SoftwareFamilyId
        on $ version ^. SoftwareVersionSoftware ==. software ^. SoftwareId
        on $ sos ^. SoftwareOSSoftware ==. version ^. SoftwareVersionId
        on $ instance' ^. ServiceInstanceSoftware ==. sos ^. SoftwareOSId
        -- find jobs via ServiceInstanceId or FailedJobJob
        on $ service ^. ServiceId ==. job ^. JobService
            &&. autotest ^. AutoTestInputFile ==. job ^. JobInput
            &&. (just (instance' ^. ServiceInstanceId) ==. job ^. JobServiceInstance
                 ||. failed ?. FailedJobJob ==. just (job ^. JobId))
        on $ just (instance' ^. ServiceInstanceId) ==. failed ?. FailedJobServiceInstance
        on $ service ^. ServiceId ==. instance' ^. ServiceInstanceService
        on $ otype ^. AutoTestOutputTypeFileType ==. service ^. ServiceOutputType
        on $ autotest ^. AutoTestId ==. otype ^. AutoTestOutputTypeTest
        where_ $ autotest ^. AutoTestTest ==. val testid
        groupBy (software ^. SoftwareId, version ^. SoftwareVersionId,
                 os ^. OperatingSystemId, osversion ^. OSVersionId,
                 service ^. ServiceId,
                 result ?. TestResultId,
                 job ^. JobId)
        orderBy [asc (software ^. SoftwareName),
                 asc (version ^. SoftwareVersionName),
                 asc (os ^. OperatingSystemName),
                 asc (osversion ^. OSVersionName),
                 asc (service ^. ServiceId)]
        return (software, version, os, osversion, service, result,
                job ^. JobId,
                joinV $ min_ $ joinV $ jobresult ?. JobResultResult,
                joinV $ min_ $ joinV $ pdf ?. JobResultResult)
    let outputs = map createOutput rows
        outputs' = mapMaybe outputSub outputs
        rids = map outputResult outputs'
    vers <- getVerification muid rids
    let outputs'' = map (f vers) outputs
    addPngs outputs''
  where
    f vers o = case outputSub o of
                   Nothing -> o
                   Just o' -> case M.lookup (outputResult o') vers of
                                  Just s  -> o {outputSub = Just $ o' {outputVerification = s}}
                                  Nothing -> o

addPngs :: MonadIO m => [Output] -> SqlPersistT m [Output]
addPngs o = do
    let os = mapMaybe outputSub o
    rows <- select $ from $ \render -> do
        where_ $ just (render ^. RenderInput) `in_` valList (map outputPdf os)
        orderBy [asc (render ^. RenderInput),
                 asc (render ^. RenderPageNumber)]
        return render
    return $ map (k $ map entityVal rows) o
  where
    k :: [Render] -> Output -> Output
    k pngs out = case outputSub out of
                     Nothing   -> out
                     Just out' -> out {
                         outputSub = Just $ out' {outputPng = opngs out'}
                     }
      where
        opngs out' = q $ filter ((== outputPdf out') . Just . renderInput) pngs
        q = map (renderThumbnail &&& renderPage)

getXPathResults :: MonadIO m => TestId -> [BlobId] -> SqlPersistT m [(BlobId,AutoTestXPath,Bool)]
getXPathResults testid blobs = do
    rows <- select $ from $ \(result `InnerJoin` xpath `InnerJoin` use
                             `InnerJoin` autotest) -> do
        on $ use ^. AutoTestXPathUseTest ==. autotest ^. AutoTestId
        on $ result ^. XPathResultXpath ==. use ^. AutoTestXPathUseXpath
        on $ result ^. XPathResultXpath ==. xpath ^. AutoTestXPathId
        where_ $ result ^. XPathResultBlob `in_` valList blobs
        where_ $ autotest ^. AutoTestTest ==. val testid
        groupBy (result ^. XPathResultBlob, xpath ^. AutoTestXPathId,
                 result ^. XPathResultResult)
        orderBy [asc (result ^. XPathResultBlob)]
        return ( result ^. XPathResultBlob
               , xpath
               , result ^. XPathResultResult)
    return $ map (\(Value a, Entity _ b, Value c) -> (a, b, c)) rows

createOutput :: (Entity Software,Entity SoftwareVersion,Entity OperatingSystem,Entity OSVersion,Entity Service,Maybe (Entity TestResult),Value JobId,Value (Maybe BlobId),Value (Maybe BlobId)) -> Output
createOutput (Entity _ software, Entity _ version, Entity _ os, Entity _ osv, Entity _ service, mresult, jobid, modf, mpdf) = Output {
    outputName = unwords [softwareName software,
                          softwareVersionName version,
                          operatingSystemName os,
                          oSVersionName osv],
    outputJob = unValue jobid,
    outputType = serviceOutputType service,
    outputSub = case (unValue modf, mresult) of
            (Just odf, Just (Entity resultid result))
                -> Just $ OutputSub {
                        outputResult = resultid,
                        outputPassed = testResultPass result,
                        outputVerification = (0,0,Nothing),
                        outputValidation = NoValidation,
                        outputFile = odf,
                        outputPdf = unValue mpdf,
                        outputPng = []
                    }
            _ -> Nothing
}

data Input = Input {
    inputValidation :: ValidationResult,
    inputType       :: FileType,
    inputFile       :: BlobId
}

data Output = Output {
    outputName :: Text,
    outputJob  :: JobId,
    outputType :: FileType,
    outputSub  :: Maybe OutputSub
}

data OutputSub = OutputSub {
    outputResult       :: TestResultId,
    outputPassed       :: Bool,
    outputVerification :: (Int,Int,Maybe Bool),
    outputValidation   :: ValidationResult,
    outputPdf          :: Maybe BlobId,
    outputPng          :: [(BlobId,BlobId)], -- first is clip, second is full
    outputFile         :: BlobId
}

data XPathGroup = XPathGroup {
    xpathPath   :: Text,
    groupXPaths :: [AutoTestXPath]
}

group' :: [(a, AutoTestXPath, c)] -> [XPathGroup]
group' l = map (keyList x) keys'
  where
    x = map snd' l
    keys' = nub $ map autoTestXPathPath x
    keyList k a = XPathGroup a $ nub $ filter ((a ==) . autoTestXPathPath) k
    snd' (_,b,_) = b

verificationWidget :: Lang -> OutputSub -> Widget
verificationWidget lang output = do
    let (yes, no, verified) = outputVerification output
        route = ValidateTestR lang (outputResult output)
        enctype = "multipart/form-data" :: String
    $(widgetFile "verificationwidget")

data ValidationResult = NoValidation | NotFinished JobId | Finished Int BlobId

validationWidget :: Lang -> ValidationResult -> Widget
validationWidget lang validation =
    $(widgetFile "validationtablewidget")

xpathCell :: [(BlobId,AutoTestXPath,Bool)] -> BlobId -> AutoTestXPath -> Bool
xpathCell xpaths blobid xpath = all (\(_,_,b) -> b)
    $ filter (\(a,b,_) -> a == blobid && b == xpath) xpaths

getTestResultTableWidget :: Lang -> TestId -> Handler Widget
getTestResultTableWidget lang testid = do
    muid <- maybeAuthId
    (input, software, xpaths) <- runDB $ do
        mautoTest <- getBy $ UniqueTest testid
        let inputBlobId = autoTestInputFile $ entityVal $ fromJust mautoTest
        inputBlob <- get404 inputBlobId
        software <- getOutputs muid testid
        let blobids = inputBlobId : (map outputFile $ mapMaybe outputSub software)
        v <- getValidationResults blobids
        xpaths <- getXPathResults testid blobids
        let input = Input (vali inputBlobId v) (blobFileType inputBlob) inputBlobId
        return (input, map (addValidation v) software, xpaths)
    let xpathGroups = group' xpaths
    return $(widgetFile "testresulttable")
  where
    vali blob v = case lookup blob v of
                      Just v' -> v'
                      Nothing -> NoValidation
    addValidation v s =
        case outputSub s of
            Nothing -> s
            Just f  -> s {
                outputSub = Just $ f {
                    outputValidation = vali (outputFile f) v
                }
            }
    odf' s = case outputSub s of
                 Nothing  -> Nothing
                 Just sub -> Just $ outputFile sub
