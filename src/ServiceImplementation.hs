module ServiceImplementation where

import           Data.ByteString.Lazy (ByteString)
import           Data.Int             (Int)
import           Data.Text            (Text)
import           System.IO            (IO)

import           Service              (ServiceInstance)

data ServiceResult = ServiceResult {
    commandLine :: Text,
    outputFiles :: [ByteString],
    exitCode    :: Int,
    stdout      :: Text,
    stderr      :: Text
}

class ServiceImplementation a where
    getServiceInstance :: a -> ServiceInstance
    runService :: a -> ByteString -> IO ServiceResult
