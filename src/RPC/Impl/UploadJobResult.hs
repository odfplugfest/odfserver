{-# LANGUAGE Arrows #-}

module RPC.Impl.UploadJobResult (uploadjobresult) where

import           ClassyPrelude.Yesod      hiding (error, stderr, stdout)
import qualified Data.ByteString.Char8    as BC
import           Data.ByteString.Lazy     as T (ByteString, toStrict)
import           Data.Maybe               (fromJust)
import           Data.String.Conversions  (cs)
import           Data.Tree.NTree.TypeDefs (NTree)
import           Database.Persist.Sql
import           Text.Read                (read)
import           Text.XML.HXT.Core

import qualified CommonTypes              as CT
import qualified Database                 as D
import           FileType
import           Model
import           RPC.Impl                 (AppM, checkFactoryKey, checkInput,
                                           evaluateAutoTests, runDb)
import qualified UploadJobResult          as UJ

data ValidationReport = ValidationReport { msg       :: String,
                                           component :: Maybe Text,
                                           line, col :: Maybe Int }
  deriving (Show, Eq)

atTag :: ArrowXml a => String -> a (NTree XNode) XmlTree
atTag  tag = deep (isElem >>> hasName tag)
-- atAttr :: ArrowXml cat => String -> cat XmlTree XmlTree
-- atAttr tag = getAttrl >>> deep (hasName tag)
text :: IOSLA (XIOState ()) (NTree XNode) String
text = getChildren >>> getText
getAttrOptional :: String -> IOSLA (XIOState ()) (NTree XNode) (Maybe String)
getAttrOptional aaa =
  getAttrValue aaa >>> arr Just
    `orElse` constA Nothing

stot:: Maybe String -> Maybe Text
stot s = case s of
         Nothing -> Nothing
         Just "" -> Nothing
         Just n  -> Just (cs n)
stoi:: Maybe String -> Maybe Int
stoi s = case s of
         Nothing -> Nothing
         Just "" -> Nothing
         Just n  -> Just (read n)

getValidationReport :: IOSLA (XIOState ()) (NTree XNode) ValidationReport
getValidationReport = atTag "error" >>>
  proc x -> do
    lcomponent <- getAttrOptional "component" -< x
    lline      <- getAttrOptional "start-line" -< x
    lcol       <- getAttrOptional "start-column" -< x
    lmsg       <- text -< x
    returnA   -< ValidationReport { msg = lmsg,
                                    component = stot lcomponent,
                                    line = stoi lline,
                                    col  = stoi lcol
                                  }

-- stot :: Maybe String -> Maybe Text
-- stot x = do
--    case x of
--       Nothing -> return Nothing
--       Just s  -> return Just $ cs s

uploadjobresult :: UJ.UploadJobResult -> AppM UJ.UploadJobResultResponse
uploadjobresult result = do
    let UJ.UploadJobResult {
            factory,
            number,
            started,
            finished
        } = result
    checkInput (finished < started) "Finished time is smaller than started time"
    checkFactoryKey factory
    let (jobid :: JobId) = toSqlKey $ fromIntegral number
    maybeJob <- runDb $ get jobid
    checkInput (isNothing maybeJob) "Invalid job number"
    let maybeInstance = jobServiceInstance $ fromJust maybeJob
    checkInput (isNothing maybeInstance)
        "No service instance associated with this job."
    let instanceId = fromJust maybeInstance
    (Just sinstance) <- runDb $ get instanceId
    (Just factory') <- runDb $ get (serviceInstanceFactory sinstance)
    (Just service) <- runDb $ get (serviceInstanceService sinstance)
    checkInput (factoryUuid factory' /= CT.factoryId factory)
        "This factory is not registered to handle this job."
    let error = if null (UJ.outputFiles result) || UJ.exitCode result /= 0
                    then SoftwareError
                    else mapError $ UJ.error result
    case error of
        None -> storeSuccess result service jobid
        _    -> storeFailure result instanceId jobid error
    return UJ.UploadJobResultResponse

storeSuccess :: UJ.UploadJobResult -> Service -> JobId -> AppM ()
storeSuccess jobResult service jobid = do
    runDb $ do
        blobids <- do
            let fileType = serviceOutputType service
            mapM (`D.addBlob` fileType) $ UJ.outputFiles jobResult
        jobresult <- insert JobResult {
            jobResultJob = jobid,
            jobResultFinished = UJ.finished jobResult,
            jobResultResult = listToMaybe blobids,
            jobResultCommandLine = UJ.commandLine jobResult,
            jobResultExitCode = UJ.exitCode jobResult,
            jobResultStdout = UJ.stdout jobResult,
            jobResultStderr = UJ.stderr jobResult}
        D.insertMany_ 3 $ map (\(n,blobid) -> JobResultMany jobresult blobid n)
                $ zip [1..] $ drop 1 blobids
        storeValidationReport service (UJ.outputFiles jobResult) jobresult
    evaluateAutoTests

storeFailure :: UJ.UploadJobResult -> ServiceInstanceId -> JobId -> JobResultError -> AppM ()
storeFailure jobResult service jobid error = runDb $ do
    insert_ FailedJob
        { failedJobJob = jobid
        , failedJobStarted = UJ.started jobResult
        , failedJobFinished = UJ.finished jobResult
        , failedJobServiceInstance = service
        , failedJobCommandLine = UJ.commandLine jobResult
        , failedJobError = error
        , failedJobExitCode = UJ.exitCode jobResult
        , failedJobStdout = UJ.stdout jobResult
        , failedJobStderr = UJ.stderr jobResult
        , failedJobRestarter = Nothing
        }
    -- reset failed job so it can run again
    update jobid [ JobStarted =. Nothing, JobServiceInstance =. Nothing ]

mapError :: UJ.JobError -> JobResultError
mapError UJ.None                = None
mapError UJ.ServiceNotAvailable = ServiceNotAvailable
mapError UJ.SoftwareTimeout     = SoftwareTimeout
mapError UJ.SoftwareCrash       = SoftwareCrash
mapError UJ.SoftwareError       = SoftwareError

storeValidationReport :: MonadIO m => Service -> [T.ByteString] -> JobResultId -> ReaderT SqlBackend m ()
storeValidationReport service outputFiles jobresult = do
    validationReport <- case (serviceOutputType service, outputFiles) of
        (VALIDATIONREPORT, f:_) -> do
             let outputFileAsString = BC.unpack $ T.toStrict f
             liftIO $
                 runX (
                      readString [withValidate no] outputFileAsString
                      >>> getValidationReport )
        _ -> return []
    D.insertMany_ 5 $ map (\x -> Validation {
              validationJob       = jobresult,
              validationComponent = component x,
              validationLine      = line x,
              validationColumn    = col x,
              validationError     = cs (msg x) }) validationReport
