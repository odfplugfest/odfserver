module CommonTypes where

import           Data.Aeson.TH         (defaultOptions, deriveJSON)
import           Data.Text             (Text)
import           Data.Time             (UTCTime)
import           Data.Time.Clock.POSIX (posixSecondsToUTCTime)

data FactoryKey = FactoryKey {
    factoryId  :: Text,
    factoryKey :: Text
}
$(deriveJSON defaultOptions ''FactoryKey)

-- a default value for UTCTime
epochTime :: UTCTime
epochTime = posixSecondsToUTCTime 0
