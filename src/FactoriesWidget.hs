module FactoriesWidget (
    getFactoriesWidget
) where

import           Data.List          (nubBy)
import           Database.Esqueleto
import           Import             hiding (Asc, Desc, Value, filter,
                                     formatTime, groupBy, isNothing, on, (==.),
                                     (||.))
import qualified Import             as I (on)

import           TableColumns
import           TableWidgetUtils
import           TimeUtil

-- keep only the first sort instruction per column
cleanOrder :: [FactoriesWidgetSort] -> [FactoriesWidgetSort]
cleanOrder = nubBy ((==) `I.on` factoriesSortCol)

getFactories :: MonadIO m => FactoriesView -> SqlPersistT m [(Entity Factory, Entity SoftwareVersion, Entity Software)]
getFactories fv =
    select $ from $ \(factory `InnerJoin` instance' `InnerJoin` sos
            `InnerJoin` sv `InnerJoin` software) -> do
        on (sv ^. SoftwareVersionSoftware ==. software ^. SoftwareId)
        on (sos ^. SoftwareOSSoftware ==. sv ^. SoftwareVersionId)
        on (instance' ^. ServiceInstanceSoftware ==. sos ^. SoftwareOSId)
        on (factory ^. FactoryId ==. instance' ^. ServiceInstanceFactory)
        mapM_ (addFilter instance') $ factoriesViewFilter fv
        orderBy $ concatMap (toOrder factory software sv) $ factoriesViewSort fv
        groupBy (instance' ^. ServiceInstanceId)
        offset $ fromIntegral $ factoriesViewOffset fv
        limit $ fromIntegral $ factoriesViewLimit fv
        return (factory, sv, software)
  where
    toOrder factory _ _ (FactoriesWidgetSort FactoriesWidgetLastSeen a) =
        [s a $ factory ^. FactoryLastSeen]
    toOrder factory _ _ (FactoriesWidgetSort FactoriesWidgetUuid a) =
        [s a $ factory ^. FactoryUuid]
    toOrder _ sw _ (FactoriesWidgetSort FactoriesWidgetSoftwareName a) =
        [s a $ sw ^. SoftwareName]
    toOrder _ _ sv (FactoriesWidgetSort FactoriesWidgetSoftwareVersion a) =
        [s a $ sv ^. SoftwareVersionName]
    s a f = case a of
                SortAsc  -> asc f
                SortDesc -> desc f
    addFilter instance' (FactoriesWidgetFilterService serviceid) =
        where_ $ instance' ^. ServiceInstanceService ==. val serviceid

getFactoriesWidget :: Lang -> Bool -> FactoriesView -> Handler Widget
getFactoriesWidget lang navLinks factoriesview = getFactoriesWidget' lang navLinks factoriesview'
  where
    order = cleanOrder $ factoriesViewSort factoriesview
                         ++ [FactoriesWidgetSort FactoriesWidgetUuid SortDesc]
    factoriesview' = factoriesview {factoriesViewSort = order}

getFactoriesWidget' :: Lang -> Bool -> FactoriesView -> Handler Widget
getFactoriesWidget' lang navLinks factoriesview = do
    rows' <- runDB $ getFactories factoriesview
    let rows = zip [start + 1 ..] rows'
        mPrevPage = if start > 0
                    then Just $ changeOffset (max 0 $ start - size)
                    else Nothing
        mNextPage = if size == length rows
                    then Just $ changeOffset (start + size)
                    else Nothing
    return $(widgetFile "factorieswidget")
  where
    start = factoriesViewOffset factoriesview
    size = factoriesViewLimit factoriesview
    changeOffset o = FactoriesViewR lang $ factoriesview { factoriesViewOffset = o }
    condensed = not navLinks
    order = factoriesViewSort factoriesview
    sortButton c = makeSortWidget (tor c SortAsc) (tor c SortDesc)
    tor c d = if o == order then Nothing else Just $ FactoriesViewR lang $ factoriesview {factoriesViewSort = o}
      where o = cleanOrder (FactoriesWidgetSort c d : order)
