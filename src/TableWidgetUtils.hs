module TableWidgetUtils (
    makeSortWidget
) where

import           Import

makeSortWidget :: Maybe (Route App) -> Maybe (Route App) -> Widget
makeSortWidget mup mdown = $(widgetFile "sort-button")

