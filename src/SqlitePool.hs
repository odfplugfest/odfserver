module SqlitePool (createSqlitePool) where

import           ClassyPrelude.Yesod
import           Data.Pool               (Pool)
import           Database.Persist.Sql    (runSqlPool)
import qualified Database.Persist.Sqlite as S

import           Model

createSqlitePool :: Text -> IO (Pool SqlBackend)
createSqlitePool databasePath = do
    let conf = S.SqliteConf databasePath 1
    -- create configuration with poolsize of 1 to avoid sqlite busy error
    pool <- S.createPoolConfig conf
    flip runSqlPool pool $ do
        S.rawExecute "COMMIT TRANSACTION" []
        S.rawExecute "PRAGMA encoding = 'UTF-8'" []
        S.rawExecute "PRAGMA synchronous = OFF" []
        S.rawExecute "PRAGMA temp_store = MEMORY" []
        S.rawExecute "PRAGMA busy_timeout = 10000" [] -- 10 seconds
        S.rawExecute "PRAGMA foreign_keys = ON" []
        S.rawExecute "BEGIN TRANSACTION" []
        S.runMigration migrateAll
--        S.rawExecute "CREATE INDEX IF NOT EXISTS request_time ON request (time);" []
    return pool
