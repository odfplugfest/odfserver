module JobsWidget (
    getJobsWidget
) where

import           Data.List          (nubBy)
import           Database.Esqueleto
import           Import             hiding (Asc, Desc, Value, filter,
                                     formatTime, isNothing, on, (==.), (||.))
import qualified Import             as I (on)

import           FileType           (showNice)
import           TableColumns
import           TableWidgetUtils
import           TimeUtil

-- keep only the first sort instruction per column
cleanOrder :: [JobsWidgetSort] -> [JobsWidgetSort]
cleanOrder = nubBy ((==) `I.on` jobSortCol)

getJobs :: MonadIO m => JobsView -> SqlPersistT m [(Entity Job, Entity User, Entity Service, Maybe (Entity JobResult), Maybe (Entity ServiceInstance))]
getJobs jv =
    select $ from $ \(job `InnerJoin` user `InnerJoin` service
            `LeftOuterJoin` jobresult `LeftOuterJoin` instance') -> do
        on $ job ^. JobServiceInstance ==. instance' ?. ServiceInstanceId
        on $ just (job ^. JobId) ==. jobresult ?. JobResultJob
        on $ job ^. JobService ==. service ^. ServiceId
        on $ job ^. JobUser ==. user ^. UserId
        mapM_ (addFilter job user service instance') $ jobsViewFilter jv
        orderBy $ concatMap (toOrder job user service jobresult) $ jobsViewSort jv
        offset $ fromIntegral $ jobsViewOffset jv
        limit $ fromIntegral $ jobsViewLimit jv
        return (job, user, service, jobresult, instance')
  where
    toOrder job _ _ _ (JobsWidgetSort JobsWidgetCreated a) =
        [s a $ job ^. JobCreated]
    toOrder _ _ _ result (JobsWidgetSort JobsWidgetFinished a) =
        [s a $ not_ $ isNothing $ result ?. JobResultFinished,
         s a $ result ?. JobResultFinished]
    toOrder _ user _ _ (JobsWidgetSort JobsWidgetUser a) =
        [s a $ user ^. UserName]
    toOrder _ _ service _ (JobsWidgetSort JobsWidgetService a) =
        [s a $ service ^. ServiceName]
    toOrder _ _ service _ (JobsWidgetSort JobsWidgetInput a) =
        [s a $ service ^. ServiceInputType]
    toOrder _ _ service _ (JobsWidgetSort JobsWidgetOutput a) =
        [s a $ service ^. ServiceOutputType]
    toOrder job _ _ _ (JobsWidgetSort JobsWidgetAssigned a) =
        [s a $ job ^. JobServiceInstance]
    s a f = case a of
                SortAsc  -> asc f
                SortDesc -> desc f
    addFilter _ user _ _ (JobsWidgetFilterUser userid) =
        where_ $ user ^. UserId ==. val userid
    addFilter _ _ service _ (JobsWidgetFilterInput input) =
        where_ $ service ^. ServiceInputType ==. val input
    addFilter _ _ service _ (JobsWidgetFilterOutput output) =
        where_ $ service ^. ServiceOutputType ==. val output
    addFilter _ _ _ instance' (JobsWidgetFilterFactory (Just fac)) =
        where_ $ instance' ?. ServiceInstanceFactory ==. val (Just fac)
    addFilter job _ _ _ (JobsWidgetFilterFactory Nothing) =
        where_ $ isNothing $ job ^. JobServiceInstance

getJobsWidget :: Lang -> Bool -> JobsView -> Handler Widget
getJobsWidget lang navLinks jobsview = getJobsWidget' lang navLinks jobsview'
  where
    order = cleanOrder $ jobsViewSort jobsview
                         ++ [JobsWidgetSort JobsWidgetCreated SortDesc]
    jobsview' = jobsview {jobsViewSort = order}

getJobsWidget' :: Lang -> Bool -> JobsView -> Handler Widget
getJobsWidget' lang navLinks jobsview = do
    let showUser = JobsWidgetUser `elem` jobsViewCols jobsview
    rows' <- runDB $ getJobs jobsview
    let rows = zip [start + 1 ..] rows'
        mPrevPage = if start > 0
                    then Just $ changeOffset (max 0 $ start - size)
                    else Nothing
        mNextPage = if size == length rows
                    then Just $ changeOffset (start + size)
                    else Nothing
    return $(widgetFile "jobswidget")
  where
    start = jobsViewOffset jobsview
    size =jobsViewLimit jobsview
    changeOffset o = JobsR $ jobsview { jobsViewOffset = o }
    condensed = not navLinks
    order = jobsViewSort jobsview
    sortButton c = makeSortWidget (tor c SortAsc) (tor c SortDesc)
    tor c d = if o == order then Nothing else Just $ JobsR $ jobsview {jobsViewSort = o}
      where o = cleanOrder (JobsWidgetSort c d : order)
