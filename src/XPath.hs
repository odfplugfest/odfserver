module XPath (
    parseXPath,
    prefixes
) where

import           ClassyPrelude.Yesod          hiding (compare, compareLength,
                                               length)
import           Data.Text                    (splitOn)
import           Data.Tree.NTree.TypeDefs
import           Text.Read                    (read)
import           Text.Regex.Posix             hiding (match)
import           Text.XML.HXT.Core            (XNode (..), XmlTree)
import           Text.XML.HXT.XPath.XPathEval

prefixes :: [(Text,Text)]
prefixes = map (first tons) [ ("chart", "chart")
           , ("dr3d", "dr3d")
           , ("drawing", "draw")
           , ("form", "form")
           , ("manifest", "manifest")
           , ("meta", "meta")
           , ("office", "office")
           , ("presentation", "presentation")
           , ("script", "script")
           , ("smil-compatible", "smil")
           , ("style", "style")
           , ("svg-compatible", "svg")
           , ("table", "table")
           , ("text", "text")
           , ("xsl-fo-compatible", "fo") ]
           ++ [ ("http://purl.org/dc/elements/1.1/", "dc"),
                ("http://www.w3.org/1999/xhtml", "xhtml"),
                ("http://www.w3.org/1999/xlink", "xlink")]
  where
    tons n = "urn:oasis:names:tc:opendocument:xmlns:" ++ n ++ ":1.0"

compareLength :: Maybe Text -> String -> Maybe Text
compareLength Nothing _  = Nothing
compareLength (Just a) b
    = case (apx, bpx) of
          (Just val, Just ref) -> if compare val ref
                                      then Just "True"
                                      else Just "False"
          _                    -> Just "False"
  where
    apx = convertToPx $ unpack a
    bpx = convertToPx $ b
    compare val ref = abs (val - ref) < 0.03 * abs ref

lengthRegExp :: String
lengthRegExp = "^(-?([0-9]+(\\.[0-9]*)?)|(\\.[0-9]+))(cm|mm|in|pt|pc|px)$"

convertToPx :: String -> Maybe Double
convertToPx length = case match of
                         (_,_,_,[val,_,_,_,unit]) -> convert (read val) unit
                         _                        -> Nothing
  where
    match = length =~ lengthRegExp :: (String,String,String,[String])
    convert val "cm" = Just $ val * 96 / 2.54
    convert val "mm" = Just $ val * 96 / 25.4
    convert val "in" = Just $ val * 96
    convert val "pt" = Just $ val / 0.75
    convert val "pc" = Just $ val * 16
    convert val "px" = Just $ val
    convert _ _      = Nothing

atMay :: [a] -> Int -> Maybe a
atMay a i = listToMaybe $ drop i a

getItem :: Text -> Int -> String -> Maybe Text
getItem a pos sep = atMay (splitOn (pack sep) a) pos

compareItem :: Maybe Text -> Int -> String -> String -> Maybe Text
compareItem Nothing _ _ _ = Nothing
compareItem (Just a) pos sep val | val == "" =
        Just $ fromMaybe "" $ getItem a pos sep
compareItem (Just a) pos sep val =
    if getItem a pos sep == Just (pack val) then Just "True" else Just "False"

parseXPath' :: String -> Bool -> Either String (XmlTree -> Maybe Text)
parseXPath' xpath nested =
    case clerMatch of
        (_,_,_,[a,len]) ->
            case parseXPath' a True of
                 Left err -> Left err
                 Right f  -> Right (\tree -> compareLength (f tree) len)
        _ -> case iterMatch of
                (_,_,_,[a,pos,sep,_,val]) ->
                    case parseXPath' a True of
                        Left err -> Left err
                        Right f -> Right (\tree -> compareItem (f tree) (read pos) sep val)

                _ -> case parsed of
                        Left err   -> Left err
                        Right expr -> Right $ eval (getXPath' expr)
  where
    atts = map (\(a,b) -> (unpack b, unpack a)) prefixes
    eval expr xml = case expr xml of
                        [NTree (XText val) []] -> Just $ pack val
                        _                      -> Nothing
    (cler :: String) = "^\\w*f:compareLength\\((.*),\\w*['\"](.*)['\"]\\)\\w*$"
    clerMatch = xpath =~ cler :: (String,String,String,[String])
    (iter :: String) = "^\\w*f:item\\((.*),\\w*([0-9]+)\\w*,\\w*['\"](.*)['\"]\\w*\\)\\w*(=['\"](.*)['\"])?\\w*$"
    iterMatch = xpath =~ iter :: (String,String,String,[String])
    parsed = if nested
                 then parse $ "string(" ++ xpath ++ ")"
                 else parse xpath
    parse = parseXPathExprWithNsEnv atts

parseXPath :: String -> Either String (XmlTree -> Maybe Text)
parseXPath s = parseXPath' s' False
  where
    (csre :: String) = "(.*)f:(.*)-style\\(([^\\)]*)\\)(.*)"
    csreMatch = s =~ csre :: (String,String,String,[String])
    s' = case csreMatch of
        (_,_,_,[a,family,name,b]) -> a ++ "//style:style[@style:display-name="
               ++ name ++ " or (not(@style:display-name) and @style:name="
               ++ name ++ ")][@style:family='" ++ family ++ "']" ++ b
        _ -> s
