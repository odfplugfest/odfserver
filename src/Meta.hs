module Meta (metaFromXml) where

import qualified Data.ByteString.Lazy as L
import           Import
import           Text.XML
import           Text.XML.Cursor

parseSettings :: ParseSettings
parseSettings = def { psRetainNamespaces = True }

metaFromXml :: L.ByteString -> [(Text,Text)]
metaFromXml bytes = case parseLBS parseSettings bytes of
              Right doc -> metaFromDoc doc
              Left _    -> []

metaFromDoc :: Document -> [(Text,Text)]
metaFromDoc doc = filter (not . null . snd) $ a els
  where
    metans = "urn:oasis:names:tc:opendocument:xmlns:meta:1.0"
    officens = "urn:oasis:names:tc:opendocument:xmlns:office:1.0"
    dcns = "http://purl.org/dc/elements/1.1/"
    meta = Name "meta" (Just officens) Nothing
    els = child (fromDocument doc) >>= element meta >>= child >>= anyElement
    t e = concat $ els >>= element e >>= descendant >>= content
    v ns n = t $ Name n (Just ns) Nothing
    a e | null e = []
    a _ = [("Title", v dcns "title"),
           ("Creator", v dcns "creator"),
           ("Generator", v metans "generator")]
