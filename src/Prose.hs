-- | This module has a form for creating and editing 'Prose' instances.
module Prose (
    FormProse(..),
    content,
    createProse,
    deriveProse,
    formatMarkdown,
    proseFormWidget,
    runProseForm,
    wrapProseFormWidget
) where

import           Control.Applicative   as A
import           Data.Text             (replace)
import           Import                hiding (replace)
import           Skylighting.Styles    (tango)
import           Text.Julius
import           Text.Pandoc
import           Yesod.Form.Bootstrap3

-- | The data used in the HTML form for creating or editing a 'Prose' object.
data FormProse = FormProse {
-- | The title of the 'Prose;
    formProseTitle     :: Text,
-- | The content of the 'Prose', the format is currently MarkDown
    formProseContent   :: Textarea,
-- | The 'Prose' to which this 'Prose' replies
    formProseParent    :: Maybe ProseId,
-- | When editing a 'Prose', 'formProseReference' points to the 'Prose' that is
-- being edited. For a new 'Prose', there is no reference yet. So in a form for
-- a new 'Prose', 'formProseReference' points to the newest existing 'Prose' in
-- the same group. Each 'Prose' in the group is written by the same user and has
-- the same 'formProseParent'. In addition, it belongs to the same category,
-- e.g. 'Page', 'Upload', or 'Comment'.
-- This reference is necessary to prevent a form that is submitted twice
-- from creating multiple 'Prose' instances.
    formProseReference :: Maybe ProseId
}

-- pandoc does not like \r\n, so change it to \n
toUnix :: Text -> Text
toUnix = replace "\r" "\n" . replace "\r\n" "\n"

-- | Unwrap the 'Text' from a 'FormProse'
content :: FormProse -> Text
content = toUnix . unTextarea . formProseContent

formatMarkdown :: Text -> Html
formatMarkdown md = case readMarkdown def $ unpack md of
                        Left _       -> "Parse error"
                        Right pandoc -> writeHtml settings pandoc
  where
    settings = def {writerHighlight = True, writerHighlightStyle = tango}

-- | A simple form for writing prose
proseForm :: Maybe FormProse -> Form FormProse
proseForm prose = renderDivs $ FormProse
    A.<$> areq textField (bfs ("Title" :: Text)) (formProseTitle <$> prose)
    A.<*> areq textareaField (bfs ("Content" :: Text)) (formProseContent <$> prose)
    A.<*> areq hiddenField "" (formProseParent <$> prose)
    A.<*> areq hiddenField "" (formProseReference <$> prose)

-- | Create a Prose from a FormProse
-- The contents from the textarea is formatted to HTML with MarkDown
createProse :: MonadIO m => UserId -> FormProse -> m Prose
createProse userid formProse = do
    now <- liftIO getCurrentTime
    return Prose
        { proseFirstVersion = Nothing
        , prosePreviousVersion = Nothing
        , proseCreated = now
        , proseAuthor = userid
        , proseVisible = True
        , proseTitle = formProseTitle formProse
        , proseHtmlContent = formatMarkdown c
        , proseContent = c
        }
    where c = content formProse

-- | Create a Prose from an existing Prose and a FormProse
-- The new Prose takes the title and content from the FormProse. The new
-- Prose refers to the old prose as its previous version.
deriveProse :: MonadIO m => ProseId -> UserId -> FormProse -> ReaderT SqlBackend m (Prose, Prose)
deriveProse proseid userid formProse = do
    now <- liftIO getCurrentTime
    oldProse <- get404 proseid
    let firstVersion = case proseFirstVersion oldProse of
            Just p  -> Just p
            Nothing -> Just proseid
    let newProse = Prose {
          proseFirstVersion = firstVersion
        , prosePreviousVersion = Just proseid
        , proseCreated = now
        , proseAuthor = userid
        , proseVisible = proseVisible oldProse
        , proseTitle = formProseTitle formProse
        , proseHtmlContent = formatMarkdown c
        , proseContent = c
        }
    return (oldProse, newProse)
    where c = content formProse

-- | Process the submitted form
-- The returned widget can be wrapped in a \<form\> with
-- 'wrapProseFormWidget'.
--
-- When processing the 'FormProse' form, the user information is also
-- always required. Therefor, it is also retrieved by this function.
runProseForm :: Handler (Key User, User, FormResult FormProse, Widget, Enctype, Maybe Bool)
runProseForm = do
    userid <- requireAuthId
    user <- runDB $ get404 userid
    ((result, widget), enctype) <- runFormPost $ proseForm Nothing
    isSave <- runInputPost $ iopt boolField "save"
    return (userid, user, result, widget, enctype, isSave)

-- | Wrap the FormProse in a \<form\> with css styling
-- If 'withSave' is @True@ a save button is shown.
wrapProseFormWidget :: Enctype -> Bool -> Widget -> Widget
wrapProseFormWidget enctype withSave widget = $(widgetFile "prose_form")

-- | Create a form and fill it with information from the provided
-- 'FormProse'
proseFormWidget :: FormProse -> Widget
proseFormWidget prose = do
    let formPost = generateFormPost $ proseForm $ Just prose
    (widget, enctype) <- handlerToWidget formPost
    wrapProseFormWidget enctype False widget

-- | Add JavaScript that renders the MarkDown to a preview as the user is typing
-- This uses the JavaScript library \'showdown\'.
livePreview :: Widget
livePreview =
#if DEVELOPMENT
    toWidget $(juliusFileReload "templates/pagepreview.julius")
#else
    toWidget $(juliusFile "templates/pagepreview.julius")
#endif
