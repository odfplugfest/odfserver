module PdfRenderer (
    getPdfPage,
    renderPdfs
) where

import           ClassyPrelude.Yesod           hiding (isNothing, on, (==.))
import           Data.Knob                     (newKnob, withFileHandle)
import           Data.List                     ((!!))
import           Data.Maybe                    (fromJust)
import           Database.Esqueleto
import           Pdf.Toolbox.Core.Error        (PdfE)
import           Pdf.Toolbox.Document.Catalog  (catalogPageNode)
import           Pdf.Toolbox.Document.Document (documentCatalog)
import           Pdf.Toolbox.Document.Monad    (MonadPdf)
import           Pdf.Toolbox.Document.Page     (pageMediaBox)
import           Pdf.Toolbox.Document.PageNode (PageTree (..), loadPageNode,
                                                pageNodeKids)
import           Pdf.Toolbox.Document.Pdf      (document, knownFilters,
                                                runPdfWithHandle)
import           Pdf.Toolbox.Document.Types    (Rectangle (..))
import           System.Directory              (getDirectoryContents)
import           System.Exit                   (ExitCode)
import           System.IO                     (IOMode (ReadMode))
import           System.IO.Temp                (withTempDirectory)
import           System.Process                (StdStream (..), createProcess,
                                                cwd, proc, std_in,
                                                waitForProcess)

import           Database
import           FileType
import           Model

renderPdfs :: (MonadIO m, MonadBaseControl IO m) => Database -> m ()
renderPdfs db = do
    -- list the pdfs that are not yet rendered
    rows <- flip runDb db $ select $ from $ \(blob `LeftOuterJoin` render) -> do
        on $ just (blob ^. BlobId) ==. render ?. RenderInput
        where_ (isNothing (render ?. RenderInput))
        where_ (blob ^. BlobFileType ==. val PDF)
        return $ blob ^. BlobId
    return ()
    mapM_ (renderPdf db . unValue) rows

renderPdf :: (MonadIO m, MonadBaseControl IO m) => Database -> Key Blob -> m ()
renderPdf db blobid = do
    mblob <- flip runDb db $ get blobid
    let blob = blobContent $ fromJust mblob
    pages <- liftIO $ getPdfPages blob
    let pageCount = length pages
    pngs <- liftIO $ renderPdf' blob
    flip runDb db $ do
        ids <- mapM addPNG pngs
        let r = zip ids pages
        _ <- mapM (insertOrGet . render pageCount) r
        return ()
  where
    render :: Int -> ((Int, BlobId, BlobId), Rectangle Double) -> Render
    render pageCount ((n,p,t),Rectangle x y w h) = Render {
                                                renderInput = blobid,
                                                renderPageNumber = n,
                                                renderPageCount = pageCount,
                                                renderPage = p,
                                                renderThumbnail = t,
                                                renderOptimized = False,
                                                renderX = x,
                                                renderY = y,
                                                renderWidth = w,
                                                renderHeight = h }

addPNG :: MonadIO m => (t, ByteString, ByteString) -> ReaderT SqlBackend m (t, BlobId, BlobId)
addPNG (num, page, thumbnail) = do
    pageid <- addBlob (fromStrict page) PNG
    thumbnailid <- addBlob (fromStrict thumbnail) PNG
    return (num, pageid, thumbnailid)

renderPdf' :: ByteString -> IO [(Int,ByteString,ByteString)]
renderPdf' blob =
    withTempDirectory "." "pdftoppm" (\tmpdir -> do
        -- create thumbnails
        _ <- pdftoppm tmpdir blob ["-r", "150"] "page"
        _ <- pdftoppm tmpdir blob ["-scale-to", "256"] "thumbnail"
        f <- getDirectoryContents tmpdir
        let l = filter (isSuffixOf ".png") f
            p = filter (isPrefixOf "page") l
            t = filter (isPrefixOf "thumbnail") l
        -- optimize png files with optipng
        -- _ <- optipng $ map (tmpdir </>) (t ++ p)
        -- read all the files
        ps <- readFiles tmpdir p
        ts <- readFiles tmpdir t
        return $ z [1..] ps ts )
  where
    readFiles tmpdir = mapM (\f -> readFile $ tmpdir </> f) . sort
    z (a:as) (b:bs) (c:cs) = (a,b,c):z as bs cs
    z _ _ _                = []

renderPdfPage :: MonadIO m => BlobId -> ByteString -> Int -> Int -> Rectangle Double -> ReaderT SqlBackend m (Maybe Render)
renderPdfPage pdfid blob page pageCount box = do
    (big, thumb) <- liftIO $ withTempDirectory "." "pdftoppm" (\tmpdir -> do
        big <- pdftoppm1 tmpdir blob ["-r", "150"] page pageCount
        thumb <- pdftoppm1 tmpdir blob ["-scale-to", "256"] page pageCount
        return (big, thumb))
    (n, p, t) <- addPNG (page, big, thumb)
    let Rectangle x y w h = box
    let render = Render pdfid n pageCount p t False x y w h
    _ <- insertOrGet render
    return $ Just render

pdftoppm :: FilePath -> ByteString -> [String] -> String -> IO ExitCode
pdftoppm tmpdir blob args prefix = do
    (Just std_in, _, _, hdl) <- createProcess (proc "pdftoppm" (args ++
            ["-png", "-l", "1", "-", prefix])) {
        cwd = Just tmpdir,
        std_in = CreatePipe}
    hPut std_in blob
    hClose std_in
    waitForProcess hdl

pdftoppm1 :: FilePath -> ByteString -> [String] -> Int -> Int -> IO ByteString
pdftoppm1 tmpdir blob args page pageCount = do
    let p = if pageCount > 9 && page < 10 then "0" ++ show page else show page
    (Just std_in, _, _, hdl) <- createProcess (proc "pdftoppm" (args ++
            ["-png", "-f", p, "-l", p, "-", "page"])) {
        cwd = Just tmpdir,
        std_in = CreatePipe}
    hPut std_in blob
    hClose std_in
    _ <- waitForProcess hdl
    readFile $ tmpdir </> "page-" ++ p ++ ".png"

optipng :: [FilePath] -> IO ExitCode
optipng files = do
    (_,_,_,hdl) <- createProcess (proc "optipng" files)
    waitForProcess hdl

getPdfPages :: ByteString -> IO [Rectangle Double]
getPdfPages bytes = do
    knob <- newKnob bytes
    pages <- withFileHandle knob "pdffile" ReadMode (\handle' ->
        runPdfWithHandle handle' knownFilters $ do
            pdf <- document
            catalog <- documentCatalog pdf
            rootNode <- catalogPageNode catalog
            pdfNodes $ PageTreeNode rootNode
        )
    return $ case pages of
        Left _  -> []
        Right n -> n

-- get dimensions for each page
pdfNodes :: MonadPdf m => PageTree -> PdfE m [Rectangle Double]
pdfNodes (PageTreeNode n) = do
    nodes <- pageNodeKids n
    trees <- mapM loadPageNode nodes
    ns <- mapM pdfNodes trees
    return $ concat ns
pdfNodes (PageTreeLeaf p) = do
    box <- pageMediaBox p
    return [box]

getPdfPage :: MonadIO m => BlobId -> Int -> ReaderT SqlBackend m (Int, Maybe Render)
getPdfPage pdfid page = do
    rows <- select $ from $ \render -> do
        where_ (render ^. RenderInput ==. val pdfid)
        where_ (render ^. RenderPageNumber ==. val page)
        return render
    case rows of
        [] -> do
            (Blob _ pdf _ fileType) <- get404 pdfid
            pages <- case fileType of
                FileType.PDF -> liftIO $ getPdfPages pdf
                _            -> return []
            let pageCount = length pages
            mrender <- if page > 0 && page <= pageCount
                    then do
                        let box = pages !! (page - 1)
                        renderPdfPage pdfid pdf page pageCount box
                    else return Nothing
            return (pageCount, mrender)
        Entity _ png:_ -> return (renderPageCount png, Just png)
