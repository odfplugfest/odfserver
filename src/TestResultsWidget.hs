module TestResultsWidget (
    getTestResultsWidget
) where

import           Data.List          (nubBy)
import           Database.Esqueleto
import           Import             hiding (Asc, Desc, Value, filter,
                                     formatTime, isNothing, on, (==.), (||.))
import qualified Import             as I (on)

import           TableColumns
import           TableWidgetUtils

-- keep only the first sort instruction per column
cleanOrder :: [TestResultsWidgetSort] -> [TestResultsWidgetSort]
cleanOrder = nubBy ((==) `I.on` testResultSortCol)

getTests :: MonadIO m => TestResultsView -> SqlPersistT m [(Entity TestResult, Entity Test, Entity SoftwareOS, Entity SoftwareVersion, Entity Software, Entity SoftwareFamily, Entity Platform, Entity OSVersion, Entity OperatingSystem)]
getTests jv =
    select $ from $ \(result `InnerJoin` test `InnerJoin` sos
                     `InnerJoin` version `InnerJoin` software
                     `InnerJoin` family `InnerJoin` platform
                     `InnerJoin` osversion `InnerJoin` os) -> do
        on $ osversion ^. OSVersionOs ==. os ^. OperatingSystemId
        on $ sos ^. SoftwareOSOs ==. osversion ^. OSVersionId
        on $ sos ^. SoftwareOSPlatform ==. platform ^. PlatformId
        on $ software ^. SoftwareSoftwareFamily ==. family ^. SoftwareFamilyId
        on $ version ^. SoftwareVersionSoftware ==. software ^. SoftwareId
        on $ sos ^. SoftwareOSSoftware ==. version ^. SoftwareVersionId
        on $ result ^. TestResultSoftware ==. sos ^. SoftwareOSId
        on $ result ^. TestResultTest ==. test ^. TestId
        mapM_ (addFilter result) $ testResultsViewFilter jv
        orderBy $ map (toOrder result family) $ testResultsViewSort jv
        offset $ fromIntegral $ testResultsViewOffset jv
        limit $ fromIntegral $ testResultsViewLimit jv
        return (result, test, sos, version, software, family, platform, osversion, os)
  where
    toOrder result _ (TestResultsWidgetSort TestResultsWidgetPassed a) = s a $ result ^. TestResultPass
    toOrder result _ (TestResultsWidgetSort TestResultsWidgetTest a) = s a $ result ^. TestResultTest
    toOrder _ family (TestResultsWidgetSort TestResultsWidgetFamily a) = s a $ family ^. SoftwareFamilyName
    s a f = case a of
                SortAsc  -> asc f
                SortDesc -> desc f
    addFilter result (TestResultsWidgetFilterTest testid) =
        where_ $ result ^. TestResultTest ==. val testid

getTestResultsWidget :: Lang -> Bool -> TestResultsView -> Handler Widget
getTestResultsWidget lang navLinks trv = getTestResultsWidget' lang navLinks trv'
  where
    order = cleanOrder $ testResultsViewSort trv
                         ++ [TestResultsWidgetSort TestResultsWidgetPassed SortDesc]
    trv' = trv {testResultsViewSort = order}

getTestResultsWidget' :: Lang -> Bool -> TestResultsView -> Handler Widget
getTestResultsWidget' lang navLinks trv = do
    let showTest = TestResultsWidgetTest `elem` testResultsViewCols trv
    rows' <- runDB $ getTests trv
    let rows = zip [start + 1 ..] rows'
        mPrevPage = if start > 0
                    then Just $ changeOffset (max 0 $ start - size)
                    else Nothing
        mNextPage = if size == length rows
                    then Just $ changeOffset (start + size)
                    else Nothing
    return $(widgetFile "testresultswidget")
  where
    start = testResultsViewOffset trv
    size =testResultsViewLimit trv
    changeOffset o = TestResultsR lang $ trv { testResultsViewOffset = o }
    condensed = not navLinks
    order = testResultsViewSort trv
    sortButton c = makeSortWidget (tor c SortAsc) (tor c SortDesc)
    tor c d = if o == order then Nothing else Just $ TestResultsR lang $ trv {testResultsViewSort = o}
      where o = cleanOrder (TestResultsWidgetSort c d : order)

