module MimeType (FileInfo(..), getFileInfo) where

import           ClassyPrelude.Yesod  hiding (Element, FileInfo, path, zip)
import           Codec.Archive.Zip
import qualified Data.ByteString.Lazy as L
import qualified Data.Map.Lazy        as M
import           Data.Maybe           (fromJust)
import qualified Data.Set             as S
import           Data.Text.Encoding   (decodeUtf8')
import           Text.XML
import           Text.XML.Cursor

import           FileType             (FileType (..), getFileType)

data FileInfo = FileInfo {
    mimetype :: Text,
    fileType :: FileType
}

parseSettings :: ParseSettings
parseSettings = def { psRetainNamespaces = True }

slice :: Int64 -> Int64 -> L.ByteString -> L.ByteString
slice start len = L.take len . L.drop start

readMimetypeFromHeader :: L.ByteString -> Maybe Text
readMimetypeFromHeader bytes =
    if name == "mimetype" && L.length mimetype' == clength
    then case decodeUtf8' $ L.toStrict mimetype' of
        Left _  -> Nothing
        Right m -> Just m
    else Nothing
    where
        name = slice 30 8 bytes
        clength = fromIntegral $ toInteger $ L.index bytes 22
        mimetype' = slice 38 clength bytes


getOpenPackageMimeType :: L.ByteString -> Maybe Text
getOpenPackageMimeType bytes =
    case readMimetypeFromHeader bytes of
        Just m -> Just m
        -- out of luck, invalid file, but we try anyway, have to unzip now
        _ -> case toArchiveOrFail bytes of
            Right archive -> case findEntryByPath "mimetype" archive of
                                 Just entry -> readMimeType entry
                                 Nothing    -> mimeTypeFromManifest archive
            Left _ -> Nothing
  where
    readMimeType entry = case decodeUtf8' $ toStrict $ fromEntry entry of
                             Left _  -> Nothing
                             Right m -> Just m
    -- read manifest:media-type from
    -- <manifest:file-entry manifest:full-path="/">
    mimeTypeFromManifest :: Archive -> Maybe Text
    mimeTypeFromManifest archive = case findEntryByPath "META-INF/manifest.xml" archive of
        Just entry -> case parseLBS parseSettings $ fromEntry entry of
            Right doc -> getMimeType doc
            _         -> Nothing
        _ -> Nothing
    getMimeType :: Document -> Maybe Text
    getMimeType doc = headMay $ concatMap (attribute mediaType) $ rootEntry doc
    rootEntry :: Document -> [Cursor]
    rootEntry doc = [fromDocument doc] >>= element manifest >>= child
        >>= element fileEntry >>= attributeIs fullPath "/"
    mns = "urn:oasis:names:tc:opendocument:xmlns:manifest:1.0"
    manifest = qname mns "manifest"
    fileEntry = qname mns "file-entry"
    fullPath = qname mns "full-path"
    mediaType = qname mns "media-type"

isZip :: L.ByteString -> Bool
isZip = L.isPrefixOf zipHeader
    where zipHeader = L.pack [0x50,0x4b,0x03,0x04]

octetStream :: Text
octetStream = "application/octet-stream"

applicationXml :: Text
applicationXml = "application/xml"

officens :: Text
officens = "urn:oasis:names:tc:opendocument:xmlns:office:1.0"

manifestns :: Text
manifestns = "urn:oasis:names:tc:opendocument:xmlns:manifest:1.0"

attr :: M.Map Name Text -> Text -> Text -> Maybe Text
attr atts ns name = M.lookup (Name name (Just ns) Nothing) atts

getMimeTypeBin :: L.ByteString -> Maybe (Text, Bool)
getMimeTypeBin bytes | isZip bytes =
    case getOpenPackageMimeType bytes of
        Just mime -> zip mime
        Nothing   -> zip "application/zip"
getMimeTypeBin bytes | L.isPrefixOf "%PDF-" bytes = nozip "application/pdf"
getMimeTypeBin bytes | L.isPrefixOf pngmagic bytes = nozip "image/png"
    where pngmagic = L.pack [0x89,0x50,0x4e,0x47,0x0d,0x0a,0x1a,0x0a]
getMimeTypeBin bytes | L.isPrefixOf jpgmagic bytes = nozip "image/jpeg"
    where jpgmagic = L.pack [0xff,0xd8,0xff]
getMimeTypeBin bytes | L.isPrefixOf pngmagic bytes = nozip "image/png"
    where pngmagic = L.pack [0x89,0x50,0x4e,0x47,0x0d,0x0a,0x1a,0x0a]
getMimeTypeBin bytes | L.isPrefixOf "GIF87a" bytes
                       || L.isPrefixOf "GIF89a" bytes = nozip "image/gif"
getMimeTypeBin bytes | L.isPrefixOf (L.pack [0x77,0x4f,0x46,0x46]) bytes
                       || L.isPrefixOf (L.pack [0x77,0x4f,0x46,0x32]) bytes
                       = nozip "application/font-woff"
getMimeTypeBin _ = Nothing

zip :: Text -> Maybe (Text, Bool)
zip m = Just (m, True)
nozip :: Text -> Maybe (Text, Bool)
nozip m = Just (m, False)

getMimeTypeXml :: Document -> Text
getMimeTypeXml (Document _ (Element (Name name ns _) atts _) _)
    | ns == Just officens && name == "document" && isJust mimetype' =
      fromJust mimetype'
    | ns == Just svgns && name == "svg" = "image/svg+xml"
    | ns == Just rdfns && name == "RDF" = "application/rdf+xml"
    | otherwise = applicationXml
    where mimetype' = attr atts officens "mimetype"
          svgns = "http://www.w3.org/2000/svg"
          rdfns = "http://www.w3.org/1999/02/22-rdf-syntax-ns#"

qname :: Text -> Text -> Name
qname ns name = Name name (Just ns) Nothing

-- | Return the version from the 'version' attribute for the given
-- namespace.
-- If the attribute is not present, this function returns Nothing.
getOdfVersionXml :: Text -> Document -> Maybe Text
getOdfVersionXml ns doc
    = headMay $ attribute (qname ns "version") $ fromDocument doc

-- | Read the version attribute from the given XML file.
-- If the file is not present, not valid XML or has no 'version'
-- attribute on the document element, Nothing is returned.
getOdfVersion :: Archive -> Text -> FilePath -> Maybe Text
getOdfVersion archive ns path = case findEntryByPath path archive of
    Just entry -> case parseLBS parseSettings $ fromEntry entry of
        Right doc -> getOdfVersionXml ns doc
        Left _    -> Nothing
    Nothing -> Nothing

-- | Get the ODF version from the ODF file
-- Look in XML files in the zip container for a 'version' attribute on the
-- document element. If no version can be found, return Nothing.
getZipOdfVersion :: L.ByteString -> Maybe Text
getZipOdfVersion bytes = case toArchiveOrFail bytes of
    -- look for odf version in files from small to large
    Right archive -> listToMaybe $ catMaybes [
        getOdfVersion archive officens "meta.xml",
        getOdfVersion archive officens "settings.xml",
        getOdfVersion archive officens "styles.xml",
        getOdfVersion archive manifestns "META-INF/manifest.xml",
        getOdfVersion archive officens "content.xml"]
    Left _ -> Nothing

zipHasOdfExtNs :: L.ByteString -> Bool
zipHasOdfExtNs bytes = case toArchiveOrFail bytes of
    -- look for odf version in files from small to large
    Right archive -> any id $ catMaybes [
        z archive "meta.xml",
        z archive "settings.xml",
        z archive "styles.xml",
        z archive "META-INF/manifest.xml",
        z archive "content.xml"]
    Left _ -> False
  where
    z archive path = case findEntryByPath path archive of
        Just entry -> case parseLBS parseSettings $ fromEntry entry of
            Right doc -> Just $ hasOdfExtNs doc
            Left _    -> Nothing
        Nothing -> Nothing

cleanOdfVersion :: Maybe Text -> Maybe Text
cleanOdfVersion versionOrNothing = case versionOrNothing of
    Just version -> if version `elem` ["1.0", "1.1", "1.2"]
        then Just version else Nothing
    Nothing -> Nothing

makeFileInfo :: Text -> Maybe Text -> Bool -> Bool -> Bool -> FileInfo
makeFileInfo mimetype' versionOrMaybe odfxml extended iszip=
    FileInfo mimetype' $ getFileType mimetype' versionOrMaybe odfxml extended iszip

nonExtNamespaces :: Set (Maybe Text)
nonExtNamespaces = S.fromList [
    Nothing,
    Just "http://docs.oasis-open.org/ns/office/1.2/meta/odf#",
    Just "http://docs.oasis-open.org/ns/office/1.2/meta/pkg#",
    Just "http://purl.org/dc/elements/1.1/",
    Just "http://www.w3.org/1998/Math/MathML",
    Just "http://www.w3.org/1999/xhtml",
    Just "http://www.w3.org/1999/xlink",
    Just "http://www.w3.org/2000/xmlns/",
    Just "http://www.w3.org/2002/xforms",
    Just "http://www.w3.org/2003/g/data-view#",
    Just "http://www.w3.org/XML/1998/namespace",
    Just "urn:oasis:names:tc:opendocument:xmlns:animation:1.0",
    Just "urn:oasis:names:tc:opendocument:xmlns:chart:1.0",
    Just "urn:oasis:names:tc:opendocument:xmlns:config:1.0",
    Just "urn:oasis:names:tc:opendocument:xmlns:database:1.0",
    Just "urn:oasis:names:tc:opendocument:xmlns:datastyle:1.0",
    Just "urn:oasis:names:tc:opendocument:xmlns:dr3d:1.0",
    Just "urn:oasis:names:tc:opendocument:xmlns:drawing:1.0",
    Just "urn:oasis:names:tc:opendocument:xmlns:form:1.0",
    Just "urn:oasis:names:tc:opendocument:xmlns:manifest:1.0",
    Just "urn:oasis:names:tc:opendocument:xmlns:meta:1.0",
    Just "urn:oasis:names:tc:opendocument:xmlns:of:1.2",
    Just "urn:oasis:names:tc:opendocument:xmlns:office:1.0",
    Just "urn:oasis:names:tc:opendocument:xmlns:presentation:1.0",
    Just "urn:oasis:names:tc:opendocument:xmlns:script:1.0",
    Just "urn:oasis:names:tc:opendocument:xmlns:smil-compatible:1.0",
    Just "urn:oasis:names:tc:opendocument:xmlns:style:1.0",
    Just "urn:oasis:names:tc:opendocument:xmlns:svg-compatible:1.0",
    Just "urn:oasis:names:tc:opendocument:xmlns:table:1.0",
    Just "urn:oasis:names:tc:opendocument:xmlns:text:1.0",
    Just "urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0"]

hasOdfExtNs :: Document -> Bool
hasOdfExtNs doc = not $ null c
  where
    c = [fromDocument doc] >>= descendant >>= checkElement checkE
    ens e = nameNamespace $ elementName e
    ans e = map nameNamespace $ keys $ elementAttributes e
    checkE e = any (`notMember` nonExtNamespaces) $ ens e:ans e

getFileInfo :: L.ByteString -> FileInfo
getFileInfo bytes =
    case getMimeTypeBin bytes of
        Just (mimetype', iszip) ->
            if "application/vnd.oasis.opendocument." `isPrefixOf` mimetype'
            then makeFileInfo mimetype' version False ext iszip
            else makeFileInfo mimetype' Nothing False False iszip
                where version = cleanOdfVersion $ m $ getZipOdfVersion bytes
        Nothing -> case parseLBS parseSettings bytes of
            Right doc ->
                if "application/vnd.oasis.opendocument." `isPrefixOf` mimetype'
                then makeFileInfo mimetype' (cleanOdfVersion $ m version) True
                        (hasOdfExtNs doc) False
                else makeFileInfo mimetype' Nothing False False False
                where mimetype' = getMimeTypeXml doc
                      version = getOdfVersionXml officens doc
            Left _ ->
                case decodeUtf8' $ toStrict bytes of
                    Right _ -> makeFileInfo "text/plain; charset=UTF-8" Nothing False False False
                    Left _ -> makeFileInfo octetStream Nothing False False False
    where ext = zipHasOdfExtNs bytes
          m Nothing = Nothing
          m t       = Just $ fromJust t
