module TestScheduler (
    evaluateAutoTests,
    scheduleTestJobs,
    createTestScheduler,
    stopTestScheduler,
    TestScheduler
) where

import           ClassyPrelude.Yesod                  hiding (Value, groupBy,
                                                       isNothing, on, toList,
                                                       update, (<.), (=.),
                                                       (==.))
import           Codec.Archive.Zip
import           Control.Concurrent                   (forkIO)
import           Data.List                            (nub)
import           Data.Map                             (fromListWith, toList)
import           Data.Maybe                           (fromJust)
import           Data.Time.Clock                      (addUTCTime)
import           Database.Esqueleto                   hiding (count)
import           Database.Esqueleto.Internal.Language (Insertion)
import           GHC.Conc.Sync                        (ThreadId)
import           Text.XML.HXT.Core                    hiding (Blob)

import qualified Database                             as D
import           FileType
import           Model
import           PdfRenderer
import           TestInputCreator
import           XPath                                (parseXPath)

data SchedulerTask = AUTOTESTS | JOBS | QUIT
                   deriving Eq

data TestScheduler = TestScheduler {
    tsDatabase :: D.Database,
    tsThread   :: ThreadId,
    tsChan     :: TChan SchedulerTask
}

createTestScheduler :: D.Database -> IO TestScheduler
createTestScheduler database = do
    chan <- atomically newTChan
    thread <- forkIO $ runScheduler database chan
    return TestScheduler
        { tsDatabase = database
        , tsThread = thread
        , tsChan = chan }

stopTestScheduler :: TestScheduler -> IO ()
stopTestScheduler scheduler = atomically $ writeTChan (tsChan scheduler) QUIT

readAll :: Eq a => TChan a -> [a] -> IO [a]
readAll chan xs = do
    r <- atomically $ tryReadTChan chan
    case r of
        Just v  -> readAll chan (nub $ v:xs)
        Nothing -> return xs

runScheduler :: D.Database -> TChan SchedulerTask -> IO ()
runScheduler database chan = forever $ do
    top <- atomically $ readTChan chan
    rest <- readAll chan []
    let msgs = nub (top:rest)
    mapM_ work msgs
 where
    work AUTOTESTS = D.runDb evaluateAutoTests' database
    work JOBS      = scheduleTestJobs' database
    work QUIT      = return ()

evaluateAutoTests :: TestScheduler -> IO ()
evaluateAutoTests scheduler =
    atomically $ writeTChan (tsChan scheduler) AUTOTESTS

scheduleTestJobs :: TestScheduler -> IO ()
scheduleTestJobs scheduler =
    atomically $ writeTChan (tsChan scheduler) JOBS

onJob :: SqlExpr (Entity Blob) -> SqlExpr (Entity Service) -> SqlExpr (Entity ServiceInstance) -> SqlExpr (Maybe (Entity Job)) -> SqlQuery ()
onJob blob service instance' job = do
    on (job ?. JobInput ==. just (blob ^. BlobId)
        &&. job ?. JobService ==. just (service ^. ServiceId)
        &&. job ?. JobSoftware ==. just (instance' ^. ServiceInstanceSoftware))
    on (service ^. ServiceId ==. instance' ^. ServiceInstanceService)
    on (blob ^. BlobFileType ==. service ^. ServiceInputType)

makeJob :: UTCTime -> SqlExpr (Value UserId) -> SqlExpr (Entity Blob) -> SqlExpr (Entity Service) -> SqlExpr (Entity ServiceInstance) -> SqlExpr (Maybe (Entity Job)) -> SqlQuery (SqlExpr (Insertion Job))
makeJob now author blob service instance' job = do
    where_ (isNothing $ job ?. JobId)
    groupBy (author, blob ^. BlobId, instance' ^. ServiceInstanceSoftware, service ^. ServiceId)
    return $ Job <#  author
                 <&>  val now
                 <&> (blob ^. BlobId)
                 <&> (instance' ^. ServiceInstanceSoftware)
                 <&> (service ^. ServiceId)
                 <&> val Nothing
                 <&> val Nothing

-- | Create jobs for all tests that do not yet have jobs
-- A job is created for each available combination of service and
-- software_os
scheduleTests ::MonadIO m => UTCTime -> SqlPersistT m ()
scheduleTests now =
    insertSelect $ from $ \(autotest `InnerJoin` output `InnerJoin` test `InnerJoin` blob `InnerJoin` service `InnerJoin` instance' `LeftOuterJoin` job) -> do
        onJob blob service instance' job
        on (autotest ^. AutoTestInputFile ==. blob ^.BlobId)
        on (autotest ^. AutoTestTest ==. test ^. TestId)
        on (autotest ^. AutoTestId ==. output ^. AutoTestOutputTypeTest)
        where_ (service ^. ServiceOutputType ==. output ^. AutoTestOutputTypeFileType)
        makeJob now (test ^. TestAuthor) blob service instance' job

-- | For each ODF result from an AutoTest, create a job to convert the
-- obtained result ODF to a PDF with the same software as the software that
-- created the ODF, if that software supports the creation of PDF files.
scheduleTestPdfs :: MonadIO m => UTCTime -> SqlPersistT m ()
scheduleTestPdfs now =
    insertSelect $ from $ \(autoresult `InnerJoin` jobresult
                           `InnerJoin` testjob `InnerJoin` blob
                           `InnerJoin` service `InnerJoin` instance'
                           `LeftOuterJoin` job) -> do
        onJob blob service instance' job
        on (jobresult ^. JobResultResult ==. just (blob ^. BlobId))
        on (jobresult ^. JobResultJob ==. testjob ^. JobId)
        on (autoresult ^. AutoTestResultJob ==. jobresult ^. JobResultId)
        where_ (service ^. ServiceOutputType ==. val PDF)
        where_ (service ^. ServiceName ==. val "convert")
        where_ (testjob ^. JobSoftware ==. instance' ^. ServiceInstanceSoftware)
        makeJob now (testjob ^. JobUser) blob service instance' job

-- | Create a job to convert a PDF to PNG for every PDF that was created
-- from an ODF that came out of a test job.
scheduleTestPngs :: MonadIO m => UTCTime -> SqlPersistT m ()
scheduleTestPngs now =
    insertSelect $ from $ \(autoresult `InnerJoin` odfresult
                           `InnerJoin` pdfjob `InnerJoin` pdfresult
                           `InnerJoin` pdfblob
                           `InnerJoin` service `InnerJoin` instance'
                           `LeftOuterJoin` job) -> do
        onJob pdfblob service instance' job
        on (pdfresult ^. JobResultResult ==. just (pdfblob ^. BlobId))
        on (pdfjob ^. JobId ==. pdfresult ^. JobResultJob)
        on (odfresult ^. JobResultResult ==. just (pdfjob ^. JobInput))
        on (autoresult ^. AutoTestResultJob ==. odfresult ^. JobResultId)
        where_ (pdfblob ^. BlobFileType ==. val PDF)
        where_ (service ^. ServiceOutputType ==. val PNG)
        where_ (service ^. ServiceName ==. val "render")
        makeJob now (pdfjob ^. JobUser) pdfblob service instance' job

-- | Create a job to create a clipped PNG from every PNG that is the result
-- of rendering a PNG file.
scheduleClipPdfPngs :: MonadIO m => UTCTime -> SqlPersistT m ()
scheduleClipPdfPngs now =
    insertSelect $ from $ \(pngjob `InnerJoin` pngservice
                           `InnerJoin` pngresult `InnerJoin` pngblob
                           `InnerJoin` service `InnerJoin` instance'
                           `LeftOuterJoin` job) -> do
        onJob pngblob service instance' job
        on (pngresult ^. JobResultResult ==. just (pngblob ^. BlobId))
        on (pngjob ^. JobId ==. pngresult ^. JobResultJob)
        on (pngjob ^. JobService ==. pngservice ^. ServiceId)
        where_ (pngblob ^. BlobFileType ==. val PNG)
        where_ (service ^. ServiceOutputType ==. val PNG)
        where_ (service ^. ServiceName ==. val "clip")
        where_ (pngservice ^. ServiceName ==. val "render")
        makeJob now (pngjob ^. JobUser) pngblob service instance' job

-- | Each job result for which there is a validation service is schedule
-- a validation job.
scheduleJobValidation :: MonadIO m => UTCTime -> SqlPersistT m ()
scheduleJobValidation now =
    insertSelect $ from $ \(job' `InnerJoin` jobresult `InnerJoin` blob `InnerJoin` service `InnerJoin` instance' `LeftOuterJoin` job) -> do
        onJob blob service instance' job
        on (jobresult ^. JobResultResult ==. just (blob ^. BlobId))
        on (job' ^. JobId ==. jobresult ^. JobResultJob)
        where_ (service ^. ServiceOutputType ==. val VALIDATIONREPORT)
        makeJob now (job' ^. JobUser) blob service instance' job

scheduleUploadValidation :: MonadIO m => UTCTime -> SqlPersistT m ()
scheduleUploadValidation now =
    insertSelect $ from $ \(upload `InnerJoin` blob `InnerJoin` service `InnerJoin` instance' `LeftOuterJoin` job) -> do
        onJob blob service instance' job
        on (upload ^. UploadContent  ==. blob ^. BlobId)
        where_ (service ^. ServiceOutputType ==. val VALIDATIONREPORT)
        makeJob now (upload ^. UploadUploader) blob service instance' job

scheduleTestValidation :: MonadIO m => UTCTime -> SqlPersistT m ()
scheduleTestValidation now =
    insertSelect $ from $ \(test `InnerJoin` autotest `InnerJoin` blob `InnerJoin` service `InnerJoin` instance' `LeftOuterJoin` job) -> do
        onJob blob service instance' job
        on (autotest ^. AutoTestInputFile  ==. blob ^. BlobId)
        on (test ^. TestId  ==. autotest ^. AutoTestTest)
        where_ (service ^. ServiceOutputType ==. val VALIDATIONREPORT)
        makeJob now (test ^. TestAuthor) blob service instance' job

-- | Insert a failed job result for jobs where the serviceinstance has not
-- been seen for a long time and for which there is no result yet.
markTimedOutJobs :: MonadIO m => UTCTime -> SqlPersistT m ()
markTimedOutJobs now = do
    let cutOffTime = addUTCTime (-5*60) now -- 5 minutes before now
    -- get all timed out jobs
    rows <- select $ from $ \(job `LeftOuterJoin` jobresult) -> do
        on (just (job ^. JobId) ==. jobresult ?. JobResultJob)
        where_ (isNothing (jobresult ?. JobResultJob)
                &&. (job ^. JobStarted <. just (val cutOffTime)))
        return (job ^. JobId, job ^. JobStarted, job ^. JobServiceInstance)
    let ids = map (\(a,_,_) -> unValue a) rows
    -- log the failed jobs
    D.insertMany_ 8 $ map (\(jobid,started,instance') ->
              FailedJob {
                failedJobJob = unValue jobid,
                failedJobStarted = fromJust $ unValue started,
                failedJobFinished = now,
                failedJobServiceInstance = fromJust $ unValue instance',
                failedJobCommandLine = "",
                failedJobError = FactoryTimeout,
                failedJobExitCode = -1,
                failedJobStdout = "",
                failedJobStderr = "",
                failedJobRestarter = Nothing}) rows
    -- reset failed jobs so they can run again
    update $ \job -> do
        set job [ JobStarted =. val Nothing, JobServiceInstance =. val Nothing ]
        where_ ((job ^. JobId) `in_` valList ids)
    return ()

countJobs :: MonadIO m => ReaderT SqlBackend m Int
countJobs = count ([] :: [Filter Job])

scheduleTestJobs' :: (MonadIO m, MonadBaseControl IO m) => D.Database -> m ()
scheduleTestJobs' db = do
    now <- liftIO getCurrentTime
    newJobs <- flip D.runDb db $ do
        before <- countJobs
        scheduleJobValidation now
        scheduleUploadValidation now
        scheduleTestValidation now
        scheduleTests now
        scheduleTestPdfs now
        markTimedOutJobs now
        after <- countJobs
        return $ before /= after
    renderPdfs db
    unless (not newJobs) (liftIO $ D.signalNewJob db)

getUnevaluatedInputXPaths :: MonadIO m => ReaderT SqlBackend m [(BlobId,Entity AutoTestXPath)]
getUnevaluatedInputXPaths = do
    -- get all the xpaths without result
    rows <- select $ from $ \(autotest `InnerJoin` use `InnerJoin` xpath
                             `LeftOuterJoin` result) -> do
        on (result ?. XPathResultBlob ==. just (autotest ^. AutoTestInputFile)
            &&. result ?. XPathResultXpath ==. just (xpath ^. AutoTestXPathId))
        on (use ^. AutoTestXPathUseXpath ==. xpath ^. AutoTestXPathId)
        on (autotest ^. AutoTestId ==. use ^. AutoTestXPathUseTest)
        where_ (isNothing $ result ?. XPathResultId)
        return (autotest ^. AutoTestInputFile, xpath)
    return $ map (\(Value a, b) -> (a, b)) rows

getUnevaluatedOutputXPaths :: MonadIO m => ReaderT SqlBackend m [(BlobId,Entity AutoTestXPath)]
getUnevaluatedOutputXPaths = do
    -- get all the xpaths without result
    rows <- select $ from $ \(autotest `InnerJoin` job `InnerJoin` jobresult
                             `InnerJoin` use `InnerJoin` xpath
                             `LeftOuterJoin` result) -> do
        on (result ?. XPathResultBlob ==. jobresult ^. JobResultResult
            &&. result ?. XPathResultXpath ==. just (xpath ^. AutoTestXPathId))
        on (use ^. AutoTestXPathUseXpath ==. xpath ^. AutoTestXPathId)
        on (autotest ^. AutoTestId ==. use ^. AutoTestXPathUseTest)
        on (job ^. JobId ==. jobresult ^. JobResultJob)
        on (autotest ^. AutoTestInputFile ==. job ^. JobInput)
        where_ (isNothing $ result ?. XPathResultId)
        where_ (not_ (isNothing $ jobresult ^. JobResultResult))
        return (jobresult ^. JobResultResult, xpath)
    return $ map (\(Value (Just a), b) -> (a, b)) rows

groupBy' :: (Ord b, Eq a) => (a -> b) -> [a] -> [(b,[a])]
groupBy' f l = toList $ map nub $ fromListWith (++) $ map (\a -> (f a, [a])) l

evaluateXPaths' :: MonadIO m => (BlobId, [Entity AutoTestXPath]) -> ReaderT SqlBackend m [XPathResult]
evaluateXPaths' (blobid, xpaths) = do
    mblob <- get blobid
    case mblob of
        Nothing -> return []
        Just blob ->
            case toArchiveOrFail $ fromStrict $ blobContent blob of
                Left _ -> return []
                Right archive -> do
                    r' <- mapM (check' archive) (groupBy' getPath xpaths)
                    return $ concat r'
  where
    getPath (Entity _ (AutoTestXPath path _)) = path
    check' archive (path, xp) = do
        let mentry = findEntryByPath (unpack path) archive
        case mentry of
            Nothing -> return $ map failed xp
            Just entry -> do
                mtree <- liftIO $ parseXML $ blobToString $ fromEntry entry
                case mtree of
                    Left _     -> return $ map failed xp
                    Right tree -> return $ map (check'' tree) xp
    check'' tree (Entity key (AutoTestXPath _ xpath)) =
        case parseXPath $ unpack xpath of
            Left _     -> XPathResult blobid key False
            Right expr -> check''' key $ expr tree
    check''' key (Just "True") = XPathResult blobid key True
    check''' key _             = XPathResult blobid key False
    failed (Entity key _) = XPathResult blobid key False

evaluateXPaths :: MonadIO m => [(BlobId,Entity AutoTestXPath)] -> ReaderT SqlBackend m ()
evaluateXPaths xpaths = do
    r <- mapM evaluateXPaths' grouped
    D.insertMany_ 3 $ concat r
  where
    up (k, v) = (k,[v])
    grouped = toList $ fromListWith (++) $ map up xpaths

evaluateUnevaluatedXPaths :: MonadIO m => ReaderT SqlBackend m ()
evaluateUnevaluatedXPaths = do
    input <- getUnevaluatedInputXPaths
    output <- getUnevaluatedOutputXPaths
    evaluateXPaths (input ++ output)

evaluateAutoTests' :: MonadIO m => ReaderT SqlBackend m ()
evaluateAutoTests' = do
    evaluateUnevaluatedXPaths
    -- Find out for which autotests, there is no AutoTestResult yet, but
    -- for which all xpaths have been evaluated.
    r <- select $ from $ \(autotest `InnerJoin` use `InnerJoin` job
                          `InnerJoin` jobresult `InnerJoin` xpathresult
                          `InnerJoin` xpath) -> do
        on (use ^. AutoTestXPathUseXpath ==. xpath ^. AutoTestXPathId)
        on (jobresult ^. JobResultResult ==. just (xpathresult ^. XPathResultBlob))
        on (job ^. JobId ==. jobresult ^. JobResultJob)
        on (autotest ^. AutoTestInputFile ==. job ^. JobInput)
        on (autotest ^. AutoTestId ==. use ^. AutoTestXPathUseTest)
        where_ (xpath ^. AutoTestXPathId ==. xpathresult ^. XPathResultXpath)
        where_ (notExists $
                from $ \(testresult `InnerJoin` autoresult) -> do
                on (testresult ^. TestResultId
                        ==. autoresult ^. AutoTestResultResult)
                where_ (autoresult ^. AutoTestResultJob
                       ==. jobresult ^. JobResultId)
                where_ (testresult ^. TestResultTest
                          ==. autotest ^. AutoTestTest))
        let (ok :: SqlExpr (Value Int)) =
                case_ [ when_ (xpathresult ^. XPathResultResult ==. val True)
                        then_ $ val 1]
                      (else_ $ val 0)
        groupBy (autotest ^. AutoTestId, jobresult ^. JobResultId, job ^. JobId)
        return (autotest ^. AutoTestTest, jobresult ^. JobResultId, job ^. JobSoftware, countRows, sum_ ok)
    forM_ r $ \(Value testid, Value jobresultid, Value softwareid, Value (rows :: Int), Value (mpassed :: Maybe Int)) ->
        case mpassed of
            (Just passed) -> do
                let test = TestResult testid softwareid (rows == passed)
                testresultid <- insert test
                insert_ $ AutoTestResult testresultid jobresultid
            _ -> return ()
    return ()
