module TestInputCreator (
    createTestInput,
    createOdf,
    getXmlEntries,
    parseXML
) where

import           ClassyPrelude.Yesod      hiding (first, replace, when)
import           Codec.Archive.Zip
import qualified Data.ByteString.Lazy     as L
import           Data.List                (nubBy, unionBy)
import           Data.Text                (replace)
import           Data.Tree.NTree.TypeDefs
import           Text.XML.HXT.Core        hiding (getChildren, isElem)
import           Text.XML.HXT.DOM.XmlNode

createOdf :: [(Text, XmlTree)] -> Archive -> L.ByteString
createOdf xmlEntries archive = fromArchive $ newArchive {zEntries = a++b}
    where newArchive = foldr updateEntry archive xmlEntries
          -- make sure that the file 'mimetype' comes first
          (a, b) = partition isMimetype $ zEntries newArchive
          isMimetype = ("mimetype" ==) . eRelativePath

writeConfig :: SysConfigList
writeConfig = []

updateEntry :: (Text, XmlTree) -> Archive -> Archive
updateEntry (path, tree) archive = addEntryToArchive entry archive
  where
    filePath = unpack path
    mentry = findEntryByPath filePath archive
    mtime = maybe 0 eLastModified mentry
    string = runLA (writeDocumentToString writeConfig) tree
    blob (x:_) = stringToBlob x
    blob []    = ""
    entry = toEntry (unpack filePath) mtime $ blob string

lccmp :: (Text, XmlTree) -> (Text, XmlTree) -> Ordering
lccmp (s1, _) (s2, _) = compare (toLower s1) (toLower s2)

-- parser chokes on declaration of 'xml' as namespace prefix, even if that
-- is correct, this is very rough way to avoid that problem
fixXmlNs :: String -> String
fixXmlNs = unpack . replace " xmlns:xml=\"http://www.w3.org/XML/1998/namespace\"" "" . pack

parseXML :: String -> IO (Either Text XmlTree)
parseXML xml = do
    -- parser can trip up on BOM character (ef bb bf)
    let cleanXml = fixXmlNs $ dropWhile (/= '<') xml
    r <- runX $ readString [ withValidate no
                           , withSubstDTDEntities no
                           , withSubstHTMLEntities no
                           , withParseHTML no
                           , withParseByMimeType no
                           , withCheckNamespaces yes
                           , withEncodingErrors yes
                           , withErrors no
                           , withWarnings no
                           ] cleanXml
    return $ checkNode r
  where
    checkNode [NTree node cs] | not (null cs) = Right $ NTree node cs
    checkNode _               = Left "Invalid XML"

-- | Return an xmltree for each entry in the zip file that can be parsed as
-- xml.
getXmlEntries :: ByteString -> IO (Either Text (Archive, [(Text, XmlTree)]))
getXmlEntries bytes =
    case toArchiveOrFail $ fromStrict bytes of
        Left errMsg -> return $ Left $ pack errMsg
        Right archive -> do
            entries <- mapM parseEntry $ zEntries archive
            return $ Right (archive, sortBy lccmp $ catMaybes entries)

parseEntry :: Entry -> IO (Maybe (Text, XmlTree))
parseEntry entry = do
    let path = pack $ eRelativePath entry
    mtree <- parseXML $ blobToString $ fromEntry entry
    case mtree of
        Left _     -> return Nothing
        Right tree -> return $ Just (path, tree)

-- | Put the elements from the tree into the map with xml trees from the
-- ODF document.
-- Only the modified trees are returned.
createTestInput :: [(Text, XmlTree)] -> XmlTree -> Either Text [(Text, XmlTree)]
createTestInput entries (NTree (XTag _ atts) cs) = do
    let fragments = filter isElem cs
        (f, e) = foldr doEntry (fragments, []) entries
        unused = mapMaybe qname f
    case unused of
        [] -> Right e
        (n:_) -> Left $ "The element <"
              ++ pack (namePrefix n ++ ":" ++ localPart n)
              ++ "> is not available in the template."
  where
    doEntry (path,c) (fs, acc) = (f, changedEntries c')
      where (f, c') = addFragments (fs, c)
            changedEntries nc | c == nc = acc
            changedEntries nc = (path,addDNS nc):acc
    addDNS (NTree a c) = NTree a $ map addDefaultNamespaces c
    addDefaultNamespaces (NTree (XTag qn att) c)
        = NTree (XTag qn (unionBy cmpns att defaultNS)) c
    addDefaultNamespaces a = a
    defaultNS = getNamespaceDefinitions atts
    cmpns (NTree (XAttr qn1) _) (NTree (XAttr qn2) _) = qn1 == qn2
    cmpns a b                                         = a == b
createTestInput _ _ = Right []

-- | Get the namespace definitions from a list of attributes.
getNamespaceDefinitions :: [XmlTree] -> [XmlTree]
getNamespaceDefinitions = filter isNSAttr
  where
    isNSAttr (NTree (XAttr qn) _) = namePrefix qn == "xmlns"
    isNSAttr _                    = False

-- |
addFragments :: ([XmlTree], XmlTree) -> ([XmlTree], XmlTree)
addFragments ([], t) = ([], t)
addFragments (frags, NTree (XTag qn atts) cs) | with /= []
    = (without, NTree (XTag qn (nubBy compareAtt $ attrs with ++ atts)) childs)
  where
    (with, without) = partition (\n -> qname n == Just qn) frags
    getAttrs (NTree (XTag _ a) _) = a
    getAttrs _                    = []
    attrs = concatMap getAttrs
    compareAtt (NTree (XAttr a) _) (NTree (XAttr b) _) = a == b
    compareAtt _ _                                     = False
    childs = cs ++ concatMap getChildren with
addFragments (frags, NTree (XTag qn atts) cs) = (f, NTree (XTag qn atts) cs')
  where
    (f, cs') = foldr doChild (frags, []) cs
    doChild node (frags', acc) = (fs, c:acc)
      where
        (fs, c) = addFragments (frags', node)
addFragments o = o

qname :: XmlTree -> Maybe QName
qname (NTree (XTag qn _) _) = Just qn
qname _                     = Nothing
