-- | Collection of widgets to show comment threads in a web page.
module CommentWidgets (
    discussionWidget,
    getCommentRoot,
    commentBodyWidget,
    commentWidget
) where

import           Database.Esqueleto
import           Import             hiding (Value, formatTime, on, tail, (==.))

import           TimeUtil

type CommentRow = (Entity Comment,ProseId,UserId,UTCTime,Text,Html,Text)

-- | Return the root ProseId for the comment with the given 'ProseId'.
-- If no root is found, the given 'ProseId' is returned.
getCommentRoot :: MonadIO m => ProseId -> ReaderT SqlBackend m ProseId
getCommentRoot proseid = do
    mkey <- select $ from $ \comment -> do
        where_ $ comment ^. CommentComment ==. val proseid
        return (comment ^. CommentRoot)
    case mkey of
        [Value root] -> return root
        _            -> return proseid

-- | A widget that displays a comment and all the replies to it.
-- If a route to a parent is provided, it will be shown as a link.
commentWidget :: CommentId -> Route App -> Widget
commentWidget commentid parentRoute = do
    comments <- handlerToWidget $ runDB $ do
        comment <- get404 commentid
        getComments $ commentComment comment
    let r = filter (\(Entity rid _,_,_,_,_,_,_) -> rid == commentid) comments
    mapM_ (\comment -> renderComment comments comment (Just parentRoute) Nothing) r

-- | Retrieve all comments that have the same 'CommentRoot' as the comment
-- with the given 'ProseId'.
getComments :: MonadIO m => ProseId -> ReaderT SqlBackend m [CommentRow]
getComments proseid = do
    root <- getCommentRoot proseid
    wrappedComments <- select $ from $ \(comment `InnerJoin` prose `InnerJoin` user) -> do
        on $ prose ^. ProseAuthor ==. user ^. UserId
        on $ comment ^. CommentComment ==. prose ^. ProseId
        where_ $ comment ^. CommentRoot ==. val root
        return ( comment
               , prose ^. ProseId
               , prose ^. ProseAuthor
               , prose ^. ProseCreated
               , prose ^. ProseTitle
               , prose ^. ProseHtmlContent
               , user  ^. UserName
               )
    return $ map unwrapComment wrappedComments
  where
    unwrapComment (a, Value b, Value c, Value d, Value e, Value f, Value g)
        = (a, b, c, d, e, f, g)

-- | A widget that displays one comment.
commentBodyWidget
    :: Maybe CommentId -- ^ Optional id for the comment to link to the comment
    -> Text            -- ^ The title of the comment
    -> UserId          -- ^ The id of the user that wrote the comment
    -> Text            -- ^ The name of author of the comment
    -> UTCTime         -- ^ The time at which the comment was written
    -> Html            -- ^ The body of the comment
    -> Maybe (Route App) -- ^ A route to the 'Prose' to which this comment is a
                         -- reply
    -> Maybe Widget    -- ^ An optional widget to append after the comment.
                       -- This could be a form for editing the comment.
    -> Widget
commentBodyWidget mcommentid title authorid author date content mparent mtail = do
    let xmlid = case mcommentid of
            Just commentid -> ("comment-" :: String) ++ show (fromSqlKey commentid)
            Nothing -> "comment-form" :: String
        abstime = formatTime date
    reltime <- handlerToWidget $ formatRelativeTime date
    muser <- handlerToWidget maybeAuthId
    lang <- getLanguage
    $(widgetFile "comment_body")

renderComment :: [CommentRow] -> CommentRow -> Maybe (Route App) -> Maybe (ProseId, Widget, Bool) -> Widget
renderComment comments comment mparent mform = do
    let (Entity commentid _,proseid,authorid,date,title,htmlcontent,author) = comment
        (childs, rest) = partition (predicate proseid) comments
    let tail = $(widgetFile "comment_tail")
    case mform of
        Just (commentParent, commentForm, True) | commentParent == proseid ->
            $(widgetFile "comment_form")
        _ ->
            commentBodyWidget (Just commentid) title authorid author date
                htmlcontent mparent (Just tail)
    where
        predicate proseid (Entity _ comment',_,_,_,_,_,_)
            = commentParent comment' == proseid

-- | A widget that displays a discussion thread.
-- The top level comments that are shown are the ones that reply to
-- @proseid@.
discussionWidget
    :: ProseId -- ^ The 'Prose' instance to which the comments in this thread
               -- reply.
    -> Maybe (ProseId, Widget, Bool) -- ^ An optional widget that will be
               -- inserted after or instead of the comment with the given
               -- 'ProseId'.
    -> Widget
discussionWidget proseid mCommentForm = do
    prose <- handlerToWidget $ runDB $ get404 proseid
    let firstProseId = fromMaybe proseid $ proseFirstVersion prose
    comments <- handlerToWidget $ runDB $ getComments firstProseId
    let topComments =
            filter (\(Entity _ comment,_,_,_,_,_,_) -> commentParent comment == firstProseId) comments
    $(widgetFile "discussion")
