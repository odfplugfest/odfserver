module TableColumns where

import           ClassyPrelude.Yesod    hiding (decompress, get)
import           Codec.Compression.Zlib (compress, decompress)
import           Data.Binary            (Binary (..), decodeOrFail, encode)
import qualified Data.ByteString.Base64 as B (decode, encode)

import           FileType
import           Model

-- utils

fromPathPiece' :: Binary a => Text -> Maybe a
fromPathPiece' t = case m of
                       Nothing -> Nothing
                       Just d -> case decodeOrFail d of
                           Left _        -> Nothing
                           Right (_,_,a) -> Just a
  where
    m = case B.decode $ encodeUtf8 t of
            Left _  -> Nothing
            Right z -> Just $ decompress $ fromStrict z

toPathPiece' :: Binary a => a -> Text
toPathPiece' = decodeUtf8 . B.encode . toStrict . compress . encode

data SortDirection = SortAsc | SortDesc
  deriving (Eq, Read, Show, Generic)
instance Binary SortDirection

-- JobsWidget

data JobsWidgetColumn = JobsWidgetCreated
                     | JobsWidgetFinished
                     | JobsWidgetUser
                     | JobsWidgetService
                     | JobsWidgetInput
                     | JobsWidgetOutput
                     | JobsWidgetAssigned
  deriving (Eq, Read, Show, Generic)

data JobsWidgetSort = JobsWidgetSort {
    jobSortCol :: JobsWidgetColumn,
    jobSortDir :: SortDirection
} deriving (Eq, Read, Show, Generic)

data JobsWidgetFilter = JobsWidgetFilterUser UserId
                     | JobsWidgetFilterInput FileType
                     | JobsWidgetFilterOutput FileType
                     | JobsWidgetFilterFactory (Maybe FactoryId)
  deriving (Eq, Generic, Read, Show)

data JobsView = JobsView {
    jobsViewOffset :: Int,
    jobsViewLimit  :: Int,
    jobsViewCols   :: [JobsWidgetColumn],
    jobsViewSort   :: [JobsWidgetSort],
    jobsViewFilter :: [JobsWidgetFilter]
} deriving (Eq, Generic, Read, Show)

instance PathPiece JobsView where
    fromPathPiece = fromPathPiece'
    toPathPiece = toPathPiece'

instance Binary JobsView
instance Binary JobsWidgetColumn
instance Binary JobsWidgetSort
instance Binary JobsWidgetFilter

-- TestsWidget

data TestsWidgetColumn = TestsWidgetName
                     | TestsWidgetCreated
                     | TestsWidgetPassCount
                     | TestsWidgetResultCount
                     | TestsWidgetPassFraction
                     | TestsWidgetUser
  deriving (Eq, Read, Show, Generic)

data TestsWidgetSort = TestsWidgetSort {
    testSortCol :: TestsWidgetColumn,
    testSortDir :: SortDirection
} deriving (Eq, Read, Show, Generic)

data TestsWidgetFilter = TestsWidgetFilterUser UserId
  deriving (Eq, Generic, Read, Show)

data TestsView = TestsView {
    testsViewOffset :: Int,
    testsViewLimit  :: Int,
    testsViewCols   :: [TestsWidgetColumn],
    testsViewSort   :: [TestsWidgetSort],
    testsViewFilter :: [TestsWidgetFilter]
} deriving (Eq, Generic, Read, Show)

instance PathPiece TestsView where
    fromPathPiece = fromPathPiece'
    toPathPiece = toPathPiece'

instance Binary TestsView
instance Binary TestsWidgetColumn
instance Binary TestsWidgetSort
instance Binary TestsWidgetFilter

-- TestResultsWidget

data TestResultsWidgetColumn = TestResultsWidgetPassed
                     | TestResultsWidgetTest
                     | TestResultsWidgetFamily
  deriving (Eq, Read, Show, Generic)

data TestResultsWidgetSort = TestResultsWidgetSort {
    testResultSortCol :: TestResultsWidgetColumn,
    testResultSortDir :: SortDirection
} deriving (Eq, Read, Show, Generic)

data TestResultsWidgetFilter = TestResultsWidgetFilterTest TestId
  deriving (Eq, Generic, Read, Show)

data TestResultsView = TestResultsView {
    testResultsViewOffset :: Int,
    testResultsViewLimit  :: Int,
    testResultsViewCols   :: [TestResultsWidgetColumn],
    testResultsViewSort   :: [TestResultsWidgetSort],
    testResultsViewFilter :: [TestResultsWidgetFilter]
} deriving (Eq, Generic, Read, Show)

instance PathPiece TestResultsView where
    fromPathPiece = fromPathPiece'
    toPathPiece = toPathPiece'

instance Binary TestResultsView
instance Binary TestResultsWidgetColumn
instance Binary TestResultsWidgetSort
instance Binary TestResultsWidgetFilter

-- FactoriesWidget

data FactoriesWidgetColumn = FactoriesWidgetLastSeen
                     | FactoriesWidgetUuid
                     | FactoriesWidgetSoftwareName
                     | FactoriesWidgetSoftwareVersion
  deriving (Eq, Read, Show, Generic)

data FactoriesWidgetSort = FactoriesWidgetSort {
    factoriesSortCol :: FactoriesWidgetColumn,
    factoriesSortDir :: SortDirection
} deriving (Eq, Read, Show, Generic)

data FactoriesWidgetFilter = FactoriesWidgetFilterService ServiceId
  deriving (Eq, Generic, Read, Show)

data FactoriesView = FactoriesView {
    factoriesViewOffset :: Int,
    factoriesViewLimit  :: Int,
    factoriesViewCols   :: [FactoriesWidgetColumn],
    factoriesViewSort   :: [FactoriesWidgetSort],
    factoriesViewFilter :: [FactoriesWidgetFilter]
} deriving (Eq, Generic, Read, Show)

instance PathPiece FactoriesView where
    fromPathPiece = fromPathPiece'
    toPathPiece = toPathPiece'

instance Binary FactoriesView
instance Binary FactoriesWidgetColumn
instance Binary FactoriesWidgetSort
instance Binary FactoriesWidgetFilter

-- ServicesWidget

data ServicesWidgetColumn = ServicesWidgetName
                     | ServicesWidgetInput
                     | ServicesWidgetOutput
  deriving (Eq, Read, Show, Generic)

data ServicesWidgetSort = ServicesWidgetSort {
    servicesSortCol :: ServicesWidgetColumn,
    servicesSortDir :: SortDirection
} deriving (Eq, Read, Show, Generic)

data ServicesWidgetFilter = ServicesWidgetFilterName Text
  deriving (Eq, Generic, Read, Show)

data ServicesView = ServicesView {
    servicesViewOffset :: Int,
    servicesViewLimit  :: Int,
    servicesViewCols   :: [ServicesWidgetColumn],
    servicesViewSort   :: [ServicesWidgetSort],
    servicesViewFilter :: [ServicesWidgetFilter]
} deriving (Eq, Generic, Read, Show)

instance PathPiece ServicesView where
    fromPathPiece = fromPathPiece'
    toPathPiece = toPathPiece'

instance Binary ServicesView
instance Binary ServicesWidgetColumn
instance Binary ServicesWidgetSort
instance Binary ServicesWidgetFilter
