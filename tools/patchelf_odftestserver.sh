#!/usr/bin/env bash

# Add the directories in the variable NIX_LDFLAGS to the rpath of the executable.
# Call this script from nix-shell

# nix-shell --pure patchelf_odftestserver.nix --run ./patchelf_odftestserver.sh

set -o errexit
set -o nounset
set -o pipefail

CWD=$(pwd)
ARG=${1:-}
EXE=dist/build/odftestserver/odftestserver

if [ ! -z "${ARG}" ]; then
    RPATH=$(echo -n "$NIX_LDFLAGS" | sed 's/ \?-rpath /:/g' | sed 's/ \+-L/:/g' | cut -b 2-):$(patchelf --print-rpath "$EXE")
    patchelf --set-rpath "$RPATH" "$EXE"
    patchelf --shrink-rpath "$EXE"
    exit
fi

dir=$(mktemp -d) && cd "$dir"
tar xzf "$CWD/odftestserver.keter"
nix-shell --pure "$CWD/patchelf_odftestserver.nix" --run "bash $CWD/$0 dir"
tar czf "$CWD/odftestserver.keter" ./*
rm -rf "$dir"
