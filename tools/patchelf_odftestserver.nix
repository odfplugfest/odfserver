with import <nixpkgs> {}; {
  keterEnv = stdenv.mkDerivation {
    name = "patchelf_odftestserver";
    buildInputs = [ stdenv patchelf postgresql libyaml gmp lua51Packages.lua nix ];
  };
}
