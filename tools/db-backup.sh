#!/usr/bin/env bash
set -o errexit
set -o nounset
set -o pipefail

outfile=${1:?Supply location to store backup file as arg1};

database=${DATABASE:-odftestserver.sqlite3};
dbuser=${PGUSER:-odfdb}
dbpassword=${PGPASS:-odfdb}
dbhost=${PGHOST:-localhost}
dbport=${PGPORT:-5432}

echo "db:$database  host:$dbhost port:$dbport    user:$dbuser pass:$dbpassword"


if [[ $database == *"sqlite"* ]]; then
    echo "backing up SQLite database..."
    sqlite3 $database ".backup main $outfile"
else
    echo "backing up PostgreSQL database..."
    pg_dump --clean --if-exists \
	    --file="$outfile" \
	    --host=$dbhost --port=$dbport \
	    --username=$dbuser \
	    $database
fi
