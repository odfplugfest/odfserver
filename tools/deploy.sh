#!/usr/bin/env bash

# This script builds the server and deploys it to the production server.
# Keter must be running on the remote server in the directory $HOME/keter

host=blob

set -o errexit
set -o nounset
set -o pipefail

if [ ! -e odftestserver.cabal ]; then
    echo run this commend in het main odftestserver directory
    exit 1
fi

# build the keter file
# TODO write the version that was obtained with git describe --all --long into
# the keter file
yesod keter --build-args exe:odftestserver
scp tools/patch_and_deploy.sh tools/patchelf_odftestserver.* odftestserver.keter $host:keter
ssh $host 'bash keter/patch_and_deploy.sh'
