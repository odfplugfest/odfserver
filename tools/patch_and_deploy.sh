#!/usr/bin/env bash

# This is the server-side script for deploying odftestserver on keter

set -o errexit
set -o nounset
set -o pipefail

. .nix-profile/etc/profile.d/nix.sh

cd keter

if [ ! -e backup ]; then
    mkdir backup
fi

if [ -e incoming/odftestserver.keter ]; then
    now=$(date +%FT%T)
    cp incoming/odftestserver.keter "backup/odftestserver-$now.keter"
fi

./patchelf_odftestserver.sh
mv odftestserver.keter incoming
