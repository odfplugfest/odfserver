import           Data.Char
import           Data.Maybe
import           System.Environment
import           Text.Regex.Posix

-- Read the file 'config/models' and print a CREATE INDEX statement for
-- each foreign key.
-- In Postgres, defining a foreign key does not automatically create an
-- index.

-- runhaskell tools/print_create_index.hs config/models

tableNamePattern :: String
tableNamePattern = "^([A-Z]\\w+)$"

getTableName :: String -> Maybe String
getTableName line = case line =~ tableNamePattern of
                         ""    -> Nothing
                         match -> Just match

tableColumnPattern :: String
tableColumnPattern = "^\\s+([a-z]\\w+)\\s+([A-Z]\\w+)"

getTableColumn :: String -> (Maybe String, [(String,String,String)])
                  -> (Maybe String, [(String,String,String)])
getTableColumn line (mtable,xs) = case (mtable, line =~ tableNamePattern) of
    (Nothing,"")    -> (Nothing,xs)
    (Just table,"") ->
        case line =~ tableColumnPattern:: (String,String,String,[String]) of
            (_,_,_,name:type':_) -> (mtable,(table,name,type'):xs)
            _                    -> (mtable,xs)
    (_,table)       -> (Just table,xs)


lw :: Char -> String
lw c | isLower c = [c]
lw c = "_" ++ [toLower c]

low :: String -> String
low (i:xs) = toLower i:(concatMap lw xs)
low xs     = xs

getIndexes :: [String] -> IO ()
getIndexes lines = do
    let tableNames = mapMaybe getTableName lines
        refs = map (++ "Id") tableNames
        (_,cols) = foldr getTableColumn (Nothing, []) $ reverse lines
        refs' = filter (\(_,_,ref) -> ref `elem` refs) cols
        cmds = reverse $ map cmd refs'
    mapM_ putStrLn cmds
    return ()
  where
    cmd (t, c, _) = "CREATE INDEX \"" ++ (low t) ++ "_" ++ (low c)
        ++ "\" ON \"" ++ (low t) ++ "\" (\"" ++ (low c) ++ "\");"

run :: [String] -> IO ()
run (filename:[]) = do
    contents <- readFile filename
    getIndexes $ lines contents
run _ = do
    progName <- getProgName
    putStrLn $ "Usage: " ++ progName ++ " config/models"

main = do
    args <- getArgs
    run args
