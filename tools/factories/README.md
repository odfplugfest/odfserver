# Build the jar file
```bash
ant
```

# To see the commands that are in the resulting jar file

```bash
java -jar odffactories.jar 
```

The above command will list the ConfigreAll and RunAll which
are the core functioanlity you are after.




For the factories, things will be simplifed soon but for right now
this is the process. You can detect a specific office suite using the
configure mode as follows. The -h tells what server to talk with and
-d 1 turns off SSL certificate verification if you are using self
signed certs.


```bash
java -cp  odffactories.jar org.odfserver.commands.Configure -c validator.properties -d 1 -h https://localhost:3443 -s ODFValidator
```

If you want to see the possibilities for the -s option run

```bash
java -cp  odffactories.jar org.odfserver.commands.ListPossibleFactories
The following Office Suites are supported for detection.

Abiword
Calligra
WebODF
GoogleDocs
ODFValidator
```

To run this new factory all you need to know is the name of the generated config (from -c) above.

```bash
java -cp odffactories.jar org.odfserver.commands.RunFactory --config validator.properties
```

The console is left with messages to give an impression of what is going down.


In the future this will be simplified to two commands; a single "configure all"
and a single "run all" that will detect everything and run all the detected servers.


Fail Factories
==============

There are a number of factories which are designed to check what
happens when things fail. These should not be configured by default
using the ConfigureAll command, you have to explicitly set them up.

The Fail Factories rely on the FailFactoryProcess executable to
perform their actions. This lets you peel back to the lowest levels if
you think that the fail factories are not working on your platform.

The FailFactoryProcess takes an argument to tell it what sort of failure
state to generate and an optional path to an input and output document. The
paths might not be needed by all modes, for example the --die ignores the
document paths if given.

```bash
java -cp odffactories.jar  org.odfserver.factories.FailFactoryProcess --die /tmp/twopage.odt /tmp/mangled.odt
```

The commands that FailFactoryProcess can process are:

--wait   which just stops for an hour
--die    which causes a segv to exit the process
--mangle which uses the doc parameters. It will put the input document at the output
         document path with some of the bytes turned into zeros. The mangling leave the first
	 bytes as they were so that the ODF file seems like it might be legit.
--stderr say something on stderr and return an exit status of 1.

Each of the states that FailFactoryProcess can generate will have an odf factory
listed by the command

```bash
java -cp odffactories.jar org.odfserver.commands.ListPossibleFactories
Abiword
Calligra
WebODF
GoogleDocs
ODFValidator
FailTimeout
FailWithException
FailSegv
FailMangle
```

These factories are setup just like any other factory. For example as shown
below:

```bash
java -cp odffactories.jar org.odfserver.commands.Configure \
  -d 1 -h https://localhost:3443 \
  -c failmangle.properties \
  -s FailMangle
```

And the factory is stated as any other single factory can be:

```bash
java -cp odffactories.jar org.odfserver.commands.RunFactory \
  -c failmangle.properties
```
