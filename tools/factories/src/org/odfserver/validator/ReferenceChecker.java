package org.odfserver.validator;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import javax.xml.transform.Source;
import javax.xml.transform.TransformerException;
import javax.xml.transform.URIResolver;
import javax.xml.transform.stream.StreamSource;

import org.eclipse.jdt.annotation.Nullable;

import net.sf.saxon.Configuration;
import net.sf.saxon.s9api.DocumentBuilder;
import net.sf.saxon.s9api.Processor;
import net.sf.saxon.s9api.SaxonApiException;
import net.sf.saxon.s9api.XPathCompiler;
import net.sf.saxon.s9api.XPathExecutable;
import net.sf.saxon.s9api.XPathSelector;
import net.sf.saxon.s9api.XdmDestination;
import net.sf.saxon.s9api.XdmItem;
import net.sf.saxon.s9api.XdmNode;
import net.sf.saxon.s9api.XsltCompiler;
import net.sf.saxon.s9api.XsltExecutable;
import net.sf.saxon.s9api.XsltTransformer;

public class ReferenceChecker {

	final Processor processor;
	final XsltExecutable xslt;
	final XPathSelector errorSelector;

	public ReferenceChecker() throws SaxonApiException {
		InputStream xsl = ReferenceChecker.class.getClassLoader()
				.getResourceAsStream("identifiers-and-references.xsl");
		processor = new Processor(new Configuration());
		XsltCompiler compiler = processor.newXsltCompiler();
		xslt = compiler.compile(new StreamSource(xsl));

		XPathCompiler xpath = processor.newXPathCompiler();
		xpath.declareNamespace("svrl", "http://purl.oclc.org/dsdl/svrl");
		XPathExecutable xx = xpath
				.compile("//svrl:failed-assert/svrl:text/text()");
		errorSelector = xx.load();
	}

	private XdmNode getXml(byte file[], String name)
			throws IOException, SaxonApiException {
		ZipInputStream zip = new ZipInputStream(new ByteArrayInputStream(file));
		ZipEntry entry = zip.getNextEntry();
		while (entry != null) {
			if (name.equals(entry.getName())) {
				DocumentBuilder db = processor.newDocumentBuilder();
				XdmNode node = db.build(new StreamSource(zip));
				zip.close();
				return node;
			}
			entry = zip.getNextEntry();
		}
		zip.close();
		throw new IOException("No file 'styles.xml' was found.");
	}

	public List<ReferenceError> checkReferences(byte file[]) {
		List<ReferenceError> errors = new ArrayList<ReferenceError>();
		try {
			XdmNode stylesXml = getXml(file, "styles.xml");
			XdmNode contentXml = getXml(file, "content.xml");
			XdmNode metaXml = getXml(file, "meta.xml");

			CheckURIResolver resolver = new CheckURIResolver(stylesXml,
					contentXml, metaXml);

			check(resolver, "styles.xml", stylesXml, errors);
			check(resolver, "content.xml", contentXml, errors);
			check(resolver, "meta.xml", metaXml, errors);
		} catch (IOException | SaxonApiException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return errors;
	}

	private void check(CheckURIResolver resolver, String component,
			XdmNode node, List<ReferenceError> errors)
			throws SaxonApiException {
		XsltTransformer transformer = xslt.load();
		transformer.setURIResolver(resolver);
		transformer.setSource(node.asSource());

		XdmDestination destination = new XdmDestination();
		transformer.setDestination(destination);
		transformer.transform();

		errorSelector.setContextItem(destination.getXdmNode());
		for (XdmItem item : errorSelector) {
			errors.add(new ReferenceError(item.getStringValue(), component));
		}
	}
}

class CheckURIResolver implements URIResolver {

	public final XdmNode stylesXml;
	public final XdmNode contentXml;
	public final XdmNode metaXml;

	CheckURIResolver(XdmNode stylesXml, XdmNode contentXml, XdmNode metaXml) {
		this.stylesXml = stylesXml;
		this.contentXml = contentXml;
		this.metaXml = metaXml;
	}

	@Override
	public @Nullable Source resolve(@Nullable String href,
			@Nullable String base) throws TransformerException {
		if ("styles.xml".equals(href)) {
			return stylesXml.asSource();
		}
		if ("content.xml".equals(href)) {
			return contentXml.asSource();
		}
		if ("meta.xml".equals(href)) {
			return metaXml.asSource();
		}
		System.err.println("cannot find " + href);
		return null;
	}

}