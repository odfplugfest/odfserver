package org.odfserver.validator;

public class ReferenceError {
	final public String error;
	final public String component;

	public ReferenceError(String error, String component) {
		this.error = error;
		this.component = component;
	}

}
