
package org.odfserver;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import org.eclipse.jdt.annotation.Nullable;
import org.odfserver.exceptions.ArgumentException;
import org.odfserver.exceptions.NoFactoryException;
import org.odfserver.factories.AbiwordSniffer;
import org.odfserver.factories.CalligraKarbonSniffer;
import org.odfserver.factories.CalligraSheetsSniffer;
import org.odfserver.factories.CalligraStageSniffer;
import org.odfserver.factories.CalligraWordsSniffer;
import org.odfserver.factories.ClipSniffer;
import org.odfserver.factories.GnumericSniffer;
import org.odfserver.factories.GoogleDocsSniffer;
import org.odfserver.factories.LO;
import org.odfserver.factories.LibreOfficeCalcSniffer;
import org.odfserver.factories.LibreOfficeDrawSniffer;
import org.odfserver.factories.LibreOfficeImpressSniffer;
import org.odfserver.factories.LibreOfficeWriterSniffer;
import org.odfserver.factories.MicrosoftExcelSniffer;
import org.odfserver.factories.MicrosoftPowerPointSniffer;
import org.odfserver.factories.MicrosoftWordSniffer;
import org.odfserver.factories.OdfValidatorSniffer;
import org.odfserver.factories.PdfRendererSniffer;
import org.odfserver.factories.WebOdfSniffer;
import org.odfserver.types.JobResult;
import org.odfserver.types.RetrieveJob;
import org.odfserver.types.TempDir;

/**
 * The design sniff()ing for an office suite during configuration and
 * perform()ing conversions thereafter is supported by this class. All the
 * factories as of March 2016 will be subclasses of this class and this class
 * helps the command line tools to configure and start up factories.
 *
 * There are a number of support methods to help make sniff() simpler for
 * subclasses to write.
 *
 * There are many abstract methods for a subclass to consider but the main ones
 * are:
 * 
 * sniff() -- detect the suite and configure yourself perform() -- convert an
 * input file to an output file with the detected suite.
 */
public abstract class OdfFactory {

	public final OdfFactoryConfig config;

	public static final String[] noenv = {};

	abstract public boolean checkVersion(String version)
			throws IOException, NoFactoryException;

	protected OdfFactory(OdfFactoryConfig config) {
		this.config = config;
	}

	/**
	 * perform the conversion, most likely by passing a started Process to
	 * performProcess()
	 */
	public abstract JobResult perform(RetrieveJob job)
			throws ArgumentException, IOException;

	public static JobResult executeProcess(TempDir dir,
			String environmentToPassAlong[], ProcessBuilder pb) {
		return executeProcess(dir, environmentToPassAlong, pb, false);
	}

	protected static JobResult executeProcess(TempDir dir,
			String environmentToPassAlong[], ProcessBuilder pb,
			boolean addDelay) {
		Runner runner = new Runner();
		runner.setEnvironmentToPassAlong(environmentToPassAlong);
		byte outputFiles[][] = JobResult.noFiles;
		String stderr = "";
		try {
			Path outputPath = runner.executeProcess(dir.outputPath, pb,
					addDelay);
			outputFiles = new byte[][] { Files.readAllBytes(outputPath) };
		} catch (IOException e) {
			e.printStackTrace();
			stderr = Utility.stacktraceToString(e);
		} catch (InterruptedException e) {
			e.printStackTrace();
			stderr = Utility.stacktraceToString(e);
		}
		stderr = runner.getStdErr() + "\n" + stderr;
		return new JobResult(runner.cmd(), outputFiles, runner.getExitCode(),
				runner.getStdOut(), stderr, runner.isTimeout());
	}

	protected static JobResult executeProcessStdOutToDocument(TempDir dir,
			String environmentToPassAlong[], ProcessBuilder pb,
			boolean addDelay) {
		Runner runner = new Runner();
		runner.setEnvironmentToPassAlong(environmentToPassAlong);
		byte outputFiles[][] = JobResult.noFiles;
		String stderr = "";
		try {
			runner.executeProcess(dir.outputPath, pb, addDelay);
			outputFiles = new byte[][] { runner.getStdOutRaw() };
		} catch (IOException e) {
			e.printStackTrace();
			stderr = Utility.stacktraceToString(e);
		} catch (InterruptedException e) {
			e.printStackTrace();
			stderr = Utility.stacktraceToString(e);
		}
		stderr = runner.getStdErr() + "\n" + stderr;
		return new JobResult(runner.cmd(), outputFiles, runner.getExitCode(),
				"", stderr, runner.isTimeout());
	}

	public static final List<Sniffer> snifferList = makeSnifferList();

	private static List<Sniffer> makeSnifferList() {
		List<Sniffer> sniffer = new LinkedList<Sniffer>();
		sniffer.add(new AbiwordSniffer());
		sniffer.add(new CalligraWordsSniffer());
		sniffer.add(new CalligraSheetsSniffer());
		sniffer.add(new CalligraStageSniffer());
		sniffer.add(new CalligraKarbonSniffer());
		sniffer.add(new WebOdfSniffer());
		sniffer.add(new GoogleDocsSniffer());
		sniffer.add(new MicrosoftWordSniffer());
		sniffer.add(new MicrosoftExcelSniffer());
		sniffer.add(new MicrosoftPowerPointSniffer());
		sniffer.add(new OdfValidatorSniffer());
		sniffer.add(new GnumericSniffer());
		sniffer.add(new ClipSniffer());
		sniffer.add(new LibreOfficeCalcSniffer(LO.LibreOffice));
		sniffer.add(new LibreOfficeDrawSniffer(LO.LibreOffice));
		sniffer.add(new LibreOfficeImpressSniffer(LO.LibreOffice));
		sniffer.add(new LibreOfficeWriterSniffer(LO.LibreOffice));
		sniffer.add(new LibreOfficeCalcSniffer(LO.OpenOffice));
		sniffer.add(new LibreOfficeDrawSniffer(LO.OpenOffice));
		sniffer.add(new LibreOfficeImpressSniffer(LO.OpenOffice));
		sniffer.add(new LibreOfficeWriterSniffer(LO.OpenOffice));
		sniffer.add(new LibreOfficeCalcSniffer(LO.NDCODFApplicationTools));
		sniffer.add(new LibreOfficeDrawSniffer(LO.NDCODFApplicationTools));
		sniffer.add(new LibreOfficeImpressSniffer(LO.NDCODFApplicationTools));
		sniffer.add(new LibreOfficeWriterSniffer(LO.NDCODFApplicationTools));
		sniffer.add(new PdfRendererSniffer());
		sniffer.add(new FailSniffer(FailType.FailTimeout));
		sniffer.add(new FailSniffer(FailType.FailWithException));
		sniffer.add(new FailSniffer(FailType.FailSegv));
		sniffer.add(new FailSniffer(FailType.FailMangle));
		return Collections.unmodifiableList(sniffer);
	}

	public static Sniffer getFactory(String name) throws ArgumentException {
		for (Sniffer f : snifferList) {
			if (f.name.equals(name))
				return f;
		}
		throw new ArgumentException("OdfFactory not found:" + name);
	}

	public static TempDir createTemporaryDirectory(RetrieveJob job)
			throws IOException {
		Path dir = Files.createTempDirectory("odffactory");
		Path inputFile = dir
				.resolve("input" + job.service.inputType.extension());
		System.out.println("Writing input file to " + inputFile);
		Files.write(inputFile, job.inputFile);
		Path outputFile = dir
				.resolve("output" + job.service.outputType.extension());
		return new TempDir(dir, inputFile, outputFile);
	}

	public static void deleteDirAndContents(@Nullable TempDir dir) {
		if (dir != null) {
			try {
				deleteDirAndContents(dir.directoryPath);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public static void deleteDirAndContents(Path path) throws IOException {
		// Files.walkFile is depth-first and does not follow symlinks by
		// default.
		Files.walkFileTree(path, new SimpleFileVisitor<Path>() {
			private FileVisitResult delete(@Nullable Path p) {
				try {
					Files.delete(p);
				} catch (IOException e) {
				}
				return FileVisitResult.CONTINUE;
			}

			@Override
			public FileVisitResult postVisitDirectory(@Nullable Path path,
					@Nullable IOException e) throws IOException {
				return delete(path);
			}

			@Override
			public FileVisitResult visitFile(@Nullable Path path,
					@Nullable BasicFileAttributes arg1) throws IOException {
				return delete(path);
			}

		});
	}
};
