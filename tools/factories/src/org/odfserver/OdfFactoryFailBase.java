package org.odfserver;

import java.io.IOException;
import java.time.Instant;
import java.util.List;

import org.odfserver.factories.FailMangleFactory;
import org.odfserver.factories.FailSegvFactory;
import org.odfserver.factories.FailTimeoutFactory;
import org.odfserver.factories.FailWithExceptionFactory;
import org.odfserver.types.FileType;
import org.odfserver.types.JobResult;
import org.odfserver.types.RetrieveJob;
import org.odfserver.types.Service;
import org.odfserver.types.ServiceType;
import org.odfserver.types.TempDir;

public abstract class OdfFactoryFailBase extends OdfFactory {

	static final String version = "0.1";

	public OdfFactoryFailBase(OdfFactoryConfig config) {
		super(config);
	}

	public boolean checkVersion(String version) throws IOException {
		return true;
	}

	private static ProcessBuilder makeFailFactoryProcessBuilderWithArg(
			String arg, String inputFilePath, String outputFilePath) {
		return new ProcessBuilder("java", "-cp", "odffactories.jar",
				"org.odfserver.factories.FailFactoryProcess", arg,
				inputFilePath, outputFilePath);
	}

	protected static JobResult failJob(String arg, RetrieveJob job)
			throws IOException {
		TempDir dir = null;
		try {
			dir = createTemporaryDirectory(job);
			ProcessBuilder pb = makeFailFactoryProcessBuilderWithArg("--mangle",
					dir.inputPath.toString(), dir.outputPath.toString());
			return executeProcess(dir, noenv, pb);
		} finally {
			deleteDirAndContents(dir);
		}
	}
};

enum FailType {
	FailMangle, FailSegv, FailTimeout, FailWithException
}

class FailSniffer extends Sniffer {

	final FailType failType;

	public FailSniffer(FailType failType) {
		super("Fail", failType.toString(), false, true);
		this.failType = failType;
	}

	public OdfFactory createFactory(OdfFactoryConfig config) {
		switch (failType) {
		case FailMangle:
			return new FailMangleFactory(config);
		case FailSegv:
			return new FailSegvFactory(config);
		case FailTimeout:
			return new FailTimeoutFactory(config);
		case FailWithException:
			return new FailWithExceptionFactory(config);
		}
		throw new Error("Implementation error: no constructor for " + failType);
	}

	public final List<Service> services = a(
			service(ServiceType.convert, FileType.ODT1_0, FileType.ODT1_1),
			service(ServiceType.roundtrip, FileType.ODT1_1, FileType.ODT1_1),
			service(ServiceType.convert, FileType.ODT1_2, FileType.ODT1_1),
			service(ServiceType.convert, FileType.ODT1_2EXT, FileType.ODT1_1),
			service(ServiceType.convert, FileType.ODT1_0, FileType.PDF),
			service(ServiceType.convert, FileType.ODT1_1, FileType.PDF),
			service(ServiceType.convert, FileType.ODT1_2, FileType.PDF),
			service(ServiceType.convert, FileType.ODT1_2EXT, FileType.PDF));

	public List<Service> getServices() {
		return services;
	}

	public OdfFactoryConfig sniff() {
		return new OdfFactoryConfig(family, failType.toString(), null,
				Instant.now().getEpochSecond(), OdfFactoryFailBase.version,
				true);
	}
};
