
package org.odfserver.commands;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.odfserver.OdfFactory;
import org.odfserver.ResourceLoader;
import org.odfserver.Server;
import org.odfserver.ServerConfig;
import org.odfserver.Sniffer;
import org.odfserver.SslTweaks;
import org.odfserver.Utility;
import org.odfserver.exceptions.ArgumentException;
import org.odfserver.exceptions.NoFactoryException;
import org.odfserver.types.JobError;
import org.odfserver.types.JobResult;
import org.odfserver.types.RetrieveJob;
import org.odfserver.types.ServiceInstance;
import org.odfserver.types.UploadJobResult;

public class RunFactory {

	int m_nextNumber;
	final Map<ServiceInstance, OdfFactory> m_factories;
	final ServerConfig m_config;

	public RunFactory(ServerConfig config,
			Map<ServiceInstance, OdfFactory> factories) {
		m_nextNumber = 1;
		m_factories = factories;
		m_config = config;
	}

	static int EXITCODE_SEGV_POSIX = 134; // on Linux
	static int EXITCODE_SEGV_WINDOWS = 139; // on cygwin

	private void runJob(RetrieveJob job) {
		System.out.print(job.service.softwareName + "\t" + job.service.inputType
				+ " -> " + job.service.outputType + "\t");
		UploadJobResult jobresult = new UploadJobResult(m_config, job.number);
		JobResult result;
		JobError error = JobError.None;
		String state = "OK";

		OdfFactory f = m_factories.get(job.service);
		if (f == null) {
			error = JobError.ServiceNotAvailable;
			state = "No such service.";
			result = new JobResult("", JobResult.noFiles, -1, "", "", false);
		} else {
			try {
				result = f.perform(job);
				if (result.timeout) {
					error = JobError.SoftwareTimeout;
					state = "Time out.";
				} else if (result.exitCode == EXITCODE_SEGV_POSIX
						|| result.exitCode == EXITCODE_SEGV_WINDOWS) {
					// FIXME: This needs testing on windows to see
					// what exit
					// code is reported by a segv process.
					error = JobError.SoftwareCrash;
					state = "Program crash.";
				}
			} catch (ArgumentException | IOException | RuntimeException e) {
				result = new JobResult("", JobResult.noFiles, -1, "",
						Utility.getStackTraceAsString(e), false);
				error = JobError.SoftwareCrash;
				state = "Java exception " + e.getMessage() + ".";
			}
		}
		// send that result back to /uploadjobresult
		System.out.println("Created " + result.outputFiles.length + " files.");
		for (byte b[] : result.outputFiles) {
			System.out.println("size: " + b.length + " bytes");
		}
		try {
			Server.postUploadJobResult(m_config,
					jobresult.finish(result, error));
		} catch (IOException e) {
			System.err.println("Cannot upload result.");
			e.printStackTrace();
		}
		System.out.println(state);
	}

	public void run() {
		if (m_factories.size() == 0) {
			System.err.println("There are no factories to run.");
			return;
		}
		if (m_config.m_disableEncryption) {
			SslTweaks.disableSslVerification();
		}
		try {
			new ResourceLoader();
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(1);
			return;
		}

		while (true) {
			long start = System.nanoTime();
			int numJobs = 0;
			try {
				List<RetrieveJob> jobs = Server.postRetrieveJobs(m_config,
						m_nextNumber, m_factories.keySet());
				numJobs = jobs.size();
				for (RetrieveJob job : jobs) {
					runJob(job);
					m_nextNumber = job.number + 1;
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
			// if there was no job and the wait time was less than a five
			// seconds, wait to the full five seconds
			// this is meant to avoid clobbering the server
			// normally the connection hangs until there is a job or until
			// a wait time of more than 10 seconds has elapsed
			long milliDuration = (System.nanoTime() - start) / 1000000;
			System.out.println("jobs: " + numJobs + " wait: " + milliDuration);
			if (numJobs == 0 && milliDuration < 5000) {
				try {
					Thread.sleep(5000 - milliDuration);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public static void loadFactory(Properties properties,
			Map<ServiceInstance, OdfFactory> factories)
					throws IOException, ArgumentException, NoFactoryException {
		String name = properties.getProperty("name");
		if (name == null) {
			System.err.println("No name defined in properties file.");
			return;
		}
		Sniffer sniffer = OdfFactory.getFactory(name);
		OdfFactory factory = sniffer.createFactory(properties);

		if (factory != null) {
			for (ServiceInstance si : sniffer.getServiceInstances(factory)) {
				factories.put(si, factory);
			}
		}
	}

	public static void main(String[] argv) {
		try {
			String configPath = "";
			if (argv.length > 1) {
				if ("-c".equals(argv[0]) || "--config".equals(argv[0]))
					configPath = argv[1];
			}
			Properties properties = new Properties();
			try {
				FileInputStream iss = new FileInputStream(configPath);
				properties.load(iss);
			} catch (IOException e) {
				System.err.println("Cannot read file " + configPath + ".");
				return;
			}
			Map<ServiceInstance, OdfFactory> factories = new HashMap<ServiceInstance, OdfFactory>();
			loadFactory(properties, factories);
			ServerConfig config = ServerConfig.configRead(properties);
			RunFactory r = new RunFactory(config,
					Collections.unmodifiableMap(factories));
			r.run();
		} catch (IOException | ArgumentException | NoFactoryException e) {
			e.printStackTrace();
		}
	}
}