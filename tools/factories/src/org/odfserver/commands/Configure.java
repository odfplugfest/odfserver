package org.odfserver.commands;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Properties;

import org.odfserver.OdfFactoryConfig;
import org.odfserver.ServerConfig;
import org.odfserver.SslTweaks;
import org.odfserver.exceptions.ArgumentException;
import org.odfserver.exceptions.NoFactoryException;

public class Configure {

	public static Properties readProperties(File file) {
		Properties properties = new Properties();
		if (file.exists()) {
			try {
				FileInputStream iss = new FileInputStream(file);
				properties.load(iss);
			} catch (IOException e) {
				System.err.println(
						"Cannot read file " + file.getAbsolutePath() + ".");
				return properties;
			}
		}
		return properties;
	}

	public int run(String configPath, ConfigureRunConfiguration conf) {
		try {
			System.out.println("             suite: " + conf.sniffer.name);
			System.out.println("              host: " + conf.serverHostAndPort);
			System.out.println(" disableEncryption: " + conf.disableEncryption);
			System.out.println("     configuration: " + conf.configuration);

			if (conf.disableEncryption) {
				SslTweaks.disableSslVerification();
			}

			Properties properties = readProperties(new File(configPath));
			// set command-line options in the property file
			properties.setProperty("serverHostAndPort", conf.serverHostAndPort);
			properties.setProperty("disableEncryption",
					String.valueOf(conf.disableEncryption));

			final ServerConfig serverConfig = ServerConfig
					.configRead(properties);
			final OdfFactoryConfig config = conf.sniffer.sniff();
			properties = new Properties();
			serverConfig.write(properties);
			config.write(properties);
			OutputStream output = new FileOutputStream(configPath);
			properties.store(output, null);
			output.close();

			System.out.println(
					"factory is setup and registered with the server...");
			System.out.println("You can start this factory using:");
			System.out.println("");
			System.out.println(
					"java -cp odffactories.jar org.odfserver.commands.RunFactory --config "
							+ conf.configuration.getCanonicalPath());
			return 0;
		} catch (NoFactoryException e) {
			System.out.println(e);
			return 1;
		} catch (IOException e) {
			System.out.println(e);
			return 1;
		}
	}

	public static void main(String[] args) {
		try {
			ConfigureRunConfiguration conf;
			try {
				conf = ConfigureRunConfiguration.parseArguments(args);
			} catch (ArgumentException e) {
				System.out.println(e);
				return;
			}
			System.out.println("suite:" + conf.sniffer.name);

			Configure me = new Configure();
			System.exit(me.run(conf.configuration.getCanonicalPath(), conf));
		} catch (IOException e) {
			System.out.println(e);
		}
	}
};
