package org.odfserver.commands;

import org.odfserver.OdfFactory;
import org.odfserver.Sniffer;

public class ListPossibleFactories {

	public static void main(String[] args) {
		System.out.println(
				"The following Office Suites are supported for detection:");
		for (Sniffer f : OdfFactory.snifferList) {
			System.out.println(f.name);
		}
	}
};
