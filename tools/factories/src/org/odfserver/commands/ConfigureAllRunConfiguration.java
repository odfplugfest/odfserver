package org.odfserver.commands;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.PosixParser;
import org.odfserver.exceptions.ArgumentException;

public class ConfigureAllRunConfiguration {

	final String serverHostAndPort;
	final boolean disableEncryption;

	protected ConfigureAllRunConfiguration(String serverHostAndPort,
			boolean disableEncryption) {
		this.serverHostAndPort = serverHostAndPort;
		this.disableEncryption = disableEncryption;
	}

	static ConfigureAllRunConfiguration parseArguments(String[] args)
			throws ArgumentException {// create the command line parser
		CommandLineParser parser = new PosixParser();

		// create the Options
		Options options = new Options();

		Option serverHostAndPort = new Option("h", "host", true,
				"URL for server, eg. https://localhost:3443/");
		serverHostAndPort.setRequired(true);
		options.addOption(serverHostAndPort);

		Option disableEncryption = new Option("d", "disable-encryption", true,
				"for testing, do not verify certificates");
		disableEncryption.setRequired(false);
		options.addOption(disableEncryption);

		// parse the command line arguments
		ConfigureAllRunConfiguration conf = null;
		try {
			CommandLine line;
			line = parser.parse(options, args);
			String hp = ConfigureRunConfiguration.getString(options, line,
					serverHostAndPort);
			boolean de = ConfigureRunConfiguration.getBoolean(options, line,
					disableEncryption);

			conf = new ConfigureAllRunConfiguration(hp, de);
		} catch (ParseException e) {
			throw ConfigureRunConfiguration.error(options, e.getMessage());
		}
		return conf;
	}

};
