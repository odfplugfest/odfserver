package org.odfserver.commands;

public class ListPossibleCommands {

	public static void main(String[] argv) {
		System.out.println(
				"Please execute one or more of the following commands");
		System.out.println("");
		System.out.println("To setup all your factories:");
		System.out.println(
				"  java -cp odffactories.jar org.odfserver.commands.ConfigureAll");
		System.out.println("    args example: -d 1 -h https://localhost:3443 ");
		System.out.println("");
		System.out.println("To run all your automatically setup factories:");
		System.out.println(
				"  java -cp odffactories.jar org.odfserver.commands.RunAll");
		System.out.println("");
		System.out.println("--- The below are the more specific commands");
		System.out.println("");
		System.out.println(
				"java -cp odffactories.jar org.odfserver.commands.ListPossibleFactories");
		System.out.println(
				"  No arguments needed, shows what Factories have support");
		System.out.println("");
		System.out.println(
				"java -cp odffactories.jar org.odfserver.commands.Configure");
		System.out.println("  Try to configure a given factory");
		System.out.println(
				"    args example: -c validator.properties -d 1 -h https://localhost:3443 -s ODFValidator");
		System.out.println("");
		System.out.println(
				"java -cp odffactories.jar org.odfserver.commands.RunFactory");
		System.out.println(
				"  Execute a factory using only the config.properties from a configure run");
		System.out.println("    args example: -c validator.properties");
		System.out.println("");
	}
};
