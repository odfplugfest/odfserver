package org.odfserver.commands;

import java.io.File;
import java.io.IOException;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.PosixParser;
import org.eclipse.jdt.annotation.Nullable;
import org.odfserver.OdfFactory;
import org.odfserver.Sniffer;
import org.odfserver.exceptions.ArgumentException;

public class ConfigureRunConfiguration {

	final Sniffer sniffer;
	final String serverHostAndPort;
	final boolean disableEncryption;
	final File configuration;

	protected ConfigureRunConfiguration(Sniffer sniffer,
			String serverHostAndPort, boolean disableEncryption,
			File configuration) {
		this.sniffer = sniffer;
		this.serverHostAndPort = serverHostAndPort;
		this.disableEncryption = disableEncryption;
		this.configuration = configuration;
	}

	static @Nullable File getOptionalReadableFile(Options options,
			CommandLine line, Option option) throws ArgumentException {
		String shorto = option.getOpt();
		File file = null;
		if (line.hasOption(shorto)) {
			file = getReadableFile(options, line, option);
		}
		return file;
	}

	static @Nullable File getOptionalWriteableFile(Options options,
			CommandLine line, Option option) throws ArgumentException {
		String shorto = option.getOpt();
		File file = null;
		if (line.hasOption(shorto)) {
			file = getWriteableFile(options, line, option);
		}
		return file;
	}

	static String getString(Options options, CommandLine line, Option option)
			throws ArgumentException {
		String shorto = option.getOpt();
		return line.getOptionValue(shorto);
	}

	static boolean getBoolean(Options options, CommandLine line, Option option)
			throws ArgumentException {
		String shorto = option.getOpt();
		String v = line.getOptionValue(shorto);
		return "1".equals(v);
	}

	static Sniffer getFactory(Options options, CommandLine line, Option option)
			throws ArgumentException {
		String shorto = option.getOpt();
		String v = line.getOptionValue(shorto);
		try {
			Sniffer f = OdfFactory.getFactory(v);
			return f;
		} catch (ArgumentException e) {
		}
		throw error(options, "Option " + shorto + "/" + option.getLongOpt()
				+ " should be a valid factory name.\n"
				+ "Please run ListPossibleFactories for posible valid values.");
	}

	static File getReadableFile(Options options, CommandLine line,
			Option option) throws ArgumentException {
		String shorto = option.getOpt();
		File file = new File(line.getOptionValue(shorto));
		if (!file.exists() || !file.isFile() || !file.canRead()) {
			throw error(options, "Option " + shorto + "/" + option.getLongOpt()
					+ " should be followed by a readable file path.");
		}
		return file;
	}

	static File getWriteableFile(Options options, CommandLine line,
			Option option) throws ArgumentException {
		String shorto = option.getOpt();
		File file = new File(line.getOptionValue(shorto));
		if (!file.exists()) {
			try {
				file.createNewFile();
			} catch (IOException e) {
				throw error(options, "Option " + shorto + "/"
						+ option.getLongOpt()
						+ " should be followed by a writeable file path.");
			}
		}
		if (!file.exists() || !file.isFile() || !file.canWrite()) {
			throw error(options, "Option " + shorto + "/" + option.getLongOpt()
					+ " should be followed by a writeable file path.");
		}
		return file;
	}

	static File getDir(Options options, CommandLine line, Option option)
			throws ArgumentException {
		String shorto = option.getOpt();
		File dir = new File(line.getOptionValue(shorto));
		if (dir.exists() && !dir.isDirectory()) {
			throw error(options, "Option " + shorto + "/" + option.getLongOpt()
					+ " should be followed by an directory path.");
		} else if (!dir.exists() && !dir.mkdirs()) {
			throw error(options,
					"The directory " + dir.getPath() + " cannot be created.");
		}
		return dir;
	}

	static ArgumentException error(Options options, String msg) {
		System.out.println("Error: " + msg);
		usage(options);
		return new ArgumentException(msg);
	}

	static ConfigureRunConfiguration parseArguments(String[] args)
			throws ArgumentException {// create the command line parser
		CommandLineParser parser = new PosixParser();

		// create the Options
		Options options = new Options();

		Option suiteName = new Option("s", "suite", true,
				"the office suite to try to find");
		suiteName.setRequired(true);
		options.addOption(suiteName);

		Option serverHostAndPort = new Option("h", "host", true,
				"URL for server, eg. https://localhost:3443/");
		serverHostAndPort.setRequired(true);
		options.addOption(serverHostAndPort);

		Option disableEncryption = new Option("d", "disable-encryption", true,
				"for testing, do not verify certificates");
		disableEncryption.setRequired(false);
		options.addOption(disableEncryption);

		Option configuration = new Option("c", "config", true,
				"write a configuration file to this file for this office suite if successful.");
		configuration.setRequired(true);
		options.addOption(configuration);

		// parse the command line arguments
		ConfigureRunConfiguration conf = null;
		try {
			CommandLine line;
			line = parser.parse(options, args);
			Sniffer f = getFactory(options, line, suiteName);
			String hp = getString(options, line, serverHostAndPort);
			boolean de = getBoolean(options, line, disableEncryption);
			File c = getWriteableFile(options, line, configuration);

			conf = new ConfigureRunConfiguration(f, hp, de, c);
		} catch (ParseException e) {
			throw error(options, e.getMessage());
		}
		return conf;
	}

	static void usage(Options options) {
		String cmd = System.getProperty("sun.java.command");
		if (cmd.endsWith(".jar")) {
			cmd = "java -jar " + cmd;
		} else {
			cmd = "java " + cmd;
		}
		HelpFormatter formatter = new HelpFormatter();
		formatter.setWidth(80);
		formatter.printHelp(cmd, options, true);
	}
}