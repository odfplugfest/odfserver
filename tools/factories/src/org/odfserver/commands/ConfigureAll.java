package org.odfserver.commands;

import java.io.File;

import org.odfserver.OdfFactory;
import org.odfserver.Sniffer;
import org.odfserver.exceptions.ArgumentException;

public class ConfigureAll {

	public static void main(String[] args) {
		Configure c = new Configure();
		ConfigureAllRunConfiguration conf;
		try {
			conf = ConfigureAllRunConfiguration.parseArguments(args);
		} catch (ArgumentException e) {
			System.err.println(e);
			return;
		}

		for (Sniffer f : OdfFactory.snifferList) {
			System.out.println("trying: " + f.name);
			if (f.debugOnly) {
				System.out.println(
						"  this is a factory which is not for production...");
				continue;
			}

			File configFile = new File(f.family + " " + f.name + ".properties");

			ConfigureRunConfiguration crc = new ConfigureRunConfiguration(f,
					conf.serverHostAndPort, conf.disableEncryption, configFile);

			c.run(configFile.getPath(), crc);
		}

		System.out.println("");
		System.out.println(
				"Please use the following command to start all the configured factories");
		System.out.println("");
		System.out.println(
				"java -cp odffactories.jar org.odfserver.commands.RunAll");
	}

};
