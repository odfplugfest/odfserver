package org.odfserver.commands;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.eclipse.jdt.annotation.Nullable;
import org.odfserver.OdfFactory;
import org.odfserver.PlatformInformation;
import org.odfserver.ServerConfig;
import org.odfserver.exceptions.ArgumentException;
import org.odfserver.exceptions.NoFactoryException;
import org.odfserver.types.ServiceInstance;

public class RunAll {

	public static void main(String[] args) {
		System.out.println(
				PlatformInformation.osName + " " + PlatformInformation.osVersion
						+ " " + PlatformInformation.osPlatform);
		try {
			Map<ServiceInstance, OdfFactory> factories = new HashMap<ServiceInstance, OdfFactory>();
			ServerConfig config = null;

			File cwd = Paths.get("").toAbsolutePath().toFile();
			File files[] = cwd.listFiles(new FilenameFilter() {
				public boolean accept(@Nullable File dir,
						@Nullable String name) {
					return name != null && name.endsWith(".properties");
				}
			});
			for (File configFile : files) {
				Properties properties = Configure.readProperties(configFile);
				if (config == null
						&& properties.containsKey("serverHostAndPort")) {
					config = ServerConfig.configRead(properties);
				}
				RunFactory.loadFactory(properties, factories);
			}
			if (config != null && factories.size() > 0) {
				RunFactory r = new RunFactory(config,
						Collections.unmodifiableMap(factories));
				r.run();
			}
		} catch (ArgumentException | IOException | NoFactoryException e) {
			System.err.println(e);
		}
	}

};
