package org.odfserver;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Properties;

import javax.json.JsonObject;

public class ServerConfig {
	public final String m_serverHostAndPort;
	public final boolean m_disableEncryption;
	public final String m_factoryKey;
	public final String m_factoryId;

	private ServerConfig(Properties p) throws IOException {
		m_serverHostAndPort = p.getProperty("serverHostAndPort");
		m_disableEncryption = "true".equals(p.getProperty("disableEncryption"));
		final String factoryKey = p.getProperty("factoryKey", "");
		final String factoryId = p.getProperty("factoryId", "");
		if (("".equals(factoryKey) || "".equals(factoryId))) {
			ServerConfig tmpConfig = new ServerConfig(m_serverHostAndPort,
					m_disableEncryption);
			JsonObject reply = Server.postRegister(tmpConfig);
			m_factoryKey = reply.getString("factoryKey");
			m_factoryId = reply.getString("factoryId");
		} else {
			m_factoryKey = factoryKey;
			m_factoryId = factoryId;
		}
	}

	private ServerConfig(String serverHostAndPort, boolean disableEncryption) {
		m_serverHostAndPort = serverHostAndPort;
		m_disableEncryption = disableEncryption;
		m_factoryKey = m_factoryId = "";
	}

	public void write(String configPath) throws IOException {
		Properties p = new Properties();
		write(p);
		OutputStream output = new FileOutputStream(configPath);
		p.store(output, null);
	}

	public void write(Properties p) {
		p.setProperty("serverHostAndPort", m_serverHostAndPort);
		p.setProperty("disableEncryption", String.valueOf(m_disableEncryption));
		p.setProperty("factoryKey", m_factoryKey);
		p.setProperty("factoryId", m_factoryId);
	}

	public static ServerConfig configRead(Properties properties)
			throws IOException {
		return new ServerConfig(properties);
	}
}