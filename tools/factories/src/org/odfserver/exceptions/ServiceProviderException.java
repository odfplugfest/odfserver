package org.odfserver.exceptions;

/**
 * An issue has occurred interacting with a web service that is used to complete
 * an action. For example, if you can not upload a document to a web service.
 *
 */
public class ServiceProviderException extends Exception {
	private static final long serialVersionUID = 2289722464605670683L;

	public ServiceProviderException(String msg) {
		super(msg);
	}

}
