package org.odfserver.exceptions;

public class ArgumentException extends Exception {
	private static final long serialVersionUID = 2289722464605670683L;

	public ArgumentException(String msg) {
		super(msg);
	}

}
