package org.odfserver.exceptions;

/**
 * An issue has occurred interacting with a web service that is used to complete
 * an action. For example, if you can not upload a document to a web service.
 *
 */
public class ConversionFailedException extends Exception {

	private static final long serialVersionUID = 8595960875265464901L;

	public ConversionFailedException(String msg) {
		super(msg);
	}

}
