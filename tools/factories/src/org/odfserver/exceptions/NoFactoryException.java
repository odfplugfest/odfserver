package org.odfserver.exceptions;

public class NoFactoryException extends Exception {
	private static final long serialVersionUID = 2289722464605670683L;

	public NoFactoryException(String msg) {
		super(msg);
	}

	public NoFactoryException(String msg, Exception e) {
		super(msg, e);
	}

}
