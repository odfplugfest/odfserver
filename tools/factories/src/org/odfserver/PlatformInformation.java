
package org.odfserver;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.jdt.annotation.Nullable;
import org.odfserver.types.Software;

/**
 * This class contains the methods that relate to putting and getting JSON to
 * the odf server. More specific designs like handling process execution and
 * detecting office suites for these factory implementations are moved to
 * OdfFactory instead.
 */
public class PlatformInformation {

	public static final String osName = getOSName();

	private static String getOSName() {
		String osname = null;
		try {
			osname = getWindowsInformation().get("Caption");
			if (osname == null || "".equals(osname)) {
				osname = getInformation("lsb_release", "-si");
			}
			if (osname == null) {
				osname = getInformation("uname", "-o");
			}
		} catch (IOException | InterruptedException e) {
			System.err.println(e);
		}
		if (osname == null) {
			throw new Error(
					"Cannot determine the name of the operating system.");
		}
		return osname;
	}

	public static boolean isWindows() {
		String o = getOSName();
		return o.contains("Microsoft") && o.contains("Windows");
	}

	public static final String osVersion = getOSVersion();

	private static String getOSVersion() {
		String osversion = null;
		try {
			osversion = getWindowsInformation().get("Version");
			if (osversion == null || "".equals(osversion)) {
				osversion = getInformation("lsb_release", "-sr");
			}
			if (osversion == null) {
				osversion = getInformation("uname", "-r");
			}
		} catch (IOException | InterruptedException e) {
			System.err.println(e);
		}
		if (osversion == null) {
			throw new Error(
					"Cannot determine the version of the operating system.");
		}
		return osversion;
	}

	private static String getOSPlatform() {
		String osplatform = null;
		try {
			osplatform = getWindowsInformation().get("OSArchitecture");
			if (osplatform == null || "".equals(osplatform)) {
				osplatform = getInformation("uname", "-m");
			}
			// normalize the platform description
			if ("64-bit".equals(osplatform)) {
				osplatform = "x86_64";
			}
		} catch (IOException | InterruptedException e) {
			System.err.println(e);
		}
		if (osplatform == null) {
			throw new Error("Cannot determine the compute platform.");
		}
		return osplatform;
	}

	public static final String osPlatform = getOSPlatform();

	@Nullable
	private static String getInformation(String exe, String arg)
			throws IOException, InterruptedException {
		String value = null;
		String path = Runner.resolveExe(exe);
		if (path != null) {
			Runner r = new Runner();
			ProcessBuilder pb = new ProcessBuilder(path, arg);
			r.executeProcess(null, pb, true);
			value = Utility.chomp(r.getStdOut());
		}
		return "".equals(value) ? null : value;
	}

	// possible fields are Caption,CSDVersion,Version,OSArchitecture,Name
	private static Map<String, String> getWindowsInformation()
			throws IOException {
		final Map<String, String> p = new HashMap<String, String>();
		String path = Runner.resolveExe("wmic");
		if (path != null) {
			String s[] = Utility.executeProcessToStrings(
					new ProcessBuilder(path, "OS", "get", "/Value").start());
			for (String str : s) {
				String fields[] = str.split("=", 2);
				if (fields.length == 2) {
					p.put(Utility.chomp(fields[0]), Utility.chomp(fields[1]));
				}
			}
		}
		return p;
	}

	public static Software createSoftware(String softwareFamily,
			String software, String version) {
		return new Software(softwareFamily, software, version, osName,
				osVersion, osPlatform);
	}
}
