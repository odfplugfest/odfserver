
package org.odfserver.factories;

import java.io.IOException;
import java.time.Instant;
import java.util.List;

import org.odfserver.OdfFactoryConfig;
import org.odfserver.Sniffer;
import org.odfserver.exceptions.ArgumentException;
import org.odfserver.exceptions.NoFactoryException;
import org.odfserver.types.FileType;
import org.odfserver.types.Service;

public class CalligraWordsSniffer extends Sniffer {

	static final String family = "Calligra";

	public CalligraWordsSniffer() {
		super(CalligraWordsSniffer.family, "Words", true, false);
	}

	public CalligraWordsFactory createFactory(OdfFactoryConfig config)
			throws ArgumentException, IOException {
		String appPath = config.appPath;
		if (appPath == null) {
			throw new ArgumentException("No path was specified.");
		}
		return new CalligraWordsFactory(appPath, config);
	}

	public final List<Service> services = a(
			new FileType[] { FileType.ODT1_0, FileType.ODT1_1, FileType.ODT1_2,
					FileType.ODT1_2EXT },
			new FileType[] { FileType.ODT1_2EXT, FileType.PDF });

	public List<Service> getServices() {
		return services;
	}

	static public OdfFactoryConfig sniff(String name, String exename,
			String versionName) throws NoFactoryException {
		String appPath;
		long detectionTime;
		String version;
		boolean enabled;
		try {
			appPath = getExecutableFullPath(exename);
			detectionTime = Instant.now().getEpochSecond();
			version = CalligraWordsFactory.getVersion(appPath, exename, versionName);
			runSanityCheckConversion();
			enabled = true;
		} catch (IOException e) {
			enabled = false;
			throw new NoFactoryException("Cannot determine version.", e);
		}
		return new OdfFactoryConfig(CalligraWordsSniffer.family, name, appPath, detectionTime,
				version, enabled);
	}

	public OdfFactoryConfig sniff() throws NoFactoryException {
		return CalligraWordsSniffer.sniff(name,
				CalligraWordsFactory.exeName,
				CalligraWordsFactory.versionName);
	}
};
