
package org.odfserver.factories;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

import org.odfserver.OdfFactory;
import org.odfserver.OdfFactoryConfig;
import org.odfserver.Utility;
import org.odfserver.Waiter;
import org.odfserver.exceptions.ArgumentException;
import org.odfserver.exceptions.NoFactoryException;
import org.odfserver.types.FileType;
import org.odfserver.types.JobResult;
import org.odfserver.types.RetrieveJob;
import org.odfserver.types.TempDir;

import com.jacob.activeX.ActiveXComponent;
import com.jacob.com.Dispatch;
import com.jacob.com.Variant;

public class MicrosoftExcelFactory extends OdfFactory {

	static final int excelOdsFormat = 60;
	static final int excelPdfFormat = 0;

	protected MicrosoftExcelFactory(OdfFactoryConfig config) {
		super(config);
	}

	public boolean checkVersion(String version) throws NoFactoryException {
		return version.equals(MicrosoftWordSniffer.getVersion());
	}

	static String convertOds(String inputDoc, String outputDoc,
			int fileFormat) {
		final Waiter waiter = new Waiter(Thread.currentThread(), 60000);
		final boolean saveOnExit = false;
		final boolean excelVisible = false;
		String stderr = "";
		ActiveXComponent excel = new ActiveXComponent("Excel.Application");
		try {
			excel.setProperty("Visible", new Variant(excelVisible));
			Dispatch document;
			{
				Dispatch documents = excel.getProperty("Workbooks")
						.toDispatch();
				// Always update links for this workbook on opening
				Variant updateLinks = new Variant(3);
				Variant readOnly = new Variant(false);
				Variant format = Variant.VT_MISSING;
				Variant password = Variant.VT_MISSING;
				Variant writeResPassword = Variant.VT_MISSING;
				Variant ignoreReadOnlyRecommended = new Variant(true);
				Variant origin = Variant.VT_MISSING;
				Variant delimiter = Variant.VT_MISSING;
				Variant editable = Variant.VT_MISSING;
				Variant notify = Variant.VT_MISSING;
				Variant converter = Variant.VT_MISSING;
				Variant addToMru = Variant.VT_MISSING;
				Variant local = new Variant(true);
				Variant corruptLoad = new Variant(1); // xlRepairFile
				document = Dispatch.call(documents, "Open", inputDoc,
						updateLinks, readOnly, format, password,
						writeResPassword, ignoreReadOnlyRecommended, origin,
						delimiter, editable, notify, converter, addToMru, local,
						corruptLoad).toDispatch();
			}
			new File(outputDoc).delete();
			try {
				if (fileFormat == excelPdfFormat) {
					// make sure that there is some content, otherwise excel
					// will complain that there is nothing to print
					Dispatch sheet = Dispatch.get(document, "ActiveSheet")
							.toDispatch();
					Dispatch cell = Dispatch
							.invoke(sheet, "Range", Dispatch.Get,
									new Object[] { "A1" }, new int[1])
							.toDispatch();
					Variant value = Dispatch.get(cell, "Value");
					String stringValue = null;
					if (value != null
							&& value.getvt() == Variant.VariantString) {
						stringValue = value.getString();
					}
					if (stringValue == null || "".equals(stringValue)) {
						Dispatch.put(cell, "Value", " ");
					}
					Variant quality = Variant.VT_MISSING;
					Variant includeDocProperties = new Variant(true);
					Variant ignorePrintAreas = Variant.VT_MISSING;
					Variant from = Variant.VT_MISSING;
					Variant to = Variant.VT_MISSING;
					Variant openAfterPublish = new Variant(false);
					Variant fixedFormatExtClassPtr = Variant.VT_MISSING;
					Dispatch.call(document, "ExportAsFixedFormat",
							new Variant(fileFormat), new Variant(outputDoc),
							quality, includeDocProperties, ignorePrintAreas,
							from, to, openAfterPublish, fixedFormatExtClassPtr);
				} else {
					Variant password = Variant.VT_MISSING;
					Variant writeResPassword = Variant.VT_MISSING;
					Variant readOnlyRecommended = new Variant(false);
					Variant createBackup = new Variant(false);
					Variant accessMode = Variant.VT_MISSING;
					Variant conflictResolution = Variant.VT_MISSING;
					Variant addToMru = Variant.VT_MISSING;
					Variant textCodepage = Variant.VT_MISSING;
					Variant textVisualLayout = Variant.VT_MISSING;
					Variant local = new Variant(true);
					Dispatch.call(document, "SaveAs", new Variant(outputDoc),
							new Variant(fileFormat), password, writeResPassword,
							readOnlyRecommended, createBackup, accessMode,
							conflictResolution, addToMru, textCodepage,
							textVisualLayout, local);
				}
			} finally {
				try {
					// set to true, so no dialog is shown to save the modified
					// document
					Dispatch.put(document, "Saved", true);
				} catch (RuntimeException e) {
					stderr += Utility.getStackTraceAsString(e);
				}
				try {
					Variant filename = Variant.VT_MISSING;
					Variant routeWorkbook = Variant.VT_MISSING;
					Dispatch.call(document, "Close", new Variant(saveOnExit),
							filename, routeWorkbook);
				} catch (RuntimeException e) {
					stderr += Utility.getStackTraceAsString(e);
				}
			}
		} finally {
			try {
				Dispatch.call(excel, "Quit");
			} catch (RuntimeException e) {
				stderr += Utility.getStackTraceAsString(e);
			}
			waiter.stop();
			Utility.killWindowsProcess("EXCEL.EXE");
		}
		return stderr;
	}

	public JobResult perform(RetrieveJob job)
			throws ArgumentException, IOException {
		int fileFormat;
		if (job.service.outputType.equals(FileType.PDF)) {
			fileFormat = excelPdfFormat;
		} else if (job.service.outputType.isOds()) {
			fileFormat = excelOdsFormat;
		} else {
			throw new ArgumentException("Unsupported output type.");
		}
		TempDir dir = createTemporaryDirectory(job);
		JobResult result;
		byte outputFiles[][] = JobResult.noFiles;
		try {
			convertOds(dir.inputPath.toString(), dir.outputPath.toString(),
					fileFormat);
			outputFiles = new byte[][] { Files.readAllBytes(dir.outputPath) };
			result = new JobResult("ActiveX", outputFiles, 0, "", "", false);
		} finally {
			deleteDirAndContents(dir);
		}
		return result;
	}
};
