package org.odfserver.factories;

import java.io.IOException;

import org.odfserver.OdfFactoryConfig;
import org.odfserver.OdfFactoryFailBase;
import org.odfserver.types.JobResult;
import org.odfserver.types.RetrieveJob;

public class FailTimeoutFactory extends OdfFactoryFailBase {

	public FailTimeoutFactory(OdfFactoryConfig config) {
		super(config);
	}

	public String getName() {
		return "FailTimeout";
	}

	public JobResult perform(RetrieveJob job) throws IOException {
		// just wait for ages here.
		System.err.println(
				"The FailTimeoutFactory is going to start an \"Office Suite\" that will wait for an hour");
		System.err.println(
				" this should be detected by the code after a delay and aborted instead.");
		return failJob("--wait", job);
	}

};
