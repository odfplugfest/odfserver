
package org.odfserver.factories;

import java.io.IOException;
import java.nio.file.Files;

import org.odfserver.OdfFactory;
import org.odfserver.OdfFactoryConfig;
import org.odfserver.Utility;
import org.odfserver.Waiter;
import org.odfserver.exceptions.ArgumentException;
import org.odfserver.exceptions.NoFactoryException;
import org.odfserver.types.FileType;
import org.odfserver.types.JobResult;
import org.odfserver.types.RetrieveJob;
import org.odfserver.types.TempDir;

import com.jacob.activeX.ActiveXComponent;
import com.jacob.com.Dispatch;
import com.jacob.com.Variant;

public class MicrosoftWordFactory extends OdfFactory {

	static final int wordPdfFormat = 17;
	static final int wordOdtFormat = 23;

	protected MicrosoftWordFactory(OdfFactoryConfig config) {
		super(config);
	}

	public boolean checkVersion(String version) throws NoFactoryException {
		return version.equals(MicrosoftWordSniffer.getVersion());
	}

	static String convertOdt(String inputDoc, String outputDoc,
			int fileFormat) {
		final Waiter waiter = new Waiter(Thread.currentThread(), 60000);
		final boolean saveOnExit = false;
		final boolean wordVisible = false;
		String stderr = "";
		ActiveXComponent word = new ActiveXComponent("Word.Application");
		try {
			word.setProperty("Visible", new Variant(wordVisible));
			Dispatch documents = word.getProperty("Documents").toDispatch();
			Dispatch document;
			{
				Variant confirmConversions = new Variant(false);
				Variant readOnly = new Variant(false);
				Variant addToRecentFiles = new Variant(false);
				Variant passwordDocument = Variant.VT_MISSING;
				Variant passwordTemplate = Variant.VT_MISSING;
				Variant revert = new Variant(true);
				Variant writePasswordDocument = Variant.VT_MISSING;
				Variant writePasswordTemplate = Variant.VT_MISSING;
				Variant format = Variant.VT_MISSING;
				Variant encoding = Variant.VT_MISSING;
				Variant visible = new Variant(false);
				Variant openAndRepair = new Variant(true);
				Variant documentDirection = Variant.VT_MISSING;
				Variant noEncodingDialog = new Variant(false);
				Variant xmlTransform = Variant.VT_MISSING;
				document = Dispatch.call(documents, "Open", inputDoc,
						confirmConversions, readOnly, addToRecentFiles,
						passwordDocument, passwordTemplate, revert,
						writePasswordDocument, writePasswordTemplate, format,
						encoding, visible, openAndRepair, documentDirection,
						noEncodingDialog, xmlTransform).toDispatch();
			}
			try {
				Variant lockComments = Variant.VT_MISSING;//new Variant(false);
				Variant password = Variant.VT_MISSING;
				Variant addToRecentFiles = Variant.VT_MISSING;//new Variant(false);
				Variant writePassword = Variant.VT_MISSING;
				Variant readOnlyRecommended = Variant.VT_MISSING;//new Variant(false);
				Variant embedTrueTypeFonts = Variant.VT_MISSING;//new Variant(true);
				Variant saveNativePictureFormat = Variant.VT_MISSING;//new Variant(false);
				Variant saveFormsData = Variant.VT_MISSING;//new Variant(true);
				Variant saveAsAOCELetter = Variant.VT_MISSING;//new Variant(false);
				Variant encoding = Variant.VT_MISSING;
				Variant insertLineBreaks = Variant.VT_MISSING;//new Variant(false);
				Variant allowSubstitutions = Variant.VT_MISSING;//new Variant(false);
				Variant lineEnding = Variant.VT_MISSING;
				Variant addBiDiMarks = Variant.VT_MISSING;//new Variant(true);
				Variant compatibilityMode = Variant.VT_MISSING;
				Dispatch.call(document, "SaveAs2", new Variant(outputDoc),
						new Variant(fileFormat), lockComments, password,
						addToRecentFiles, writePassword, readOnlyRecommended,
						embedTrueTypeFonts, saveNativePictureFormat,
						saveFormsData, saveAsAOCELetter, encoding,
						insertLineBreaks, allowSubstitutions, lineEnding,
						addBiDiMarks, compatibilityMode);
			} finally {
				try {
					// set to true, so no dialog is shown to save the modified
					// document
					Dispatch.put(document, "Saved", true);
				} catch (RuntimeException e) {
					stderr += Utility.getStackTraceAsString(e);
				}
				try {
					Dispatch.call(document, "Close", new Variant(saveOnExit));
				} catch (RuntimeException e) {
					stderr += Utility.getStackTraceAsString(e);
				}
			}
		} finally {
			try {
				Dispatch.call(word, "Quit");
			} catch (RuntimeException e) {
				stderr += Utility.getStackTraceAsString(e);
			}
			waiter.stop();
			Utility.killWindowsProcess("WINWORD.EXE");
		}
		return stderr;
	}

	public JobResult perform(RetrieveJob job)
			throws ArgumentException, IOException {
		int fileFormat;
		if (job.service.outputType.isOdt()) {
			fileFormat = wordOdtFormat;
		} else if (job.service.outputType.equals(FileType.PDF)) {
			fileFormat = wordPdfFormat;
		} else {
			throw new ArgumentException(
					"Unsupported output type for your specified input type.");
		}
		TempDir dir = createTemporaryDirectory(job);
		JobResult result;
		byte outputFiles[][] = JobResult.noFiles;
		String stderr = "";
		try {
			stderr = convertOdt(dir.inputPath.toString(),
					dir.outputPath.toString(), fileFormat);
			outputFiles = new byte[][] { Files.readAllBytes(dir.outputPath) };
			result = new JobResult("ActiveX", outputFiles, 0, "", stderr,
					false);
		} finally {
			deleteDirAndContents(dir);
		}
		return result;
	}
};
