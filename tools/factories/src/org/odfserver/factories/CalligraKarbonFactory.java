
package org.odfserver.factories;

import java.io.IOException;

import org.odfserver.OdfFactory;
import org.odfserver.OdfFactoryConfig;
import org.odfserver.exceptions.ArgumentException;
import org.odfserver.types.FileType;
import org.odfserver.types.JobResult;
import org.odfserver.types.RetrieveJob;
import org.odfserver.types.TempDir;

public class CalligraKarbonFactory extends OdfFactory {

	public final String appPath;
	public static final String versionName = "Karbon";
	static public final String exeName = "karbon";

	protected CalligraKarbonFactory(String appPath, OdfFactoryConfig config)
			throws IOException {
		super(config);
		this.appPath = appPath;
	}

	public boolean checkVersion(String version) throws IOException {
		return version
				.equals(CalligraWordsFactory.getVersion(appPath, exeName, versionName));
	}

	public JobResult perform(RetrieveJob job)
			throws ArgumentException, IOException {
		if (!job.service.inputType.isOdg()) {
			throw new ArgumentException("Unsupported input type.");
		}
		JobResult result;
		TempDir dir = createTemporaryDirectory(job);
		try {
			ProcessBuilder pb;
			if (job.service.outputType.equals(FileType.ODG1_2EXT)) {
				pb = new ProcessBuilder(appPath, "--roundtrip-filename",
						dir.outputPath.toString(), dir.inputPath.toString());
			} else if (job.service.outputType.equals(FileType.PDF)) {
				pb = new ProcessBuilder(appPath, "--export-pdf",
						"--export-filename", dir.outputPath.toString(),
						dir.inputPath.toString());
			} else {
				throw new ArgumentException("Unsupported output type.");
			}
			result = executeProcess(dir,
					CalligraWordsFactory.environmentToPassAlong, pb);
		} finally {
			deleteDirAndContents(dir);
		}
		return result;
	}
};
