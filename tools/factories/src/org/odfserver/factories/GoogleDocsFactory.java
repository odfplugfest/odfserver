
package org.odfserver.factories;

import java.io.IOException;
import java.nio.file.Files;
import java.util.regex.Pattern;

import org.odfserver.OdfFactory;
import org.odfserver.OdfFactoryConfig;
import org.odfserver.Runner;
import org.odfserver.Utility;
import org.odfserver.exceptions.ArgumentException;
import org.odfserver.exceptions.ServiceProviderException;
import org.odfserver.types.FileType;
import org.odfserver.types.JobResult;
import org.odfserver.types.RetrieveJob;
import org.odfserver.types.TempDir;

public class GoogleDocsFactory extends OdfFactory {

	public final String appPath;

	protected GoogleDocsFactory(String appPath, OdfFactoryConfig config) {
		super(config);
		this.appPath = appPath;
	}

	public boolean checkVersion(String version) throws IOException {
		return true;
	}

	public final String[] environmentToPassAlong = { "HOME" };

	public JobResult perform(RetrieveJob job)
			throws ArgumentException, IOException {
		if (!job.service.outputType.equals(FileType.ODT1_2EXT)
				&& !job.service.outputType.equals(FileType.ODS1_2EXT)
				&& !job.service.outputType.equals(FileType.PDF)) {
			throw new ArgumentException("Unsupported output type.");
		}

		TempDir dir = createTemporaryDirectory(job);
		String id = null;
		String stdout = "";
		String stderr = "";
		try {
			Runner runner = new Runner();
			try {
				ProcessBuilder pb = new ProcessBuilder(appPath, "import",
						"--no-progress", "--mime",
						job.service.inputType.mimeType(),
						dir.inputPath.toString());
				runner.setEnvironmentToPassAlong(environmentToPassAlong);
				runner.executeProcess(dir.outputPath, pb, true);
				id = Utility.pickSelectedFromMultiLineString(runner.getStdOut(),
						Pattern.compile(
								".*Imported (?<selected>.*) with mime type.*"));
			} catch (InterruptedException e) {
				stdout += runner.getStdOut();
				stderr += runner.getStdErr()
						+ "\nFailed to upload document to Google Docs. Reason: "
						+ stderr;
				return new JobResult(runner.cmd(), JobResult.noFiles,
						runner.getExitCode(), stdout, stderr,
						runner.isTimeout());
			}
			stdout += runner.getStdOut();
			stderr += runner.getStdErr();
			if (id == null || id.equals("")) {
				stderr += "\nNo ID was returned after uploading document to Google Docs.";
				return new JobResult(runner.cmd(), JobResult.noFiles,
						runner.getExitCode(), stdout, stderr,
						runner.isTimeout());
			}
			String path;
			try {
				runner = new Runner();
				ProcessBuilder pb = new ProcessBuilder(appPath, "export", "-f",
						"--mime", job.service.outputType.mimeType(), id);
				pb.directory(dir.directoryPath.toFile());
				runner.setEnvironmentToPassAlong(environmentToPassAlong);
				runner.executeProcess(dir.outputPath, pb, true);
				path = Utility.pickSelectedFromMultiLineString(
						runner.getStdOut(), Pattern.compile(
								".*Exported '(?<selected>.*)' with mime type.*"));
			} catch (InterruptedException e) {
				stdout += runner.getStdOut();
				stderr += runner.getStdErr()
						+ "\nFailed to export document from Google Docs. Reason: "
						+ stderr;
				return new JobResult(runner.cmd(), JobResult.noFiles,
						runner.getExitCode(), stdout, stderr,
						runner.isTimeout());
			}
			stdout += runner.getStdOut();
			stderr += runner.getStdErr();
			if (path == null || "".equals(path)) {
				stderr += "\nNo exported file path was given.";
				return new JobResult(runner.cmd(), JobResult.noFiles,
						runner.getExitCode(), stdout, stderr,
						runner.isTimeout());
			}
			byte outputFiles[][] = new byte[][] {
					Files.readAllBytes(dir.directoryPath.resolve(path)) };
			return new JobResult(runner.cmd(), outputFiles,
					runner.getExitCode(), stdout, stderr, runner.isTimeout());
		} finally {
			deleteDirAndContents(dir);
			// we should try hard to clean up after ourselves on the Google
			// Drive too
			try {
				if (id != null && !"".equals(id)) {
					ProcessBuilder pb = new ProcessBuilder(appPath, "delete",
							id);
					Runner runner = new Runner();
					runner.setEnvironmentToPassAlong(environmentToPassAlong);
					runner.executeProcess(null, pb, true);
					if (runner.getExitCode() != 0) {
						throw new ServiceProviderException(
								"Failed to delete document on Google Docs. Reason: "
										+ runner.getStdErr());
					}
				}
			} catch (IOException | InterruptedException
					| ServiceProviderException e) {
				e.printStackTrace();
			}
		}
	}
};
