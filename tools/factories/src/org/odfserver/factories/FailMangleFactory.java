package org.odfserver.factories;

import java.io.IOException;

import org.odfserver.OdfFactoryConfig;
import org.odfserver.OdfFactoryFailBase;
import org.odfserver.types.JobResult;
import org.odfserver.types.RetrieveJob;

public class FailMangleFactory extends OdfFactoryFailBase {

	public FailMangleFactory(OdfFactoryConfig config) {
		super(config);
	}

	public String getName() {
		return "FailMangle";
	}

	public JobResult perform(RetrieveJob job) throws IOException {
		// just wait for ages here.
		System.err.println(
				"The FailMangleFactory is going to start an \"Office Suite\" that will mangle the input");
		System.err.println(
				" this should be sent right on back to the server to deal with.");
		return failJob("--mangle", job);
	}

};
