
package org.odfserver.factories;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

import org.odfserver.OdfFactory;
import org.odfserver.OdfFactoryConfig;
import org.odfserver.Utility;
import org.odfserver.Waiter;
import org.odfserver.exceptions.ArgumentException;
import org.odfserver.exceptions.NoFactoryException;
import org.odfserver.types.FileType;
import org.odfserver.types.JobResult;
import org.odfserver.types.RetrieveJob;
import org.odfserver.types.TempDir;

import com.jacob.activeX.ActiveXComponent;
import com.jacob.com.Dispatch;
import com.jacob.com.Variant;

public class MicrosoftPowerPointFactory extends OdfFactory {

	static final Variant msoTrue = new Variant(-1);
	static final Variant msoFalse = new Variant(0);

	static final int powerpointPdfFormat = 32;
	static final int powerpointOdfFormat = 35;

	protected MicrosoftPowerPointFactory(OdfFactoryConfig config) {
		super(config);
	}

	public boolean checkVersion(String version) throws NoFactoryException {
		return version.equals(MicrosoftWordSniffer.getVersion());
	}

	static String convertOdp(String inputDoc, String outputDoc,
			int fileFormat) {
		final Waiter waiter = new Waiter(Thread.currentThread(), 60000);
		final Variant embedFonts = msoTrue;
		ActiveXComponent powerpoint = new ActiveXComponent(
				"Powerpoint.Application");
		String stderr = "";
		try {
			Dispatch documents = powerpoint.getProperty("Presentations")
					.toDispatch();
			Dispatch document;
			{
				Variant readOnly = msoFalse;
				Variant untitled = Variant.VT_MISSING;
				Variant withWindow = msoFalse;
				Variant openAndRepair = msoTrue;
				document = Dispatch
						.call(documents, "Open2007", inputDoc, readOnly,
								untitled, withWindow, openAndRepair)
						.toDispatch();
			}
			new File(outputDoc).delete();
			try {
				Dispatch.call(document, "SaveAs", new Variant(outputDoc),
						new Variant(fileFormat), embedFonts);
			} finally {
				try {
					// set to true, so no dialog is shown to save the modified
					// document
					Dispatch.put(document, "Saved", true);
				} catch (RuntimeException e) {
					stderr += Utility.getStackTraceAsString(e);
				}
				try {
					Dispatch.call(document, "Close");
				} catch (RuntimeException e) {
					stderr += Utility.getStackTraceAsString(e);
				}
				document.safeRelease();
				documents.safeRelease();
			}
		} finally {
			try {
				Dispatch.call(powerpoint, "Quit");
			} catch (RuntimeException e) {
				stderr += Utility.getStackTraceAsString(e);
			}
			waiter.stop();
			powerpoint.safeRelease();
			Utility.killWindowsProcess("POWERPNT.EXE");
		}
		return stderr;
	}

	public JobResult perform(RetrieveJob job)
			throws ArgumentException, IOException {
		int fileFormat;
		if (job.service.outputType.equals(FileType.PDF)) {
			fileFormat = powerpointPdfFormat;
		} else if (job.service.outputType.isOdp()) {
			fileFormat = powerpointOdfFormat;
		} else {
			throw new ArgumentException("Unsupported output type.");
		}
		TempDir dir = createTemporaryDirectory(job);
		JobResult result;
		byte outputFiles[][] = JobResult.noFiles;
		try {
			String stderr = convertOdp(dir.inputPath.toString(),
					dir.outputPath.toString(), fileFormat);
			outputFiles = new byte[][] { Files.readAllBytes(dir.outputPath) };
			result = new JobResult("ActiveX", outputFiles, 0, "", stderr,
					false);
		} finally {
			deleteDirAndContents(dir);
		}
		return result;
	}
};
