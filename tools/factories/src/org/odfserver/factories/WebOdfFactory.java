
package org.odfserver.factories;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.odfserver.OdfFactory;
import org.odfserver.OdfFactoryConfig;
import org.odfserver.Utility;
import org.odfserver.exceptions.ArgumentException;
import org.odfserver.types.FileType;
import org.odfserver.types.JobResult;
import org.odfserver.types.RetrieveJob;
import org.odfserver.types.TempDir;

public class WebOdfFactory extends OdfFactory {
	public final String appPath;

	protected WebOdfFactory(String appPath, OdfFactoryConfig config) {
		super(config);
		this.appPath = appPath;
	}

	public boolean checkVersion(String version) throws IOException {
		return "0.5.10".equals(version);
	}

	public final String[] environmentToPassAlong = { "DISPLAY", "XAUTHORITY",
			"KDEDIRS", "HOME" };

	public JobResult perform(RetrieveJob job)
			throws ArgumentException, IOException {
		JobResult result;
		TempDir dir = createTemporaryDirectory(job);
		try {
			FileType outType = job.service.outputType;
			String exportodf = String.valueOf(
					outType.isOdp() || outType.isOds() || outType.isOdt());
			String exportFormat = "--export-pdf";
			String template = new String(
					Utility.readResource(
							"/org/odfserver/factories/webodf-index.html.in"),
					"UTF-8");
			Path customPagePath = dir.directoryPath.resolve("thepage.html");
			Path webodfDir = Paths.get(appPath).getParent();
			Path webodfjs = webodfDir.resolve("../../webodf/webodf.js");
			if (!Files.exists(webodfjs)) {
				throw new IOException("File " + webodfjs + " does not exist.");
			}

			template = template.replaceAll("\\$THEODFOUTPUTPATH",
					dir.outputPath.toString());
			template = template.replaceAll("\\$WEBODFJSPATH",
					webodfjs.toString());
			template = template.replaceAll("\\$THEODFFILEPATH",
					dir.inputPath.toString());
			template = template.replaceAll("\\$EXPORTODF", exportodf);

			Files.write(customPagePath, template.getBytes());

			// We always produce a pdf file as a side effect if we want a
			// pdf to send back then the side effect is the main show,
			// otherwise just produce and throw away the pdf.
			String pdfFilePath = dir.outputPath.toString();
			if (pdfFilePath.indexOf(".") != -1)
				pdfFilePath = pdfFilePath.substring(0,
						pdfFilePath.lastIndexOf("."));
			pdfFilePath += ".pdf";
			ProcessBuilder pb = new ProcessBuilder(appPath, exportFormat,
					pdfFilePath, customPagePath.toString());
			result = executeProcess(dir, environmentToPassAlong, pb);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			deleteDirAndContents(dir);
		}
		return result;
	}
};
