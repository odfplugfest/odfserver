
package org.odfserver.factories;

import java.io.IOException;

import org.odfserver.OdfFactory;
import org.odfserver.OdfFactoryConfig;
import org.odfserver.exceptions.ArgumentException;
import org.odfserver.types.FileType;
import org.odfserver.types.JobResult;
import org.odfserver.types.RetrieveJob;

public class LibreOfficeWriterFactory extends OdfFactory {

	public final String appPath;

	protected LibreOfficeWriterFactory(String appPath, OdfFactoryConfig config) throws IOException {
		super(config);
		this.appPath = appPath;
	}

	public boolean checkVersion(String version) throws IOException {
		return version.equals(LibreOfficeUtils.getVersion(appPath));
	}

	public JobResult perform(RetrieveJob job)
			throws ArgumentException, IOException {
		if (!job.service.inputType.isOdt()) {
			throw new ArgumentException("Unsupported input type.");
		}
		String filter;
		if (job.service.outputType.isOdt()) {
			filter = "odt:writer8";
		} else if (job.service.outputType.equals(FileType.PDF)) {
			filter = "pdf:writer_pdf_Export";
		} else {
			throw new ArgumentException("Unsupported output type.");
		}
		return LibreOfficeUtils.perform(appPath, job, filter);
	}
};
