
package org.odfserver.factories;

import java.time.Instant;
import java.util.List;

import org.odfserver.OdfFactoryConfig;
import org.odfserver.Sniffer;
import org.odfserver.exceptions.ArgumentException;
import org.odfserver.types.FileType;
import org.odfserver.types.Service;
import org.odfserver.types.ServiceType;

public class PdfRendererSniffer extends Sniffer {

	static public final String pdfRendererVersion = "0.1";

	public PdfRendererSniffer() {
		super("PDF Renderer", "PDF Renderer", false, false);
	}

	public PdfRendererFactory createFactory(OdfFactoryConfig config)
			throws ArgumentException {
		String appPath = config.appPath;
		if (appPath != null) {
			throw new ArgumentException("An unneeded path was specified.");
		}
		return new PdfRendererFactory(config);
	}

	public final List<Service> services = a(
			service(ServiceType.render, FileType.PDF, FileType.PNG));

	public List<Service> getServices() {
		return services;
	}

	public OdfFactoryConfig sniff() {
		long detectionTime = Instant.now().getEpochSecond();
		boolean enabled = true;
		return new OdfFactoryConfig(family, name, null, detectionTime,
				PdfRendererSniffer.pdfRendererVersion, enabled);
	}
};
