
package org.odfserver.factories;

import java.io.IOException;
import java.time.Instant;
import java.util.List;
import java.util.regex.Pattern;

import org.apache.commons.io.IOUtils;
import org.odfserver.OdfFactoryConfig;
import org.odfserver.Sniffer;
import org.odfserver.Utility;
import org.odfserver.exceptions.ArgumentException;
import org.odfserver.exceptions.NoFactoryException;
import org.odfserver.types.FileType;
import org.odfserver.types.Service;
import org.odfserver.types.ServiceType;

public class MicrosoftWordSniffer extends Sniffer {

	public MicrosoftWordSniffer() {
		super("Office","Word", true, false);
	}

	public MicrosoftWordFactory createFactory(OdfFactoryConfig config)
			throws ArgumentException {
		String appPath = config.appPath;
		if (appPath != null) {
			throw new ArgumentException("An unneeded path was specified.");
		}
		return new MicrosoftWordFactory(config);
	}

	public final List<Service> services20132016 = a(
			service(ServiceType.convert, FileType.ODT1_0, FileType.ODT1_2EXT),
			service(ServiceType.convert, FileType.ODT1_1, FileType.ODT1_2EXT),
			service(ServiceType.convert, FileType.ODT1_2, FileType.ODT1_2EXT),
			service(ServiceType.roundtrip, FileType.ODT1_2EXT,
					FileType.ODT1_2EXT),
			service(ServiceType.convert, FileType.ODT1_0, FileType.PDF),
			service(ServiceType.convert, FileType.ODT1_1, FileType.PDF),
			service(ServiceType.convert, FileType.ODT1_2, FileType.PDF),
			service(ServiceType.convert, FileType.ODT1_2EXT, FileType.PDF));

	public final List<Service> services2010 = a();

	public List<Service> getServices() {
		List<Service> services = a();
		try {
			String version = getVersion();
			if ("2010".equals(version)) {
				services = services2010;
			} else if ("2013".equals(version) || "2016".equals(version)) {
				services = services20132016;
			}
		} catch (NoFactoryException e) {
		}
		return services;
	}

	static String getVersion() throws NoFactoryException {
		String version = null;
		try {
			Process proc = new ProcessBuilder("reg", "query",
					"HKEY_CLASSES_ROOT\\Word.Application\\CurVer").start();
			version = Utility.pickSelectedFromMultiLineString(
					IOUtils.toString(proc.getInputStream(), "utf-8"),
					Pattern.compile(
							".*REG_SZ[ ]*Word.Application.(?<selected>[0-9.]+).*"));
		} catch (IOException e) {
			throw new NoFactoryException("Cannot determine version.");
		}
		if ("16".equals(version)) {
			version = "2016";
		} else if ("15".equals(version)) {
			version = "2013";
		} else if ("14".equals(version)) {
			version = "2010";
		} else if ("12".equals(version)) {
			version = "2007";
		} else {
			throw new NoFactoryException("Cannot determine version.");
		}
		return version;
	}

	public OdfFactoryConfig sniff() throws NoFactoryException {
		long detectionTime = Instant.now().getEpochSecond();
		runSanityCheckConversion();
		return new OdfFactoryConfig(family, name, null, detectionTime,
				getVersion(), true);
	}
};
