
package org.odfserver.factories;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPageTree;
import org.apache.pdfbox.rendering.ImageType;
import org.apache.pdfbox.rendering.PDFRenderer;
import org.odfserver.OdfFactory;
import org.odfserver.OdfFactoryConfig;
import org.odfserver.types.JobResult;
import org.odfserver.types.RetrieveJob;

public class PdfRendererFactory extends OdfFactory {

	public final int resolutiondpi;

	protected PdfRendererFactory(OdfFactoryConfig config) {
		super(config);
		this.resolutiondpi = 150;
	}

	public boolean checkVersion(String version) throws IOException {
		return version.equals(PdfRendererSniffer.pdfRendererVersion);
	}

	public JobResult perform(RetrieveJob job) throws IOException {
		PDDocument document = PDDocument
				.load(new ByteArrayInputStream(job.inputFile));
		try {
			PDFRenderer pdfRenderer = new PDFRenderer(document);
			PDPageTree pageTree = document.getPages();
			byte outputFiles[][] = new byte[pageTree.getCount()][];
			for (int pageCounter = 0; pageCounter < pageTree
					.getCount(); ++pageCounter) {
				BufferedImage bim = pdfRenderer.renderImageWithDPI(pageCounter,
						resolutiondpi, ImageType.RGB);
				ByteArrayOutputStream os = new ByteArrayOutputStream();
				ImageIO.write(bim, "png", os);
				outputFiles[pageCounter] = os.toByteArray();
			}
			return new JobResult("jar", outputFiles, 0, "", "", false);
		} finally {
			document.close();
		}
	}
};
