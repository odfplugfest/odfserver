
package org.odfserver.factories;

import java.io.IOException;
import java.util.List;

import org.odfserver.OdfFactoryConfig;
import org.odfserver.Sniffer;
import org.odfserver.exceptions.ArgumentException;
import org.odfserver.exceptions.NoFactoryException;
import org.odfserver.types.FileType;
import org.odfserver.types.Service;

public class CalligraKarbonSniffer extends Sniffer {

	public CalligraKarbonSniffer() {
		super(CalligraWordsSniffer.family, "Karbon", true, false);
	}

	public CalligraKarbonFactory createFactory(OdfFactoryConfig config)
			throws ArgumentException, IOException {
		String appPath = config.appPath;
		if (appPath == null) {
			throw new ArgumentException("No path was specified.");
		}
		return new CalligraKarbonFactory(appPath, config);
	}

	public final List<Service> services = a(
			new FileType[] { FileType.ODG1_0, FileType.ODG1_1, FileType.ODG1_2,
					FileType.ODG1_2EXT },
			new FileType[] { FileType.ODG1_2EXT, FileType.PDF });

	public List<Service> getServices() {
		return services;
	}

	public OdfFactoryConfig sniff() throws NoFactoryException {
		return CalligraWordsSniffer.sniff(name,
				CalligraKarbonFactory.exeName,
				CalligraKarbonFactory.versionName);
	}
};
