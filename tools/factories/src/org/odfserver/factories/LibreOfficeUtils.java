
package org.odfserver.factories;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Instant;
import java.util.Properties;
import java.util.regex.Pattern;

import org.odfserver.OdfFactory;
import org.odfserver.OdfFactoryConfig;
import org.odfserver.Sniffer;
import org.odfserver.Utility;
import org.odfserver.exceptions.ArgumentException;
import org.odfserver.exceptions.NoFactoryException;
import org.odfserver.types.FileType;
import org.odfserver.types.JobResult;
import org.odfserver.types.RetrieveJob;
import org.odfserver.types.TempDir;

public class LibreOfficeUtils {

	public static final String exename = "soffice";

	static public String getFamilyNameExe(Properties properties) throws IOException {
		if (properties.getProperty("OOOPackageVersion") == null) {
			return "LibreOffice";
		}
		return "OpenOffice";
	}

	static public String getFamilyName(String appPath) throws IOException {
		if (appPath.toLowerCase().endsWith(".exe")) {
			return getFamilyNameExe(readProperties(appPath));
		}
		String version[] = Utility
				.executeProcessToStrings(new ProcessBuilder(appPath, "--headless", "--version").start());
		for (String v : version) {
			if (v.contains("LibreOffice")) {
				return "LibreOffice";
			}
		}
		return "OpenOffice";
	}

	static public Properties readProperties(String appPath) throws IOException {
		File exe = new File(appPath);
		File ini = new File(exe.getParentFile(), "version.ini");
		Properties properties = new Properties();
		FileInputStream iss = new FileInputStream(ini);
		properties.load(iss);
		return properties;
	}

	static public String getExeVersion(String appPath) throws IOException {
		File exe = new File(appPath);
		File ini = new File(exe.getParentFile(), "version.ini");
		Properties properties = new Properties();
		FileInputStream iss = new FileInputStream(ini);
		properties.load(iss);
		String version = properties.getProperty("OOOPackageVersion");
		if (version == null) {
			version = properties.getProperty("MsiProductVersion");
		}
		if (version == null) {
			String emsg = "can't work out software version for executable at: " + appPath;
			throw new IOException(emsg);
		}
		return version;
	}

	static public String getVersion(String appPath) throws IOException {
		if (appPath.toLowerCase().endsWith(".exe")) {
			return getExeVersion(appPath);
		}
		String ver = Utility.executeProcessToRegexCapture(
				new ProcessBuilder(appPath, "--headless", "--version").start(),
				Pattern.compile("[^0-9]*(?<selected>[0-9.]+[0-9]).*"));
		return Sniffer.sniffCheckHaveVersionOrDie(ver, appPath);
	}

	static public OdfFactoryConfig sniff(String name, LO lo) throws NoFactoryException {
		String appPath;
		long detectionTime;
		String version;
		boolean enabled;
		String family;
		String dir = null;
		switch (lo) {
		case LibreOffice:
			dir = "LibreOffice 5";
			break;
		case OpenOffice:
			dir = "OpenOffice 4";
			break;
		case NDCODFApplicationTools:
			dir = "NDCODFApplicationTools 6";
			break;
		}
		if (dir == null) {
			throw new NoFactoryException("Cannot find factory for " + lo);
		}
		try {
			Path path = Paths.get("C:", "Program Files (x86)", dir, "program", "soffice.exe");
			if (Files.isExecutable(path)) {
				appPath = path.toString();
			} else {
				appPath = Sniffer.getExecutableFullPath(exename);
			}
			detectionTime = Instant.now().getEpochSecond();
			version = getVersion(appPath);
			Sniffer.runSanityCheckConversion();
			enabled = true;
			family = getFamilyName(appPath);
		} catch (IOException e) {
			enabled = false;
			throw new NoFactoryException("Cannot determine version.", e);
		}
		if (!lo.toString().equals(family)) {
			throw new NoFactoryException("Different family: " + family);
		}
		return new OdfFactoryConfig(family, name, appPath, detectionTime, version, enabled);
	}

	static public final String[] environmentToPassAlong = { "PATH" };

	static private String getConfiguration(int version) {
		return "<?xml version='1.0' encoding='UTF-8'?>\n"
				+ "<oor:items xmlns:oor='http://openoffice.org/2001/registry'>\n"
				+ "  <item oor:path='/org.openoffice.Setup/Office'>\n"
				+ "    <prop oor:name='ooSetupInstCompleted' oor:op='fuse'>\n" + "      <value>true</value>\n"
				+ "    </prop>\n" + "  </item>\n" + "  <item oor:path='/org.openoffice.Office.Common/Save/ODF'>\n"
				+ "    <prop oor:name='DefaultVersion' oor:op='fuse'>\n" + "      <value>" + version + "</value>\n"
				+ "    </prop>\n" + "  </item>\n" + "</oor:items>";
	}

	static private void writeConfiguration(Path configDir, FileType outputType) throws IOException {
		Path userDir = configDir.resolve("user");
		Files.createDirectories(userDir);
		Path configPath = userDir.resolve("registrymodifications.xcu");
		int version = 3; // ODF 1.2 extended or PDF
		if (!FileType.PDF.equals(outputType) && !outputType.isExtendedODF()) {
			switch (outputType.odfVersion()) {
			case V1_0:
				throw new IOException("ODF 1.0 is not supported.");
			case V1_1:
				version = 2;
				break;
			case V1_2:
				version = 4;
			}
		}
		Files.write(configPath, getConfiguration(version).getBytes());
	}

	static public JobResult perform(String appPath, RetrieveJob job, String filter)
			throws ArgumentException, IOException {
		TempDir dir = OdfFactory.createTemporaryDirectory(job);
		// change the output path: soffice requires a separate output directory
		Path outDir = dir.directoryPath.resolve("out");
		Files.createDirectories(outDir);
		dir = new TempDir(dir.directoryPath, dir.inputPath,
				outDir.resolve("input" + job.service.outputType.extension()));
		Path configDir = dir.directoryPath.resolve("config");
		Files.createDirectories(configDir);
		writeConfiguration(configDir, job.service.outputType);
		JobResult result;
		URI dirURI = configDir.toUri();
		try {
			final ProcessBuilder pb = new ProcessBuilder(appPath, "--headless",
					"-env:UserInstallation=" + dirURI.toString(), "--convert-to", filter, "--outdir", outDir.toString(),
					dir.inputPath.toString());
			result = OdfFactory.executeProcess(dir, environmentToPassAlong, pb);
		} finally {
			OdfFactory.deleteDirAndContents(dir);
		}
		return result;
	}
};
