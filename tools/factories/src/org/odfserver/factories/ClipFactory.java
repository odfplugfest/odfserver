
package org.odfserver.factories;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.odfserver.OdfFactory;
import org.odfserver.OdfFactoryConfig;
import org.odfserver.exceptions.ArgumentException;
import org.odfserver.types.FileType;
import org.odfserver.types.JobResult;
import org.odfserver.types.RetrieveJob;

public class ClipFactory extends OdfFactory {

	protected ClipFactory(OdfFactoryConfig config) {
		super(config);
	}

	public boolean checkVersion(String version) throws IOException {
		return true;
	}

	public BufferedImage getCroppedImage(BufferedImage source) {
		int baseColor = source.getRGB(0, 0);

		int width = source.getWidth();
		int height = source.getHeight();

		int topY;
		for (topY = 0; topY < height - 1; topY++) {
			boolean isBase = true;
			for (int x = 0; isBase && x < width; x++) {
				isBase = baseColor == source.getRGB(x, topY);
			}
			if (!isBase) {
				break;
			}
		}
		topY = Math.max(0, topY - 3);
		int bottomY;
		for (bottomY = height - 1; bottomY > 0; bottomY--) {
			boolean isBase = true;
			for (int x = 0; isBase && x < width; x++) {
				isBase = baseColor == source.getRGB(x, bottomY);
			}
			if (!isBase) {
				break;
			}
		}
		bottomY = Math.max(topY, Math.min(height - 1, bottomY + 3));
		int leftX;
		for (leftX = 0; leftX < width - 1; leftX++) {
			boolean isBase = true;
			for (int y = 0; isBase && y < height; y++) {
				isBase = baseColor == source.getRGB(leftX, y);
			}
			if (!isBase) {
				break;
			}
		}
		leftX = Math.max(0, leftX - 3);
		int rightX;
		for (rightX = width - 1; rightX > 0; rightX--) {
			boolean isBase = true;
			for (int y = 0; isBase && y < height - 1; y++) {
				isBase = baseColor == source.getRGB(rightX, y);
			}
			if (!isBase) {
				break;
			}
		}
		rightX = Math.max(leftX, Math.min(width - 1, rightX + 3));

		int dwidth = rightX - leftX + 1;
		int dheight = bottomY - topY + 1;
		double ratio = dwidth * 1.0 / dheight;
		int max = 128;
		dwidth = Math.min(dwidth, max);
		dheight = Math.min(dheight, max);
		if (dheight * ratio > dwidth) {
			dheight = (int) (dwidth / ratio);
		} else {
			dwidth = (int) (dheight * ratio);
		}

		BufferedImage destination = new BufferedImage(dwidth, dheight,
				BufferedImage.TYPE_INT_ARGB);

		destination.getGraphics().drawImage(source, 0, 0, dwidth, dheight,
				leftX, topY, rightX, bottomY, null);

		return destination;
	}

	public JobResult perform(RetrieveJob job)
			throws ArgumentException, IOException {
		if (!job.service.inputType.equals(FileType.PNG)) {
			throw new ArgumentException("Unsupported input type.");
		}
		if (!job.service.outputType.equals(FileType.PNG)) {
			throw new ArgumentException("Unsupported output type.");
		}

		JobResult result = null;
		byte outputFiles[][] = JobResult.noFiles;
		BufferedImage image = ImageIO
				.read(new ByteArrayInputStream(job.inputFile));
		image = getCroppedImage(image);
		ByteArrayOutputStream bout = new ByteArrayOutputStream();
		ImageIO.write(image, "png", bout);
		outputFiles = new byte[][] { bout.toByteArray() };
		result = new JobResult("", outputFiles, 0, "", "", false);
		return result;
	}
};
