
package org.odfserver.factories;

import java.io.IOException;
import java.time.Instant;
import java.util.List;
import java.util.regex.Pattern;

import org.odfserver.OdfFactoryConfig;
import org.odfserver.Sniffer;
import org.odfserver.Utility;
import org.odfserver.exceptions.ArgumentException;
import org.odfserver.exceptions.NoFactoryException;
import org.odfserver.types.FileType;
import org.odfserver.types.Service;
import org.odfserver.types.ServiceType;

public class GoogleDocsSniffer extends Sniffer {

	public GoogleDocsSniffer() {
		super("Google", "Docs", false, false);
	}

	public GoogleDocsFactory createFactory(OdfFactoryConfig config)
			throws ArgumentException {
		String appPath = config.appPath;
		if (appPath == null) {
			throw new ArgumentException("No path was specified.");
		}
		return new GoogleDocsFactory(appPath, config);
	}

	public final List<Service> services = a(
			service(ServiceType.convert, FileType.ODT1_0, FileType.ODT1_2EXT),
			service(ServiceType.convert, FileType.ODT1_1, FileType.ODT1_2EXT),
			service(ServiceType.convert, FileType.ODT1_2, FileType.ODT1_2EXT),
			service(ServiceType.roundtrip, FileType.ODT1_2EXT, FileType.ODT1_2EXT),
			service(ServiceType.convert, FileType.ODT1_0, FileType.PDF),
			service(ServiceType.convert, FileType.ODT1_1, FileType.PDF),
			service(ServiceType.convert, FileType.ODT1_2, FileType.PDF),
			service(ServiceType.convert, FileType.ODT1_2EXT, FileType.PDF),

			service(ServiceType.convert, FileType.ODS1_0, FileType.ODS1_2EXT),
			service(ServiceType.convert, FileType.ODS1_1, FileType.ODS1_2EXT),
			service(ServiceType.convert, FileType.ODS1_2, FileType.ODS1_2EXT),
			service(ServiceType.roundtrip, FileType.ODS1_2EXT, FileType.ODS1_2EXT),
			service(ServiceType.convert, FileType.ODS1_0, FileType.PDF),
			service(ServiceType.convert, FileType.ODS1_1, FileType.PDF),
			service(ServiceType.convert, FileType.ODS1_2, FileType.PDF),
			service(ServiceType.convert, FileType.ODS1_2EXT, FileType.PDF),

			// Note that as of August 2016
			//
			// odg can be uploaded but not converted to pdf or any
			// format for download.
			//
			// There is no ability to download ODP format again after
			// the upload.
			service(ServiceType.convert, FileType.ODP1_0, FileType.PDF),
			service(ServiceType.convert, FileType.ODP1_1, FileType.PDF),
			service(ServiceType.convert, FileType.ODP1_2, FileType.PDF),
			service(ServiceType.convert, FileType.ODP1_2EXT, FileType.PDF));

	public List<Service> getServices() {
		return services;
	}

	public OdfFactoryConfig sniff() throws NoFactoryException {
		String appPath;
		long detectionTime;
		final String version = ""; // Google Docs has no version numbers
		boolean enabled;
		try {
			appPath = getExecutableFullPath("gdrive");

			detectionTime = Instant.now().getEpochSecond();

			String quota = Utility.executeProcessToRegexCapture(
					new ProcessBuilder(appPath, "about").start(),
					Pattern.compile(".*Total:[ ]*(?<selected>[0-9.]+) .*"));
			if (quota == null || quota.equals("")) {
				enabled = false;
				String emsg = "Can't find the quota of used storage. This normally means you have not authenticated with GDrive properly. "
						+ appPath;
				throw new NoFactoryException(emsg);
			} else {
				runSanityCheckConversion();
				enabled = true;
			}
		} catch (IOException e) {
			enabled = false;
			throw new NoFactoryException("Cannot determine quota.", e);
		}
		return new OdfFactoryConfig(family, name, appPath, detectionTime,
				version, enabled);
	}
};
