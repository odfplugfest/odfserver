
package org.odfserver.factories;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.odfserver.OdfFactory;
import org.odfserver.OdfFactoryConfig;
import org.odfserver.Utility;
import org.odfserver.types.FileType;
import org.odfserver.types.JobResult;
import org.odfserver.types.RetrieveJob;
import org.odfserver.validator.ReferenceChecker;
import org.odfserver.validator.ReferenceError;
import org.odftoolkit.odfvalidator.Logger;
import org.odftoolkit.odfvalidator.ODFValidator;
import org.odftoolkit.odfvalidator.ODFValidatorException;
import org.odftoolkit.odfvalidator.OdfValidatorMode;
import org.odftoolkit.odfvalidator.OdfVersion;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import net.sf.saxon.s9api.SaxonApiException;

public class OdfValidatorFactory extends OdfFactory {

	static public final String version = "0.6.2";

	final private ReferenceChecker referenceChecker;

	public OdfValidatorFactory(OdfFactoryConfig config)
			throws SaxonApiException {
		super(config);
		referenceChecker = new ReferenceChecker();
	}

	public boolean checkVersion(String version) throws IOException {
		return version.equals(OdfValidatorFactory.version);
	}

	private void validateSingleFile(byte file[], FileType inputType,
			Document doc, Element fe)
			throws ODFValidatorException, IOException {
		addReferenceChecks(file, fe);
		String inputPath = "input" + inputType.extension();

		OdfVersion odfvers = inputType.odfVersion();
		OdfValidatorMode validatorMode = (inputType.isExtendedODF())
				? OdfValidatorMode.EXTENDED_CONFORMANCE
				: OdfValidatorMode.VALIDATE;

		ByteArrayOutputStream bytes = new ByteArrayOutputStream();
		PrintStream ps = new PrintStream(bytes);
		ODFValidator validator = new ODFValidator(null, Logger.LogLevel.ERROR,
				odfvers, false);

		boolean hasIssues;
		try {
			hasIssues = validator.validate(ps, new ByteArrayInputStream(file),
					inputPath, validatorMode, null);
		} catch (RuntimeException e) {
			throw new IOException(e);
		}
		String content;
		try {
			content = bytes.toString("UTF-8");
		} catch (UnsupportedEncodingException e1) {
			content = bytes.toString();
		}
		if (!hasIssues) {
			fe.setAttribute("result", "success");
			return;
		}

		Pattern errlinepatternMostSpecific = Pattern.compile(inputPath
				+ "/(?<component>[^\\[]+)\\[(?<line>[0-9]+),(?<column>[0-9]+)\\]:[ ]*(Error:[ ])?(?<msg>.*)");
		Pattern errlinepatternFreeScope = Pattern
				.compile(inputPath + "(/(?<component>[^:]+))?:[ ]*(?<msg>.*)");
		Pattern blackList = Pattern
				.compile(inputPath + ":[ ]*Info[: ]*Generator.*");

		BufferedReader reader = new BufferedReader(new StringReader(content));
		String line = reader.readLine();
		while (line != null) {

			Matcher mMostSpecific = errlinepatternMostSpecific.matcher(line);

			Matcher m = mMostSpecific;
			if (m.matches()) {
				Element e = doc.createElement("error");
				e.setAttribute("component", m.group("component"));
				e.setAttribute("start-line", m.group("line"));
				e.setAttribute("start-column", m.group("column"));
				e.appendChild(doc.createTextNode(m.group("msg")));
				fe.appendChild(e);
			} else {
				Matcher mFreeScope = errlinepatternFreeScope.matcher(line);
				Matcher mBlackList = blackList.matcher(line);
				m = mFreeScope;
				if (!mBlackList.matches()) {
					if (m.matches()) {
						Element e = doc.createElement("error");
						if (m.group("component") != null)
							e.setAttribute("component", m.group("component"));
						e.appendChild(doc.createTextNode(m.group("msg")));
						fe.appendChild(e);
					} else {
						Element e = doc.createElement("error");
						e.appendChild(doc.createTextNode(line));
						fe.appendChild(e);
					}
				}
			}

			line = reader.readLine();
		}
	}

	private void addReferenceChecks(byte file[], Element fe) {
		final List<ReferenceError> errors = referenceChecker
				.checkReferences(file);
		final Document doc = fe.getOwnerDocument();
		for (ReferenceError error : errors) {
			Element e = doc.createElement("error");
			e.setAttribute("component", error.component);
			e.appendChild(doc.createTextNode(error.error));
			fe.appendChild(e);
		}
	}

	private Document validate(byte file[], FileType inputType)
			throws ParserConfigurationException, ODFValidatorException,
			IOException {
		Document doc = null;
		Element fe = null;

		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = dbf.newDocumentBuilder();
		doc = builder.newDocument();
		Element root = doc.createElement("validation-report");
		doc.appendChild(root);
		fe = doc.createElement("file");
		root.appendChild(fe);

		fe.setAttribute("name", "input" + inputType.extension());

		// expand the DOM shell doc to include the sha-256.
		fe.setAttribute("sha256", Utility.digest(file));

		// process this ODF
		validateSingleFile(file, inputType, doc, fe);
		return doc;
	}

	public JobResult perform(RetrieveJob job) {
		byte outputFiles[][] = JobResult.noFiles;
		String stderr = "";
		int exitCode = 0;
		try {
			Document doc = validate(job.inputFile, job.service.inputType);
			outputFiles = new byte[][] { Utility.serialize(doc) };
		} catch (ParserConfigurationException | ODFValidatorException
				| IOException e) {
			exitCode = -1;
			stderr = e.getMessage() + Utility.stacktraceToString(e);
		}
		return new JobResult("jar", outputFiles, exitCode, "", stderr, false);
	}

	/**
	 * This is here for short, quick, testing only. It may well, and can easily
	 * be removed in the future.
	 *
	 * Running this you can see the output for a specific file without using the
	 * server at all. For example;
	 *
	 * java -cp odffactories.jar org.odfserver.factories.OdfValidatorFactory
	 * /tmp/twopage.odt 1.2ext | xmllint --format -
	 *
	 * The 1.2ext can be replaced with any of 1.0, 1.1, 1.2, and 1.2ext which
	 * work to select desired ODF version.
	 *
	 * With this mechanism you can force the validator to generate some data for
	 * a valid document. For example by validating a 1.2ext file against a 1.2
	 * validation check.
	 * 
	 * @throws SaxonApiException
	 */
	public static void main(String[] argv) throws SaxonApiException {
		try {
			if (argv.length <= 1) {
				System.err.println("");
				System.err.println("Usage: ");
				System.err.println("  ThisProgram inputFilePath inputType");
				System.err.println("");
				System.exit(1);
			}

			String inputFilePath = argv[0];
			String inputTypeString = argv[1];

			FileType inputType = FileType.ODT1_2;
			if (inputTypeString.contains("1.0"))
				inputType = FileType.ODT1_0;
			else if (inputTypeString.contains("1.1"))
				inputType = FileType.ODT1_1;
			else if (inputTypeString.contains("1.2ext"))
				inputType = FileType.ODT1_2EXT;
			else if (inputTypeString.contains("1.2"))
				inputType = FileType.ODT1_2;
			else {
				System.err.println(
						"Invalid ODF version specified: " + inputTypeString);
				System.err.println(
						" please use one of [ 1.0, 1.1, 1.2, 1.2ext ]");
				System.exit(1);
			}

			OdfValidatorFactory factory = new OdfValidatorFactory(
					new OdfFactoryConfig("", "", "", 0, "", true));
			byte[] inputFile = Files.readAllBytes(Paths.get(inputFilePath));
			Document doc = factory.validate(inputFile, inputType);
			byte[] bytes = Utility.serialize(doc);
			System.out.println(new String(bytes, "UTF-8"));

		} catch (ODFValidatorException | IOException
				| ParserConfigurationException e) {
			e.printStackTrace();
		}
	}

};
