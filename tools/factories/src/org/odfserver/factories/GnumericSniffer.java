
package org.odfserver.factories;

import java.io.IOException;
import java.time.Instant;
import java.util.List;
import java.util.regex.Pattern;

import org.odfserver.OdfFactoryConfig;
import org.odfserver.Sniffer;
import org.odfserver.Utility;
import org.odfserver.exceptions.ArgumentException;
import org.odfserver.exceptions.NoFactoryException;
import org.odfserver.types.FileType;
import org.odfserver.types.Service;

public class GnumericSniffer extends Sniffer {

	public GnumericSniffer() {
		super("Gnumeric", "Gnumeric", true, false);
	}

	public static final String exename = "ssconvert";

	public GnumericFactory createFactory(OdfFactoryConfig config)
			throws ArgumentException {
		String appPath = config.appPath;
		if (appPath == null) {
			throw new ArgumentException("No path was specified.");
		}
		return new GnumericFactory(appPath, config);
	}

	public final List<Service> services = a(
			new FileType[] { FileType.ODS1_0, FileType.ODS1_1, FileType.ODS1_2,
					FileType.ODS1_2EXT },
			new FileType[] { FileType.ODS1_2, FileType.ODS1_2EXT,
					FileType.PDF });

	public List<Service> getServices() {
		return services;
	}

	public static String getVersion(String appPath) throws IOException {
		/*
		 * run it to see the version
		 */
		ProcessBuilder pb = new ProcessBuilder(appPath, "--version");
		// gnumeric prints an internationalized version number
		pb.environment().put("LANGUAGE", "C");
		String ver = Utility.executeProcessToRegexCapture(pb.start(), Pattern
				.compile(".*ssconvert version[' ]*(?<selected>[0-9.]+).*"));
		return sniffCheckHaveVersionOrDie(ver, appPath);
	}

	public OdfFactoryConfig sniff() throws NoFactoryException {
		String appPath;
		long detectionTime;
		String version;
		boolean enabled;
		try {
			appPath = getExecutableFullPath(exename);
			detectionTime = Instant.now().getEpochSecond();
			version = getVersion(appPath);
			runSanityCheckConversion();
			enabled = true;
		} catch (IOException e) {
			enabled = false;
			throw new NoFactoryException("Cannot determine version.", e);
		}
		return new OdfFactoryConfig(family, name, appPath, detectionTime,
				version, enabled);
	}
};
