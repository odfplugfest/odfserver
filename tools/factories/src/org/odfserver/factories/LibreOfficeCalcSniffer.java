
package org.odfserver.factories;

import java.io.IOException;
import java.util.List;

import org.odfserver.OdfFactoryConfig;
import org.odfserver.Sniffer;
import org.odfserver.exceptions.ArgumentException;
import org.odfserver.exceptions.NoFactoryException;
import org.odfserver.types.FileType;
import org.odfserver.types.Service;

public class LibreOfficeCalcSniffer extends Sniffer {

	final LO family;

	public LibreOfficeCalcSniffer(LO family) {
		super(family.toString(), "Calc", true, false);
		this.family = family;
	}

	public LibreOfficeCalcFactory createFactory(OdfFactoryConfig config)
			throws ArgumentException, IOException {
		String appPath = config.appPath;
		if (appPath == null) {
			throw new ArgumentException("No path was specified.");
		}
		return new LibreOfficeCalcFactory(appPath, config);
	}

	public final List<Service> services = a(
			new FileType[] { FileType.ODS1_0, FileType.ODS1_1, FileType.ODS1_2,
					FileType.ODS1_2EXT },
			new FileType[] { FileType.ODS1_1, FileType.ODS1_2,
					FileType.ODS1_2EXT, FileType.PDF });

	public List<Service> getServices() {
		return services;
	}

	public OdfFactoryConfig sniff() throws NoFactoryException {
		return LibreOfficeUtils.sniff(name, family);
	}
};
