package org.odfserver.factories;

import org.odfserver.OdfFactoryConfig;
import org.odfserver.OdfFactoryFailBase;
import org.odfserver.exceptions.ArgumentException;
import org.odfserver.types.JobResult;
import org.odfserver.types.RetrieveJob;

public class FailWithExceptionFactory extends OdfFactoryFailBase {

	public FailWithExceptionFactory(OdfFactoryConfig config) {
		super(config);
	}

	public String getName() {
		return "FailWithException";
	}

	public JobResult perform(RetrieveJob job) throws ArgumentException {
		throw new ArgumentException(
				"This factory will always fail, it love to throw "
						+ "exceptions back to you, we all hope the server "
						+ "gets to know about this happening too");
	}

};
