
package org.odfserver.factories;

import java.io.IOException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.odfserver.OdfFactoryConfig;
import org.odfserver.Sniffer;
import org.odfserver.exceptions.ArgumentException;
import org.odfserver.types.FileType;
import org.odfserver.types.Service;
import org.odfserver.types.ServiceType;

import net.sf.saxon.s9api.SaxonApiException;

public class OdfValidatorSniffer extends Sniffer {

	public OdfValidatorSniffer() {
		super("ODF Toolkit", "ODF Validator", false, false);
	}

	public OdfValidatorFactory createFactory(OdfFactoryConfig config)
			throws ArgumentException, IOException {
		String appPath = config.appPath;
		if (appPath != null) {
			throw new ArgumentException("An unneeded path was specified.");
		}
		try {
			return new OdfValidatorFactory(config);
		} catch (SaxonApiException e) {
			throw new IOException(e);
		}
	}

	public final List<Service> services = createServices();

	private static List<Service> createServices() {
		final FileType[] inputTypes = { FileType.ODT1_0, FileType.ODT1_1,
				FileType.ODT1_2, FileType.ODT1_2EXT, FileType.ODS1_0,
				FileType.ODS1_1, FileType.ODS1_2, FileType.ODS1_2EXT,
				FileType.ODP1_0, FileType.ODP1_1, FileType.ODP1_2,
				FileType.ODP1_2EXT, FileType.ODG1_0, FileType.ODG1_1,
				FileType.ODG1_2, FileType.ODG1_2EXT };
		final ArrayList<Service> services = new ArrayList<Service>(
				inputTypes.length);
		for (int i = 0; i < inputTypes.length; ++i) {
			services.add(service(ServiceType.validate, inputTypes[i],
					FileType.VALIDATIONREPORT));
		}
		return Collections.unmodifiableList(services);
	}

	public List<Service> getServices() {
		return services;
	}

	public OdfFactoryConfig sniff() {
		long detectionTime = Instant.now().getEpochSecond();
		return new OdfFactoryConfig(family, name, null, detectionTime,
				OdfValidatorFactory.version, true);
	}
};
