package org.odfserver.factories;

import java.io.IOException;
import java.time.Instant;
import java.util.List;

import org.odfserver.OdfFactoryConfig;
import org.odfserver.Sniffer;
import org.odfserver.exceptions.ArgumentException;
import org.odfserver.exceptions.NoFactoryException;
import org.odfserver.types.FileType;
import org.odfserver.types.Service;

public class AbiwordSniffer extends Sniffer {

	public AbiwordSniffer() {
		super("Abiword", "Abiword", true, false);
	}

	public AbiwordFactory createFactory(OdfFactoryConfig config)
			throws ArgumentException, IOException {
		String appPath = config.appPath;
		if (appPath == null) {
			throw new ArgumentException("No path was specified.");
		}
		return new AbiwordFactory(appPath, config);
	}

	public final List<Service> services = a(
			new FileType[] { FileType.ODT1_0, FileType.ODT1_1, FileType.ODT1_2,
					FileType.ODT1_2EXT },
			new FileType[] { FileType.ODT1_1, FileType.PDF });

	public List<Service> getServices() {
		return services;
	}

	public OdfFactoryConfig sniff() throws NoFactoryException {
		String appPath;
		long detectionTime;
		String version;
		boolean enabled;
		try {
			appPath = getExecutableFullPath("abiword");
			detectionTime = Instant.now().getEpochSecond();
			version = AbiwordFactory.getVersion(appPath);
			runSanityCheckConversion();
			enabled = true;
		} catch (IOException e) {
			enabled = false;
			throw new NoFactoryException("Cannot determine version.", e);
		}
		return new OdfFactoryConfig(family, name, appPath, detectionTime,
				version, enabled);
	}
};
