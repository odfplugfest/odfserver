
package org.odfserver.factories;

import java.time.Instant;
import java.util.List;

import org.odfserver.OdfFactoryConfig;
import org.odfserver.Sniffer;
import org.odfserver.exceptions.ArgumentException;
import org.odfserver.exceptions.NoFactoryException;
import org.odfserver.types.FileType;
import org.odfserver.types.Service;
import org.odfserver.types.ServiceType;

public class ClipSniffer extends Sniffer {

	public ClipSniffer() {
		super("Clip", "Clip", false, false);
	}

	public ClipFactory createFactory(OdfFactoryConfig config)
			throws ArgumentException {
		if (config.appPath != null) {
			throw new ArgumentException("An unneeded path was specified.");
		}
		return new ClipFactory(config);
	}

	public final List<Service> services = a(
			service(ServiceType.clip, FileType.PNG, FileType.PNG));

	public List<Service> getServices() {
		return services;
	}

	public OdfFactoryConfig sniff() throws NoFactoryException {
		String appPath = null;
		long detectionTime = Instant.now().getEpochSecond();
		String version = "0.1";
		boolean enabled = true;
		return new OdfFactoryConfig(family, name, appPath, detectionTime,
				version, enabled);
	}
};
