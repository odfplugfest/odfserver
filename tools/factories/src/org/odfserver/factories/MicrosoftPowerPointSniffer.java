
package org.odfserver.factories;

import java.time.Instant;
import java.util.List;

import org.odfserver.OdfFactoryConfig;
import org.odfserver.Sniffer;
import org.odfserver.exceptions.ArgumentException;
import org.odfserver.exceptions.NoFactoryException;
import org.odfserver.types.FileType;
import org.odfserver.types.Service;
import org.odfserver.types.ServiceType;

public class MicrosoftPowerPointSniffer extends Sniffer {

	public MicrosoftPowerPointSniffer() {
		super("Office", "PowerPoint", true, false);
	}

	public MicrosoftPowerPointFactory createFactory(OdfFactoryConfig config)
			throws ArgumentException {
		String appPath = config.appPath;
		if (appPath != null) {
			throw new ArgumentException("An unneeded path was specified.");
		}
		return new MicrosoftPowerPointFactory(config);
	}

	public final List<Service> services20132016 = a(
			service(ServiceType.convert, FileType.ODP1_0, FileType.ODP1_2EXT),
			service(ServiceType.convert, FileType.ODP1_1, FileType.ODP1_2EXT),
			service(ServiceType.convert, FileType.ODP1_2, FileType.ODP1_2EXT),
			service(ServiceType.roundtrip, FileType.ODP1_2EXT,
					FileType.ODP1_2EXT),
			service(ServiceType.convert, FileType.ODP1_0, FileType.PDF),
			service(ServiceType.convert, FileType.ODP1_1, FileType.PDF),
			service(ServiceType.convert, FileType.ODP1_2, FileType.PDF),
			service(ServiceType.convert, FileType.ODP1_2EXT, FileType.PDF));

	public final List<Service> services2010 = a();

	public List<Service> getServices() {
		List<Service> services = a();
		try {
			String version = getVersion();
			if ("2010".equals(version)) {
				services = services2010;
			} else if ("2013".equals(version) || "2016".equals(version)) {
				services = services20132016;
			}
		} catch (NoFactoryException e) {
		}
		return services;
	}

	static String getVersion() throws NoFactoryException {
		return MicrosoftWordSniffer.getVersion();
	}

	public OdfFactoryConfig sniff() throws NoFactoryException {
		long detectionTime = Instant.now().getEpochSecond();
		runSanityCheckConversion();
		return new OdfFactoryConfig(family, name, null, detectionTime,
				getVersion(), true);
	}
};
