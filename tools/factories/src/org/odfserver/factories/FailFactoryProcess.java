package org.odfserver.factories;

import java.io.IOException;
import java.lang.management.ManagementFactory;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.odfserver.PlatformInformation;

public class FailFactoryProcess {

	public static void main(String[] argv) {
		try {
			boolean wait = false;
			boolean die = false;
			boolean mangleInputAsOutput = false;
			boolean msgOnStdErrorAndReturn1 = false;
			String outputDoc = null, inputDoc = null;

			System.out.println("argv.length :" + argv.length);

			if (argv.length >= 1) {
				System.out.println("argv.0 :" + argv[0]);
				if ("-w".equals(argv[0]) || "--wait".equals(argv[0]))
					wait = true;
				if ("-d".equals(argv[0]) || "--die".equals(argv[0]))
					die = true;
				if ("-m".equals(argv[0]) || "--mangle".equals(argv[0]))
					mangleInputAsOutput = true;
				if ("-e".equals(argv[0]) || "--stderr".equals(argv[0]))
					msgOnStdErrorAndReturn1 = true;
			}
			if (argv.length >= 2) {
				inputDoc = argv[1];
			}
			if (argv.length >= 3) {
				outputDoc = argv[2];
			}

			System.out.println("wait :" + wait);
			System.out.println("in   :" + inputDoc);
			System.out.println("out  :" + outputDoc);

			if (wait) {
				System.err
						.println("sleeping for an hour... stop me if you can!");
				String pid = ManagementFactory.getRuntimeMXBean().getName()
						.split("@")[0];
				System.err.println("my PID is " + pid);
				Thread.sleep(3600 * 1000);
			}
			if (die) {
				System.err.println("I'm about to kill myself with a segv");
				String pid = ManagementFactory.getRuntimeMXBean().getName()
						.split("@")[0];
				System.err.println("my PID is " + pid);

				String cmd = "kill -SIGSEGV " + pid;
				if (PlatformInformation.isWindows())
					cmd = "taskkill /pid " + pid + " /f";
				System.err.println("cmd to kill outself: " + cmd);
				Runtime.getRuntime().exec(cmd);
			}
			if (mangleInputAsOutput) {
				if (outputDoc == null || inputDoc == null) {
					System.err.println(
							"You must supply the input and output document paths!");
					System.exit(1);
					return;
				}

				byte[] d = Files.readAllBytes(Paths.get(inputDoc));
				int dmax = d.length;
				if (dmax > 4096) {
					int i = 0;
					for (i = 4096; i < dmax; i++)
						d[i] = 0;
				}
				Files.write(Paths.get(outputDoc), d);
			}
			if (msgOnStdErrorAndReturn1) {
				System.err.println(
						"This is the message on stderr from the Test Office Suite");
				System.exit(1);
			}
		} catch (IOException | InterruptedException e) {
			e.printStackTrace();
			System.exit(2);
		}
		System.exit(0);
	}

};
