
package org.odfserver.factories;

import java.io.IOException;

import org.odfserver.OdfFactory;
import org.odfserver.OdfFactoryConfig;
import org.odfserver.exceptions.ArgumentException;
import org.odfserver.types.FileType;
import org.odfserver.types.JobResult;
import org.odfserver.types.RetrieveJob;
import org.odfserver.types.TempDir;

public class GnumericFactory extends OdfFactory {

	public final String appPath;

	protected GnumericFactory(String appPath, OdfFactoryConfig config) {
		super(config);
		this.appPath = appPath;
	}

	public boolean checkVersion(String version) throws IOException {
		return version.equals(GnumericSniffer.getVersion(appPath));
	}

	public final String[] environmentToPassAlong = {};

	public JobResult perform(RetrieveJob job)
			throws ArgumentException, IOException {
		if (!job.service.inputType.isOds()) {
			throw new ArgumentException("Unsupported input type.");
		}
		String exportType;
		if (job.service.outputType.equals(FileType.ODS1_2)) {
			exportType = "--export-type=Gnumeric_OpenCalc:openoffice";
		} else if (job.service.outputType.equals(FileType.ODS1_2EXT)) {
			exportType = "--export-type=Gnumeric_OpenCalc:odf";
		} else if (job.service.outputType.equals(FileType.PDF)) {
			exportType = "--export-type=Gnumeric_pdf:pdf_assistant";
		} else {
			throw new ArgumentException("Unsupported output type.");
		}
		JobResult result;
		TempDir dir = createTemporaryDirectory(job);
		try {
			ProcessBuilder pb = new ProcessBuilder(appPath, exportType,
					"--recalc", dir.inputPath.toString(),
					dir.outputPath.toString());
			result = executeProcess(dir, environmentToPassAlong, pb);
		} finally {
			deleteDirAndContents(dir);
		}
		return result;
	}
};
