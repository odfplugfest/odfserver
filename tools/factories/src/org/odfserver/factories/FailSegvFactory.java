package org.odfserver.factories;

import java.io.IOException;

import org.odfserver.OdfFactoryConfig;
import org.odfserver.OdfFactoryFailBase;
import org.odfserver.types.JobResult;
import org.odfserver.types.RetrieveJob;

public class FailSegvFactory extends OdfFactoryFailBase {

	public FailSegvFactory(OdfFactoryConfig config) {
		super(config);
	}

	public String getName() {
		return "FailSegv";
	}

	public JobResult perform(RetrieveJob job) throws IOException {
		// just wait for ages here.
		System.err.println(
				"The FailSegvFactory is going to start an \"Office Suite\" that will segv");
		System.err.println(
				" this should be detected by the code right away and reported to the server.");

		return failJob("--die", job);
	}

};
