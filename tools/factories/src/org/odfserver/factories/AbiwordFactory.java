
package org.odfserver.factories;

import java.io.IOException;

import org.apache.commons.io.IOUtils;
import org.odfserver.OdfFactory;
import org.odfserver.OdfFactoryConfig;
import org.odfserver.Sniffer;
import org.odfserver.Utility;
import org.odfserver.exceptions.ArgumentException;
import org.odfserver.types.FileType;
import org.odfserver.types.JobResult;
import org.odfserver.types.RetrieveJob;
import org.odfserver.types.TempDir;

public class AbiwordFactory extends OdfFactory {

	public final String appPath;

	static public String getVersion(String appPath) throws IOException {
		Process proc = new ProcessBuilder(appPath, "--version").start();
		String ver = IOUtils.toString(proc.getInputStream(), "utf-8");
		ver = Utility.chomp(ver);
		return Sniffer.sniffCheckHaveVersionOrDie(ver, appPath);
	}

	public boolean checkVersion(String version) throws IOException {
		return version.equals(getVersion(appPath));
	}

	protected AbiwordFactory(String appPath, OdfFactoryConfig config)
			throws IOException {
		super(config);
		this.appPath = appPath;
	}

	public JobResult perform(RetrieveJob job)
			throws ArgumentException, IOException {
		if (!job.service.inputType.isOdt()) {
			throw new ArgumentException("Unsupported input type.");
		}
		String outputType;
		if (job.service.outputType.equals(FileType.ODT1_1)) {
			outputType = "odt";
		} else if (job.service.outputType.equals(FileType.PDF)) {
			outputType = "pdf";
		} else {
			throw new ArgumentException("Unsupported output type.");
		}
		TempDir dir = createTemporaryDirectory(job);
		JobResult result;
		ProcessBuilder pb = new ProcessBuilder(appPath, "-t", outputType, "-o",
				dir.outputPath.toString(), dir.inputPath.toString());
		result = executeProcess(dir, noenv, pb);
		deleteDirAndContents(dir);
		return result;
	}
};
