
package org.odfserver.factories;

import java.io.IOException;
import java.util.regex.Pattern;

import org.odfserver.OdfFactory;
import org.odfserver.OdfFactoryConfig;
import org.odfserver.Sniffer;
import org.odfserver.Utility;
import org.odfserver.exceptions.ArgumentException;
import org.odfserver.types.FileType;
import org.odfserver.types.JobResult;
import org.odfserver.types.RetrieveJob;
import org.odfserver.types.TempDir;

public class CalligraWordsFactory extends OdfFactory {

	static public final String versionName = "Calligra Words";

	static public final String exeName = "calligrawords";

	public final String appPath;

	static public final String getVersion(String appPath, String exeName, String name)
			throws IOException {
		String ver = Utility.executeProcessToRegexCapture(
				new ProcessBuilder(appPath, "--version").start(),
				Pattern.compile(".*(" + exeName + "|" + name + ":)[ ]*(?<selected>[0-9.]+).*"));
		return Sniffer.sniffCheckHaveVersionOrDie(ver, appPath);
	}

	public boolean checkVersion(String version) throws IOException {
		return version.equals(getVersion(appPath, exeName, versionName));
	}

	protected CalligraWordsFactory(String appPath, OdfFactoryConfig config)
			throws IOException {
		super(config);
		this.appPath = appPath;
	}

	static public final String[] environmentToPassAlong = { "DISPLAY",
			"XAUTHORITY", "KDEDIRS", "HOME", "DBUS_SESSION_BUS_ADDRESS",
			"XDG_DATA_DIRS" };

	public JobResult perform(RetrieveJob job)
			throws ArgumentException, IOException {
		if (!job.service.inputType.isOdt()) {
			throw new ArgumentException("Unsupported input type.");
		}
		JobResult result;
		TempDir dir = createTemporaryDirectory(job);
		try {
			ProcessBuilder pb;
			if (job.service.outputType.equals(FileType.ODT1_2EXT)) {
				pb = new ProcessBuilder(appPath, "--roundtrip-filename",
						dir.outputPath.toString(), dir.inputPath.toString());
			} else if (job.service.outputType.equals(FileType.PDF)) {
				pb = new ProcessBuilder(appPath, "--export-pdf",
						"--export-filename", dir.outputPath.toString(),
						dir.inputPath.toString());
			} else {
				throw new ArgumentException("Unsupported output type.");
			}
			result = executeProcess(dir, environmentToPassAlong, pb);
		} finally {
			deleteDirAndContents(dir);
		}
		return result;
	}
};
