
package org.odfserver.factories;

import java.time.Instant;
import java.util.List;

import org.odfserver.OdfFactoryConfig;
import org.odfserver.Sniffer;
import org.odfserver.exceptions.ArgumentException;
import org.odfserver.exceptions.NoFactoryException;
import org.odfserver.types.FileType;
import org.odfserver.types.Service;
import org.odfserver.types.ServiceType;

public class WebOdfSniffer extends Sniffer {

	public WebOdfSniffer() {
		super("WebODF", "WebODF", true, false);
	}

	public WebOdfFactory createFactory(OdfFactoryConfig config)
			throws ArgumentException {
		String appPath = config.appPath;
		if (appPath == null) {
			throw new ArgumentException("No path was specified.");
		}
		return new WebOdfFactory(appPath, config);
	}

	public final List<Service> services = a(
			service(ServiceType.roundtrip, FileType.ODT1_0, FileType.ODT1_0),
			service(ServiceType.roundtrip, FileType.ODT1_1, FileType.ODT1_1),
			service(ServiceType.roundtrip, FileType.ODT1_2, FileType.ODT1_2),
			service(ServiceType.roundtrip, FileType.ODT1_2EXT,
					FileType.ODT1_2EXT),
			service(ServiceType.convert, FileType.ODT1_0, FileType.PDF),
			service(ServiceType.convert, FileType.ODT1_1, FileType.PDF),
			service(ServiceType.convert, FileType.ODT1_2, FileType.PDF),
			service(ServiceType.convert, FileType.ODT1_2EXT, FileType.PDF),

			service(ServiceType.roundtrip, FileType.ODS1_0, FileType.ODS1_0),
			service(ServiceType.roundtrip, FileType.ODS1_1, FileType.ODS1_1),
			service(ServiceType.roundtrip, FileType.ODS1_2, FileType.ODS1_2),
			service(ServiceType.roundtrip, FileType.ODS1_2EXT,
					FileType.ODS1_2EXT),
			service(ServiceType.convert, FileType.ODS1_0, FileType.PDF),
			service(ServiceType.convert, FileType.ODS1_1, FileType.PDF),
			service(ServiceType.convert, FileType.ODS1_2, FileType.PDF),
			service(ServiceType.convert, FileType.ODS1_2EXT, FileType.PDF),

			service(ServiceType.roundtrip, FileType.ODP1_0, FileType.ODP1_0),
			service(ServiceType.roundtrip, FileType.ODP1_1, FileType.ODP1_1),
			service(ServiceType.roundtrip, FileType.ODP1_2, FileType.ODP1_2),
			service(ServiceType.roundtrip, FileType.ODP1_2EXT,
					FileType.ODP1_2EXT),
			service(ServiceType.convert, FileType.ODP1_0, FileType.PDF),
			service(ServiceType.convert, FileType.ODP1_1, FileType.PDF),
			service(ServiceType.convert, FileType.ODP1_2, FileType.PDF),
			service(ServiceType.convert, FileType.ODP1_2EXT, FileType.PDF));

	public List<Service> getServices() {
		return services;
	}

	public OdfFactoryConfig sniff() throws NoFactoryException {
		String appPath;
		long detectionTime;
		String version;
		boolean enabled;
		appPath = getExecutableFullPath("qtjsruntime");

		detectionTime = Instant.now().getEpochSecond();

		/*
		 * run it to see the version
		 */
		version = "0.5.10";

		runSanityCheckConversion();
		enabled = true;
		return new OdfFactoryConfig(family, name, appPath, detectionTime,
				version, enabled);
	}
};
