package org.odfserver;

import org.eclipse.jdt.annotation.Nullable;

public class Waiter {

	final private WaiterRunner runner;

	public Waiter(Thread t, int timeout_ms) {
		runner = new WaiterRunner(t, timeout_ms);
		runner.start();
	}

	public void stop() {
		runner.clear();
	}

	private class WaiterRunner extends Thread {

		int timeout_ms;
		@Nullable
		private Thread thread;

		public WaiterRunner(Thread t, int timeout_ms) {
			this.timeout_ms = timeout_ms;
			this.thread = t;
		}

		public void clear() {
			stopWaiting();
			interrupt();
			try {
				join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		private synchronized void stopWaiting() {
			thread = null;
		}

		public void run() {
			try {
				sleep(timeout_ms);
			} catch (InterruptedException e) {
			}
			if (thread != null) {
				thread.interrupt();
			}
		}
	}
}
