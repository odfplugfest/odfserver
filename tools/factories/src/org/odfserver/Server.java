
package org.odfserver;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonStructure;

import org.odfserver.types.RetrieveJob;
import org.odfserver.types.ServiceInstance;

/**
 * This class contains the methods that relate to putting and getting JSON to
 * the odf server. More specific designs like handling process execution and
 * detecting office suites for these factory implementations are moved to
 * OdfFactory instead.
 */
public class Server {

	private static JsonObject createRetrieveJobsRequest(ServerConfig config,
			int nextNumber, Set<ServiceInstance> services) {
		JsonArrayBuilder a = Json.createArrayBuilder();
		for (ServiceInstance service : services) {
			a.add(service.toJson());
		}
		JsonObject request = Json.createObjectBuilder()
				.add("factoryKey", getFactoryKey(config))
				.add("maxWaitTime", 60000000).add("nextNumber", nextNumber)
				.add("serviceInstances", a.build()).build();
		return request;
	}

	public static List<RetrieveJob> postRetrieveJobs(ServerConfig config,
			int nextNumber, Set<ServiceInstance> services) throws IOException {
		List<RetrieveJob> list = new LinkedList<RetrieveJob>();
		JsonObject reply = (JsonObject) post(config.m_serverHostAndPort,
				"/factory/retrievejobs",
				createRetrieveJobsRequest(config, nextNumber, services));
		JsonArray jobs = reply.getJsonArray("jobs");
		for (int ji = 0; ji < jobs.size(); ji++) {
			list.add(new RetrieveJob(jobs.getJsonObject(ji)));
		}
		return list;
	}

	private static JsonStructure post(String serverHostAndPort, String path,
			JsonStructure request) throws IOException {
		String req = Utility.jsonToString(request);
		URL u;
		try {
			u = new URI(serverHostAndPort + path).normalize().toURL();
		} catch (URISyntaxException e) {
			throw new Error(e);
		}
		HttpURLConnection conn = (HttpURLConnection) u.openConnection();
		conn.setDoOutput(true);
		conn.setRequestMethod("POST");
		conn.setRequestProperty("Content-Type", "application/json");
		conn.setRequestProperty("Accept", "application/json");
		OutputStream oss = conn.getOutputStream();
		oss.write(req.getBytes());

		try {
			InputStream iss = conn.getInputStream();
			JsonReader rdr = Json.createReader(iss);
			return rdr.read();
		} catch (IOException e) {
			Utility.printJsonMessage("IOException when sending ", request);
			throw e;
		} finally {
			oss.close();
		}
	}

	public static void postUploadJobResult(ServerConfig config, JsonObject msg)
			throws IOException {
		post(config.m_serverHostAndPort, "/factory/uploadjobresult", msg);
	}

	public static JsonObject postRegister(ServerConfig config)
			throws IOException {
		JsonStructure request = Json.createArrayBuilder().build();
		return (JsonObject) post(config.m_serverHostAndPort,
				"/factory/register", request);
	}

	public static JsonStructure getFactoryKey(ServerConfig config) {
		return (Json.createObjectBuilder()
				.add("factoryKey", config.m_factoryKey)
				.add("factoryId", config.m_factoryId).build());
	}
}