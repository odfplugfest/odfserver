package org.odfserver;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.jdt.annotation.Nullable;

public class Runner {
	private String m_stdout = "";
	@Nullable
	private byte[] m_stdoutraw;
	private String m_stderr = "";
	private int m_exitCode;
	private String m_cmd = "";
	private boolean m_isTimeout;

	public int getExitCode() {
		return m_exitCode;
	}

	public boolean isTimeout() {
		return m_isTimeout;
	}

	public String getStdOut() {
		return m_stdout;
	}

	public String cmd() {
		return m_cmd;
	}

	@Nullable
	public byte[] getStdOutRaw() {
		return m_stdoutraw;
	}

	public String getStdErr() {
		return m_stderr;
	}

	static final Map<String, String> exeCache = new HashMap<String, String>();

	@Nullable
	public static String resolveExe(String exe) {
		if (exeCache.containsKey(exe)) {
			return exeCache.get(exe);
		}
		File f = new File(exe);
		if (!f.exists()) {
			String paths[] = System.getenv("PATH").split(File.pathSeparator);
			for (String p : paths) {
				f = new File(p, exe);
				if (f.exists()) {
					break;
				}
				// look for windows executable
				f = new File(p, exe + ".exe");
				if (f.exists()) {
					break;
				}
			}
		}
		try {
			File n = f.getCanonicalFile();
			// getCanonicalFile resolves symbolic links, this is only ok if
			// the name of the executable does not change
			if (n.getName().equals(f.getName())) {
				f = n;
			}
		} catch (IOException e) {
		}
		if (f.isFile()) {
			exeCache.put(exe, f.getPath());
			return f.getPath();
		}
		return null;
	}

	private void clear() {
		m_cmd = "";
		m_stdout = "";
		m_stdoutraw = null;
		m_stderr = "";
		m_exitCode = 0;
		m_isTimeout = false;
	}

	@Nullable
	public Path executeProcess(@Nullable Path outputFilePath, ProcessBuilder pb,
			boolean delay) throws IOException, InterruptedException {
		clear();
		pb.environment().clear();
		Map<String, String> env = pb.environment();
		for (String s : m_environmentToPassAlong) {
			String val = System.getenv(s);
			if (val != null) {
				env.put(s, val);
			}
		}
		m_cmd = String.join(" ", pb.command());
		Process p = pb.start();
		Reader stdout = new Reader(p.getInputStream());
		Reader stderr = new Reader(p.getErrorStream());
		stdout.start();
		stderr.start();

		try {
			p.getOutputStream().close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		if (delay) {
			Thread.sleep(50);
		}
		Waiter w = new Waiter(Thread.currentThread(), 60000);
		try {
			p.waitFor();
		} catch (InterruptedException e) {
			m_isTimeout = true;
		} finally {
			w.stop();
			stdout.stopReading();
			stderr.stopReading();
			p.destroy();
			if (m_isTimeout) {
				m_exitCode = -17;
			} else {
				m_exitCode = p.exitValue();
			}
			try {
				m_stdoutraw = stdout.out.toByteArray();
				try {
					m_stdout = stdout.out.toString("UTF-8");
				} catch (UnsupportedEncodingException e) {
					m_stdout = stdout.out.toString();
				}
				try {
					m_stderr = stderr.out.toString("UTF-8");
				} catch (UnsupportedEncodingException e) {
					m_stderr = stderr.out.toString();
				}
			} catch (OutOfMemoryError e) {
				// happens when there a lot of output on stderr or stdout
			}
		}
		return outputFilePath;
	}

	private String m_environmentToPassAlong[] = {};

	public void setEnvironmentToPassAlong(String environmentToPassAlong[]) {
		m_environmentToPassAlong = environmentToPassAlong;
	}
};

class Reader extends Thread {

	final InputStream in;
	final ByteArrayOutputStream out;
	boolean keepRunning;

	Reader(InputStream in) {
		this.in = in;
		out = new ByteArrayOutputStream();
		keepRunning = true;
	}

	public void stopReading() {
		setKeepRunning(false);
		this.interrupt();
		try {
			join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	synchronized private void setKeepRunning(boolean value) {
		keepRunning = value;
	}

	synchronized private boolean keepRunning() {
		return keepRunning;
	}

	protected void tryToReadSomeBytes(byte b[], int l) throws IOException {
		int n = in.available();
		while (n > 0) {
			if (n > l) {
				n = l;
			}
			in.read(b, 0, n);
			out.write(b, 0, n);
			n = in.available();
		}
	}

	public void run() {
		final int l = 1024;
		byte b[] = new byte[l];
		try {
			while (keepRunning()) {
				tryToReadSomeBytes(b, l);
				try {
					sleep(1);
				} catch (InterruptedException e) {
				}
			}
			tryToReadSomeBytes(b, l);
			in.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
