package org.odfserver;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Properties;

import org.eclipse.jdt.annotation.Nullable;
import org.odfserver.exceptions.ArgumentException;
import org.odfserver.exceptions.NoFactoryException;
import org.odfserver.types.FileType;
import org.odfserver.types.Service;
import org.odfserver.types.ServiceInstance;
import org.odfserver.types.ServiceType;
import org.odfserver.types.Software;

public abstract class Sniffer {

	public final String family;
	public final String name;
	public final boolean osDependent;
	public final boolean debugOnly;

	protected Sniffer(String family, String name, boolean osDependent,
			boolean debugOnly) {
		this.family = family;
		this.name = name;
		this.osDependent = osDependent;
		this.debugOnly = debugOnly;
	}

	public abstract OdfFactoryConfig sniff() throws NoFactoryException;

	@Nullable
	public OdfFactory createFactory(Properties properties)
			throws IOException, ArgumentException, NoFactoryException {
		OdfFactoryConfig config = OdfFactoryConfig.configRead(properties);
		if (config.appPath != null) {
			File app = new File(config.appPath);
			if (!app.isFile()) {
				throw new ArgumentException(app + " is not file.");
			}
			if (!app.canExecute()) {
				throw new ArgumentException(
						app + " is not an executable program.");
			}
		}
		OdfFactory factory = createFactory(config);
		if (!factory.checkVersion(config.version)) {
			throw new ArgumentException(
					"Version has changed from " + config.version + ".");
		}
		if (!config.enabled) {
			return null;
		}
		return createFactory(config);
	}

	protected abstract OdfFactory createFactory(OdfFactoryConfig config)
			throws IOException, ArgumentException;

	protected List<Service> a(Service... c) {
		return Collections.unmodifiableList(Arrays.asList(c));
	}

	protected List<Service> a(FileType in[], FileType out[]) {
		List<Service> l = new ArrayList<Service>(in.length * out.length);
		for (FileType i : in) {
			for (FileType o : out) {
				if (i.equals(o)) {
					l.add(new Service(ServiceType.roundtrip, i, o));
				} else {
					l.add(new Service(ServiceType.convert, i, o));
				}
			}
		}
		return Collections.unmodifiableList(l);
	}

	public abstract List<Service> getServices();

	public static Service service(ServiceType s, FileType i, FileType o) {
		if (!i.equals(o) && ServiceType.roundtrip.equals(s)) {
			throw new Error("Implementation error: " + i + " != " + o
					+ ". In a roundtrip service input and output types must be the same.");
		}
		return new Service(s, i, o);
	}

	public List<ServiceInstance> getServiceInstances(OdfFactory factory) {
		Software software = new Software(factory.config.family, name,
				factory.config.version, "", "", "");
		if (osDependent) {
			software = PlatformInformation.createSoftware(factory.config.family,
					name, factory.config.version);
		}
		List<Service> services = getServices();
		List<ServiceInstance> list = new ArrayList<ServiceInstance>(
				services.size());
		for (Service service : services) {
			list.add(new ServiceInstance(software, service));
		}
		return Collections.unmodifiableList(list);
	}

	public static String getExecutableFullPath(String executableName)
			throws NoFactoryException {
		final String appPath = Runner.resolveExe(executableName);
		if (appPath == null) {
			String emsg = "can't find " + executableName
					+ " please make sure it is in your PATH.";
			throw new NoFactoryException(emsg);
		}
		return appPath;
	}

	public static String sniffCheckHaveVersionOrDie(@Nullable String ver,
			@Nullable String appPath) throws IOException {
		if (ver == null || ver.equals("")) {
			String emsg = "can't work out software version for executable at: "
					+ appPath;
			throw new IOException(emsg);
		}
		return ver;
	}

	public static void runSanityCheckConversion() {
		// FIXME
	}
}