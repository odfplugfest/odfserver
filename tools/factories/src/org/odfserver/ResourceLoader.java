
package org.odfserver;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;

// The PNG factories make use of pdfbox.
// 
// pdfbox uses log4j and wants to find it initialized. it seems better
// to abstract the initialization out to here so that multiple
// factories might use log4j and do not step on each other by
// reinitializing things if they, or code that they call does use
// log4j
// 
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.PropertyConfigurator;

public class ResourceLoader {

	static private void extract(String target, String path) throws IOException {
		Files.createDirectories(Paths.get(target).getParent());
		InputStream i = ResourceLoader.class.getClassLoader()
				.getResourceAsStream(path);
		try {
			FileOutputStream out = new FileOutputStream(target);
			int c;
			while ((c = i.read()) != -1) {
				out.write(c);
			}
			out.flush();
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static String getExtractedJarFilePath(String tmpdir, String path)
			throws IOException {
		File p = new File(tmpdir + File.separator + path);
		if (!p.exists()) {
			extract(p.getPath(), path);
		}
		return p.getAbsolutePath();
	}

	void ensureDLLs() throws IOException {
		String dll = "jacob-1.18-x64.dll";
		if ("32".equals(System.getProperty("sun.arch.data.model"))) {
			dll = "jacob-1.18-x86.dll";
		}
		if (new File(dll).exists()) {
			return;
		}
		getExtractedJarFilePath("lib", dll);
		Files.copy(Paths.get("lib/" + dll), Paths.get(dll));
	}

	public ResourceLoader() throws IOException {
		if (PlatformInformation.isWindows()) {
			ensureDLLs();
		}
		URL url = Utility.class.getResource("/log4j.config");
		InputStream iss = url.openStream();
		PropertyConfigurator.configure(iss);
	}

}
