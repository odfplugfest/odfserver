package org.odfserver.types;

import java.util.Objects;

import org.eclipse.jdt.annotation.Nullable;

public class Service {
	public final ServiceType name;
	public final FileType inputType;
	public final FileType outputType;

	public Service(ServiceType name, FileType inputType, FileType outputType) {
		this.name = name;
		this.inputType = inputType;
		this.outputType = outputType;
	}

	public boolean equals(@Nullable Object o) {
		if (o == this) {
			return true;
		} else if (o == null || !(o instanceof Service)) {
			return false;
		}
		Service j = (Service) o;
		return name.equals(j.name) && inputType.equals(j.inputType)
				&& outputType.equals(j.outputType);
	}

	public int hashCode() {
		return Objects.hash(name, inputType, outputType);
	}
}
