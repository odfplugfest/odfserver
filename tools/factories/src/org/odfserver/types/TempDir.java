package org.odfserver.types;

import java.nio.file.Path;
import java.util.Objects;

import org.eclipse.jdt.annotation.Nullable;

public class TempDir {
	public final Path directoryPath;
	public final Path inputPath;
	public final Path outputPath;

	public TempDir(Path directoryPath, Path inputPath, Path outputPath) {
		this.directoryPath = directoryPath;
		this.inputPath = inputPath;
		this.outputPath = outputPath;
	}

	public boolean equals(@Nullable Object o) {
		if (o == this) {
			return true;
		} else if (o == null || !(o instanceof TempDir)) {
			return false;
		}
		TempDir j = (TempDir) o;
		return directoryPath.equals(j.directoryPath)
				&& inputPath.equals(j.inputPath)
				&& outputPath.equals(j.outputPath);
	}

	public int hashCode() {
		return Objects.hash(directoryPath, inputPath, outputPath);
	}
}
