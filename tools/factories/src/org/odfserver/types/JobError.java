package org.odfserver.types;

public enum JobError {
	None, ServiceNotAvailable, SoftwareTimeout, SoftwareCrash, SoftwareError;
}
