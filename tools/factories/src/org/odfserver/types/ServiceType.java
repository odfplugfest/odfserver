package org.odfserver.types;

public enum ServiceType {
	clip, convert, roundtrip, validate, render
}
