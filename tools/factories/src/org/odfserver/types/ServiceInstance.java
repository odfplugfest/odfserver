package org.odfserver.types;

import java.util.Objects;

import javax.json.Json;
import javax.json.JsonObject;

import org.eclipse.jdt.annotation.Nullable;

public class ServiceInstance {

	public final String softwareFamily;
	public final String softwareName;
	public final String softwareVersion;
	public final String osName;
	public final String osVersion;
	public final String platform;
	public final ServiceType serviceName;
	public final FileType inputType;
	public final FileType outputType;

	public ServiceInstance(JsonObject service) {
		JsonObject software = service.getJsonObject("software");
		softwareFamily = software.getString("softwareFamily");
		softwareName = software.getString("softwareName");
		softwareVersion = software.getString("softwareVersion");
		osName = software.getString("osName");
		osVersion = software.getString("osVersion");
		platform = software.getString("platform");
		service = service.getJsonObject("service");
		serviceName = ServiceType.valueOf(service.getString("name"));
		inputType = FileType.valueOf(service.getString("inputType"));
		outputType = FileType.valueOf(service.getString("outputType"));
	}

	public ServiceInstance(Software software, Service service) {
		softwareFamily = software.softwareFamily;
		softwareName = software.softwareName;
		softwareVersion = software.softwareVersion;
		osName = software.osName;
		osVersion = software.osVersion;
		platform = software.platform;
		serviceName = service.name;
		inputType = service.inputType;
		outputType = service.outputType;
	}

	public JsonObject toJson() {
		JsonObject software = Json.createObjectBuilder()
				.add("softwareFamily", softwareFamily)
				.add("softwareName", softwareName)
				.add("softwareVersion", softwareVersion).add("osName", osName)
				.add("osVersion", osVersion).add("platform", platform).build();
		JsonObject service = Json.createObjectBuilder()
				.add("name", serviceName.toString())
				.add("inputType", inputType.toString())
				.add("outputType", outputType.toString()).build();
		return Json.createObjectBuilder().add("software", software)
				.add("service", service).build();
	}

	public boolean equals(@Nullable Object o) {
		if (o == this) {
			return true;
		} else if (o == null || !(o instanceof ServiceInstance)) {
			return false;
		}
		ServiceInstance j = (ServiceInstance) o;
		return softwareFamily.equals(j.softwareFamily)
				&& softwareName.equals(j.softwareName)
				&& softwareVersion.equals(j.softwareVersion)
				&& osName.equals(j.osName) && osVersion.equals(j.osVersion)
				&& platform.equals(j.platform)
				&& serviceName.equals(j.serviceName)
				&& inputType.equals(j.inputType)
				&& outputType.equals(j.outputType);
	}

	public int hashCode() {
		return Objects.hash(softwareFamily, softwareName, softwareVersion,
				osName, platform, serviceName, inputType, outputType);
	}
}
