package org.odfserver.types;

import java.util.Objects;

import org.eclipse.jdt.annotation.Nullable;

public class Software {
	public final String softwareFamily;
	public final String softwareName;
	public final String softwareVersion;
	public final String osName;
	public final String osVersion;
	public final String platform;

	public Software(String softwareFamily, String softwareName,
			String softwareVersion, String osName, String osVersion,
			String platform) {
		this.softwareFamily = softwareFamily;
		this.softwareName = softwareName;
		this.softwareVersion = softwareVersion;
		this.osName = osName;
		this.osVersion = osVersion;
		this.platform = platform;
	}

	public boolean equals(@Nullable Object o) {
		if (o == this) {
			return true;
		} else if (o == null || !(o instanceof Software)) {
			return false;
		}
		Software j = (Software) o;
		return softwareFamily.equals(j.softwareFamily)
				&& softwareName.equals(j.softwareName)
				&& softwareVersion.equals(j.softwareVersion)
				&& osName.equals(j.osName) && osVersion.equals(j.osVersion)
				&& platform.equals(j.platform);
	}

	public int hashCode() {
		return Objects.hash(softwareFamily, softwareName, softwareVersion,
				osName, osVersion, platform);
	}
}
