package org.odfserver.types;

public enum OdfVersion {
	v1_0, v1_1, v1_2, v1_2EXT
}
