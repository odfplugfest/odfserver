package org.odfserver.types;

public enum FileType {
	ODT1_0, ODT1_1, ODT1_2, ODT1_2EXT,
	//
	ODS1_0, ODS1_1, ODS1_2, ODS1_2EXT,
	//
	ODP1_0, ODP1_1, ODP1_2, ODP1_2EXT,
	//
	ODG1_0, ODG1_1, ODG1_2, ODG1_2EXT,
	//
	PDF, PNG, VALIDATIONREPORT, XML;

	public boolean isOdt() {
		return this.equals(ODT1_0) || this.equals(ODT1_1) || this.equals(ODT1_2)
				|| this.equals(ODT1_2EXT);
	}

	public boolean isOds() {
		return this.equals(ODS1_0) || this.equals(ODS1_1) || this.equals(ODS1_2)
				|| this.equals(ODS1_2EXT);
	}

	public boolean isOdp() {
		return this.equals(ODP1_0) || this.equals(ODP1_1) || this.equals(ODP1_2)
				|| this.equals(ODP1_2EXT);
	}

	public boolean isOdg() {
		return this.equals(ODG1_0) || this.equals(ODG1_1) || this.equals(ODG1_2)
				|| this.equals(ODG1_2EXT);
	}

	public String extension() {
		String extension;
		if (isOdt()) {
			extension = ".odt";
		} else if (isOds()) {
			extension = ".ods";
		} else if (isOdp()) {
			extension = ".odp";
		} else if (isOdg()) {
			extension = ".odg";
		} else if (PNG.equals(this)) {
			extension = ".png";
		} else if (PDF.equals(this)) {
			extension = ".pdf";
		} else if (XML.equals(this) || VALIDATIONREPORT.equals(this)) {
			extension = ".xml";
		} else {
			throw new Error(
					"Implementation error: no extension defined for " + this);
		}
		return extension;
	}

	public String mimeType() {
		String extension;
		if (isOdt()) {
			extension = "application/vnd.oasis.opendocument.text";
		} else if (isOds()) {
			extension = "application/vnd.oasis.opendocument.spreadsheet";
		} else if (isOdp()) {
			extension = "application/vnd.oasis.opendocument.presentation";
		} else if (isOdg()) {
			extension = "application/vnd.oasis.opendocument.graphics";
		} else if (PNG.equals(this)) {
			extension = "image/png";
		} else if (PDF.equals(this)) {
			extension = "application/pdf";
		} else if (XML.equals(this) || VALIDATIONREPORT.equals(this)) {
			extension = "application/xml";
		} else {
			throw new Error(
					"Implementation error: no extension defined for " + this);
		}
		return extension;
	}

	public org.odftoolkit.odfvalidator.OdfVersion odfVersion() {
		switch (this) {
		case ODT1_0:
		case ODS1_0:
		case ODP1_0:
		case ODG1_0:
			return org.odftoolkit.odfvalidator.OdfVersion.V1_0;
		case ODT1_1:
		case ODS1_1:
		case ODP1_1:
		case ODG1_1:
			return org.odftoolkit.odfvalidator.OdfVersion.V1_1;
		case ODT1_2:
		case ODS1_2:
		case ODP1_2:
		case ODG1_2:
		case ODT1_2EXT:
		case ODS1_2EXT:
		case ODP1_2EXT:
		case ODG1_2EXT:
			return org.odftoolkit.odfvalidator.OdfVersion.V1_2;
		default:
			throw new Error(
					"Implementation error:  odfVersion with " + this + ".");
		}
	}

	public boolean isExtendedODF() {
		switch (this) {
		case ODT1_2EXT:
		case ODS1_2EXT:
		case ODP1_2EXT:
		case ODG1_2EXT:
			return true;
		default:
			return false;
		}
	}

	public static FileType odf(OdfDocumentType type, OdfVersion version) {
		switch (type) {
		case odt:
			switch (version) {
			case v1_0:
				return ODT1_0;
			case v1_1:
				return ODT1_1;
			case v1_2:
				return ODT1_2;
			case v1_2EXT:
				return ODT1_2EXT;
			}
		case ods:
			switch (version) {
			case v1_0:
				return ODS1_0;
			case v1_1:
				return ODS1_1;
			case v1_2:
				return ODS1_2;
			case v1_2EXT:
				return ODS1_2EXT;
			}
		case odp:
			switch (version) {
			case v1_0:
				return ODP1_0;
			case v1_1:
				return ODP1_1;
			case v1_2:
				return ODP1_2;
			case v1_2EXT:
				return ODP1_2EXT;
			}
		case odg:
			switch (version) {
			case v1_0:
				return ODG1_0;
			case v1_1:
				return ODG1_1;
			case v1_2:
				return ODG1_2;
			case v1_2EXT:
				return ODG1_2EXT;
			}
		}
		throw new Error("Implementation error:  odf with " + type + " "
				+ version + ".");
	}

}
