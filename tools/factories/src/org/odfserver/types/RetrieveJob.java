package org.odfserver.types;

import java.util.Objects;

import javax.json.JsonObject;
import javax.xml.bind.DatatypeConverter;

import org.eclipse.jdt.annotation.Nullable;

public class RetrieveJob {

	public final int number;
	public final ServiceInstance service;
	public final byte[] inputFile;

	public RetrieveJob(JsonObject job) {
		number = job.getInt("number");
		service = new ServiceInstance(job.getJsonObject("service"));
		inputFile = DatatypeConverter
				.parseBase64Binary(job.getString("inputFile"));
	}

	public boolean equals(@Nullable Object o) {
		if (o == this) {
			return true;
		} else if (o == null || !(o instanceof RetrieveJob)) {
			return false;
		}
		RetrieveJob j = (RetrieveJob) o;
		return number == j.number && service.equals(j.service)
				&& inputFile.equals(j.inputFile);
	}

	public int hashCode() {
		return Objects.hash(number, service, inputFile);
	}
}