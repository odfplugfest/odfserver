package org.odfserver.types;

import java.util.Objects;

import org.eclipse.jdt.annotation.Nullable;

public class JobResult {

	public final byte outputFiles[][];
	public final int exitCode;
	public final String cmd;
	public final boolean timeout;
	public final String stdout;
	public final String stderr;

	public JobResult(String cmd, byte outputFiles[][], int exitCode,
			@Nullable String stdout, @Nullable String stderr, boolean timeout) {
		this.cmd = cmd;
		this.outputFiles = outputFiles;
		this.exitCode = exitCode;
		this.stdout = stdout == null ? "" : stdout;
		this.stderr = stderr == null ? "" : stderr;
		this.timeout = timeout;
	}

	public static final byte noFiles[][] = new byte[0][];

	public boolean equals(@Nullable Object o) {
		if (o == this) {
			return true;
		} else if (o == null || !(o instanceof JobResult)) {
			return false;
		}

		JobResult j = (JobResult) o;
		return cmd.equals(j.cmd) && outputFiles.equals(j.outputFiles)
				&& exitCode == j.exitCode && stdout.equals(j.stdout)
				&& stderr.equals(j.stderr) && timeout == j.timeout;
	}

	public int hashCode() {
		return Objects.hash(cmd, outputFiles, exitCode, stdout, stderr,
				timeout);
	}
}