package org.odfserver.types;

import java.time.Instant;
import java.util.Objects;

import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.xml.bind.DatatypeConverter;

import org.eclipse.jdt.annotation.Nullable;
import org.odfserver.ServerConfig;

public class UploadJobResult {

	public final String factoryKey;
	public final String factoryId;
	public final int number;
	public final Instant started;

	public UploadJobResult(ServerConfig c, int number) {
		factoryKey = c.m_factoryKey;
		factoryId = c.m_factoryId;
		this.number = number;
		this.started = Instant.now();
	}

	// for some reason uploading jobs fails if stdout or stderr is too large
	// this function is there, hopefully temporarily to limit the size of
	// stderr and stdout
	private static String cullStdOutErr(String std) {
		final int maxStdOutSize = 1024;
		if (std.length() > maxStdOutSize) {
			std = std.substring(0, maxStdOutSize);
		}
		return std;
	}

	public JsonObject finish(JobResult result, JobError error) {
		final Instant finished = Instant.now();

		final JsonArrayBuilder fileBuilder = Json.createArrayBuilder();
		for (byte file[] : result.outputFiles) {
			fileBuilder.add(DatatypeConverter.printBase64Binary(file));
		}
		if (result.timeout) {
			error = JobError.SoftwareTimeout;
		}
		// if exit code was not 0 or there were no output files, there was
		// a software error
		if (JobError.None.equals(error)
				&& (result.exitCode != 0 || result.outputFiles.length == 0)) {
			error = JobError.SoftwareError;
		}
		JsonObject request = Json.createObjectBuilder()
				.add("factory",
						Json.createObjectBuilder().add("factoryKey", factoryKey)
								.add("factoryId", factoryId))
				.add("number", number).add("started", started.toString())
				.add("finished", finished.toString())
				.add("commandLine", result.cmd)
				.add("outputFiles", fileBuilder.build())
				.add("error", error.toString()).add("exitCode", result.exitCode)
				.add("stdout", cullStdOutErr(result.stdout))
				.add("stderr", cullStdOutErr(result.stderr)).build();
		return request;
	}

	public boolean equals(@Nullable Object o) {
		if (o == this) {
			return true;
		} else if (o == null || !(o instanceof UploadJobResult)) {
			return false;
		}
		UploadJobResult j = (UploadJobResult) o;
		return factoryKey.equals(j.factoryKey) && factoryId.equals(j.factoryId)
				&& number == j.number && started.equals(j.started);
	}

	public int hashCode() {
		return Objects.hash(factoryKey, factoryId, number, started);
	}
}