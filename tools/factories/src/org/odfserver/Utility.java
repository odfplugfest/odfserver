package org.odfserver;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonStructure;
import javax.json.JsonValue;
import javax.json.JsonWriter;
import javax.json.JsonWriterFactory;
import javax.json.stream.JsonGenerator;

import org.apache.commons.io.IOUtils;
import org.eclipse.jdt.annotation.Nullable;
import org.odfserver.exceptions.NoFactoryException;
import org.w3c.dom.Document;
import org.w3c.dom.bootstrap.DOMImplementationRegistry;
import org.w3c.dom.ls.DOMImplementationLS;
import org.w3c.dom.ls.LSOutput;
import org.w3c.dom.ls.LSSerializer;

public class Utility {

	final protected static char[] hexArray = "0123456789ABCDEF".toCharArray();

	public static String bytesToHex(byte[] bytes) {
		char[] hexChars = new char[bytes.length * 2];
		for (int j = 0; j < bytes.length; j++) {
			int v = bytes[j] & 0xFF;
			hexChars[j * 2] = hexArray[v >>> 4];
			hexChars[j * 2 + 1] = hexArray[v & 0x0F];
		}
		return new String(hexChars);
	}

	public static final String digest(byte data[]) {
		MessageDigest md;
		try {
			md = MessageDigest.getInstance("SHA-256");
		} catch (NoSuchAlgorithmException e) {
			throw new Error(e);
		}
		md.update(data);
		return bytesToHex(md.digest());
	}

	private static DOMImplementationRegistry domRegistry = getRegistry();

	private static final DOMImplementationRegistry getRegistry() {
		DOMImplementationRegistry d = null;
		try {
			d = DOMImplementationRegistry.newInstance();
		} catch (ClassNotFoundException | InstantiationException
				| IllegalAccessException | ClassCastException e) {
			throw new Error(e);
		}
		return d;
	}

	public static final String stacktraceToString(Exception e) {
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		e.printStackTrace(pw);
		return sw.toString();
	}

	public static final byte[] serialize(Document xml) {
		DOMImplementationLS impl = (DOMImplementationLS) domRegistry
				.getDOMImplementation("LS");
		LSSerializer serializer = impl.createLSSerializer();
		LSOutput lso = impl.createLSOutput();
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		lso.setByteStream(out);
		serializer.write(xml, lso);
		return out.toByteArray();
	}

	public static byte[] readResource(String relativePath) throws IOException {
		URL url = Utility.class.getResource(relativePath);
		return IOUtils.toByteArray(url.openStream());
	}

	@Nullable
	public static String chomp(@Nullable String s) {
		if (s != null)
			return s.trim();
		return s;
	}

	@Nullable
	public static String findInStrings(String lines[], Pattern needle) {
		String ret = "";
		for (String line : lines) {
			Matcher m = needle.matcher(line);
			if (m.matches()) {
				ret = m.group("selected");
				return ret;
			}
		}
		return ret;
	}

	@Nullable
	public static String pickSelectedFromMultiLineString(
			@Nullable String haystack, Pattern needle) {
		if (haystack == null) {
			haystack = "";
		}
		String lines[] = haystack.split("\\r?\\n");
		return findInStrings(lines, needle);
	}

	public static String executeProcessToString(Process proc)
			throws IOException {
		byte[] bytes = IOUtils.toByteArray(proc.getInputStream());
		byte[] err = IOUtils.toByteArray(proc.getErrorStream());
		if (err.length > 0) {
			System.err.println(new String(err));
		}
		String str;
		try {
			str = new String(bytes, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			str = new String(bytes);
		}
		return str;
	}

	public static String[] executeProcessToStrings(Process proc)
			throws IOException {
		return executeProcessToString(proc).split("\\r?\\n");
	}

	@Nullable
	public static String executeProcessToRegexCapture(Process proc, Pattern pat)
			throws IOException {
		String lines[] = executeProcessToStrings(proc);
		return findInStrings(lines, pat);
	}

	private static JsonValue cloneForPrint(JsonValue s) {
		if (s instanceof JsonObject) {
			JsonObjectBuilder clone = Json.createObjectBuilder();
			for (Entry<String, JsonValue> e : ((JsonObject) s).entrySet()) {
				if ("inputFile".equals(e.getKey())
						|| "outputFiles".equals(e.getKey())) {
					clone.add(e.getKey(), "...");
				} else {
					clone.add(e.getKey(), cloneForPrint(e.getValue()));
				}
			}
			s = clone.build();
		} else if (s instanceof JsonArray) {
			JsonArrayBuilder clone = Json.createArrayBuilder();
			for (JsonValue v : ((JsonArray) s)) {
				clone.add(cloneForPrint(v));
			}
			s = clone.build();
		}
		return s;
	}

	private static final JsonWriterFactory prettyJsonFactory;

	static {
		Map<String, Boolean> config = new HashMap<String, Boolean>();
		config.put(JsonGenerator.PRETTY_PRINTING, true);
		prettyJsonFactory = Json.createWriterFactory(config);
	}

	public static void printJsonMessage(String msg, JsonStructure json) {
		json = (JsonStructure) cloneForPrint(json);
		StringWriter stringWriter = new StringWriter();
		JsonWriter writer = prettyJsonFactory.createWriter(stringWriter);
		writer.write(json);
		writer.close();
		System.out.println(msg + stringWriter.toString());
	}

	public static String jsonToString(JsonStructure value) {
		ByteArrayOutputStream jsonbytes = new ByteArrayOutputStream();
		JsonWriter writer = Json.createWriter(jsonbytes);
		writer.write(value);
		try {
			return jsonbytes.toString("UTF-8");
		} catch (UnsupportedEncodingException e) {
			throw new Error("Implementation error", e);
		}
	}

	public static String getStackTraceAsString(Throwable t) {
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		t.printStackTrace(pw);
		return sw.toString();
	}

	public static void killWindowsProcess(String name) {
		String taskkill;
		try {
			taskkill = Sniffer.getExecutableFullPath("taskkill.exe");
		} catch (NoFactoryException e1) {
			e1.printStackTrace();
			return;
		}
		ProcessBuilder pb = new ProcessBuilder(taskkill, "/F", "/IM",
				name.toUpperCase(), "/T");
		try {
			String s = executeProcessToString(pb.start());
			System.out.println(s);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
};
