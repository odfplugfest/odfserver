
package org.odfserver;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Properties;

import org.eclipse.jdt.annotation.Nullable;

public class OdfFactoryConfig {
	final public String family;
	final public String name;
	@Nullable
	public final String appPath;
	public final long detectionTime;
	public final String version;
	public final boolean enabled;

	static String getProperty(Properties p, String name) {
		String v = p.getProperty(name);
		if (v == null) {
			throw new NullPointerException(
					name + " was not in the properties file.");
		}
		return v;
	}

	private OdfFactoryConfig(Properties p) {
		family = getProperty(p, "family");
		name = getProperty(p, "name");
		appPath = p.getProperty("appPath");
		detectionTime = Long.parseLong(getProperty(p, "detection-time"));
		version = getProperty(p, "version");
		enabled = "true".equals(getProperty(p, "enabled"));
	}

	public OdfFactoryConfig(String family, String name,
			@Nullable String appPath, long detectionTime, String version,
			boolean enabled) {
		this.family = family;
		this.name = name;
		this.appPath = appPath;
		this.detectionTime = detectionTime;
		this.version = version;
		this.enabled = enabled;
	}

	public void write(Properties p) {
		p.setProperty("family", family);
		p.setProperty("name", name);
		if (appPath != null) {
			p.setProperty("appPath", appPath);
		}
		p.setProperty("detection-time", String.valueOf(detectionTime));
		p.setProperty("version", version);
		p.setProperty("enabled", String.valueOf(enabled));
	}

	public void write(String configPath) throws IOException {
		Properties p = new Properties();
		write(p);
		OutputStream output = new FileOutputStream(configPath);
		p.store(output, null);
	}

	public static OdfFactoryConfig configRead(Properties properties) {
		return new OdfFactoryConfig(properties);
	}

};
