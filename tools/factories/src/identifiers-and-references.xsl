<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<xsl:stylesheet xmlns:xs="http://www.w3.org/2001/XMLSchema"
                xmlns:xsd="http://www.w3.org/2001/XMLSchema"
                xmlns:saxon="http://saxon.sf.net/"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:schold="http://www.ascc.net/xml/schematron"
                xmlns:iso="http://purl.oclc.org/dsdl/schematron"
                xmlns:xhtml="http://www.w3.org/1999/xhtml"
                xmlns:f="urn:f"
                xmlns:anim="urn:oasis:names:tc:opendocument:xmlns:animation:1.0"
                xmlns:chart="urn:oasis:names:tc:opendocument:xmlns:chart:1.0"
                xmlns:config="urn:oasis:names:tc:opendocument:xmlns:config:1.0"
                xmlns:db="urn:oasis:names:tc:opendocument:xmlns:database:1.0"
                xmlns:dc="http://purl.org/dc/elements/1.1/"
                xmlns:dr3d="urn:oasis:names:tc:opendocument:xmlns:dr3d:1.0"
                xmlns:draw="urn:oasis:names:tc:opendocument:xmlns:drawing:1.0"
                xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0"
                xmlns:form="urn:oasis:names:tc:opendocument:xmlns:form:1.0"
                xmlns:grddl="http://www.w3.org/2003/g/data-view#"
                xmlns:math="http://www.w3.org/1998/Math/MathML"
                xmlns:meta="urn:oasis:names:tc:opendocument:xmlns:meta:1.0"
                xmlns:number="urn:oasis:names:tc:opendocument:xmlns:datastyle:1.0"
                xmlns:office="urn:oasis:names:tc:opendocument:xmlns:office:1.0"
                xmlns:presentation="urn:oasis:names:tc:opendocument:xmlns:presentation:1.0"
                xmlns:rng="http://relaxng.org/ns/structure/1.0"
                xmlns:script="urn:oasis:names:tc:opendocument:xmlns:script:1.0"
                xmlns:smil="urn:oasis:names:tc:opendocument:xmlns:smil-compatible:1.0"
                xmlns:style="urn:oasis:names:tc:opendocument:xmlns:style:1.0"
                xmlns:svg="urn:oasis:names:tc:opendocument:xmlns:svg-compatible:1.0"
                xmlns:table="urn:oasis:names:tc:opendocument:xmlns:table:1.0"
                xmlns:text="urn:oasis:names:tc:opendocument:xmlns:text:1.0"
                xmlns:xforms="http://www.w3.org/2002/xforms"
                xmlns:xlink="http://www.w3.org/1999/xlink"
                version="2.0"><!--Implementers: please note that overriding process-prolog or process-root is 
    the preferred method for meta-stylesheets to use where possible. -->
   <xsl:param name="archiveDirParameter"/>
   <xsl:param name="archiveNameParameter"/>
   <xsl:param name="fileNameParameter"/>
   <xsl:param name="fileDirParameter"/>
   <xsl:variable name="document-uri">
      <xsl:value-of select="document-uri(/)"/>
   </xsl:variable>

   <!--PHASES-->


   <!--PROLOG-->
   <xsl:output xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
               method="xml"
               omit-xml-declaration="no"
               standalone="yes"
               indent="yes"/>

   <!--XSD TYPES FOR XSLT2-->


   <!--KEYS AND FUNCTIONS-->
   <xsl:function as="xsd:string*" name="f:missing-ids">
      <xsl:param as="attribute(*)*" name="ids"/>
      <xsl:param as="attribute(*)*" name="refs"/>
      <xsl:param as="xsd:string" name="separator"/>
      <xsl:for-each select="if ($separator) then $refs/tokenize(.,$separator) else $refs/string(.)">
         <xsl:variable as="xsd:string" name="ref" select="."/>
         <xsl:if test="not($ids[string(.)=$ref])">
            <xsl:sequence select="$ref"/>
         </xsl:if>
      </xsl:for-each>
   </xsl:function>
   <xsl:function as="xsd:string*" name="f:not-preceding-ids">
      <xsl:param as="attribute(*)*" name="ids"/>
      <xsl:param as="attribute(*)*" name="refs"/>
      <xsl:param as="xsd:string" name="separator"/>
      <xsl:for-each select="$refs">
         <xsl:variable as="attribute(*)" name="ref" select="."/>
         <xsl:for-each select="if ($separator) then tokenize($ref,$separator) else string($ref)">
            <xsl:variable as="xsd:string" name="str" select="."/>
            <xsl:if test="not($ids[$ref &gt;&gt; .][string(.)=$str])">
               <xsl:sequence select="$ref"/>
            </xsl:if>
         </xsl:for-each>
      </xsl:for-each>
   </xsl:function>

   <!--DEFAULT RULES-->


   <!--MODE: SCHEMATRON-SELECT-FULL-PATH-->
   <!--This mode can be used to generate an ugly though full XPath for locators-->
   <xsl:template match="*" mode="schematron-select-full-path">
      <xsl:apply-templates select="." mode="schematron-get-full-path"/>
   </xsl:template>

   <!--MODE: SCHEMATRON-FULL-PATH-->
   <!--This mode can be used to generate an ugly though full XPath for locators-->
   <xsl:template match="*" mode="schematron-get-full-path">
      <xsl:apply-templates select="parent::*" mode="schematron-get-full-path"/>
      <xsl:text>/</xsl:text>
      <xsl:choose>
         <xsl:when test="namespace-uri()=''">
            <xsl:value-of select="name()"/>
         </xsl:when>
         <xsl:otherwise>
            <xsl:text>*:</xsl:text>
            <xsl:value-of select="local-name()"/>
            <xsl:text>[namespace-uri()='</xsl:text>
            <xsl:value-of select="namespace-uri()"/>
            <xsl:text>']</xsl:text>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="preceding"
                    select="count(preceding-sibling::*[local-name()=local-name(current())                                   and namespace-uri() = namespace-uri(current())])"/>
      <xsl:text>[</xsl:text>
      <xsl:value-of select="1+ $preceding"/>
      <xsl:text>]</xsl:text>
   </xsl:template>
   <xsl:template match="@*" mode="schematron-get-full-path">
      <xsl:apply-templates select="parent::*" mode="schematron-get-full-path"/>
      <xsl:text>/</xsl:text>
      <xsl:choose>
         <xsl:when test="namespace-uri()=''">@<xsl:value-of select="name()"/>
         </xsl:when>
         <xsl:otherwise>
            <xsl:text>@*[local-name()='</xsl:text>
            <xsl:value-of select="local-name()"/>
            <xsl:text>' and namespace-uri()='</xsl:text>
            <xsl:value-of select="namespace-uri()"/>
            <xsl:text>']</xsl:text>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>

   <!--MODE: SCHEMATRON-FULL-PATH-2-->
   <!--This mode can be used to generate prefixed XPath for humans-->
   <xsl:template match="node() | @*" mode="schematron-get-full-path-2">
      <xsl:for-each select="ancestor-or-self::*">
         <xsl:text>/</xsl:text>
         <xsl:value-of select="name(.)"/>
         <xsl:if test="preceding-sibling::*[name(.)=name(current())]">
            <xsl:text>[</xsl:text>
            <xsl:value-of select="count(preceding-sibling::*[name(.)=name(current())])+1"/>
            <xsl:text>]</xsl:text>
         </xsl:if>
      </xsl:for-each>
      <xsl:if test="not(self::*)">
         <xsl:text/>/@<xsl:value-of select="name(.)"/>
      </xsl:if>
   </xsl:template>
   <!--MODE: SCHEMATRON-FULL-PATH-3-->
   <!--This mode can be used to generate prefixed XPath for humans 
	(Top-level element has index)-->
   <xsl:template match="node() | @*" mode="schematron-get-full-path-3">
      <xsl:for-each select="ancestor-or-self::*">
         <xsl:text>/</xsl:text>
         <xsl:value-of select="name(.)"/>
         <xsl:if test="parent::*">
            <xsl:text>[</xsl:text>
            <xsl:value-of select="count(preceding-sibling::*[name(.)=name(current())])+1"/>
            <xsl:text>]</xsl:text>
         </xsl:if>
      </xsl:for-each>
      <xsl:if test="not(self::*)">
         <xsl:text/>/@<xsl:value-of select="name(.)"/>
      </xsl:if>
   </xsl:template>

   <!--MODE: GENERATE-ID-FROM-PATH -->
   <xsl:template match="/" mode="generate-id-from-path"/>
   <xsl:template match="text()" mode="generate-id-from-path">
      <xsl:apply-templates select="parent::*" mode="generate-id-from-path"/>
      <xsl:value-of select="concat('.text-', 1+count(preceding-sibling::text()), '-')"/>
   </xsl:template>
   <xsl:template match="comment()" mode="generate-id-from-path">
      <xsl:apply-templates select="parent::*" mode="generate-id-from-path"/>
      <xsl:value-of select="concat('.comment-', 1+count(preceding-sibling::comment()), '-')"/>
   </xsl:template>
   <xsl:template match="processing-instruction()" mode="generate-id-from-path">
      <xsl:apply-templates select="parent::*" mode="generate-id-from-path"/>
      <xsl:value-of select="concat('.processing-instruction-', 1+count(preceding-sibling::processing-instruction()), '-')"/>
   </xsl:template>
   <xsl:template match="@*" mode="generate-id-from-path">
      <xsl:apply-templates select="parent::*" mode="generate-id-from-path"/>
      <xsl:value-of select="concat('.@', name())"/>
   </xsl:template>
   <xsl:template match="*" mode="generate-id-from-path" priority="-0.5">
      <xsl:apply-templates select="parent::*" mode="generate-id-from-path"/>
      <xsl:text>.</xsl:text>
      <xsl:value-of select="concat('.',name(),'-',1+count(preceding-sibling::*[name()=name(current())]),'-')"/>
   </xsl:template>

   <!--MODE: GENERATE-ID-2 -->
   <xsl:template match="/" mode="generate-id-2">U</xsl:template>
   <xsl:template match="*" mode="generate-id-2" priority="2">
      <xsl:text>U</xsl:text>
      <xsl:number level="multiple" count="*"/>
   </xsl:template>
   <xsl:template match="node()" mode="generate-id-2">
      <xsl:text>U.</xsl:text>
      <xsl:number level="multiple" count="*"/>
      <xsl:text>n</xsl:text>
      <xsl:number count="node()"/>
   </xsl:template>
   <xsl:template match="@*" mode="generate-id-2">
      <xsl:text>U.</xsl:text>
      <xsl:number level="multiple" count="*"/>
      <xsl:text>_</xsl:text>
      <xsl:value-of select="string-length(local-name(.))"/>
      <xsl:text>_</xsl:text>
      <xsl:value-of select="translate(name(),':','.')"/>
   </xsl:template>
   <!--Strip characters-->
   <xsl:template match="text()" priority="-1"/>

   <!--SCHEMA SETUP-->
   <xsl:template match="/">
      <svrl:schematron-output xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                              title=""
                              schemaVersion="ISO19757-3">
         <xsl:comment>
            <xsl:value-of select="$archiveDirParameter"/>   
		 <xsl:value-of select="$archiveNameParameter"/>  
		 <xsl:value-of select="$fileNameParameter"/>  
		 <xsl:value-of select="$fileDirParameter"/>
         </xsl:comment>
         <svrl:ns-prefix-in-attribute-values uri="urn:f" prefix="f"/>
         <svrl:ns-prefix-in-attribute-values uri="urn:oasis:names:tc:opendocument:xmlns:animation:1.0" prefix="anim"/>
         <svrl:ns-prefix-in-attribute-values uri="urn:oasis:names:tc:opendocument:xmlns:chart:1.0" prefix="chart"/>
         <svrl:ns-prefix-in-attribute-values uri="urn:oasis:names:tc:opendocument:xmlns:config:1.0" prefix="config"/>
         <svrl:ns-prefix-in-attribute-values uri="urn:oasis:names:tc:opendocument:xmlns:database:1.0" prefix="db"/>
         <svrl:ns-prefix-in-attribute-values uri="http://purl.org/dc/elements/1.1/" prefix="dc"/>
         <svrl:ns-prefix-in-attribute-values uri="urn:oasis:names:tc:opendocument:xmlns:dr3d:1.0" prefix="dr3d"/>
         <svrl:ns-prefix-in-attribute-values uri="urn:oasis:names:tc:opendocument:xmlns:drawing:1.0" prefix="draw"/>
         <svrl:ns-prefix-in-attribute-values uri="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0"
                                             prefix="fo"/>
         <svrl:ns-prefix-in-attribute-values uri="urn:oasis:names:tc:opendocument:xmlns:form:1.0" prefix="form"/>
         <svrl:ns-prefix-in-attribute-values uri="http://www.w3.org/2003/g/data-view#" prefix="grddl"/>
         <svrl:ns-prefix-in-attribute-values uri="http://www.w3.org/1998/Math/MathML" prefix="math"/>
         <svrl:ns-prefix-in-attribute-values uri="urn:oasis:names:tc:opendocument:xmlns:meta:1.0" prefix="meta"/>
         <svrl:ns-prefix-in-attribute-values uri="urn:oasis:names:tc:opendocument:xmlns:datastyle:1.0"
                                             prefix="number"/>
         <svrl:ns-prefix-in-attribute-values uri="urn:oasis:names:tc:opendocument:xmlns:office:1.0" prefix="office"/>
         <svrl:ns-prefix-in-attribute-values uri="urn:oasis:names:tc:opendocument:xmlns:presentation:1.0"
                                             prefix="presentation"/>
         <svrl:ns-prefix-in-attribute-values uri="http://relaxng.org/ns/structure/1.0" prefix="rng"/>
         <svrl:ns-prefix-in-attribute-values uri="urn:oasis:names:tc:opendocument:xmlns:script:1.0" prefix="script"/>
         <svrl:ns-prefix-in-attribute-values uri="urn:oasis:names:tc:opendocument:xmlns:smil-compatible:1.0"
                                             prefix="smil"/>
         <svrl:ns-prefix-in-attribute-values uri="urn:oasis:names:tc:opendocument:xmlns:style:1.0" prefix="style"/>
         <svrl:ns-prefix-in-attribute-values uri="urn:oasis:names:tc:opendocument:xmlns:svg-compatible:1.0"
                                             prefix="svg"/>
         <svrl:ns-prefix-in-attribute-values uri="urn:oasis:names:tc:opendocument:xmlns:table:1.0" prefix="table"/>
         <svrl:ns-prefix-in-attribute-values uri="urn:oasis:names:tc:opendocument:xmlns:text:1.0" prefix="text"/>
         <svrl:ns-prefix-in-attribute-values uri="http://www.w3.org/2002/xforms" prefix="xforms"/>
         <svrl:ns-prefix-in-attribute-values uri="http://www.w3.org/1999/xhtml" prefix="xhtml"/>
         <svrl:ns-prefix-in-attribute-values uri="http://www.w3.org/1999/xlink" prefix="xlink"/>
         <svrl:ns-prefix-in-attribute-values uri="http://www.w3.org/XML/1998/namespace" prefix="xml"/>
         <svrl:active-pattern>
            <xsl:attribute name="document">
               <xsl:value-of select="document-uri(/)"/>
            </xsl:attribute>
            <xsl:apply-templates/>
         </svrl:active-pattern>
         <xsl:apply-templates select="/" mode="M30"/>
         <svrl:active-pattern>
            <xsl:attribute name="document">
               <xsl:value-of select="document-uri(/)"/>
            </xsl:attribute>
            <xsl:apply-templates/>
         </svrl:active-pattern>
         <xsl:apply-templates select="/" mode="M31"/>
      </svrl:schematron-output>
   </xsl:template>

   <!--SCHEMATRON PATTERNS-->
   <xsl:param name="sep" select="&#34;', '&#34;"/>

   <!--PATTERN -->


	  <!--RULE -->
   <xsl:template match="office:document" priority="1002" mode="M30">
      <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl" context="office:document"/>
      <xsl:variable name="document" select="."/>
      <xsl:variable name="office-meta" select="office:meta"/>
      <xsl:variable name="document-styles" select="()"/>
      <xsl:variable name="common-styles"
                    select="office:styles|$document-styles/office:styles"/>
      <xsl:variable name="all-styles" select="$common-styles|office:automatic-styles"/>
      <xsl:variable name="master-pages"
                    select="$document-styles/office:master-styles|office:master-styles"/>
      <xsl:variable name="ids"
                    select="$document-styles/office:font-face-decls/style:font-face/@style:name[ancestor::office:document[1]=$document]|.//style:font-face/@style:name[ancestor::office:document[1]=$document]"/>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@style:font-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for all-fonts: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@style:font-name-asian[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for all-fonts: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@style:font-name-complex[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for all-fonts: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select=".//style:font-face/@style:name[ancestor::office:document[1]=$document]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for local-fonts: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select="$common-styles/style:style[@style:family='text']/@style:name[ancestor::office:document[1]=$document]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for text-style-common: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,$all-styles/style:style[@style:family='text']/@style:parent-style-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for text-style-common: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select="$all-styles/style:style[@style:family='text']/@style:name[ancestor::office:document[1]=$document]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for text-style: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@style:leader-text-style[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for text-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@style:style-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for text-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@style:text-line-through-text-style[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for text-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@text:citation-body-style-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for text-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@text:citation-style-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for text-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:span/@text:class-names[ancestor::office:document[1]=$document],'\s+')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for text-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@text:main-entry-style-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for text-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:a/@text:style-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for text-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:alphabetical-index/@text:style-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for text-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:index-entry-bibliography/@text:style-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for text-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:index-entry-chapter/@text:style-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for text-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:index-entry-link-end/@text:style-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for text-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:index-entry-link-start/@text:style-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for text-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:index-entry-page-number/@text:style-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for text-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:index-entry-span/@text:style-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for text-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:index-entry-tab-stop/@text:style-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for text-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:index-entry-text/@text:style-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for text-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:index-title-template/@text:style-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for text-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:linenumbering-configuration/@text:style-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for text-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:list-level-style-bullet/@text:style-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for text-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:list-level-style-number/@text:style-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for text-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:outline-level-style/@text:style-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for text-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:ruby-text/@text:style-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for text-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:span/@text:style-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for text-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@text:visited-style-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for text-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select="$common-styles/style:style[@style:family='paragraph']/@style:name[ancestor::office:document[1]=$document]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for paragraph-style-common: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,$all-styles/style:style[@style:family='paragraph']/@style:parent-style-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for paragraph-style-common: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,$all-styles/style:style[@style:family='paragraph']//@style:apply-style-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for paragraph-style-common: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select="$all-styles/style:style[@style:family='paragraph']/@style:name[ancestor::office:document[1]=$document]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for paragraph-style: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@draw:text-style-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for paragraph-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@form:text-style-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for paragraph-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,$all-styles/style:style[@style:family='paragraph']/@style:next-style-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for paragraph-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@style:register-truth-ref-style-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for paragraph-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@table:paragraph-style-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for paragraph-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:h/@text:class-names[ancestor::office:document[1]=$document],'\s+')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for paragraph-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:p/@text:class-names[ancestor::office:document[1]=$document],'\s+')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for paragraph-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@text:cond-style-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for paragraph-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@text:default-style-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for paragraph-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:alphabetical-index-entry-template/@text:style-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for paragraph-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:bibliography-entry-template/@text:style-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for paragraph-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:h/@text:style-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for paragraph-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:illustration-index-entry-template/@text:style-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for paragraph-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:index-source-style/@text:style-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for paragraph-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:object-index-entry-template/@text:style-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for paragraph-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:p/@text:style-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for paragraph-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:table-index-entry-template/@text:style-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for paragraph-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:table-index-entry-template/@text:style-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for paragraph-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:table-of-content-entry-template/@text:style-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for paragraph-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:user-index-entry-template/@text:style-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for paragraph-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select="$common-styles/style:style[@style:family='chart']/@style:name[ancestor::office:document[1]=$document]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for chart-style-common: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,$all-styles/style:style[@style:family='chart']/@style:parent-style-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for chart-style-common: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select="$all-styles/style:style[@style:family='chart']/@style:name[ancestor::office:document[1]=$document]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for chart-style: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@chart:style-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for chart-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select="$common-styles/style:style[@style:family='section']/@style:name[ancestor::office:document[1]=$document]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for section-style-common: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,$all-styles/style:style[@style:family='section']/@style:parent-style-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for section-style-common: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select="$all-styles/style:style[@style:family='section']/@style:name[ancestor::office:document[1]=$document]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for section-style: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:alphabetical-index/@text:style-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for section-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:bibliography/@text:style-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for section-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:illustration-index/@text:style-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for section-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:index-title/@text:style-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for section-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:object-index/@text:style-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for section-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:section/@text:style-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for section-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:table-index/@text:style-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for section-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:table-of-content/@text:style-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for section-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:user-index/@text:style-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for section-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select="$common-styles/style:style[@style:family='ruby']/@style:name[ancestor::office:document[1]=$document]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for ruby-style-common: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,$all-styles/style:style[@style:family='ruby']/@style:parent-style-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for ruby-style-common: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select="$all-styles/style:style[@style:family='ruby']/@style:name[ancestor::office:document[1]=$document]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for ruby-style: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:ruby/@text:style-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for ruby-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select="$common-styles/style:style[@style:family='table']/@style:name[ancestor::office:document[1]=$document]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for table-style-common: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,$all-styles/style:style[@style:family='table']/@style:parent-style-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for table-style-common: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select="$all-styles/style:style[@style:family='table']/@style:name[ancestor::office:document[1]=$document]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for table-style: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//db:query/@db:style-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for table-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//db:table-representation/@db:style-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for table-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//table:background/@table:style-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for table-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//table:table/@table:style-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for table-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select="$common-styles/style:style[@style:family='table-column']/@style:name[ancestor::office:document[1]=$document]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for table-column-style-common: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,$all-styles/style:style[@style:family='table-column']/@style:parent-style-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for table-column-style-common: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select="$all-styles/style:style[@style:family='table-column']/@style:name[ancestor::office:document[1]=$document]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for table-column-style: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//db:column/@db:style-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for table-column-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//table:table-column/@table:style-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for table-column-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select="$common-styles/style:style[@style:family='table-row']/@style:name[ancestor::office:document[1]=$document]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for table-row-style-common: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,$all-styles/style:style[@style:family='table-row']/@style:parent-style-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for table-row-style-common: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select="$all-styles/style:style[@style:family='table-row']/@style:name[ancestor::office:document[1]=$document]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for table-row-style: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@db:default-row-style-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for table-row-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//table:table-row/@table:style-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for table-row-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select="$common-styles/style:style[@style:family='table-cell']/@style:name[ancestor::office:document[1]=$document]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for table-cell-style-common: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,$all-styles/style:style[@style:family='table-cell']/@style:parent-style-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for table-cell-style-common: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,$all-styles/style:style[@style:family='table-cell']//@style:apply-style-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for table-cell-style-common: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select="$all-styles/style:style[@style:family='table-cell']/@style:name[ancestor::office:document[1]=$document]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for table-cell-style: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@db:default-cell-style-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for table-cell-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@table:default-cell-style-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for table-cell-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//table:body/@table:style-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for table-cell-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//table:covered-table-cell/@table:style-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for table-cell-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//table:covered-table-cell/@table:style-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for table-cell-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//table:even-columns/@table:style-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for table-cell-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//table:even-columns/@table:style-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for table-cell-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//table:even-rows/@table:style-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for table-cell-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//table:first-column/@table:style-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for table-cell-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//table:first-row/@table:style-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for table-cell-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//table:last-column/@table:style-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for table-cell-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//table:last-row/@table:style-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for table-cell-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//table:odd-columns/@table:style-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for table-cell-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//table:odd-rows/@table:style-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for table-cell-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//table:table-cell/@table:style-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for table-cell-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select="$common-styles/style:style[@style:family='graphic']/@style:name[ancestor::office:document[1]=$document]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for graphic-style-common: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,$all-styles/style:style[@style:family='graphic']/@style:parent-style-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for graphic-style-common: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select="$all-styles/style:style[@style:family='graphic']/@style:name[ancestor::office:document[1]=$document]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for graphic-style: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@draw:class-names[ancestor::office:document[1]=$document],'\s+')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for graphic-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//dr3d:cube/@draw:style-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for graphic-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//dr3d:extrude/@draw:style-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for graphic-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//dr3d:rotate/@draw:style-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for graphic-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//dr3d:scene/@draw:style-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for graphic-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//dr3d:sphere/@draw:style-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for graphic-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//draw:caption/@draw:style-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for graphic-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//draw:circle/@draw:style-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for graphic-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//draw:connector/@draw:style-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for graphic-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//draw:control/@draw:style-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for graphic-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//draw:custom-shape/@draw:style-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for graphic-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//draw:ellipse/@draw:style-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for graphic-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//draw:frame/@draw:style-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for graphic-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//draw:g/@draw:style-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for graphic-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//draw:line/@draw:style-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for graphic-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//draw:measure/@draw:style-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for graphic-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//draw:page-thumbnail/@draw:style-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for graphic-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//draw:path/@draw:style-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for graphic-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//draw:polygon/@draw:style-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for graphic-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//draw:polyline/@draw:style-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for graphic-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//draw:rect/@draw:style-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for graphic-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//draw:regular-polygon/@draw:style-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for graphic-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//office:annotation/@draw:style-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for graphic-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select="$common-styles/style:style[@style:family='presentation']/@style:name[ancestor::office:document[1]=$document]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for presentation-style-common: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,$all-styles/style:style[@style:family='presentation']/@style:parent-style-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for presentation-style-common: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select="$all-styles/style:style[@style:family='presentation']/@style:name[ancestor::office:document[1]=$document]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for presentation-style: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@presentation:class-names[ancestor::office:document[1]=$document],'\s+')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for presentation-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@presentation:style-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for presentation-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select="$common-styles/style:style[@style:family='drawing-page']/@style:name[ancestor::office:document[1]=$document]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for drawing-page-style-common: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,$all-styles/style:style[@style:family='drawing-page']/@style:parent-style-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for drawing-page-style-common: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select="$all-styles/style:style[@style:family='drawing-page']/@style:name[ancestor::office:document[1]=$document]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for drawing-page-style: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//draw:page/@draw:style-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for drawing-page-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//presentation:notes/@draw:style-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for drawing-page-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//style:handout-master/@draw:style-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for drawing-page-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//style:master-page/@draw:style-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for drawing-page-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select="$common-styles/text:list-style/@style:name[ancestor::office:document[1]=$document]|$common-styles/style:style[@style:family='graphic']/style:graphic-properties/text:list-style/@style:name[ancestor::office:document[1]=$document]|$common-styles/text:outline-style/@style:name[ancestor::office:document[1]=$document]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for list-style-common: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,$common-styles/style:style[string-length(@style:list-style-name)&gt;0]/@style:list-style-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for list-style-common: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select="$all-styles/text:list-style/@style:name[ancestor::office:document[1]=$document]|$all-styles/style:style[@style:family='graphic']/style:graphic-properties/text:list-style/@style:name[ancestor::office:document[1]=$document]|$common-styles/text:outline-style/@style:name[ancestor::office:document[1]=$document]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for list-style: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,$all-styles/style:style[string-length(@style:list-style-name)&gt;0]/@style:list-style-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for list-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:list/@text:style-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for list-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:numbered-paragraph/@text:style-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for list-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:list-item/@text:style-override[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for list-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select="$common-styles/number:boolean-style/@style:name[ancestor::office:document[1]=$document]|$common-styles/number:currency-style/@style:name[ancestor::office:document[1]=$document]|$common-styles/number:date-style/@style:name[ancestor::office:document[1]=$document]|$common-styles/number:number-style/@style:name[ancestor::office:document[1]=$document]|$common-styles/number:percentage-style/@style:name[ancestor::office:document[1]=$document]|$common-styles/number:text-style/@style:name[ancestor::office:document[1]=$document]|$common-styles/number:time-style/@style:name[ancestor::office:document[1]=$document]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for data-style-common: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,$common-styles//*/@style:data-style-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for data-style-common: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,$all-styles/number:*//@style:apply-style-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for data-style-common: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select="$all-styles/number:boolean-style/@style:name[ancestor::office:document[1]=$document]|$all-styles/number:currency-style/@style:name[ancestor::office:document[1]=$document]|$all-styles/number:date-style/@style:name[ancestor::office:document[1]=$document]|$all-styles/number:number-style/@style:name[ancestor::office:document[1]=$document]|$all-styles/number:percentage-style/@style:name[ancestor::office:document[1]=$document]|$all-styles/number:text-style/@style:name[ancestor::office:document[1]=$document]|$all-styles/number:time-style/@style:name[ancestor::office:document[1]=$document]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for data-style: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@style:data-style-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for data-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select="$common-styles/number:percentage-style/@style:name[ancestor::office:document[1]=$document]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for percentage-style-common: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,$common-styles//*/@style:percentage-data-style-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for percentage-style-common: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select="$all-styles/number:percentage-style/@style:name[ancestor::office:document[1]=$document]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for percentage-style: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@style:percentage-data-style-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for percentage-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select="$common-styles/draw:gradient/@draw:name[ancestor::office:document[1]=$document]|$common-styles/svg:linearGradient/@draw:name[ancestor::office:document[1]=$document]|$common-styles/svg:radialGradient/@draw:name[ancestor::office:document[1]=$document]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for gradient: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@draw:fill-gradient-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for gradient: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select="$common-styles/draw:hatch/@draw:name[ancestor::office:document[1]=$document]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for hatch: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@draw:fill-hatch-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for hatch: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select="$common-styles/draw:fill-image/@draw:name[ancestor::office:document[1]=$document]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for fill-image: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@draw:fill-image-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for fill-image: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select="$common-styles/draw:marker/@draw:name[ancestor::office:document[1]=$document]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for marker: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@draw:marker-end[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for marker: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@draw:marker-start[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for marker: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select="$common-styles/draw:stroke-dash/@draw:name[ancestor::office:document[1]=$document]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for stroke-dash: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@draw:stroke-dash[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for stroke-dash: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@draw:stroke-dash-names[ancestor::office:document[1]=$document],'\s+')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for stroke-dash: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select="$common-styles/draw:opacity/@draw:name[ancestor::office:document[1]=$document]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for opacity: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@draw:opacity-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for opacity: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select="$common-styles/style:presentation-page-layout/@style:name[ancestor::office:document[1]=$document]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for presentation-page-layout: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@presentation:presentation-page-layout-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for presentation-page-layout: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select=".//table:table/@table:name[ancestor::office:document[1]=$document]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for table: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select="$common-styles/table:table-template/@table:name[ancestor::office:document[1]=$document]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for table-template: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@table:template-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for table-template: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select=".//style:page-layout/@style:name[ancestor::office:document[1]=$document]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for page-layout: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@style:page-layout-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for page-layout: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select="$master-pages/style:master-page/@style:name[ancestor::office:document[1]=$document]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for master-page: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@draw:master-page-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for master-page: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//style:master-page/@style:next-style-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for master-page: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@text:master-page-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for master-page: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select=".//text:alphabetical-index/@text:name[ancestor::office:document[1]=$document]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for alphabetical-index: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select=".//text:bibliography/@text:name[ancestor::office:document[1]=$document]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for bibliography: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select=".//text:bookmark/@text:name[ancestor::office:document[1]=$document]|.//text:bookmark-start/@text:name[ancestor::office:document[1]=$document]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for bookmark: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:bookmark-ref/@text:ref-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for bookmark: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select=".//text:bookmark-end/@text:name[ancestor::office:document[1]=$document]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for bookmark-end: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:bookmark-start/@text:name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for bookmark-end: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select=".//text:bookmark-start/@text:name[ancestor::office:document[1]=$document]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for bookmark-start: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:not-preceding-ids($ids,.//text:bookmark-end/@text:name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for bookmark-start: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select=".//text:illustration-index/@text:name[ancestor::office:document[1]=$document]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for illustration-index: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select=".//text:index-title/@text:name[ancestor::office:document[1]=$document]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for index-title: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select=".//text:object-index/@text:name[ancestor::office:document[1]=$document]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for object-index: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select=".//text:reference-mark/@text:name[ancestor::office:document[1]=$document]|.//text:reference-mark-start/@text:name[ancestor::office:document[1]=$document]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for reference-mark: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:reference-ref/@text:ref-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for reference-mark: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select=".//text:reference-mark-end/@text:name[ancestor::office:document[1]=$document]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for reference-mark-end: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:reference-mark-start/@text:name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for reference-mark-end: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select=".//text:reference-mark-start/@text:name[ancestor::office:document[1]=$document]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for reference-mark-start: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:not-preceding-ids($ids,.//text:reference-mark-end/@text:name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for reference-mark-start: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select=".//text:section/@text:name[ancestor::office:document[1]=$document]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for section: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,text:section-source[not(@xlink:href)]/@text:section-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for section: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select=".//text:sequence-decl/@text:name[ancestor::office:document[1]=$document]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for sequence-decl: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:not-preceding-ids($ids,.//text:sequence/@text:name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for sequence-decl: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select=".//text:table-of-content/@text:name[ancestor::office:document[1]=$document]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for table-of-content: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select=".//text:table-index/@text:name[ancestor::office:document[1]=$document]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for table-index: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select="$office-meta/meta:user-defined/@meta:name[ancestor::office:document[1]=$document]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for user-defined: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:user-defined/@text:name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for user-defined: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select=".//text:user-field-decl/@text:name[ancestor::office:document[1]=$document]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for user-field: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:not-preceding-ids($ids,.//text:user-field-get/@text:name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for user-field: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:not-preceding-ids($ids,.//text:user-field-input/@text:name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for user-field: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select=".//text:user-index/@text:name[ancestor::office:document[1]=$document]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for user-index: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:user-index-mark/@text:index-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for user-index: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:user-index-mark-start/@text:index-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for user-index: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:user-index-source/@text:index-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for user-index: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select=".//text:variable-decl/@text:name[ancestor::office:document[1]=$document]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for variable: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:not-preceding-ids($ids,.//text:variable-get/@text:name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for variable: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:not-preceding-ids($ids,.//text:variable-input/@text:name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for variable: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:not-preceding-ids($ids,.//text:variable-set/@text:name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for variable: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select=".//text:note/@text:id[ancestor::office:document[1]=$document]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for note: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:note-ref/@text:ref-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for note: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select=".//text:sequence/@text:ref-name[ancestor::office:document[1]=$document]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for sequence: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:sequence-ref/@text:ref-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for sequence: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select=".//draw:text-box/@text:id[ancestor::office:document[1]=$document]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for text-box: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select=".//text:alphabetical-index-mark-end/@text:id[ancestor::office:document[1]=$document]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for alphabetical-index-mark-end: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:alphabetical-index-mark-start/@text:id[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for alphabetical-index-mark-end: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select=".//text:alphabetical-index-mark-start/@text:id[ancestor::office:document[1]=$document]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for alphabetical-index-mark-start: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:not-preceding-ids($ids,.//text:alphabetical-index-mark-end/@text:id[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for alphabetical-index-mark-start: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select=".//text:changed-region/@text:id[ancestor::office:document[1]=$document]|.//text:changed-region/@xml:id[ancestor::office:document[1]=$document]"/>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@text:change-id[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for changed-region: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids" select=".//@xml:id[ancestor::office:document[1]=$document]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for id: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select=".//text:h/@text:id[ancestor::office:document[1]=$document]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for h: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select=".//text:p/@text:id[ancestor::office:document[1]=$document]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for p: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select=".//text:toc-mark-end/@text:id[ancestor::office:document[1]=$document]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for toc-mark-end: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:toc-mark-start/@text:id[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for toc-mark-end: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select=".//text:toc-mark-start/@text:id[ancestor::office:document[1]=$document]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for toc-mark-start: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:not-preceding-ids($ids,.//text:toc-mark-end/@text:id[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for toc-mark-start: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select=".//text:user-index-mark-end/@text:id[ancestor::office:document[1]=$document]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for user-index-mark-end: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:user-index-mark-start/@text:id[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for user-index-mark-end: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select=".//text:user-index-mark-start/@text:id[ancestor::office:document[1]=$document]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for user-index-mark-start: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:not-preceding-ids($ids,.//text:user-index-mark-end/@text:id[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for user-index-mark-start: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select=".//text:list/@xml:id[ancestor::office:document[1]=$document]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for list: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@text:continue-list[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for list: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select=".//dr3d:cube/@draw:id[ancestor::office:document[1]=$document]|.//dr3d:extrude/@draw:id[ancestor::office:document[1]=$document]|.//dr3d:rotate/@draw:id[ancestor::office:document[1]=$document]|.//dr3d:scene/@draw:id[ancestor::office:document[1]=$document]|.//dr3d:sphere/@draw:id[ancestor::office:document[1]=$document]|.//draw:caption/@draw:id[ancestor::office:document[1]=$document]|.//draw:circle/@draw:id[ancestor::office:document[1]=$document]|.//draw:connector/@draw:id[ancestor::office:document[1]=$document]|.//draw:control/@draw:id[ancestor::office:document[1]=$document]|.//draw:custom-shape/@draw:id[ancestor::office:document[1]=$document]|.//draw:ellipse/@draw:id[ancestor::office:document[1]=$document]|.//draw:frame/@draw:id[ancestor::office:document[1]=$document]|.//draw:frame/@draw:id[ancestor::office:document[1]=$document]|.//draw:g/@draw:id[ancestor::office:document[1]=$document]|.//draw:line/@draw:id[ancestor::office:document[1]=$document]|.//draw:measure/@draw:id[ancestor::office:document[1]=$document]|.//draw:page/@draw:id[ancestor::office:document[1]=$document]|.//draw:page-thumbnail/@draw:id[ancestor::office:document[1]=$document]|.//draw:path/@draw:id[ancestor::office:document[1]=$document]|.//draw:polygon/@draw:id[ancestor::office:document[1]=$document]|.//draw:polyline/@draw:id[ancestor::office:document[1]=$document]|.//draw:rect/@draw:id[ancestor::office:document[1]=$document]|.//draw:regular-polygon/@draw:id[ancestor::office:document[1]=$document]|.//office:annotation/@draw:id[ancestor::office:document[1]=$document]|.//text:h/@text:id[ancestor::office:document[1]=$document]|.//text:p/@text:id[ancestor::office:document[1]=$document]|.//dr3d:cube/@xml:id[ancestor::office:document[1]=$document]|.//dr3d:extrude/@xml:id[ancestor::office:document[1]=$document]|.//dr3d:rotate/@xml:id[ancestor::office:document[1]=$document]|.//dr3d:scene/@xml:id[ancestor::office:document[1]=$document]|.//dr3d:sphere/@xml:id[ancestor::office:document[1]=$document]|.//draw:caption/@xml:id[ancestor::office:document[1]=$document]|.//draw:circle/@xml:id[ancestor::office:document[1]=$document]|.//draw:connector/@xml:id[ancestor::office:document[1]=$document]|.//draw:control/@xml:id[ancestor::office:document[1]=$document]|.//draw:custom-shape/@xml:id[ancestor::office:document[1]=$document]|.//draw:ellipse/@xml:id[ancestor::office:document[1]=$document]|.//draw:frame/@xml:id[ancestor::office:document[1]=$document]|.//draw:frame/@xml:id[ancestor::office:document[1]=$document]|.//draw:g/@xml:id[ancestor::office:document[1]=$document]|.//draw:line/@xml:id[ancestor::office:document[1]=$document]|.//draw:measure/@xml:id[ancestor::office:document[1]=$document]|.//draw:page/@xml:id[ancestor::office:document[1]=$document]|.//draw:page-thumbnail/@xml:id[ancestor::office:document[1]=$document]|.//draw:path/@xml:id[ancestor::office:document[1]=$document]|.//draw:polygon/@xml:id[ancestor::office:document[1]=$document]|.//draw:polyline/@xml:id[ancestor::office:document[1]=$document]|.//draw:rect/@xml:id[ancestor::office:document[1]=$document]|.//draw:regular-polygon/@xml:id[ancestor::office:document[1]=$document]|.//office:annotation/@xml:id[ancestor::office:document[1]=$document]|.//text:h/@xml:id[ancestor::office:document[1]=$document]|.//text:p/@xml:id[ancestor::office:document[1]=$document]"/>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@smil:targetElement[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for smil:targetElement: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select=".//dr3d:cube/@draw:id[ancestor::office:document[1]=$document]|.//dr3d:extrude/@draw:id[ancestor::office:document[1]=$document]|.//dr3d:rotate/@draw:id[ancestor::office:document[1]=$document]|.//dr3d:scene/@draw:id[ancestor::office:document[1]=$document]|.//dr3d:sphere/@draw:id[ancestor::office:document[1]=$document]|.//draw:caption/@draw:id[ancestor::office:document[1]=$document]|.//draw:circle/@draw:id[ancestor::office:document[1]=$document]|.//draw:connector/@draw:id[ancestor::office:document[1]=$document]|.//draw:control/@draw:id[ancestor::office:document[1]=$document]|.//draw:custom-shape/@draw:id[ancestor::office:document[1]=$document]|.//draw:ellipse/@draw:id[ancestor::office:document[1]=$document]|.//draw:frame/@draw:id[ancestor::office:document[1]=$document]|.//draw:frame/@draw:id[ancestor::office:document[1]=$document]|.//draw:g/@draw:id[ancestor::office:document[1]=$document]|.//draw:line/@draw:id[ancestor::office:document[1]=$document]|.//draw:measure/@draw:id[ancestor::office:document[1]=$document]|.//draw:page/@draw:id[ancestor::office:document[1]=$document]|.//draw:page-thumbnail/@draw:id[ancestor::office:document[1]=$document]|.//draw:path/@draw:id[ancestor::office:document[1]=$document]|.//draw:polygon/@draw:id[ancestor::office:document[1]=$document]|.//draw:polyline/@draw:id[ancestor::office:document[1]=$document]|.//draw:rect/@draw:id[ancestor::office:document[1]=$document]|.//draw:regular-polygon/@draw:id[ancestor::office:document[1]=$document]|.//office:annotation/@draw:id[ancestor::office:document[1]=$document]|.//dr3d:cube/@xml:id[ancestor::office:document[1]=$document]|.//dr3d:extrude/@xml:id[ancestor::office:document[1]=$document]|.//dr3d:rotate/@xml:id[ancestor::office:document[1]=$document]|.//dr3d:scene/@xml:id[ancestor::office:document[1]=$document]|.//dr3d:sphere/@xml:id[ancestor::office:document[1]=$document]|.//draw:caption/@xml:id[ancestor::office:document[1]=$document]|.//draw:circle/@xml:id[ancestor::office:document[1]=$document]|.//draw:connector/@xml:id[ancestor::office:document[1]=$document]|.//draw:control/@xml:id[ancestor::office:document[1]=$document]|.//draw:custom-shape/@xml:id[ancestor::office:document[1]=$document]|.//draw:ellipse/@xml:id[ancestor::office:document[1]=$document]|.//draw:frame/@xml:id[ancestor::office:document[1]=$document]|.//draw:frame/@xml:id[ancestor::office:document[1]=$document]|.//draw:g/@xml:id[ancestor::office:document[1]=$document]|.//draw:line/@xml:id[ancestor::office:document[1]=$document]|.//draw:measure/@xml:id[ancestor::office:document[1]=$document]|.//draw:page/@xml:id[ancestor::office:document[1]=$document]|.//draw:page-thumbnail/@xml:id[ancestor::office:document[1]=$document]|.//draw:path/@xml:id[ancestor::office:document[1]=$document]|.//draw:polygon/@xml:id[ancestor::office:document[1]=$document]|.//draw:polyline/@xml:id[ancestor::office:document[1]=$document]|.//draw:rect/@xml:id[ancestor::office:document[1]=$document]|.//draw:regular-polygon/@xml:id[ancestor::office:document[1]=$document]|.//office:annotation/@xml:id[ancestor::office:document[1]=$document]"/>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@draw:end-shape[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for shape: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@draw:start-shape[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for shape: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select=".//presentation:header-decl/@presentation:name[ancestor::office:document[1]=$document]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for presentation-header: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@presentation:use-header-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for presentation-header: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select=".//presentation:footer-decl/@presentation:name[ancestor::office:document[1]=$document]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for presentation-footer: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@presentation:use-footer-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for presentation-footer: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select=".//presentation:date-time-decl/@presentation:name[ancestor::office:document[1]=$document]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for presentation-date-time: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@presentation:use-date-time-name[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for presentation-date-time: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select=".//draw:page/@draw:name[ancestor::office:document[1]=$document]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for presentation-pages: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@presentation:pages[ancestor::office:document[1]=$document],',')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for presentation-pages: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select=".//presentation:show/@presentation:name[ancestor::office:document[1]=$document]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for presentation-show: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@presentation:show[ancestor::office:document[1]=$document],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for presentation-show: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($master-pages/style:master-page) &gt; 0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                test="count($master-pages/style:master-page) &gt; 0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>The document is missing a style:master-page.</svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:apply-templates select="*" mode="M30"/>
   </xsl:template>

	  <!--RULE -->
   <xsl:template match="/office:document-styles" priority="1001" mode="M30">
      <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                       context="/office:document-styles"/>
      <xsl:variable name="office-meta"
                    select="document('meta.xml',/)/office:document-meta/office:meta"/>
      <xsl:variable name="document-styles" select="()"/>
      <xsl:variable name="common-styles"
                    select="office:styles|$document-styles/office:styles"/>
      <xsl:variable name="all-styles" select="$common-styles|office:automatic-styles"/>
      <xsl:variable name="master-pages"
                    select="$document-styles/office:master-styles|office:master-styles"/>
      <xsl:variable name="ids"
                    select="$document-styles/office:font-face-decls/style:font-face/@style:name[not(ancestor::office:document)]|.//style:font-face/@style:name[not(ancestor::office:document)]"/>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@style:font-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for all-fonts: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@style:font-name-asian[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for all-fonts: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@style:font-name-complex[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for all-fonts: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select=".//style:font-face/@style:name[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for local-fonts: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select="$common-styles/style:style[@style:family='text']/@style:name[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for text-style-common: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,$all-styles/style:style[@style:family='text']/@style:parent-style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for text-style-common: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select="$all-styles/style:style[@style:family='text']/@style:name[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for text-style: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@style:leader-text-style[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for text-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@style:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for text-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@style:text-line-through-text-style[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for text-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@text:citation-body-style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for text-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@text:citation-style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for text-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:span/@text:class-names[not(ancestor::office:document)],'\s+')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for text-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@text:main-entry-style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for text-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:a/@text:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for text-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:alphabetical-index/@text:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for text-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:index-entry-bibliography/@text:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for text-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:index-entry-chapter/@text:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for text-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:index-entry-link-end/@text:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for text-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:index-entry-link-start/@text:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for text-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:index-entry-page-number/@text:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for text-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:index-entry-span/@text:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for text-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:index-entry-tab-stop/@text:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for text-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:index-entry-text/@text:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for text-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:index-title-template/@text:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for text-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:linenumbering-configuration/@text:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for text-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:list-level-style-bullet/@text:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for text-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:list-level-style-number/@text:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for text-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:outline-level-style/@text:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for text-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:ruby-text/@text:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for text-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:span/@text:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for text-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@text:visited-style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for text-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select="$common-styles/style:style[@style:family='paragraph']/@style:name[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for paragraph-style-common: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,$all-styles/style:style[@style:family='paragraph']/@style:parent-style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for paragraph-style-common: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,$all-styles/style:style[@style:family='paragraph']//@style:apply-style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for paragraph-style-common: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select="$all-styles/style:style[@style:family='paragraph']/@style:name[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for paragraph-style: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@draw:text-style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for paragraph-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@form:text-style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for paragraph-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,$all-styles/style:style[@style:family='paragraph']/@style:next-style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for paragraph-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@style:register-truth-ref-style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for paragraph-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@table:paragraph-style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for paragraph-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:h/@text:class-names[not(ancestor::office:document)],'\s+')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for paragraph-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:p/@text:class-names[not(ancestor::office:document)],'\s+')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for paragraph-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@text:cond-style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for paragraph-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@text:default-style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for paragraph-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:alphabetical-index-entry-template/@text:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for paragraph-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:bibliography-entry-template/@text:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for paragraph-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:h/@text:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for paragraph-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:illustration-index-entry-template/@text:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for paragraph-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:index-source-style/@text:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for paragraph-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:object-index-entry-template/@text:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for paragraph-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:p/@text:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for paragraph-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:table-index-entry-template/@text:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for paragraph-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:table-index-entry-template/@text:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for paragraph-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:table-of-content-entry-template/@text:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for paragraph-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:user-index-entry-template/@text:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for paragraph-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select="$common-styles/style:style[@style:family='chart']/@style:name[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for chart-style-common: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,$all-styles/style:style[@style:family='chart']/@style:parent-style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for chart-style-common: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select="$all-styles/style:style[@style:family='chart']/@style:name[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for chart-style: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@chart:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for chart-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select="$common-styles/style:style[@style:family='section']/@style:name[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for section-style-common: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,$all-styles/style:style[@style:family='section']/@style:parent-style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for section-style-common: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select="$all-styles/style:style[@style:family='section']/@style:name[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for section-style: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:alphabetical-index/@text:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for section-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:bibliography/@text:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for section-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:illustration-index/@text:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for section-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:index-title/@text:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for section-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:object-index/@text:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for section-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:section/@text:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for section-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:table-index/@text:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for section-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:table-of-content/@text:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for section-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:user-index/@text:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for section-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select="$common-styles/style:style[@style:family='ruby']/@style:name[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for ruby-style-common: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,$all-styles/style:style[@style:family='ruby']/@style:parent-style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for ruby-style-common: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select="$all-styles/style:style[@style:family='ruby']/@style:name[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for ruby-style: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:ruby/@text:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for ruby-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select="$common-styles/style:style[@style:family='table']/@style:name[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for table-style-common: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,$all-styles/style:style[@style:family='table']/@style:parent-style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for table-style-common: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select="$all-styles/style:style[@style:family='table']/@style:name[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for table-style: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//db:query/@db:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for table-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//db:table-representation/@db:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for table-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//table:background/@table:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for table-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//table:table/@table:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for table-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select="$common-styles/style:style[@style:family='table-column']/@style:name[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for table-column-style-common: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,$all-styles/style:style[@style:family='table-column']/@style:parent-style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for table-column-style-common: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select="$all-styles/style:style[@style:family='table-column']/@style:name[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for table-column-style: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//db:column/@db:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for table-column-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//table:table-column/@table:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for table-column-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select="$common-styles/style:style[@style:family='table-row']/@style:name[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for table-row-style-common: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,$all-styles/style:style[@style:family='table-row']/@style:parent-style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for table-row-style-common: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select="$all-styles/style:style[@style:family='table-row']/@style:name[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for table-row-style: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@db:default-row-style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for table-row-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//table:table-row/@table:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for table-row-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select="$common-styles/style:style[@style:family='table-cell']/@style:name[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for table-cell-style-common: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,$all-styles/style:style[@style:family='table-cell']/@style:parent-style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for table-cell-style-common: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,$all-styles/style:style[@style:family='table-cell']//@style:apply-style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for table-cell-style-common: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select="$all-styles/style:style[@style:family='table-cell']/@style:name[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for table-cell-style: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@db:default-cell-style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for table-cell-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@table:default-cell-style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for table-cell-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//table:body/@table:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for table-cell-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//table:covered-table-cell/@table:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for table-cell-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//table:covered-table-cell/@table:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for table-cell-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//table:even-columns/@table:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for table-cell-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//table:even-columns/@table:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for table-cell-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//table:even-rows/@table:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for table-cell-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//table:first-column/@table:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for table-cell-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//table:first-row/@table:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for table-cell-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//table:last-column/@table:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for table-cell-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//table:last-row/@table:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for table-cell-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//table:odd-columns/@table:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for table-cell-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//table:odd-rows/@table:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for table-cell-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//table:table-cell/@table:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for table-cell-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select="$common-styles/style:style[@style:family='graphic']/@style:name[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for graphic-style-common: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,$all-styles/style:style[@style:family='graphic']/@style:parent-style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for graphic-style-common: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select="$all-styles/style:style[@style:family='graphic']/@style:name[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for graphic-style: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@draw:class-names[not(ancestor::office:document)],'\s+')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for graphic-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//dr3d:cube/@draw:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for graphic-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//dr3d:extrude/@draw:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for graphic-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//dr3d:rotate/@draw:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for graphic-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//dr3d:scene/@draw:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for graphic-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//dr3d:sphere/@draw:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for graphic-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//draw:caption/@draw:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for graphic-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//draw:circle/@draw:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for graphic-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//draw:connector/@draw:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for graphic-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//draw:control/@draw:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for graphic-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//draw:custom-shape/@draw:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for graphic-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//draw:ellipse/@draw:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for graphic-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//draw:frame/@draw:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for graphic-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//draw:g/@draw:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for graphic-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//draw:line/@draw:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for graphic-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//draw:measure/@draw:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for graphic-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//draw:page-thumbnail/@draw:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for graphic-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//draw:path/@draw:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for graphic-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//draw:polygon/@draw:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for graphic-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//draw:polyline/@draw:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for graphic-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//draw:rect/@draw:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for graphic-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//draw:regular-polygon/@draw:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for graphic-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//office:annotation/@draw:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for graphic-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select="$common-styles/style:style[@style:family='presentation']/@style:name[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for presentation-style-common: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,$all-styles/style:style[@style:family='presentation']/@style:parent-style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for presentation-style-common: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select="$all-styles/style:style[@style:family='presentation']/@style:name[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for presentation-style: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@presentation:class-names[not(ancestor::office:document)],'\s+')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for presentation-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@presentation:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for presentation-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select="$common-styles/style:style[@style:family='drawing-page']/@style:name[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for drawing-page-style-common: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,$all-styles/style:style[@style:family='drawing-page']/@style:parent-style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for drawing-page-style-common: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select="$all-styles/style:style[@style:family='drawing-page']/@style:name[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for drawing-page-style: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//draw:page/@draw:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for drawing-page-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//presentation:notes/@draw:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for drawing-page-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//style:handout-master/@draw:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for drawing-page-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//style:master-page/@draw:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for drawing-page-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select="$common-styles/text:list-style/@style:name[not(ancestor::office:document)]|$common-styles/style:style[@style:family='graphic']/style:graphic-properties/text:list-style/@style:name[not(ancestor::office:document)]|$common-styles/text:outline-style/@style:name[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for list-style-common: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,$common-styles/style:style[string-length(@style:list-style-name)&gt;0]/@style:list-style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for list-style-common: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select="$all-styles/text:list-style/@style:name[not(ancestor::office:document)]|$all-styles/style:style[@style:family='graphic']/style:graphic-properties/text:list-style/@style:name[not(ancestor::office:document)]|$common-styles/text:outline-style/@style:name[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for list-style: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,$all-styles/style:style[string-length(@style:list-style-name)&gt;0]/@style:list-style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for list-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:list/@text:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for list-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:numbered-paragraph/@text:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for list-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:list-item/@text:style-override[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for list-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select="$common-styles/number:boolean-style/@style:name[not(ancestor::office:document)]|$common-styles/number:currency-style/@style:name[not(ancestor::office:document)]|$common-styles/number:date-style/@style:name[not(ancestor::office:document)]|$common-styles/number:number-style/@style:name[not(ancestor::office:document)]|$common-styles/number:percentage-style/@style:name[not(ancestor::office:document)]|$common-styles/number:text-style/@style:name[not(ancestor::office:document)]|$common-styles/number:time-style/@style:name[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for data-style-common: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,$common-styles//*/@style:data-style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for data-style-common: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,$all-styles/number:*//@style:apply-style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for data-style-common: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select="$all-styles/number:boolean-style/@style:name[not(ancestor::office:document)]|$all-styles/number:currency-style/@style:name[not(ancestor::office:document)]|$all-styles/number:date-style/@style:name[not(ancestor::office:document)]|$all-styles/number:number-style/@style:name[not(ancestor::office:document)]|$all-styles/number:percentage-style/@style:name[not(ancestor::office:document)]|$all-styles/number:text-style/@style:name[not(ancestor::office:document)]|$all-styles/number:time-style/@style:name[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for data-style: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@style:data-style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for data-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select="$common-styles/number:percentage-style/@style:name[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for percentage-style-common: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,$common-styles//*/@style:percentage-data-style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for percentage-style-common: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select="$all-styles/number:percentage-style/@style:name[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for percentage-style: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@style:percentage-data-style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for percentage-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select="$common-styles/draw:gradient/@draw:name[not(ancestor::office:document)]|$common-styles/svg:linearGradient/@draw:name[not(ancestor::office:document)]|$common-styles/svg:radialGradient/@draw:name[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for gradient: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@draw:fill-gradient-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for gradient: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select="$common-styles/draw:hatch/@draw:name[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for hatch: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@draw:fill-hatch-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for hatch: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select="$common-styles/draw:fill-image/@draw:name[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for fill-image: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@draw:fill-image-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for fill-image: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select="$common-styles/draw:marker/@draw:name[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for marker: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@draw:marker-end[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for marker: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@draw:marker-start[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for marker: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select="$common-styles/draw:stroke-dash/@draw:name[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for stroke-dash: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@draw:stroke-dash[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for stroke-dash: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@draw:stroke-dash-names[not(ancestor::office:document)],'\s+')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for stroke-dash: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select="$common-styles/draw:opacity/@draw:name[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for opacity: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@draw:opacity-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for opacity: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select="$common-styles/style:presentation-page-layout/@style:name[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for presentation-page-layout: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@presentation:presentation-page-layout-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for presentation-page-layout: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select=".//table:table/@table:name[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for table: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select="$common-styles/table:table-template/@table:name[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for table-template: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@table:template-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for table-template: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select=".//style:page-layout/@style:name[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for page-layout: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@style:page-layout-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for page-layout: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select="$master-pages/style:master-page/@style:name[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for master-page: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@draw:master-page-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for master-page: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//style:master-page/@style:next-style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for master-page: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@text:master-page-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for master-page: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select=".//text:alphabetical-index/@text:name[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for alphabetical-index: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select=".//text:bibliography/@text:name[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for bibliography: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select=".//text:bookmark/@text:name[not(ancestor::office:document)]|.//text:bookmark-start/@text:name[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for bookmark: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:bookmark-ref/@text:ref-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for bookmark: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select=".//text:bookmark-end/@text:name[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for bookmark-end: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:bookmark-start/@text:name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for bookmark-end: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select=".//text:bookmark-start/@text:name[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for bookmark-start: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:not-preceding-ids($ids,.//text:bookmark-end/@text:name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for bookmark-start: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select=".//text:illustration-index/@text:name[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for illustration-index: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select=".//text:index-title/@text:name[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for index-title: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select=".//text:object-index/@text:name[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for object-index: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select=".//text:reference-mark/@text:name[not(ancestor::office:document)]|.//text:reference-mark-start/@text:name[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for reference-mark: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:reference-ref/@text:ref-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for reference-mark: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select=".//text:reference-mark-end/@text:name[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for reference-mark-end: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:reference-mark-start/@text:name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for reference-mark-end: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select=".//text:reference-mark-start/@text:name[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for reference-mark-start: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:not-preceding-ids($ids,.//text:reference-mark-end/@text:name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for reference-mark-start: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select=".//text:section/@text:name[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for section: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,text:section-source[not(@xlink:href)]/@text:section-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for section: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select=".//text:sequence-decl/@text:name[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for sequence-decl: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:not-preceding-ids($ids,.//text:sequence/@text:name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for sequence-decl: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select=".//text:table-of-content/@text:name[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for table-of-content: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select=".//text:table-index/@text:name[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for table-index: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select="$office-meta/meta:user-defined/@meta:name[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for user-defined: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:user-defined/@text:name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for user-defined: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select=".//text:user-field-decl/@text:name[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for user-field: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:not-preceding-ids($ids,.//text:user-field-get/@text:name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for user-field: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:not-preceding-ids($ids,.//text:user-field-input/@text:name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for user-field: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select=".//text:user-index/@text:name[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for user-index: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:user-index-mark/@text:index-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for user-index: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:user-index-mark-start/@text:index-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for user-index: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:user-index-source/@text:index-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for user-index: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select=".//text:variable-decl/@text:name[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for variable: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:not-preceding-ids($ids,.//text:variable-get/@text:name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for variable: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:not-preceding-ids($ids,.//text:variable-input/@text:name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for variable: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:not-preceding-ids($ids,.//text:variable-set/@text:name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for variable: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select=".//text:note/@text:id[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for note: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:note-ref/@text:ref-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for note: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select=".//text:sequence/@text:ref-name[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for sequence: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:sequence-ref/@text:ref-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for sequence: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select=".//draw:text-box/@text:id[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for text-box: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select=".//text:alphabetical-index-mark-end/@text:id[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for alphabetical-index-mark-end: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:alphabetical-index-mark-start/@text:id[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for alphabetical-index-mark-end: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select=".//text:alphabetical-index-mark-start/@text:id[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for alphabetical-index-mark-start: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:not-preceding-ids($ids,.//text:alphabetical-index-mark-end/@text:id[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for alphabetical-index-mark-start: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select=".//text:changed-region/@text:id[not(ancestor::office:document)]|.//text:changed-region/@xml:id[not(ancestor::office:document)]"/>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@text:change-id[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for changed-region: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids" select=".//@xml:id[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for id: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids" select=".//text:h/@text:id[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for h: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids" select=".//text:p/@text:id[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for p: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select=".//text:toc-mark-end/@text:id[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for toc-mark-end: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:toc-mark-start/@text:id[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for toc-mark-end: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select=".//text:toc-mark-start/@text:id[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for toc-mark-start: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:not-preceding-ids($ids,.//text:toc-mark-end/@text:id[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for toc-mark-start: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select=".//text:user-index-mark-end/@text:id[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for user-index-mark-end: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:user-index-mark-start/@text:id[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for user-index-mark-end: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select=".//text:user-index-mark-start/@text:id[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for user-index-mark-start: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:not-preceding-ids($ids,.//text:user-index-mark-end/@text:id[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for user-index-mark-start: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select=".//text:list/@xml:id[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for list: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@text:continue-list[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for list: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select=".//dr3d:cube/@draw:id[not(ancestor::office:document)]|.//dr3d:extrude/@draw:id[not(ancestor::office:document)]|.//dr3d:rotate/@draw:id[not(ancestor::office:document)]|.//dr3d:scene/@draw:id[not(ancestor::office:document)]|.//dr3d:sphere/@draw:id[not(ancestor::office:document)]|.//draw:caption/@draw:id[not(ancestor::office:document)]|.//draw:circle/@draw:id[not(ancestor::office:document)]|.//draw:connector/@draw:id[not(ancestor::office:document)]|.//draw:control/@draw:id[not(ancestor::office:document)]|.//draw:custom-shape/@draw:id[not(ancestor::office:document)]|.//draw:ellipse/@draw:id[not(ancestor::office:document)]|.//draw:frame/@draw:id[not(ancestor::office:document)]|.//draw:frame/@draw:id[not(ancestor::office:document)]|.//draw:g/@draw:id[not(ancestor::office:document)]|.//draw:line/@draw:id[not(ancestor::office:document)]|.//draw:measure/@draw:id[not(ancestor::office:document)]|.//draw:page/@draw:id[not(ancestor::office:document)]|.//draw:page-thumbnail/@draw:id[not(ancestor::office:document)]|.//draw:path/@draw:id[not(ancestor::office:document)]|.//draw:polygon/@draw:id[not(ancestor::office:document)]|.//draw:polyline/@draw:id[not(ancestor::office:document)]|.//draw:rect/@draw:id[not(ancestor::office:document)]|.//draw:regular-polygon/@draw:id[not(ancestor::office:document)]|.//office:annotation/@draw:id[not(ancestor::office:document)]|.//text:h/@text:id[not(ancestor::office:document)]|.//text:p/@text:id[not(ancestor::office:document)]|.//dr3d:cube/@xml:id[not(ancestor::office:document)]|.//dr3d:extrude/@xml:id[not(ancestor::office:document)]|.//dr3d:rotate/@xml:id[not(ancestor::office:document)]|.//dr3d:scene/@xml:id[not(ancestor::office:document)]|.//dr3d:sphere/@xml:id[not(ancestor::office:document)]|.//draw:caption/@xml:id[not(ancestor::office:document)]|.//draw:circle/@xml:id[not(ancestor::office:document)]|.//draw:connector/@xml:id[not(ancestor::office:document)]|.//draw:control/@xml:id[not(ancestor::office:document)]|.//draw:custom-shape/@xml:id[not(ancestor::office:document)]|.//draw:ellipse/@xml:id[not(ancestor::office:document)]|.//draw:frame/@xml:id[not(ancestor::office:document)]|.//draw:frame/@xml:id[not(ancestor::office:document)]|.//draw:g/@xml:id[not(ancestor::office:document)]|.//draw:line/@xml:id[not(ancestor::office:document)]|.//draw:measure/@xml:id[not(ancestor::office:document)]|.//draw:page/@xml:id[not(ancestor::office:document)]|.//draw:page-thumbnail/@xml:id[not(ancestor::office:document)]|.//draw:path/@xml:id[not(ancestor::office:document)]|.//draw:polygon/@xml:id[not(ancestor::office:document)]|.//draw:polyline/@xml:id[not(ancestor::office:document)]|.//draw:rect/@xml:id[not(ancestor::office:document)]|.//draw:regular-polygon/@xml:id[not(ancestor::office:document)]|.//office:annotation/@xml:id[not(ancestor::office:document)]|.//text:h/@xml:id[not(ancestor::office:document)]|.//text:p/@xml:id[not(ancestor::office:document)]"/>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@smil:targetElement[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for smil:targetElement: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select=".//dr3d:cube/@draw:id[not(ancestor::office:document)]|.//dr3d:extrude/@draw:id[not(ancestor::office:document)]|.//dr3d:rotate/@draw:id[not(ancestor::office:document)]|.//dr3d:scene/@draw:id[not(ancestor::office:document)]|.//dr3d:sphere/@draw:id[not(ancestor::office:document)]|.//draw:caption/@draw:id[not(ancestor::office:document)]|.//draw:circle/@draw:id[not(ancestor::office:document)]|.//draw:connector/@draw:id[not(ancestor::office:document)]|.//draw:control/@draw:id[not(ancestor::office:document)]|.//draw:custom-shape/@draw:id[not(ancestor::office:document)]|.//draw:ellipse/@draw:id[not(ancestor::office:document)]|.//draw:frame/@draw:id[not(ancestor::office:document)]|.//draw:frame/@draw:id[not(ancestor::office:document)]|.//draw:g/@draw:id[not(ancestor::office:document)]|.//draw:line/@draw:id[not(ancestor::office:document)]|.//draw:measure/@draw:id[not(ancestor::office:document)]|.//draw:page/@draw:id[not(ancestor::office:document)]|.//draw:page-thumbnail/@draw:id[not(ancestor::office:document)]|.//draw:path/@draw:id[not(ancestor::office:document)]|.//draw:polygon/@draw:id[not(ancestor::office:document)]|.//draw:polyline/@draw:id[not(ancestor::office:document)]|.//draw:rect/@draw:id[not(ancestor::office:document)]|.//draw:regular-polygon/@draw:id[not(ancestor::office:document)]|.//office:annotation/@draw:id[not(ancestor::office:document)]|.//dr3d:cube/@xml:id[not(ancestor::office:document)]|.//dr3d:extrude/@xml:id[not(ancestor::office:document)]|.//dr3d:rotate/@xml:id[not(ancestor::office:document)]|.//dr3d:scene/@xml:id[not(ancestor::office:document)]|.//dr3d:sphere/@xml:id[not(ancestor::office:document)]|.//draw:caption/@xml:id[not(ancestor::office:document)]|.//draw:circle/@xml:id[not(ancestor::office:document)]|.//draw:connector/@xml:id[not(ancestor::office:document)]|.//draw:control/@xml:id[not(ancestor::office:document)]|.//draw:custom-shape/@xml:id[not(ancestor::office:document)]|.//draw:ellipse/@xml:id[not(ancestor::office:document)]|.//draw:frame/@xml:id[not(ancestor::office:document)]|.//draw:frame/@xml:id[not(ancestor::office:document)]|.//draw:g/@xml:id[not(ancestor::office:document)]|.//draw:line/@xml:id[not(ancestor::office:document)]|.//draw:measure/@xml:id[not(ancestor::office:document)]|.//draw:page/@xml:id[not(ancestor::office:document)]|.//draw:page-thumbnail/@xml:id[not(ancestor::office:document)]|.//draw:path/@xml:id[not(ancestor::office:document)]|.//draw:polygon/@xml:id[not(ancestor::office:document)]|.//draw:polyline/@xml:id[not(ancestor::office:document)]|.//draw:rect/@xml:id[not(ancestor::office:document)]|.//draw:regular-polygon/@xml:id[not(ancestor::office:document)]|.//office:annotation/@xml:id[not(ancestor::office:document)]"/>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@draw:end-shape[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for shape: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@draw:start-shape[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for shape: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select=".//presentation:header-decl/@presentation:name[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for presentation-header: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@presentation:use-header-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for presentation-header: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select=".//presentation:footer-decl/@presentation:name[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for presentation-footer: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@presentation:use-footer-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for presentation-footer: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select=".//presentation:date-time-decl/@presentation:name[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for presentation-date-time: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@presentation:use-date-time-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for presentation-date-time: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select=".//draw:page/@draw:name[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for presentation-pages: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@presentation:pages[not(ancestor::office:document)],',')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for presentation-pages: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select=".//presentation:show/@presentation:name[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for presentation-show: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@presentation:show[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for presentation-show: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($master-pages/style:master-page) &gt; 0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                test="count($master-pages/style:master-page) &gt; 0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>The document is missing a style:master-page.</svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:apply-templates select="*" mode="M30"/>
   </xsl:template>

	  <!--RULE -->
   <xsl:template match="/office:document-content" priority="1000" mode="M30">
      <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                       context="/office:document-content"/>
      <xsl:variable name="office-meta"
                    select="document('meta.xml',/)/office:document-meta/office:meta"/>
      <xsl:variable name="document-styles"
                    select="document('styles.xml',/)/office:document-styles"/>
      <xsl:variable name="common-styles"
                    select="office:styles|$document-styles/office:styles"/>
      <xsl:variable name="all-styles" select="$common-styles|office:automatic-styles"/>
      <xsl:variable name="master-pages"
                    select="$document-styles/office:master-styles|office:master-styles"/>
      <xsl:variable name="ids"
                    select="$document-styles/office:font-face-decls/style:font-face/@style:name[not(ancestor::office:document)]|.//style:font-face/@style:name[not(ancestor::office:document)]"/>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@style:font-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for all-fonts: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@style:font-name-asian[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for all-fonts: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@style:font-name-complex[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for all-fonts: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select=".//style:font-face/@style:name[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for local-fonts: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select="$common-styles/style:style[@style:family='text']/@style:name[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for text-style-common: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,$all-styles/style:style[@style:family='text']/@style:parent-style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for text-style-common: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select="$all-styles/style:style[@style:family='text']/@style:name[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for text-style: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@style:leader-text-style[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for text-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@style:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for text-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@style:text-line-through-text-style[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for text-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@text:citation-body-style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for text-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@text:citation-style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for text-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:span/@text:class-names[not(ancestor::office:document)],'\s+')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for text-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@text:main-entry-style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for text-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:a/@text:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for text-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:alphabetical-index/@text:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for text-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:index-entry-bibliography/@text:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for text-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:index-entry-chapter/@text:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for text-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:index-entry-link-end/@text:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for text-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:index-entry-link-start/@text:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for text-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:index-entry-page-number/@text:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for text-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:index-entry-span/@text:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for text-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:index-entry-tab-stop/@text:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for text-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:index-entry-text/@text:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for text-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:index-title-template/@text:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for text-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:linenumbering-configuration/@text:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for text-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:list-level-style-bullet/@text:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for text-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:list-level-style-number/@text:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for text-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:outline-level-style/@text:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for text-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:ruby-text/@text:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for text-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:span/@text:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for text-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@text:visited-style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for text-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select="$common-styles/style:style[@style:family='paragraph']/@style:name[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for paragraph-style-common: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,$all-styles/style:style[@style:family='paragraph']/@style:parent-style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for paragraph-style-common: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,$all-styles/style:style[@style:family='paragraph']//@style:apply-style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for paragraph-style-common: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select="$all-styles/style:style[@style:family='paragraph']/@style:name[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for paragraph-style: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@draw:text-style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for paragraph-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@form:text-style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for paragraph-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,$all-styles/style:style[@style:family='paragraph']/@style:next-style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for paragraph-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@style:register-truth-ref-style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for paragraph-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@table:paragraph-style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for paragraph-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:h/@text:class-names[not(ancestor::office:document)],'\s+')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for paragraph-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:p/@text:class-names[not(ancestor::office:document)],'\s+')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for paragraph-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@text:cond-style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for paragraph-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@text:default-style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for paragraph-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:alphabetical-index-entry-template/@text:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for paragraph-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:bibliography-entry-template/@text:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for paragraph-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:h/@text:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for paragraph-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:illustration-index-entry-template/@text:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for paragraph-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:index-source-style/@text:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for paragraph-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:object-index-entry-template/@text:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for paragraph-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:p/@text:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for paragraph-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:table-index-entry-template/@text:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for paragraph-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:table-index-entry-template/@text:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for paragraph-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:table-of-content-entry-template/@text:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for paragraph-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:user-index-entry-template/@text:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for paragraph-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select="$common-styles/style:style[@style:family='chart']/@style:name[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for chart-style-common: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,$all-styles/style:style[@style:family='chart']/@style:parent-style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for chart-style-common: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select="$all-styles/style:style[@style:family='chart']/@style:name[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for chart-style: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@chart:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for chart-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select="$common-styles/style:style[@style:family='section']/@style:name[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for section-style-common: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,$all-styles/style:style[@style:family='section']/@style:parent-style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for section-style-common: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select="$all-styles/style:style[@style:family='section']/@style:name[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for section-style: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:alphabetical-index/@text:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for section-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:bibliography/@text:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for section-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:illustration-index/@text:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for section-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:index-title/@text:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for section-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:object-index/@text:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for section-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:section/@text:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for section-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:table-index/@text:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for section-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:table-of-content/@text:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for section-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:user-index/@text:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for section-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select="$common-styles/style:style[@style:family='ruby']/@style:name[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for ruby-style-common: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,$all-styles/style:style[@style:family='ruby']/@style:parent-style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for ruby-style-common: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select="$all-styles/style:style[@style:family='ruby']/@style:name[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for ruby-style: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:ruby/@text:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for ruby-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select="$common-styles/style:style[@style:family='table']/@style:name[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for table-style-common: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,$all-styles/style:style[@style:family='table']/@style:parent-style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for table-style-common: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select="$all-styles/style:style[@style:family='table']/@style:name[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for table-style: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//db:query/@db:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for table-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//db:table-representation/@db:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for table-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//table:background/@table:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for table-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//table:table/@table:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for table-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select="$common-styles/style:style[@style:family='table-column']/@style:name[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for table-column-style-common: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,$all-styles/style:style[@style:family='table-column']/@style:parent-style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for table-column-style-common: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select="$all-styles/style:style[@style:family='table-column']/@style:name[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for table-column-style: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//db:column/@db:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for table-column-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//table:table-column/@table:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for table-column-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select="$common-styles/style:style[@style:family='table-row']/@style:name[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for table-row-style-common: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,$all-styles/style:style[@style:family='table-row']/@style:parent-style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for table-row-style-common: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select="$all-styles/style:style[@style:family='table-row']/@style:name[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for table-row-style: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@db:default-row-style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for table-row-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//table:table-row/@table:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for table-row-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select="$common-styles/style:style[@style:family='table-cell']/@style:name[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for table-cell-style-common: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,$all-styles/style:style[@style:family='table-cell']/@style:parent-style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for table-cell-style-common: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,$all-styles/style:style[@style:family='table-cell']//@style:apply-style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for table-cell-style-common: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select="$all-styles/style:style[@style:family='table-cell']/@style:name[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for table-cell-style: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@db:default-cell-style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for table-cell-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@table:default-cell-style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for table-cell-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//table:body/@table:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for table-cell-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//table:covered-table-cell/@table:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for table-cell-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//table:covered-table-cell/@table:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for table-cell-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//table:even-columns/@table:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for table-cell-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//table:even-columns/@table:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for table-cell-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//table:even-rows/@table:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for table-cell-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//table:first-column/@table:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for table-cell-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//table:first-row/@table:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for table-cell-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//table:last-column/@table:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for table-cell-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//table:last-row/@table:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for table-cell-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//table:odd-columns/@table:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for table-cell-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//table:odd-rows/@table:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for table-cell-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//table:table-cell/@table:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for table-cell-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select="$common-styles/style:style[@style:family='graphic']/@style:name[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for graphic-style-common: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,$all-styles/style:style[@style:family='graphic']/@style:parent-style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for graphic-style-common: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select="$all-styles/style:style[@style:family='graphic']/@style:name[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for graphic-style: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@draw:class-names[not(ancestor::office:document)],'\s+')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for graphic-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//dr3d:cube/@draw:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for graphic-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//dr3d:extrude/@draw:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for graphic-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//dr3d:rotate/@draw:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for graphic-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//dr3d:scene/@draw:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for graphic-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//dr3d:sphere/@draw:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for graphic-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//draw:caption/@draw:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for graphic-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//draw:circle/@draw:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for graphic-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//draw:connector/@draw:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for graphic-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//draw:control/@draw:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for graphic-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//draw:custom-shape/@draw:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for graphic-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//draw:ellipse/@draw:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for graphic-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//draw:frame/@draw:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for graphic-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//draw:g/@draw:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for graphic-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//draw:line/@draw:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for graphic-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//draw:measure/@draw:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for graphic-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//draw:page-thumbnail/@draw:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for graphic-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//draw:path/@draw:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for graphic-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//draw:polygon/@draw:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for graphic-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//draw:polyline/@draw:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for graphic-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//draw:rect/@draw:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for graphic-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//draw:regular-polygon/@draw:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for graphic-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//office:annotation/@draw:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for graphic-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select="$common-styles/style:style[@style:family='presentation']/@style:name[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for presentation-style-common: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,$all-styles/style:style[@style:family='presentation']/@style:parent-style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for presentation-style-common: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select="$all-styles/style:style[@style:family='presentation']/@style:name[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for presentation-style: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@presentation:class-names[not(ancestor::office:document)],'\s+')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for presentation-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@presentation:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for presentation-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select="$common-styles/style:style[@style:family='drawing-page']/@style:name[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for drawing-page-style-common: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,$all-styles/style:style[@style:family='drawing-page']/@style:parent-style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for drawing-page-style-common: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select="$all-styles/style:style[@style:family='drawing-page']/@style:name[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for drawing-page-style: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//draw:page/@draw:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for drawing-page-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//presentation:notes/@draw:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for drawing-page-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//style:handout-master/@draw:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for drawing-page-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//style:master-page/@draw:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for drawing-page-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select="$common-styles/text:list-style/@style:name[not(ancestor::office:document)]|$common-styles/style:style[@style:family='graphic']/style:graphic-properties/text:list-style/@style:name[not(ancestor::office:document)]|$common-styles/text:outline-style/@style:name[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for list-style-common: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,$common-styles/style:style[string-length(@style:list-style-name)&gt;0]/@style:list-style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for list-style-common: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select="$all-styles/text:list-style/@style:name[not(ancestor::office:document)]|$all-styles/style:style[@style:family='graphic']/style:graphic-properties/text:list-style/@style:name[not(ancestor::office:document)]|$common-styles/text:outline-style/@style:name[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for list-style: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,$all-styles/style:style[string-length(@style:list-style-name)&gt;0]/@style:list-style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for list-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:list/@text:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for list-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:numbered-paragraph/@text:style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for list-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:list-item/@text:style-override[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for list-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select="$common-styles/number:boolean-style/@style:name[not(ancestor::office:document)]|$common-styles/number:currency-style/@style:name[not(ancestor::office:document)]|$common-styles/number:date-style/@style:name[not(ancestor::office:document)]|$common-styles/number:number-style/@style:name[not(ancestor::office:document)]|$common-styles/number:percentage-style/@style:name[not(ancestor::office:document)]|$common-styles/number:text-style/@style:name[not(ancestor::office:document)]|$common-styles/number:time-style/@style:name[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for data-style-common: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,$common-styles//*/@style:data-style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for data-style-common: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,$all-styles/number:*//@style:apply-style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for data-style-common: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select="$all-styles/number:boolean-style/@style:name[not(ancestor::office:document)]|$all-styles/number:currency-style/@style:name[not(ancestor::office:document)]|$all-styles/number:date-style/@style:name[not(ancestor::office:document)]|$all-styles/number:number-style/@style:name[not(ancestor::office:document)]|$all-styles/number:percentage-style/@style:name[not(ancestor::office:document)]|$all-styles/number:text-style/@style:name[not(ancestor::office:document)]|$all-styles/number:time-style/@style:name[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for data-style: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@style:data-style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for data-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select="$common-styles/number:percentage-style/@style:name[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for percentage-style-common: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,$common-styles//*/@style:percentage-data-style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for percentage-style-common: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select="$all-styles/number:percentage-style/@style:name[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for percentage-style: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@style:percentage-data-style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for percentage-style: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select="$common-styles/draw:gradient/@draw:name[not(ancestor::office:document)]|$common-styles/svg:linearGradient/@draw:name[not(ancestor::office:document)]|$common-styles/svg:radialGradient/@draw:name[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for gradient: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@draw:fill-gradient-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for gradient: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select="$common-styles/draw:hatch/@draw:name[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for hatch: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@draw:fill-hatch-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for hatch: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select="$common-styles/draw:fill-image/@draw:name[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for fill-image: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@draw:fill-image-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for fill-image: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select="$common-styles/draw:marker/@draw:name[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for marker: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@draw:marker-end[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for marker: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@draw:marker-start[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for marker: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select="$common-styles/draw:stroke-dash/@draw:name[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for stroke-dash: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@draw:stroke-dash[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for stroke-dash: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@draw:stroke-dash-names[not(ancestor::office:document)],'\s+')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for stroke-dash: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select="$common-styles/draw:opacity/@draw:name[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for opacity: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@draw:opacity-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for opacity: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select="$common-styles/style:presentation-page-layout/@style:name[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for presentation-page-layout: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@presentation:presentation-page-layout-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for presentation-page-layout: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select=".//table:table/@table:name[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for table: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select="$common-styles/table:table-template/@table:name[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for table-template: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@table:template-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for table-template: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select=".//style:page-layout/@style:name[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for page-layout: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@style:page-layout-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for page-layout: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select="$master-pages/style:master-page/@style:name[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for master-page: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@draw:master-page-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for master-page: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//style:master-page/@style:next-style-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for master-page: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@text:master-page-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for master-page: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select=".//text:alphabetical-index/@text:name[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for alphabetical-index: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select=".//text:bibliography/@text:name[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for bibliography: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select=".//text:bookmark/@text:name[not(ancestor::office:document)]|.//text:bookmark-start/@text:name[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for bookmark: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:bookmark-ref/@text:ref-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for bookmark: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select=".//text:bookmark-end/@text:name[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for bookmark-end: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:bookmark-start/@text:name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for bookmark-end: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select=".//text:bookmark-start/@text:name[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for bookmark-start: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:not-preceding-ids($ids,.//text:bookmark-end/@text:name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for bookmark-start: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select=".//text:illustration-index/@text:name[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for illustration-index: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select=".//text:index-title/@text:name[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for index-title: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select=".//text:object-index/@text:name[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for object-index: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select=".//text:reference-mark/@text:name[not(ancestor::office:document)]|.//text:reference-mark-start/@text:name[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for reference-mark: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:reference-ref/@text:ref-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for reference-mark: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select=".//text:reference-mark-end/@text:name[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for reference-mark-end: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:reference-mark-start/@text:name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for reference-mark-end: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select=".//text:reference-mark-start/@text:name[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for reference-mark-start: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:not-preceding-ids($ids,.//text:reference-mark-end/@text:name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for reference-mark-start: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select=".//text:section/@text:name[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for section: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,text:section-source[not(@xlink:href)]/@text:section-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for section: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select=".//text:sequence-decl/@text:name[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for sequence-decl: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:not-preceding-ids($ids,.//text:sequence/@text:name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for sequence-decl: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select=".//text:table-of-content/@text:name[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for table-of-content: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select=".//text:table-index/@text:name[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for table-index: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select="$office-meta/meta:user-defined/@meta:name[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for user-defined: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:user-defined/@text:name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for user-defined: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select=".//text:user-field-decl/@text:name[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for user-field: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:not-preceding-ids($ids,.//text:user-field-get/@text:name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for user-field: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:not-preceding-ids($ids,.//text:user-field-input/@text:name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for user-field: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select=".//text:user-index/@text:name[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for user-index: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:user-index-mark/@text:index-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for user-index: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:user-index-mark-start/@text:index-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for user-index: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:user-index-source/@text:index-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for user-index: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select=".//text:variable-decl/@text:name[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for variable: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:not-preceding-ids($ids,.//text:variable-get/@text:name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for variable: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:not-preceding-ids($ids,.//text:variable-input/@text:name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for variable: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:not-preceding-ids($ids,.//text:variable-set/@text:name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for variable: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select=".//text:note/@text:id[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for note: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:note-ref/@text:ref-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for note: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select=".//text:sequence/@text:ref-name[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for sequence: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:sequence-ref/@text:ref-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for sequence: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select=".//draw:text-box/@text:id[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for text-box: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select=".//text:alphabetical-index-mark-end/@text:id[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for alphabetical-index-mark-end: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:alphabetical-index-mark-start/@text:id[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for alphabetical-index-mark-end: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select=".//text:alphabetical-index-mark-start/@text:id[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for alphabetical-index-mark-start: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:not-preceding-ids($ids,.//text:alphabetical-index-mark-end/@text:id[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for alphabetical-index-mark-start: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select=".//text:changed-region/@text:id[not(ancestor::office:document)]|.//text:changed-region/@xml:id[not(ancestor::office:document)]"/>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@text:change-id[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for changed-region: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids" select=".//@xml:id[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for id: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids" select=".//text:h/@text:id[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for h: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids" select=".//text:p/@text:id[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for p: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select=".//text:toc-mark-end/@text:id[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for toc-mark-end: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:toc-mark-start/@text:id[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for toc-mark-end: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select=".//text:toc-mark-start/@text:id[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for toc-mark-start: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:not-preceding-ids($ids,.//text:toc-mark-end/@text:id[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for toc-mark-start: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select=".//text:user-index-mark-end/@text:id[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for user-index-mark-end: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//text:user-index-mark-start/@text:id[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for user-index-mark-end: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select=".//text:user-index-mark-start/@text:id[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for user-index-mark-start: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:not-preceding-ids($ids,.//text:user-index-mark-end/@text:id[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for user-index-mark-start: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select=".//text:list/@xml:id[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for list: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@text:continue-list[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for list: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select=".//dr3d:cube/@draw:id[not(ancestor::office:document)]|.//dr3d:extrude/@draw:id[not(ancestor::office:document)]|.//dr3d:rotate/@draw:id[not(ancestor::office:document)]|.//dr3d:scene/@draw:id[not(ancestor::office:document)]|.//dr3d:sphere/@draw:id[not(ancestor::office:document)]|.//draw:caption/@draw:id[not(ancestor::office:document)]|.//draw:circle/@draw:id[not(ancestor::office:document)]|.//draw:connector/@draw:id[not(ancestor::office:document)]|.//draw:control/@draw:id[not(ancestor::office:document)]|.//draw:custom-shape/@draw:id[not(ancestor::office:document)]|.//draw:ellipse/@draw:id[not(ancestor::office:document)]|.//draw:frame/@draw:id[not(ancestor::office:document)]|.//draw:frame/@draw:id[not(ancestor::office:document)]|.//draw:g/@draw:id[not(ancestor::office:document)]|.//draw:line/@draw:id[not(ancestor::office:document)]|.//draw:measure/@draw:id[not(ancestor::office:document)]|.//draw:page/@draw:id[not(ancestor::office:document)]|.//draw:page-thumbnail/@draw:id[not(ancestor::office:document)]|.//draw:path/@draw:id[not(ancestor::office:document)]|.//draw:polygon/@draw:id[not(ancestor::office:document)]|.//draw:polyline/@draw:id[not(ancestor::office:document)]|.//draw:rect/@draw:id[not(ancestor::office:document)]|.//draw:regular-polygon/@draw:id[not(ancestor::office:document)]|.//office:annotation/@draw:id[not(ancestor::office:document)]|.//text:h/@text:id[not(ancestor::office:document)]|.//text:p/@text:id[not(ancestor::office:document)]|.//dr3d:cube/@xml:id[not(ancestor::office:document)]|.//dr3d:extrude/@xml:id[not(ancestor::office:document)]|.//dr3d:rotate/@xml:id[not(ancestor::office:document)]|.//dr3d:scene/@xml:id[not(ancestor::office:document)]|.//dr3d:sphere/@xml:id[not(ancestor::office:document)]|.//draw:caption/@xml:id[not(ancestor::office:document)]|.//draw:circle/@xml:id[not(ancestor::office:document)]|.//draw:connector/@xml:id[not(ancestor::office:document)]|.//draw:control/@xml:id[not(ancestor::office:document)]|.//draw:custom-shape/@xml:id[not(ancestor::office:document)]|.//draw:ellipse/@xml:id[not(ancestor::office:document)]|.//draw:frame/@xml:id[not(ancestor::office:document)]|.//draw:frame/@xml:id[not(ancestor::office:document)]|.//draw:g/@xml:id[not(ancestor::office:document)]|.//draw:line/@xml:id[not(ancestor::office:document)]|.//draw:measure/@xml:id[not(ancestor::office:document)]|.//draw:page/@xml:id[not(ancestor::office:document)]|.//draw:page-thumbnail/@xml:id[not(ancestor::office:document)]|.//draw:path/@xml:id[not(ancestor::office:document)]|.//draw:polygon/@xml:id[not(ancestor::office:document)]|.//draw:polyline/@xml:id[not(ancestor::office:document)]|.//draw:rect/@xml:id[not(ancestor::office:document)]|.//draw:regular-polygon/@xml:id[not(ancestor::office:document)]|.//office:annotation/@xml:id[not(ancestor::office:document)]|.//text:h/@xml:id[not(ancestor::office:document)]|.//text:p/@xml:id[not(ancestor::office:document)]"/>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@smil:targetElement[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for smil:targetElement: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select=".//dr3d:cube/@draw:id[not(ancestor::office:document)]|.//dr3d:extrude/@draw:id[not(ancestor::office:document)]|.//dr3d:rotate/@draw:id[not(ancestor::office:document)]|.//dr3d:scene/@draw:id[not(ancestor::office:document)]|.//dr3d:sphere/@draw:id[not(ancestor::office:document)]|.//draw:caption/@draw:id[not(ancestor::office:document)]|.//draw:circle/@draw:id[not(ancestor::office:document)]|.//draw:connector/@draw:id[not(ancestor::office:document)]|.//draw:control/@draw:id[not(ancestor::office:document)]|.//draw:custom-shape/@draw:id[not(ancestor::office:document)]|.//draw:ellipse/@draw:id[not(ancestor::office:document)]|.//draw:frame/@draw:id[not(ancestor::office:document)]|.//draw:frame/@draw:id[not(ancestor::office:document)]|.//draw:g/@draw:id[not(ancestor::office:document)]|.//draw:line/@draw:id[not(ancestor::office:document)]|.//draw:measure/@draw:id[not(ancestor::office:document)]|.//draw:page/@draw:id[not(ancestor::office:document)]|.//draw:page-thumbnail/@draw:id[not(ancestor::office:document)]|.//draw:path/@draw:id[not(ancestor::office:document)]|.//draw:polygon/@draw:id[not(ancestor::office:document)]|.//draw:polyline/@draw:id[not(ancestor::office:document)]|.//draw:rect/@draw:id[not(ancestor::office:document)]|.//draw:regular-polygon/@draw:id[not(ancestor::office:document)]|.//office:annotation/@draw:id[not(ancestor::office:document)]|.//dr3d:cube/@xml:id[not(ancestor::office:document)]|.//dr3d:extrude/@xml:id[not(ancestor::office:document)]|.//dr3d:rotate/@xml:id[not(ancestor::office:document)]|.//dr3d:scene/@xml:id[not(ancestor::office:document)]|.//dr3d:sphere/@xml:id[not(ancestor::office:document)]|.//draw:caption/@xml:id[not(ancestor::office:document)]|.//draw:circle/@xml:id[not(ancestor::office:document)]|.//draw:connector/@xml:id[not(ancestor::office:document)]|.//draw:control/@xml:id[not(ancestor::office:document)]|.//draw:custom-shape/@xml:id[not(ancestor::office:document)]|.//draw:ellipse/@xml:id[not(ancestor::office:document)]|.//draw:frame/@xml:id[not(ancestor::office:document)]|.//draw:frame/@xml:id[not(ancestor::office:document)]|.//draw:g/@xml:id[not(ancestor::office:document)]|.//draw:line/@xml:id[not(ancestor::office:document)]|.//draw:measure/@xml:id[not(ancestor::office:document)]|.//draw:page/@xml:id[not(ancestor::office:document)]|.//draw:page-thumbnail/@xml:id[not(ancestor::office:document)]|.//draw:path/@xml:id[not(ancestor::office:document)]|.//draw:polygon/@xml:id[not(ancestor::office:document)]|.//draw:polyline/@xml:id[not(ancestor::office:document)]|.//draw:rect/@xml:id[not(ancestor::office:document)]|.//draw:regular-polygon/@xml:id[not(ancestor::office:document)]|.//office:annotation/@xml:id[not(ancestor::office:document)]"/>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@draw:end-shape[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for shape: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@draw:start-shape[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for shape: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select=".//presentation:header-decl/@presentation:name[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for presentation-header: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@presentation:use-header-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for presentation-header: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select=".//presentation:footer-decl/@presentation:name[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for presentation-footer: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@presentation:use-footer-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for presentation-footer: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select=".//presentation:date-time-decl/@presentation:name[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for presentation-date-time: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@presentation:use-date-time-name[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for presentation-date-time: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select=".//draw:page/@draw:name[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for presentation-pages: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@presentation:pages[not(ancestor::office:document)],',')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for presentation-pages: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="ids"
                    select=".//presentation:show/@presentation:name[not(ancestor::office:document)]"/>
      <xsl:variable name="dupes" select="$ids[index-of($ids,.)[2]]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($dupes)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($dupes)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Duplicate id(s) for presentation-show: '&#34;,string-join($dupes,$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:variable name="missing"
                    select="f:missing-ids($ids,.//@presentation:show[not(ancestor::office:document)],'')"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($missing)=0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="count($missing)=0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat(&#34;Missing id(s) for presentation-show: '&#34;,string-join(distinct-values($missing),$sep),&#34;'.&#34;)"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="count($master-pages/style:master-page) &gt; 0"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                test="count($master-pages/style:master-page) &gt; 0">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>The document is missing a style:master-page.</svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:apply-templates select="*" mode="M30"/>
   </xsl:template>
   <xsl:template match="text()" priority="-1" mode="M30"/>
   <xsl:template match="@*|node()" priority="-2" mode="M30">
      <xsl:apply-templates select="*" mode="M30"/>
   </xsl:template>

   <!--PATTERN -->


	  <!--RULE -->
   <xsl:template match="*[@anim:id and @xml:id]" priority="1004" mode="M31">
      <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                       context="*[@anim:id and @xml:id]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="@anim:id = @xml:id"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="@anim:id = @xml:id">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat('anim:id (&#34;',@anim:id,'&#34;) and xml:id (&#34;',@xml:id,'&#34;) are not equal.')"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:apply-templates select="*" mode="M31"/>
   </xsl:template>

	  <!--RULE -->
   <xsl:template match="*[not(self::draw:glue-point)][@draw:id and @xml:id]"
                 priority="1003"
                 mode="M31">
      <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                       context="*[not(self::draw:glue-point)][@draw:id and @xml:id]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="@draw:id = @xml:id"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="@draw:id = @xml:id">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat('draw:id (&#34;',@draw:id,'&#34;) and xml:id (&#34;',@xml:id,'&#34;) are not equal.')"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:apply-templates select="*" mode="M31"/>
   </xsl:template>

	  <!--RULE -->
   <xsl:template match="*[@form:id and @xml:id]" priority="1002" mode="M31">
      <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                       context="*[@form:id and @xml:id]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="@form:id = @xml:id"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="@form:id = @xml:id">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat('form:id (&#34;',@form:id,'&#34;) and xml:id (&#34;',@xml:id,'&#34;) are not equal.')"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:apply-templates select="*" mode="M31"/>
   </xsl:template>

	  <!--RULE -->
   <xsl:template match="*[self::draw:text-box or self::text:changed-region or self::text:h or self::text:p][@text:id and @xml:id]"
                 priority="1001"
                 mode="M31">
      <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                       context="*[self::draw:text-box or self::text:changed-region or self::text:h or self::text:p][@text:id and @xml:id]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="@text:id = @xml:id"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl" test="@text:id = @xml:id">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat('text:id (&#34;',@text:id,'&#34;) and xml:id (&#34;',@xml:id,'&#34;) are not equal.')"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:apply-templates select="*" mode="M31"/>
   </xsl:template>

	  <!--RULE -->
   <xsl:template match="style:style[@style:next-style-name]"
                 priority="1000"
                 mode="M31">
      <svrl:fired-rule xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                       context="style:style[@style:next-style-name]"/>

		    <!--ASSERT -->
      <xsl:choose>
         <xsl:when test="@style:family='paragraph'"/>
         <xsl:otherwise>
            <svrl:failed-assert xmlns:svrl="http://purl.oclc.org/dsdl/svrl"
                                test="@style:family='paragraph'">
               <xsl:attribute name="location">
                  <xsl:apply-templates select="." mode="schematron-select-full-path"/>
               </xsl:attribute>
               <svrl:text>
                  <xsl:text/>
                  <xsl:value-of select="concat('style:next-style-name (&#34;',@style:next-style-name,'&#34;) may only be used on style of family &#34;paragraph&#34;.')"/>
                  <xsl:text/>
               </svrl:text>
            </svrl:failed-assert>
         </xsl:otherwise>
      </xsl:choose>
      <xsl:apply-templates select="*" mode="M31"/>
   </xsl:template>
   <xsl:template match="text()" priority="-1" mode="M31"/>
   <xsl:template match="@*|node()" priority="-2" mode="M31">
      <xsl:apply-templates select="*" mode="M31"/>
   </xsl:template>
</xsl:stylesheet>
