#!/usr/bin/env bash
set -o errexit
set -o nounset
set -o pipefail

backupfile=${1:?Supply location of previously created backup file as arg1};

database=${DATABASE:-odftestserver.sqlite3};
dbuser=${PGUSER:-odfdb}
dbpassword=${PGPASS:-odfdb}
dbhost=${PGHOST:-localhost}
dbport=${PGPORT:-5432}

echo "db:$database  host:$dbhost port:$dbport    user:$dbuser pass:$dbpassword"


if [[ $database == *"sqlite"* ]]; then
    echo "restoring SQLite database..."
    sqlite3 $database ".restore main $backupfile"
else
    echo "restoring PostgreSQL database..."
    psql --file="$backupfile" \
	 --host=$dbhost --port=$dbport \
         --username=$dbuser \
         $database
fi
