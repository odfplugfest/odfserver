# Compiling the server

The odf server requires Yesod, a Haskell web framework. This is available on Windows, Mac OS and Linux. So far it has been tested in NixOS, Ubuntu and Debian.

## Compiling on Debian

The server can be built on Debian 9 (Stretch). Stack, the Haskell build tool, has to be installed from outside Debian.

```bash
sudo apt-get install git wget unzip poppler-utils optipng zlib1g-dev libpq-dev
wget -qO- https://get.haskellstack.org/ | sh
git clone https://gitlab.com/odfplugfest/odfserver.git
cd odfserver
wget https://github.com/twbs/bootstrap/releases/download/v3.3.6/bootstrap-3.3.6-dist.zip
unzip -d static bootstrap-3.3.6-dist.zip
stack setup
stack build
```

## Compiling on NixOS

On NixOS, `stack` can be used as `stack --nix`.

# Run the server

The server needs to have two executables available on the path: `pdftoppm` and `optipng`. On Debian, these are provided by the packages `poppler-utils` and `optipng`.

The server can be run like this:

```bash
stack exec odftestserver
```

# Run the development version of the server

During development, the server can be run  with the `yesod` binary. It automatically recompiles and restarts the server when any source code file changes.

```bash
stack exec -- yesod devel
```

Now browse to http://localhost:3000.

Alternatively, one can use `APPROOT=http://localhost:3000 stack build -- yesod devel -e '-f sqlite'`.

For HTTPS, a certificate is needed. This can be generated with
```bash
openssl req -newkey rsa:2048 -new -nodes -keyout key.pem -out csr.pem
openssl req -newkey rsa:2048 -new -nodes -x509 -days 3650 -keyout key.pem -out certificate.pem
```

# Running tests

`stack test`

# Packaging with Docker

`docker/recreate-docker-image.sh`

# Talking to the server with curl

```bash
# register a factory
curl -H "Accept: application/json" -H "Content-Type: application/json" -X POST --data '[]' http://localhost:3000/factory/register

# retrieve jobs
curl -H "Accept: application/json" -H "Content-Type: application/json" -X POST --data @/tmp/testinput.json http://localhost:3000/factory/retrievejobs
```

=== new ===

cp $(type -P stack) $HOME/.stack/snapshots/x86_64-linux-nix/lts-9.3/8.0.2/bin/stack
wget https://downloads.sourceforge.net/project/saxon/Saxon-HE/9.7/SaxonHE9-7-0-18J.zip
unzip -d lib SaxonHE9-7-0-18J.zip saxon9he.jar
