module Handler.ScoreBoardSpec (spec) where

import           Database.Persist.Sql (toSqlKey)
import           TestImport

import           CommonTypes          (epochTime)
import           Handler.ScoreBoard

spec :: Spec
spec = withApp $ do
    it "empty test set gives no results" $ do
        res <- runDB $ do
            let userid = toSqlKey 1
            testset <- insert $ TestSet Nothing epochTime userid "test"
            test <- insert $ Test Nothing epochTime userid "red"
            let sw = toSqlKey 1
            insert_ $ TestResult test sw True
            getSoftwareOSScores testset
        assertEq "Too many resuls" res []
    it "test results for one test are counted ok" $ do
        (sw, rows) <- runDB $ do
            let userid = toSqlKey 1
            testset <- insert $ TestSet Nothing epochTime userid "test"
            test <- insert $ Test Nothing epochTime userid "red"
            insert_ $ TestSetMember testset test
            let sw = toSqlKey 1
            insert_ $ TestResult test sw True
            insert_ $ TestResult test sw False
            rows <- getSoftwareOSScores testset
            return (sw, rows)
        assertEq "Wrong number of rows." 1 $ length rows
        assertEq "Wrong score for test." rows [(sw,"",1,2,0.5)]
    it "test results for two tests are counted ok" $ do
        (sw, rows) <- runDB $ do
            let userid = toSqlKey 1
            testset <- insert $ TestSet Nothing epochTime userid "test"
            test1 <- insert $ Test Nothing epochTime userid "red"
            test2 <- insert $ Test Nothing epochTime userid "green"
            insert_ $ TestSetMember testset test1
            insert_ $ TestSetMember testset test2
            let sw = toSqlKey 1
            insert_ $ TestResult test1 sw True
            insert_ $ TestResult test2 sw False
            rows <- getSoftwareOSScores testset
            return (sw, rows)
        assertEq "Wrong number of rows." 1 $ length rows
        assertEq "Wrong score for test." rows [(sw,"",2,2,0.5)]
    it "test results for two tests are counted ok" $ do
        (sv, rows) <- runDB $ do
            let userid = toSqlKey 1
            testset <- insert $ TestSet Nothing epochTime userid "test"
            test <- insert $ Test Nothing epochTime userid "red"
            insert_ $ TestSetMember testset test
            sv <- insert $ SoftwareVersion (toSqlKey 1) Nothing "ODF" Nothing epochTime
            sw1 <- insert $ SoftwareOS sv (toSqlKey 1) (toSqlKey 1)
            sw2 <- insert $ SoftwareOS sv (toSqlKey 2) (toSqlKey 1)
            insert_ $ TestResult test sw1 True
            insert_ $ TestResult test sw2 False
            rows <- getSoftwareVersionScores testset
            return (sv, rows)
        assertEq "Wrong number of rows." 1 $ length rows
        lift $ print rows
        assertEq "Wrong score for test." rows [(sv,"ODF",1,2,0.5)]
