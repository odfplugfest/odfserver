module Handler.ProseSpec (spec) where

import           Database.Persist.Sql (toSqlKey)
import           TestImport           hiding (getLocation)

import           TestTools


writePage :: Text -> Text -> YesodExample App ()
writePage title content = do
    get PageNewR
    statusIs 200
    request $ do
        setMethod "POST"
        setUrl PageNewR
        addToken
        addPostParam "f1" title
        addPostParam "f2" content
        addPostParam "f3" "Nothing" -- parent
        addPostParam "f4" "Nothing" -- ?
        addPostParam "save" "yes" -- press the 'save' button

writeComment :: Int64 -> Text -> Text -> YesodExample App()
writeComment parent title content = do
    get $ CommentNewR $ toSqlKey parent
    statusIs 200
    request $ do
        setMethod "POST"
        setUrl $ CommentNewR $ toSqlKey parent
        addToken
        addPostParam "f1" title
        addPostParam "f2" content
        addPostParam "f3" $ pack $ "Just " ++ show parent
        addPostParam "f4" "Nothing"
        addPostParam "save" "yes" -- press the 'save' button

spec :: Spec
spec = withApp $ do
    it "requires a login to add a new page" $ do
        get PageNewR
        statusIs 303
    it "can create a new page" $ do
        let pageid :: PageId = toSqlKey 1
        get $ PageR pageid
        statusIs 404 -- page does not exist yet
        login uid pwd
        writePage "Title" "Content"
        statusIs 303
        location <- getLocation
        let (Just i) = getTailInteger $ unpack $ decodeUtf8 location
        assertEq "Page had unexpected id" i 1
        get $ PageR pageid
        statusIs 200 -- page has been created
    it "can create a comment" $ do
        login uid pwd
        writePage "Title" "Content"
        let commentid :: ProseId = toSqlKey 2
        get $ CommentNewR commentid
        statusIs 404 -- page does not exist yet
        writeComment 1 "comment title" "comment body"
        get $ CommentNewR commentid
        statusIs 200
