module VariousSpec (spec) where

import           Data.Aeson                 (decode, encode)
import qualified Data.ByteString.Lazy.Char8 as B
import           Data.Maybe                 (fromJust)
import           Database.Persist.Sql       (toSqlKey)
import           Network.HTTP.Types.Header
import           Network.Wai.Test           (SResponse (..))
import           Test.Hspec
import           TestImport                 hiding (Job, getLocation)
import           Text.XML.Cursor
import           Yesod.Core

import           CommonTypes                as CT
import           FactoryAPI
import           FileType
import           RetrieveJobs               (Job (..), RetrieveJobsRequest (..),
                                             RetrieveJobsResponse (..))
import qualified Service                    as SE
import qualified Software                   as S
import           TestTools
import qualified UploadJobResult            as UJ

getLink :: Cursor -> [Text]
getLink = attribute "href"

testGetUploadPage :: YesodExample App ()
testGetUploadPage = do
    login uid pwd
    get UploadsR
    statusIs 200

testUploadFile :: FilePath -> YesodExample App (UploadId, B.ByteString, Text, BlobId)
testUploadFile filepath = do
    testGetUploadPage
    request $ do
        setMethod "POST"
        setUrl UploadsR
        addToken
        addFile "f1" filepath "text/plain"
    contents <- liftIO $ B.readFile filepath
    location <- getLocation
    let loc = decodeUtf8 location
        (Just i) = getTailInteger $ unpack loc
        uploadid = toSqlKey i
    upload <- runDB $ getJust uploadid
    return (uploadid, contents, loc, uploadContent upload)

postJSON' :: (ToJSON a) => Text -> a -> YesodExample App ()
postJSON' path input = do
    let json = encode input
    request $ do
        setMethod "POST"
        setUrl $ "http://localhost:3000/factory/" <> path
        addRequestHeader (hContentType, "application/json")
        setRequestBody json

postJSON :: (ToJSON a, FromJSON b) => Text -> a -> YesodExample App b
postJSON path input = do
    postJSON' path input
    statusIs 200
    maybeKey <- withResponse (\SResponse{simpleBody=b} -> return $ decode b)
    return $ fromJust maybeKey

failPostJSON :: (ToJSON a) => Text -> a -> YesodExample App ()
failPostJSON path input = do
    postJSON' path input
    statusIs 400

spec :: Spec
spec = withApp $ do
    describe "basic usage" $ do
        it "tests1a" $ do
            get ("/wiki/en" :: Text)
            statusIs 200
            bodyContains "english"
        it "tests1b" $ do
            get ("/foo" :: Text)
            statusIs 404
        it "type-safe URLs" $ do
            get HomeR
            statusIs 303

    describe "upload" $ do
        it "upload is not allowed when not logged in" $ do
            get UploadsR
            statusIs 200 -- upload page is ok, but uploading (post) is not
            -- assertHeader "Location" "https://localhost:3443/auth/login"
        it "upload with login"
            testGetUploadPage
        it "upload a file" $ do
            (uploadId, contents, loc, _) <- testUploadFile "/etc/passwd"
            -- get the page that describes the uploaded file
            get $ UploadR "en" uploadId
            statusIs 200
            -- todo: check that link is present
            links <- htmlQuery "a" -- get all links
            -- find the link to the uploaded file
            let hrefs = concatMap (getLink . parseHTML) links
                uploadHrefs = filter (isPrefixOf loc) hrefs
            -- download the uploaded file
            case uploadHrefs of
                (href:_) -> get href
                _        -> return () -- todo: signa error
            withResponse (\SResponse{simpleBody=b} ->
                assertEq "Uploaded file was changed." contents b
                         )
    describe "running a factory" $ do
        it "register factory" $ do
            (_ :: CT.FactoryKey) <- postJSON "register" RegisterRequest
            return ()
        it "fail to get jobs when no services are specified" $ do
            key <- postJSON "register" RegisterRequest
            failPostJSON "retrievejobs" $ RetrieveJobsRequest key 0 0 []
        it "get jobs with services" $ do
            -- make sure the server knows about the service
            key <- postJSON "register" RegisterRequest
            let serv = SE.Service "TestService" ODT1_2EXT PDF
                soft = S.Software "a" "b" "c" "d" "e" "f"
                si = SE.ServiceInstance soft serv
            (_ :: RetrieveJobsResponse) <- postJSON "retrievejobs" $
                RetrieveJobsRequest key 0 0 [si]
            -- upload a file that can be converted by the service
            (_, _, _, blobid) <- testUploadFile "test/test.odt"
            -- schedule the file for conversion
            let dbse = Service (SE.name serv) (SE.inputType serv) (SE.outputType serv)
                dbso = SoftwareOS (toSqlKey 1) (toSqlKey 1) (toSqlKey 1)
            (Just (Entity seid _)) <- runDB (getByValue dbse)
            (Just (Entity soid _)) <- runDB (getByValue dbso)
            post $ NewJobR blobid soid seid
            statusIs 303 -- redirect to job page
            -- retrieve the job id
            location <- getLocation
            let loc = decodeUtf8 location
            let i = getTailInteger $ unpack loc
            guard $ isJust i
            -- retrieve the job for the file conversion
            res <- postJSON "retrievejobs" $ RetrieveJobsRequest key 0 0 [si]
            let RetrieveJobsResponse (Job{number=num}:_) = res
            -- upload results
            now <- lift getCurrentTime
            (_ :: UJ.UploadJobResultResponse) <- postJSON "uploadjobresult"
                UJ.UploadJobResult {
                    UJ.factory=key,
                    UJ.number=num,
                    UJ.started=now,
                    UJ.finished=now,
                    UJ.commandLine="",
                    UJ.outputFiles=[],
                    UJ.error=UJ.None,
                    UJ.exitCode=0,
                    UJ.stdout="",
                    UJ.stderr=""
                }
            return ()
