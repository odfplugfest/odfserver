{-# LANGUAGE PackageImports #-}
import           "odftestserver" Application (develMain)
import           Prelude                     (IO)

main :: IO ()
main = develMain
