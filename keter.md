# Deploying the test server with Keter

## Creating a keter bundle

The ODF Test server can be deployed with keter. Keter makes it simple to update
the server executable. Keter replaces the running server instance gracefully.
Open connections to clients are kept open but new connections go to the new
version of the server.

First build a new keter bundle.
```bash
docker/make_keter.sh
```

This creates a file `odftestserver.keter`.

## Running keter on the server

```bash
nix-shell --pure -p haskellPackages.keter
export DATABASE=odfdb
export PGUSER=odf
epport PGPASS=password
keter keter-config.yaml
```

Make sure that keter is allowed to use port 80 and 443.

```bash
/sbin/setcap 'cap_net_bind_service=+ep' /nix/store/vjgyif5fcxvnxgazxymay2xjwdfnlgas-keter-1.4.3.2/bin/keter
```

## Deploying the bundle

If the `odftestserver` was compiled in a Nix environment, the rpath of the
executable should be adapted to the system on which it is running.
This can be done with the script `patchelf_odftestserver.sh`.

```bash
patchelf_odftestserver.sh
mv odftestserver.ketel incoming
```

## Database backup

The database from the production serve can be backed up with

```bash
ssh -C blob "pg_dump --clean --if-exists -U odf -h localhost odfdb" > backup.sql
```

## Deploying to https://beta.opendocumentformat.org

```bash
docker/deploy_keter.sh
```
