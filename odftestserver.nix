{ mkDerivation, aeson, async, base, base64-bytestring, binary
, blaze-html, blaze-markup, bytestring, case-insensitive
, classy-prelude, classy-prelude-conduit, classy-prelude-yesod
, concurrent-extra, conduit, conduit-combinators, conduit-extra
, containers, data-default, directory, either, errors, esqueleto
, fast-logger, feed, file-embed, filepath, highlighting-kate
, hjsmin, hspec, http-client, http-conduit, http-types, hxt
, hxt-tagsoup, hxt-xpath, knob, mime-mail, monad-control
, monad-logger, mono-traversable, mtl, network, pandoc
, pdf-toolbox-core, pdf-toolbox-document, persistent
, persistent-postgresql, persistent-sqlite, persistent-template
, pretty, process, random, regex-posix, resource-pool, resourcet
, safe, servant, servant-client, servant-docs, servant-pandoc
, servant-server, SHA, shakespeare, stdenv, stm, stm-lifted
, string-conversions, tagsoup, template-haskell, temporary, text
, time, tls, transformers, unordered-containers, utf8-string, uuid
, vector, wai, wai-extra, wai-logger, warp, warp-tls, xml
, xml-conduit, xss-sanitize, yaml, yesod, yesod-auth, yesod-core
, yesod-form, yesod-newsfeed, yesod-persistent, yesod-sitemap
, yesod-static, yesod-test, zip-archive, zlib
# these are added by hand
, cabal-install, yesod-bin, happy, alex, regex-compat, stylish-haskell
, hlint, poppler_utils, optipng, makeWrapper
}:
mkDerivation {
  pname = "odftestserver";
  version = "0.1";
  src = ./.;
  isLibrary = true;
  isExecutable = true;
  configureFlags = [ "--disable-tests" "--verbose=1" ];
  buildTarget = "-j4 exe:odftestserver";
  testTarget = "|| true";
  doHaddock = false;
  enableSharedLibraries = false;
  libraryHaskellDepends = [
    aeson async base base64-bytestring binary blaze-html blaze-markup
    bytestring case-insensitive classy-prelude classy-prelude-conduit
    classy-prelude-yesod concurrent-extra conduit conduit-combinators
    conduit-extra containers data-default directory either errors
    esqueleto fast-logger feed file-embed highlighting-kate hjsmin
    http-client http-conduit http-types hxt hxt-tagsoup hxt-xpath knob
    mime-mail monad-control monad-logger mtl pandoc
    pdf-toolbox-core pdf-toolbox-document persistent
    persistent-postgresql persistent-sqlite persistent-template pretty
    process random regex-posix resource-pool safe servant servant-docs
    servant-pandoc servant-server SHA shakespeare stm stm-lifted
    string-conversions tagsoup template-haskell temporary text time tls
    transformers unordered-containers utf8-string uuid vector wai
    wai-extra wai-logger warp warp-tls xml xml-conduit xss-sanitize
    yaml yesod yesod-auth yesod-core yesod-form yesod-newsfeed
    yesod-persistent yesod-sitemap yesod-static zip-archive zlib
# these are added by hand
    cabal-install yesod-bin happy alex regex-compat stylish-haskell hlint
    poppler_utils optipng
  ];
  executableHaskellDepends = [
    aeson base base64-bytestring binary bytestring classy-prelude-yesod
    concurrent-extra containers directory either filepath mtl network
    persistent persistent-sqlite process resource-pool resourcet safe
    servant servant-client servant-server SHA temporary text time tls
    transformers xml-conduit yesod-auth zip-archive
  ];
  executableToolDepends = [ optipng poppler_utils ];
  testHaskellDepends = [
    aeson async base base64-bytestring binary blaze-html blaze-markup
    bytestring case-insensitive classy-prelude classy-prelude-conduit
    classy-prelude-yesod concurrent-extra conduit conduit-combinators
    conduit-extra containers data-default directory either errors
    esqueleto fast-logger feed file-embed highlighting-kate hjsmin
    hspec http-client http-conduit http-types hxt hxt-xpath knob
    mime-mail monad-control monad-logger mono-traversable mtl
    pandoc pdf-toolbox-core pdf-toolbox-document persistent
    persistent-postgresql persistent-sqlite persistent-template pretty
    process random regex-posix resource-pool safe servant servant-docs
    servant-pandoc servant-server SHA shakespeare stm stm-lifted
    string-conversions tagsoup template-haskell temporary text time tls
    transformers unordered-containers utf8-string uuid vector wai
    wai-extra wai-logger warp warp-tls xml xml-conduit xss-sanitize
    yaml yesod yesod-auth yesod-core yesod-form yesod-newsfeed
    yesod-persistent yesod-sitemap yesod-static yesod-test zip-archive
    zlib
  ];
  postInstall = ''
    source ${makeWrapper}/nix-support/setup-hook
    cp -va static config templates client_session_key.aes $out
    wrapProgram $out/bin/odftestserver \
      --run "cd $out"
  '';
  description = "";
  homepage = "https://www.opendocumentformat.org/";
  license = stdenv.lib.licenses.agpl3;
  maintainers = [ stdenv.lib.maintainers.vandenoever ];
}
