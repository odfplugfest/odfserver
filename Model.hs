module Model where

import           ClassyPrelude.Yesod    hiding (Feed, Producer, get)
import           Data.Binary            (Binary (..), Get, Put)
import           Database.Persist.Quasi
import           Database.Persist.Sql   (fromSqlKey, toSqlKey)

import           FileType

-- You can define all of your database entities in the entities file.
-- You can find more information on persistent and how to declare entities
-- at:
-- http://www.yesodweb.com/book/persistent/
share [mkPersist sqlSettings
      , mkDeleteCascade sqlSettings
      , mkMigrate "migrateAll"]
    $(persistFileWith lowerCaseSettings "config/models")

put' :: ToBackendKey SqlBackend record => Key record -> Put
put' t = put $ fromSqlKey t
get' :: ToBackendKey SqlBackend record => Get (Key record)
get' = do
    t <- get :: Get Int64
    return $ toSqlKey t

instance Binary UserId where
    put = put'
    get = get'
instance Binary FactoryId where
    put = put'
    get = get'
instance Binary TestId where
    put = put'
    get = get'
instance Binary ServiceId where
    put = put'
    get = get'
